/**
 * @author Nicolas Andres Rivera Acevedo
 * @description generador de configuración para browserify-shim
 *              utiliza la configuración dada en ./conf/shim.config.js
 *              si se necesita realizar algún cambio, se debe revisar
 *              también la configuración en ./conf/shim.config.js y
 *              las tareas de grunt 'browserify-*' 
 */

var shim = require('./conf/shim.config');

if (!shim.libs) shim.libs = {};
if (!shim.exports) shim.exports = {};

var shimlibs = Object.keys(shim.libs);
var config = JSON.stringify(shim.exports);
shimlibs.forEach(function(lib){
    config = config.replace(new RegExp('"'+lib+'":','g'), '"' + shim.libs[lib] + '":');
});

module.exports = JSON.parse(config);