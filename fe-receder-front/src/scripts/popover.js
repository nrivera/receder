'use strict';

/**
 * @description Scripts para manejar los eventos en el document para tratar acciones
 * relacionadas con popovers
 * @author Mauricio Ross
 */
var $ = require('jquery'); 
var popover = {
    selected: null
};

/** 
 * Al hacer click mientras la directiva esta en contexto esta validara si se ha hecho click dentro o fuera de la directiva o dentro de un hijo de la directiva. 
 */ 
$(document).on('click', function (e) { 
    if (!$(e.target).is(popover.selected) && !$(e.target).closest('.popover').is(popover.selected)) { 
        $(popover.selected).popover('hide'); 
    } 
}); 

/** 
 * el evento shown.bs.popover ejecuta la accion mientras se trabaja en el css de la directiva, con el objetivo de asignar a la variable global selected el popover seleccionado. 
 */ 
$(document).on('shown.bs.popover', function () { 
    popover.selected = $('.popover'); 
});

module.exports = popover;