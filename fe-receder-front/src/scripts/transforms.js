/**
 * @author Nicolas Andres Rivera Acevedo
 * @description script con métodos genéricos que permiten la transformación de objetos
 * no utilizan inyección de dependencia de angular
 */
var lodash = require('lodash');

module.exports = {
    normaliza: _normaliza,
    agregarSeleccione: _agregarSeleccione,
    agregarItemEspecial: _agregarItemEspecial,
    obtenerMapa: _obtenerMapa,
    agruparMapa: _agruparMapa,
    remapear: _remapear,
    lowerCaseObject: _lowerCaseObject
};

/**
 * @description normaliza la colección dada por parámetro para agregar un identificador dado por key
 * @param {Array} coleccion array de objetos
 * @param {String} key identificador para agregar
 */
function _normaliza(coleccion, key){
    if (!coleccion || typeof key == 'undefined') return coleccion;
    for (var i=0; i<coleccion.length; i++){
        if (typeof coleccion[i][key] != 'undefined') continue;
        coleccion[i][key] = i+1;
    }
    return coleccion;
}

/**
 * @description agrega un item seleccione a la colección
 * @param {Array} coleccion colección de objetos
 * @param {String} key prop a la cual agregar el valor 0
 * @param {String} label prop a la cual agregar el valor '-- seleccione --'
 */
function _agregarSeleccione(coleccion, key, label){
    return _agregarItemEspecial(coleccion, key, label, 0, '-- Seleccione --');
}

/**
 * @description agrega un item especial a la colección
 * @param {Array} coleccion colección de objetos
 * @param {String} key prop a la cual agregar el valor dado por keyValue
 * @param {String} label prop a la cual agregar el valor dado por labelValue
 * @param {*} keyValue valor a agregar en la prop dada por key
 * @param {*} labelValue valor a agregar en la prop dada por label
 */
function _agregarItemEspecial(coleccion, key, label, keyValue, labelValue){
    if (!coleccion || typeof key == 'undefined' || typeof label == 'undefined') return coleccion;
    for (var i=0; i<coleccion.length; i++){
        if (coleccion[i][key] == 0) return coleccion;
    }
    var item = {};
    item[key] = keyValue;
    item[label] = labelValue;
    coleccion.splice(0,0, item);
    return coleccion;
}

/**
 * @description devuelve un objeto literal con claves obtenidas por el parámetro key
 * @param {Array} coleccion colección de objetos
 * @param {String} key nombre del campo del cual obtener la clave
 * @returns objeto donde cada propiedad es clave obtenida por el parámetro key
 */
function _obtenerMapa(coleccion, key){
    var mapa = {};
    if (!coleccion || typeof key != 'string') return mapa;
    for(var i=0; i<coleccion.length; i++){
        var obj = coleccion[i];
        mapa[lodash.get(obj, key)] = obj;
    }
    return mapa;
}

/**
 * @description Agrupa un mapa en Arrays de acuerdo a la cantidad dada por el parámetro cantidad
 * @param {Object} mapa Objeto literal
 * @param {Number} cantidad Número de keys a agrupar
 * @returns {Array} colección de keys agrupadas
 */
function _agruparMapa(mapa, cantidad){
    var result = [];
    var i = 0;
    angular.forEach(mapa, function(item){
        var nodo = result[i] || result[result.push([])-1];
        nodo.push(item)
        if (nodo.length % cantidad == 0) i++;
    });
    return result;
}

/**
 * @description Reestructura un objeto literal de acuerdo al valor dado por key
 * @param {Object} mapa Objeto literal
 * @param {String} key Nombre del campo o path del campo por cual mapear
 * @param {String} value Nombre del campo o path del campo a obtener el valor a enlazar con key (opcional)
 *  si no es dado el este parámetro, se enlazará el primer nodo/objeto/valor encontrado.
 */
function _remapear(mapa, key, value) {
    var keys = Object.keys(mapa);
    var result = {};

    for (var i=0; i<keys.length; i++){
        result[lodash.get(mapa[keys[i]], key)] = value ? lodash.get(mapa[keys[i]], value) : mapa[keys[i]];
    }

    return result;
}

/**
 * @description Función encargada de generar un objeto con propiedades en lower case
 * @param {Object} obj objeto a transformar sus claves en lower case
 */
function _lowerCaseObject(obj){
    var lower = {};
    Object.keys(obj).forEach(function(k){
        lower[k.toLowerCase()] = obj[k];
    });
    return lower;
}