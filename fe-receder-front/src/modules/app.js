'use strict';

/**
 * @author Nicolas Andres Rivera Acevedo
 * @description script de entrada, definición del modulo principal 'app' y sus
 * dependencias de submodulos y componentes de terceros
 */
require('angular');
require('bootstrap');

/**
 * 	Creación de modulo 'app' e inyección de dependencias de modulos
 */
angular.module('app', [
	/**
	 * dependencias
	 */
	require('@uirouter/angularjs'),
	require('angular-sanitize'),
	require('angular-animate'),
	require('angular-ui-bootstrap'),
	
	require('./main'),
	require('./commons'),
	require('./receder')
]);