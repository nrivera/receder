'use strict';

/**
 * @author Nicolas Andres Rivera Acevedo
 * @description Constantes de autenticación
 */
module.exports = {
    TOKEN_HEADER  : 'Authorization',
    TOKEN_TYPE    : 'Bearer',
    TOKEN_PREFIX  : 'fe',
    TOKEN_NAME    : 'fetoken',
    AUTH_URL      : 'fe/auth'
};