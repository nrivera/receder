'use strict';
/**
 * @author Nicolas Andres Rivera Acevedo
 * @description Constantes de nombres de eventos
 */
module.exports = {
    /**
     * @description Preloader
     */
    PRELOADER_HIDDEN                  : 'preload:hidden',
    PRELOADER_VISIBLE                 : 'preload:visible'
};