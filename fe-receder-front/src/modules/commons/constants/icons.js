'use strict';

/**
 * @description Constantes de iconos
 */
module.exports = {
    DOWNLOADING : 'fe-downloading',
    DOWNLOAD    : 'fa fa-download',
    PDF         : 'hst hst-document-file-pdf',
    CLOCK       : 'fa fa-clock-o',
    CHECK       : 'fa fa-check',
    TIMES       : 'fa fa-times',
    EYE         : 'fa fa-eye'
};