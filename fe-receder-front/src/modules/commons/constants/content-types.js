'use strict';
/**
 * @author Nicolas Andres Rivera Acevedo
 * @description Constantes de tipos de archivos
 */
module.exports = {
    APPLICATION_XML         : 'application/xml',
    TEXT_XML                : 'text/xml',
    APPLICATION_PFX         : 'application/x-pkcs12'
};