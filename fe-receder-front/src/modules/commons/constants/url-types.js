'use strict';

/**
 * @author Nicolas Andres Rivera Acevedo
 * @description Constantes de tipos de url
 */
module.exports = {
    NO_AUTENTICADO    : 'no-autenticado',
    URL_LOGIN_SSO     : 'https://login.factoringsecurity.cl/'
};