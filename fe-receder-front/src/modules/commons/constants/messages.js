/**
 * @description Constantes de alertas de mensajes
 */
module.exports = {
    NO_AUTENTICADO: 'Su sesión de usuario ha caducado.',

    CERRAR_SESION: '¿Desea cerrar sesión?',

    ELIMINAR_DOCUMENTO: '¿Desea eliminar el documento $1?',

    ELIMINAR_NEGOCIO: '¿Desea eliminar el negocio $1?',
    
    DOS_O_MAS_ARCHIVOS_ERRONEOS: 'hay $1 documentos con errores.',

    UN_ARCHIVO_ERRONEO: 'hay 1 documento con errores.',

    SIN_FACTURAS_SELECCIONADAS_PARA_MODIFICAR: 'No tiene facturas seleccionadas a modificar.',

    DOS_O_MAS_ARCHIVOS_NO_CUMPLEN_VALIDACION: `Se encontraron $1 documentos que no cumplen con la validación. 
    $br Corrija para proceder. $br$br $info Para visualizar detalle de las facturas con errores, 
    haga click en los iconos de estado de validación para desplegar información.`,

    UN_ARCHIVO_NO_CUMPLE_VALIDACION: `Se encontró 1 documento que no cumple con la validación. 
    $br Corrija para proceder. $br$br $info Para visualizar detalle de las facturas con errores, 
    haga click en los iconos de estado de validación para desplegar información.`,
    
    TODOS_LOS_ARCHIVOS_CUMPLEN_VALIDACION: '$check Todos los documentos cumplen con la validación.',

    NEGOCIO_REVISION_POR_EJECUTIVO: 'Su negocio está siendo revisado por su ejecutivo.',

    SELECCIONE_CERTIFICADO_PARA_NEGOCIO: 'Favor seleccione un certificado de la lista para asociar al Negocio en Curso.',

    CORREO_ACTUALIZADO: '$check Su correo ha sido actualizado exitosamente a $1.',

    DOCUMENTO_REEMPLAZO_EXITOSO: '$check Documento reemplazado exitosamente.',

    NOTA_CREDITO_DEBITO_CARGADA: '$check Nota de Crédito/Débido cargada exitosamente.',

    RECESION_EXITOSA: 'Operación realizada con éxito. $br Se enviará un email cuando la recesión sea completada.',

    SELECCIONE_FECHA_HABIL: 'Favor seleccione una fecha hábil.',

    VALIDAR_CAMPOS_REQUERIDOS: 'Ingrese los campos requeridos para continuar.',

    DESEA_ENVIAR_NEGOCIO: '¿Está seguro de Enviar el Negocio?',

    DESEA_ACEPTAR_COTIZACION: '¿Está seguro de Aceptar la Cotización?',

    FECHA_VENCIMENTO_FACTURAS_GUARDADAS: 'Fecha de vencimiento ha sido actualizada.',

    CERTIFICADO_POR_EXPIRAR: 'Certificado emitido para <strong>$1</strong> está próximo a expirar. $br Fecha de vencimiento: <strong>$2</strong>.',

    EMAIL_INVALIDO: 'E-Mail ingresado no es válido.',

    RUT_INVALIDO: 'RUT ingresado no es válido.',

    IFRAME_ERROR_CONFIG: 'Ha ocurrido un problema realizar configuración de iframe Receder'
};