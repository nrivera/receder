var parsePath = require('./parsePath');

/** 
 * @author Pablo Wenger Amengual
 * @description Servicio para comunicación asincrona para comunicación con API
 * @param {Object} http Servicio `$http` de angularjs
 * @param {String} ruta String que contiene la ruta a la API.
 * @example
 * var util = require('commons/util');
 * module.exports = [
 *    '$http', 
 *   function(http){ 
 *       return { 
 *           obtenerEvaluacion: util.obtenerServicioGet(http, 'api/evaluacionnegocio')
 *       }
 *   }
 *]
 */
module.exports = function (http, ruta) {
    return function (params, config) {
        var defaultConfig = {params: params};
        angular.extend(defaultConfig, config || {});
        return http.get(parsePath(ruta, params), defaultConfig).then(function (resp) {
            if (typeof success == 'function') {
                var r = success(resp.data, resp.status);
                return r || resp;
            }
            return resp;
        }).catch(function (resp) {
            if (typeof error == 'function') {
                var r = error(resp.data, resp.status);
                return r || resp;
            }
            return resp;
        })
    }
}