'use strict';

/**
 * @author Nicolas Andres Rivera Acevedo
 * @description script con métodos de utilidad, deben ser atomicos y genéricos
 */
module.exports = {
    esIe                    : _esIe,
    date                    : _date,
    dateToString            : _dateToString,
    obtenerFechaLarga       : _obtenerFechaLarga,
    obtenerServicioGet      : require('./obtenerServicioGet'),
    obtenerServicioPost     : require('./obtenerServicioPost'),
    obtenerServicioDelete   : require('./obtenerServicioDelete'),
    validarContentType      : require('./validarContentType'),
    base64toBlob            : require('./base64toBlob'),
    parseString             : require('./parseString'),
    joinUrl                 : require('./joinUrl'),
    normalizeUrl            : require('./normalizeUrl'),
    obtenerMesActual        : require('./obtenerMesActual'),
    parsePath               : require('./parsePath'),
    lowerCaseObject         : require('./lowerCaseObject'),
    normalizeEntities       : require('./normalizeEntities'),
    agregarSeleccione       : _agregarSeleccione,
    agregarItemEspecial     : _agregarItemEspecial
};


/**
 * @description agrega un item seleccione a la colección
 * @param {Array} coleccion colección de objetos
 * @param {String} key prop a la cual agregar el valor 0
 * @param {String} label prop a la cual agregar el valor '-- seleccione --'
 */
function _agregarSeleccione(coleccion, key, label, value){
    value = value || 0;
    return _agregarItemEspecial(coleccion, key, label, value, '-- Seleccione --');
}

/**
 * @description agrega un item especial a la colección
 * @param {Array} coleccion colección de objetos
 * @param {String} key prop a la cual agregar el valor dado por keyValue
 * @param {String} label prop a la cual agregar el valor dado por labelValue
 * @param {*} keyValue valor a agregar en la prop dada por key
 * @param {*} labelValue valor a agregar en la prop dada por label
 */
function _agregarItemEspecial(coleccion, key, label, keyValue, labelValue){
    if (!coleccion || typeof key == 'undefined' || typeof label == 'undefined') return coleccion;
    for (var i=0; i<coleccion.length; i++){
        if (coleccion[i][key] == keyValue) return coleccion;
    }
    var item = {};
    item[key] = keyValue;
    item[label] = labelValue;
    coleccion.splice(0,0, item);
    return coleccion;
}

/**
 * @description Función que permite verificar si el navegador es Explorer de Microsoft
 */
function _esIe(){
    var ua = window.navigator.userAgent.toLowerCase();
    return ua.indexOf("msie") > -1 || ua.indexOf("trident") > -1 || ua.indexOf("edge") > -1;
};

/**
 * @description Permite transformar una fecha en objeto Date
 * @param  {string} input  	string que representa una fecha ('dia/mes/año')
 * @return {object}         Date
 */
function _date(input) {
    if (input == null || input instanceof Date) return input;
    input = input.replace(/-/g, "/").split("/");
    input = new Date(input[1] + "/" + input[0] + "/" + input[2]);
    if (isNaN(input.getDay()) || input.getFullYear() <= 1900) input = null;
    return input;
}

/**
 * @description Permite obtener la fecha completa actual
 */
function _obtenerFechaLarga() {
    var fecha = new Date();
    var meses = "Enero,Febrero,Marzo,Abril,Mayo,Junio,Julio,Agosto,Septiembre,Octubre,Noviembre,Diciembre".split(",");
    var dias = "Domingo,Lunes,Martes,Miércoles,Jueves,Viernes,Sábado".split(",");
    return dias[fecha.getDay()] + " " + fecha.getDate() + " de " + meses[fecha.getMonth()] + " de " + fecha.getFullYear();
}

/**
 * @description Permite transformar una fecha a una fecha tipo dd/MM/YYYY
 */
function _dateToString(date) {
    if (date == null) return date;
    var m = date.getMonth() + 1;
    var d = date.getDate();
    d = d > 9 ? d : '0' + d;
    m = m > 9 ? m : '0' + m;
    return d + '/' + m + '/' + date.getFullYear();
}