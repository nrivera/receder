/**
 * @description Función encargada de generar un objeto con propiedades en lower case
 * @param {Object} obj objeto a transformar sus claves en lower case
 */
module.exports = function lowerCaseObject(obj){
    var lower = {};
    Object.keys(obj).forEach(function(k){
        lower[k.toLowerCase()] = obj[k];
    });
    return lower;
}