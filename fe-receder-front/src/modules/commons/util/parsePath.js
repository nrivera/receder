/**
 * @author Nicolas Andres Rivera Acevedo
 * @description Función encargada de parsear un String con parámetros.
 * @param {String} path String que contiene variables del tipo `{var}`
 * @param {Object} params Objeto literal con variables a integrar en `path`
 * @example 
 *      let path = 'path/to/movies/{idMovie}/cast/{idActor}';
 *      path = parsePath(path, {
 *          idMovie: 1,
 *          idActor: 53
 *      });
 *  
 *      console.log(path); //imprime path/to/movies/1/cast/53
 */
module.exports = function parsePath(path, params){
    if (typeof params != 'object') return path;
    Object.keys(params).forEach(function(k){
        path = path.replace(new RegExp('{'+k+'}', 'ig'), params[k] == null ? '' : params[k]);
    });
    return path;
};