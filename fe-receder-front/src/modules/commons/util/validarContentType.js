/**
 * @author Nicolas Andres Rivera Acevedo
 * @param {File} file instancia de File, archivo a validar content types
 * @param {Array|String|Function} contentType String o Array de String que representan los contentTypes validos,
 *     Si es una función se utilizará como un callback para validar el archivo, en la cual pasará el archivo
 *     como parámetro a la función.
 */
module.exports = function(file) {
    var contentTypes = [], i;
    for (i = 1; i < arguments.length; i++) {
        if (angular.isArray(arguments[i])) contentTypes = contentTypes.concat(arguments[i]);
        else contentTypes.push(arguments[i]);
    }
    for (i = 0; i < contentTypes.length; i++) {
        var type = contentTypes[i];
        if (typeof type == 'function' && type(file)) return true;
        if (type == '*/*' || (type.toLowerCase && file.type.toLowerCase() == type.toLowerCase())) return true;
    }
    return false;
}