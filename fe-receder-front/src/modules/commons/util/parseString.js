/**
 * @author Nicolas Andres Rivera Acevedo
 * @description Función encargada de parsear un string con parámetros.
 * El primer argumento de la función es el String a parsear, los siguientes son
 * convertidos a claves $n donde n es el número que identifica al argumento comenzando en 1
 * @example 
 *      let str = '$1 es una vocal y $2 es un número impar';
 *      let out = parseString(str, 'A', 3);
 */
module.exports = function parseString(){
    var input = String(arguments[0]);
    for (var i=1; i<arguments.length; i++){
        input = input.replace(new RegExp('\\$'+i, 'g'), arguments[i]);
    }
    input = input.replace(/\$\d+/g,'');
    return input;
};