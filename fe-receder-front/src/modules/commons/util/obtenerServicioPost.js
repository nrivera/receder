var parsePath = require('./parsePath');

/** 
 * @author Nicolas Andres Rivera Acevedo
 * @description Devuelve la función para generar servicio post
 * @param {Object} http Servicio `$http` de angularjs
 * @param {String} ruta String que contiene la ruta a la API.
 * @example
 * var util = require('commons/util');
 * module.exports = [
 *    '$http', 
 *   function(http){ 
 *       return { 
 *           obtenerEvaluacion: util.obtenerServicioPost(http, 'api/evaluacionnegocio')
 *       }
 *   }
 *]
 */
module.exports = function (http, ruta) {
    return function (params, config) {
        return http.post(parsePath(ruta, params), params, config || {}).then(function (resp) {
            if (typeof success == 'function') {
                var r = success(resp.data, resp.status);
                return r || resp;
            }
            return resp;
        }).catch(function (resp) {
            if (typeof error == 'function') {
                var r = error(resp.data, resp.status);
                return r || resp;
            }
            return resp;
        })
    }
}