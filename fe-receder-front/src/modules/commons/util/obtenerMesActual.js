'use strict';

/**
 * @author Nicolas Andres Rivera Acevedo
 * @description Función que permite obtener el mes de la fecha actual
 */
module.exports = function obtenerMesActual() {
    var meses = 'Enero,Febrero,Marzo,Abril,Mayo,Junio,Julio,Agosto,Septiembre,Octubre,Noviembre,Diciembre'.split(',');
    return meses[(new Date()).getMonth()];
};