/**
 * @description Función encargada de intercambiar palabras claves por
 * elementos de html
 * @param {String} input string de entrada con palabras claves de iconos:
 * 		$warn  	: icono de triangulo de warning
 * 		$info	: icono de circulo de información
 * 		$times 	: icono de x de error
 * 		$check	: icono de un tick en un circulo verde
 */
module.exports = function normalizeEntities(input){
    return input.
        replace(/\$warn/g, '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>').
        replace(/\$info/g, '<i class="fa fa-info-circle" aria-hidden="true"></i>').
        replace(/\$times/g, '<i class="fa fa-times-circle" aria-hidden="true"></i>').
        replace(/\$check/g, '<i class="fa fa-check-circle" aria-hidden="true"></i>').
        replace(/\$br/g, '<br/>');
}