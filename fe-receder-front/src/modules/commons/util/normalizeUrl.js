'use strict';

/**
 * @author Nicolas Andres Rivera Acevedo
 * @description Función que permite normalizar una url.
 * Normaliza //, /? /#, :/
 * @param {String} url string que respresenta una url
 */
module.exports = function normalizeUrl(url) {
    return url
        .replace(/[\/]+/g, '/')
        .replace(/\/\?/g, '?')
        .replace(/\/\#/g, '#')
        .replace(/\:\//g, '://');
};