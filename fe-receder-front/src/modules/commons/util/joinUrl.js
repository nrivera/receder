'use strict';

var normalize = require('./normalizeUrl');

/**
 * @author Nicolas Andres Rivera Acevedo
 * @description Función que permite concatenar dos string que representan una url
 * @param {String} baseUrl url base a concatenar con `url`
 * @param {String} url url a concatenar con `baseUrl`
 */
module.exports = function joinUrl(baseUrl, url) {
    if (/^(?:[a-z]+:)?\/\//i.test(url)) {
        return url;
    }
    return normalize([baseUrl, url].join('/'));
};