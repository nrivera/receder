/**
 * @author Nicolas Andres Rivera Acevedo
 * @description Función encargada de transformar un string de base64 a blob,
 * esta función está escrita para trabajar con archivos transportados como base64
 * @param {String} b64Data String con datos en base64
 * @param {String} contentType String que representa el tipo de archivo
 */
module.exports = function (b64Data, contentType) {
    contentType = contentType || '';

    var sliceSize = 512;
    
    /**
     * normalizar el string de datos, se quita la primera parte hasta la coma
     * y todos los espacios en blanco.
     */
    b64Data = b64Data.replace(/^[^,]+,/, '');
    b64Data = b64Data.replace(/\s/g, '');

    var byteCharacters = window.atob(b64Data);
    var byteArrays = [];

    for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
        var slice = byteCharacters.slice(offset, offset + sliceSize);

        var byteNumbers = new Array(slice.length);
        for (var i = 0; i < slice.length; i++) {
            byteNumbers[i] = slice.charCodeAt(i);
        }

        var byteArray = new Uint8Array(byteNumbers);

        byteArrays.push(byteArray);
    }

    return new Blob(byteArrays, {
        type: contentType
    });
};