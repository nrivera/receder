'use strict';

import {
    VALIDAR_CAMPOS_REQUERIDOS,
} from 'commons/constants/messages';

/**
 * @author Nicolas Andres Rivera Acevedo
 * @description Script encargado de configurar las validaciones de la directiva ux-validar.
 */
module.exports = {
    configurar  : configurar
};

/**
 * @author Nicolas Andres Rivera Acevedo
 * @description Función encargada de configurar la directiva de validación.
 * @param {Object} validateConfigProvider provider de configuración de la directiva ux-validate
 */
function configurar(validateConfigProvider) {

    validateConfigProvider
    .defaultMessage(VALIDAR_CAMPOS_REQUERIDOS)
    .alert(function(mensaje, focus){
        var popup = this.get('$popup');
        return popup.warn(mensaje).then(function(){
            focus();
        });
    }).invalidItems('select', [
        /null/, //expresion regular, cualquier valor seleccionado que coincida con null
        -1,     //valor literal == -1
        0,      //valor literal == 0 
    ])

    
};