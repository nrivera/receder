'use strict';

import validaciones from './validaciones';
import alertas from './alertas';

/**
 * @author Nicolas Andres Rivera Acevedo
 * @description configuración del submodulo 'app.commons'
 */
module.exports = [

	'$validateConfigProvider', '$modalConfigProvider','$uibModalProvider',

	function(validateConfigProvider, modalConfigProvider , modalProvider){

		validaciones.configurar(validateConfigProvider);
		alertas.configurar(modalConfigProvider);
		/** 
	     * configuración por defecto para modales 
	     */ 
		 modalProvider.options = { 
            backdrop: 'static', 
            size: 'lg' 
        };
		
	}

];