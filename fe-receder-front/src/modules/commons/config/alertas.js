'use strict';

import {normalizeEntities} from 'commons/util';

/**
 * @author Nicolas Andres Rivera Acevedo
 * @description Script encargado de configurar las alertas del servicio $modal
 */
module.exports = {
    configurar  : configurar
};

/**
 * @author Nicolas Andres Rivera Acevedo
 * @description Función encargada de configurar el servicio $modal
 * @param {Object} modalConfigProvider provider de configuración del servicio $modal
 */
function configurar(modalConfigProvider) {

    /**
     * define alertas por defecto:
     * alerta normal y confirmar
     */
    modalConfigProvider
    .alertTitle('Mensaje')
    .confirmTitle('Confirme')
    .alertView(require('commons/views/alert.html'))
    .alertController([
        '$scope', '$modalInstance', '$modalConstant',
        function(scope, modalInstance, modalConstant){

            scope.isConfirm = scope.$modalType == modalConstant.TYPE_ALERT_CONFIRM;
            scope.$message = normalizeEntities(scope.$message);

            scope.aceptar = function(){
                modalInstance.close();
            };

            scope.cancelar = function(){
                modalInstance.dismiss();
            };
        }
    ]);

    /**
     * define el tipo de alerta `warning`
     */
    modalConfigProvider.alert('warning', {
        view: require('commons/views/alert-warning.html'),
        controller: [
            '$scope', '$modalInstance',
            function(scope, modalInstance){
                scope.$message = normalizeEntities(scope.$message);
                scope.aceptar = function(){
                    modalInstance.close();
                }
            }
        ]
    });

    /**
     * define el tipo de alerta `error`
     */
    modalConfigProvider.alert('error', {
        view: require('commons/views/alert-error.html'),
        controller: [
            '$scope', '$modalInstance',
            function(scope, modalInstance){
                scope.$message = normalizeEntities(scope.$message);
                scope.aceptar = function(){
                    modalInstance.close();
                }
            }
        ]
    });
};