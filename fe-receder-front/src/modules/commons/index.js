'user strict';

/**
 * @author Nicolas Andres Rivera Acevedo
 * @description Definición del submodulo app.commons
 *              En este modulo debe ir todo lo transversal a los demás modulos
 */
var moduleName = 'app.commons';
module.exports = moduleName;

angular.module(moduleName, [
	/**
	 * dependencias:
	 */
	require('@ns-angularjs/validate'),
	require('@ns-angularjs/modal'),
])
.config(require('./config'))

require('./directives');
require('./services');
require('./providers');
require('./filters');