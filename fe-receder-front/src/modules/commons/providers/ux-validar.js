'use strict';

/**
 * @author Sinecio Bermúdez Jacque
 * @description Provider para configuración de la directiva de validaciones
 */
module.exports = function () {

    /**
     * @description Atributos privados
     */
    var _validaciones = {};             //contiene un mapa de las validaciones que puede utilizar la directiva ux-validar.
    var _msjValidacion  = null;         //contiene el mensaje de validación por defecto.

    /**
     * @description Funciones disponibles en el config.
     */
    this.mensajeDefecto     = mensajeDefecto;
    this.validacion         = validacion;

    /**
     * @description Función de instancia del factory.
     */
    this.$get = get;
    
    /**
     * @description Factory disponible para utilizar como '$uxValidar'
     */
    function get(){
        return {
            obtenerTipo             : obtenerTipo,
            obtenerMensajeDefecto   : obtenerMensajeDefecto
        };
    }

    /**
     * @description Función encargada de informar la función que realiza la validación
     * para el tipo dado por `tipo`.
     * @param {String} tipo tipo de validación a obtener
     */
    function obtenerTipo(tipo){
        tipo = String(tipo).toLowerCase();
        return _validaciones[tipo] || angular.noop();
    }

    /**
     * @description Función encargada de informar el mensaje por defecto de validación
     * establecido en la configuración.
     */
    function obtenerMensajeDefecto(){
        return _msjValidacion;
    }

    /**
     * @description Función encargada de establecer el mensaje por defecto
     * como resultado de una validación.
     * @param {Strig} msj string que representa el mensaje por defecto a utilizar
     * para los campos marcados como requeridos y no son validos.
     */
    function mensajeDefecto(msj){
        _msjValidacion = msj;
        return this;
    }

    /**
     * @description Función encargada de agregar tipos de validaciones
     * a utilizar en los campos marcados con el atributo `ux-validar-tipo`
     * @param {String} tipo tipo de validación, este es el tipo que se utiliza con
     * el atributo `ux-validar-tipo` en los campos a validar
     * @param {Function} fn función que ejecuta la validación
     */
    function validacion(tipo, fn){
        tipo = String(tipo).toLowerCase();
        var validacion = {};
        validacion[tipo] = fn;
        angular.extend(_validaciones, validacion);
        return this;
    }
};