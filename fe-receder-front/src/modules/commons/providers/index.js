'user strict';

/**
 * @author Sinecio Rodrigo Bermúdez Jacque
 * @description indice de todos los providers del modulo 'app.commons'
 */
angular.module('app.commons')

.provider('$uxValidar', require('./ux-validar'));