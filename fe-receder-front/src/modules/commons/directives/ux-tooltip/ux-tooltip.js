'use strict';

/**
 * @author Mauricio Ross
 * @description Directive para tooltip que consume tooltip de bootstrap, por lo tanto puede consumir todos los recursos de este componente en bootstrap.
 * @example 
 * <button type="button" ux-tooltip class="btn btn-secondary"  title="Tooltip on top">
                Tooltip on top
              </button>
 */
module.exports = [
    function(){
        return {
            restrict: 'A',
            scope: {},
            link: function(scope, elem, attr){
                elem.tooltip({
                    container: 'body'
                });

                elem.on('mousedown', function(e){
                    elem.tooltip('hide');
                });
            }
        }
    }
]