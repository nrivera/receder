'use strict';

import {base64toBlob, esIe} from 'commons/util';
import icons from 'commons/constants/icons';

/**
 * @author Nicolas Andres Rivera Acevedo
 * @description Directiva para descarga de documentos
 */
module.exports = [

    '$documentoDescarga', '$popup',

    function(documentoDescarga, popup){
        return {
            template: require('./template.tmpl'),
            replace: true,
            restrict: 'E',
            scope: {
                id: '=',
                icon: '@',
                tipo: '=',
                label: '@',
                class: '@'
            },
            link: function(scope, elem, attr){

                scope.descargar = descargar;
                
                /**
                 * @description Función inicial.
                 */
                function init(){
                    obtenerClaseIcono();
                    verificarId();
                }

                /**
                 * @description Función encargada de obtener el tipo de icono de documento.
                 */
                function obtenerClaseIcono(){
                    scope.claseIcono = icons[(scope.icon || '').toUpperCase()] || icons.DOWNLOAD;
                }

                /**
                 * @description Función encargada de revisar las condiciones en función del
                 * id para descarga.
                 */
                function verificarId(){
                    if (!scope.id) elem.remove();
                }

                /**
                 * @description Función encargada de establecer el icono 'descargando'
                 */
                function mostrarIconoDescargando(){
                    scope.claseIcono = icons.DOWNLOADING;
                }

                /**
                 * @description Función encargada de generar la petición de descarga.
                 */
                function descargar(){
                    mostrarIconoDescargando();
                    documentoDescarga.get({
                        ID: scope.id,
                        TipoDocumento: scope.tipo
                    }).then(function(resp){
                        if (resp.status != 200) {
                            obtenerClaseIcono();
                            popup.errorAlert(resp.data.Message);
                            return;
                        }
                        generarDescarga(resp);
                    });
                }

                /**
                 * @description Función encargada de generar el archivos y descargarlo
                 * luego de que se ha recibido respuesta desde el servidor.
                 * @param {Object} resp respuesta desde el servidor.
                 */
                function generarDescarga(resp){
                    if (esIe()) {
                        window.navigator.msSaveOrOpenBlob(base64toBlob(resp.data.Base64, resp.data.DataType), `${resp.data.FileName}.${resp.data.Extension}`);
                    } else {
                        var elem = document.createElement('a');
                        elem.setAttribute('href', `data:${resp.data.DataType};base64,${resp.data.Base64}`);
                        elem.setAttribute('download', `${resp.data.FileName}.${resp.data.Extension}`);

                        elem.style.display = 'none';
                        document.body.appendChild(elem);

                        elem.click();
                        document.body.removeChild(elem);
                    }
                    obtenerClaseIcono();
                }

                init();
            }
        }
    }
];