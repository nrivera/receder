'use strict';

/**
 * @author Nicolas Andres Rivera Acevedo
 * @description Directiva que permite parsear y formatear números agregando
 * puntos de miles a un input.
 */
module.exports = [
    '$browser',
    function(browser){
        return {
            restrict: 'A',
            require: 'ngModel',
            link: function(scope, elem, attr, ngModelCtrl){
                
                ngModelCtrl.$parsers.push(function(viewValue) {
                    return quitarLetras(viewValue);
                });
                
                ngModelCtrl.$render = function() {
                    elem.val(quitarLetras(ngModelCtrl.$viewValue));
                }
                
                elem.bind('change', listener)
                elem.bind('keydown', function(event) {
                    var key = event.keyCode
                    // If the keys include the CTRL, SHIFT, ALT, or META keys, or the arrow keys, do nothing.
                    // This lets us support copy and paste too
                    if (key == 91 || (15 < key && key < 19) || (37 <= key && key <= 40)) return;
                    browser.defer(listener) // Have to do this or changes don't get picked up properly
                });
                
                elem.bind('paste cut', function() {
                    browser.defer(listener)  
                });

                function listener() {
                    elem.val(quitarLetras(elem.val()));
                }

                function quitarLetras(value){
                    return value.toString().replace(/[^\d]/g, "");
                }
            }
        }
    }
]