'use strict';

require('bootstrap-slider');

/**
 * @author Sinecio Bermúdez Jacque
 * @description slidebar basada en seiyria-bootstrap-slider
 *              https://github.com/seiyria/bootstrap-slider
 * 
 * @param {Number} slideMin valor numerico que indica el valor mínimo del rango
 * @param {Number} slideMax valor numerico que indica el valor mínimo del rango
 * @param {String|Array} value valor que representa el rango seleccionado entre el mínimo y máximo, puede ser un array de dos valores numericos
 *      o un string de dos valores separados por coma.
 * @param {Object} fallbackObject referecia a un objeto del scope padre, esta referencia es utilizada para agregar comunicación entre directiva 
 *      y scope. En este objeto se agregará el método $update() para ser utilizado cuando sea necesario refrescar la vista de la directiva desde
 *      una acción provocada en scope padre, pero fuera del contexto del scope de la directiva.
 *      esta referencia también es pasada como argumento en el evento uxSlidebar:change
 * @param {Function} mouseUp función a ejecutar cuando se suelta el mouse del handler
 */
module.exports = function(){
    return {
        restrict: 'E',
        template: require('./template.tmpl'),
        scope: {
            slideMin: '=',
            slideMax: '=',
            value: '=',
            fallbackObject: '=',
            mouseUp: '&'
        },
        link: function(scope, elem, attr){
            var el = elem.find('input');

            el.on('change', function(){
                scope.$apply(change);
            });

            scope.$watch('slideMin', init);
            scope.$watch('slideMax', init);
            scope.$watch('value', init);

            if (!angular.isObject(scope.fallbackObject)) throw new Error('atributo fallback-object es requerido');
            else {
                scope.fallbackObject.$update = function(){
                    obtenerInstancia();
                }
            }

            function init(){
                obtenerInstancia();
                agregarEventoHandlers();
            }

            function change(){
                var values = el.val().split(',');
                var min = Number(values[0]);
                var max = Number(values[1]);
                scope.$emit('uxSlidebar:change', min, max, scope.fallbackObject);
            }

            function obtenerInstancia(){
                el.slider({
                    min: obtenerMin(), 
                    max: obtenerMax(), 
                    value: obtenerValue(),
                    focus: false,
                    tooltip: 'hide'
                });
                el.slider('refresh');
                change();
            }

            function agregarEventoHandlers(){
                elem.find('.slider-handle').unbind().on('mouseup', function(e){
                    scope.$apply(scope.mouseUp);
                });
            }

            function obtenerMin(){
                return Number(scope.slideMin) || 0;
            }

            function obtenerMax(){
                return Number(scope.slideMax) || 0;
            }

            function obtenerValue(){
                if (angular.isArray(scope.value)) return scope.value;
                else if (angular.isString(scope.value)) {
                    var values = scope.value.split(',')
                    for(var i=0; i<values.length; i++){
                        values[i] = Number(values[i]) || 0 ;
                    }
                    return values;
                }
                else return Number(scope.value) || 0;
            }
        }
    }
}