'use strict';

import { map } from 'lodash';
import $ from 'jquery';

/**
 * @author Nicolas Andres Rivera Acevedo
 * @description Directiva de paginación y ordenamiento.
 * se debe agregar el modelo de Paginación en el controller de la vista (ver ejemplo js)
 * y en la vista se agrega la directiva indicando la función que realiza la llamada de los
 * datos paginados.
 * 
 * @param {Function} load función que realiza llamada a los datos paginados.
 * @param {Object} ngModel objeto modelo que mantiene los parámetros de paginación.
 * 
 * @example
 *      js:
 *      scope.Paginacion = {
 *          Pagina			: 1,     
 *          CampoOrden		: '',     
 *          Descendente		: false
 *      };
 * 
 *      scope.obtenerDatos = function(){
 *          var params = {
 *              Rut: '16146432k'
 *          };
 *          angular.extend(params, scop.Paginacion);    //agrega información de Paginación a los parámetros
 *          service.obtenerDatos(params).then(function(resp){
 *              scope.Datos = resp.Datos;
 *              //actualiza los datos de paginación con la información del backend:
 *              angular.extend(scope.Paginacion, resp.Paginacion);
 *          });
 *      }
 *      
 *      view:
 *      <ux-pagination ng-model="Paginacion" load="obtenerDatos()"></ux-pagination>
 */
module.exports = [
    '$constantes',
    function (constantes) {
        
        
        const CANTIDAD_NUMEROS_PAGINA         = 5;     //cantidad de números de páginas visibles en la Paginación.
        const OPCIONES_REGISTROS_POR_PAGINA  = "5,10,20,50,100";     //cantidad de registros por página.
        const REGISTROS_POR_PAGINA          = 10;     //registros por página por defecto, debe ser uno de las opciones de la lista anterior.
        
        return {
            restrict: 'E',
            replace: true,
            template: require('./template.tmpl'),
            scope: {
                load: '&',
                ngModel: '='
            },
            link: function (scope, elem, attr) {
                /**
                 * @description variables para el comportamiento de la colección de botones
                 * que representan las Páginas
                 */
                var paginas        = [];       //colección de páginas generadas como objeto (revisar función generarPaginas).

                /**
                 * @description cada vez que ocurre un cambio en el modelo de paginación
                 * se debe calcular la paginación, es decir, calcular las páginas visibles
                 * en función de lo que se informe, para esto es muy importante el atributo TotalPaginas.
                 */
                scope.$watch('ngModel', function (modelValue) {
                    if (modelValue == null) return;
                    verificarAtributos();
                    calcularPaginacion();
                }, true);

                /**
                 * @description variables para utilizar en el template de la directiva
                 */
                scope.opcionesCantidadRegistros         = obtenerOpcionesCantidadRegistros();
                scope.registrosPorPagina                = Number(REGISTROS_POR_PAGINA) || 0;

                scope.primera                           = primera;
                scope.anterior                          = anterior;
                scope.siguiente                         = siguiente;
                scope.ultima                            = ultima;
                scope.cargarPagina                      = cargarPagina;
                scope.cambiarRegistrosPorPagina         = cambiarRegistrosPorPagina;

                /**
                 * @description ordenamiento
                 */
                elem.parent().find('[orden]').on('click', function (e) {
                    var header = $(this);
                    var sort = header.attr('sort') || 'asc';

                    if (header.is(scope.currentOrder)) sort = sort == 'asc' ? 'desc' : 'asc';

                    scope.currentOrder = header;

                    header.attr('sort', sort);

                    scope.ngModel.CampoOrden = header.attr('orden');
                    scope.ngModel.Descendente = sort == 'desc';

                    marcarOrdenamiento.bind(this)();

                    scope.$apply(scope.load);
                });

                /**
                 * @description marca el ordenamiento ascendente/descendente actual
                 */
                function marcarOrdenamiento() {
                    if ($(this).attr('orden') == scope.ngModel.CampoOrden) {
                        scope.currentOrder = $(this);
                        scope.currentOrder.siblings().removeClass('asc').removeClass('desc');
                        if (scope.ngModel.Descendente) {
                            scope.currentOrder.removeClass('asc').addClass('desc');
                            scope.currentOrder.attr('sort', 'desc');
                        } else {
                            scope.currentOrder.removeClass('desc').addClass('asc');
                            scope.currentOrder.attr('sort', 'asc');
                        }
                    }
                }

                /**
                 * @description Función que genera la colección de opciones
                 * de registros por página.
                 */
                function obtenerOpcionesCantidadRegistros() {
                    return map(OPCIONES_REGISTROS_POR_PAGINA.split(','), function (num) {
                        return Number(num);
                    });
                }

                /**
                 * @author Mauricio Ross
                 * @description Function que permite navegar hasta la primera pagina.
                 */
                function primera() {
                    cargarPagina(1);
                }

                /**
                 * @description Función que permite navegar a la página anterior a la actual,
                 * si la página anterior está fuera de las páginas visibles, se rebaja el indice
                 * y se generan nuevamente los números de página.
                 */
                function anterior() {
                    if (scope.numerosPagina[0].Numero <= 1) return;
                    scope.indice--;
                    obtenerNumerosPagina();
                }

                /**
                 * @description Función que permite navegar a la página siguiente a la actual,
                 * si la página siguiente está fuera de las páginas visibles, se aumenta el indice
                 * y se generan nuevamente los números de página.
                 */
                function siguiente() {
                    if (scope.numerosPagina[scope.numerosPagina.length-1].Numero >= scope.ngModel.TotalPaginas) return;
                    scope.indice++;
                    obtenerNumerosPagina();
                }

                /**
                 * @author Mauricio Ross
                 * @description Función que permite navegar hasta la ultima pagina.
                 */
                function ultima() {
                    cargarPagina(scope.ngModel.TotalPaginas);
                }

                /**
                 * @description Función que permite navegar a una página específica,
                 * se actualiza el modelo de paginación con el núevo número de página
                 * y se realiza la llamada a la función que va a buscar los datos páginados.
                 * @param {Number} nro Número de página a cargar.
                 */
                function cargarPagina(nro) {
                    scope.ngModel.Pagina = nro;
                    scope.load();
                }

                /**
                 * @description Función que se encaga de cambiar los registros por página,
                 * esta función se gatilla cada vez que hay un cambio en la opción seleccionada
                 * del select 'Ver de a' en el template y realiza la llamada a la función 
                 * que va a buscar los datos páginados.
                 */
                function cambiarRegistrosPorPagina() {
                    scope.ngModel.Registros = scope.registrosPorPagina;
                    scope.ngModel.Pagina = 1;
                    scope.load();
                }

                /**
                 * @description Función encargada de revisar los atributos del modelo de paginación
                 * si uno de ellos no existe o no es compatible, se debe generar correctamente.
                 */
                function verificarAtributos(){
                    if (!scope.ngModel.TotalPaginas) scope.ngModel.TotalPaginas = 1;
                }

                /**
                 * @description Función que se encarga de orquestar las funciones principales
                 * para generar el comportamiento de navegación entre las páginas disponibles.
                 */
                function calcularPaginacion() {
                    generarPaginas();
                    obtenerIndice();
                    obtenerNumerosPagina();
                }

                /**
                 * @description Función que genera un subconjunto de páginas para ser listadas según
                 * la constante de cantidad de números de páginas (CANTIDAD_NUMEROS_PAGINA)
                 */
                function obtenerNumerosPagina() {
                    scope.numerosPagina = paginas.slice(scope.indice * CANTIDAD_NUMEROS_PAGINA - CANTIDAD_NUMEROS_PAGINA, scope.indice * CANTIDAD_NUMEROS_PAGINA);
                }

                /**
                 * @description Función que calcula los indices necesarios para el comportamiento
                 * de subconjunto de páginas.
                 * @example 
                 *      supongamos el siguiente caso:
                 *      paginas: [1,2,3,4,5,6];
                 *      paginas visibles: 3;
                 *      subconjuntos disponibles: 2 ([1,2,3] [4,5,6])
                 *      página actual: 4;
                 * 
                 *      el indice sería 2, ya que la página 4 se encuentra en el segundo subconjunto [4,5,6]
                 */
                function obtenerIndice() {
                    scope.indice = Math.ceil(scope.ngModel.Pagina / CANTIDAD_NUMEROS_PAGINA);
                    scope.totalIndices = Math.ceil(scope.ngModel.TotalPaginas / CANTIDAD_NUMEROS_PAGINA);
                }

                /**
                 * @description Función que se encarga de generar una colección de objetos que
                 * representan los botones de cada página.
                 * Los objetos tienen la estructura: 
                 * {
                 *      Numero: Number
                 *      Active: Boolean
                 * }
                 * el atributo Active será true cuando el número de página (i+1) coincida con la página
                 * actual del modelo.
                 */
                function generarPaginas() {
                    paginas = [];
                    for (var i = 0; i < scope.ngModel.TotalPaginas; i++) paginas.push({
                        Numero: i + 1,                          //Número de Página
                        Active: i + 1 == scope.ngModel.Pagina   //Activa cuando es la página actual del modelo
                    });
                }
            }
        }
    }

];