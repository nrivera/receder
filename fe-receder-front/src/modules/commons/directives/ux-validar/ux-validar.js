'use strict';

import $ from 'jquery';
import {find, remove} from 'lodash';

/**
 * @author Sinecio Bermúdez Jacque
 * @description directiva que permite validar campos en la vista.
 * Provee la función `validar()` al controller de la vista donde está inserta la directiva.
 * Esta función permite lanzar el proceso de validación retornando true cuando los campos son válidos y false cuando no lo son.
 * además levanta una alerta indicando el mensaje de validación. 
 * 
 * El mensaje génerico `uxValidar.obtenerMensajeDefecto()` es mostrado cuando hay campos requeridos a los que no se les ha ingresado
 * un valor, cualquier otro mensaje será mostrado si un campo tiene una validación personalizada.
 * 
 * validar tiene la función `noValidar()` que permite indicarle a la directiva que se iniciará un proceso de limpieza
 * de campos.
 * 
 * @example
 * 
 *  validación simple
 *  *****************
 *  valida que los campos requeridos tengan un valor ingresado:
 * 
 *  html:
 *  <div ux-validar>
 *      <input type="text" ng-model="campoRequerido" required/>
 *  </div>
 *  <button ng-click="enviar()">Enviar</button>
 * 
 *  js:
 *  scope.enviar = function(){
 *      if (!scope.validar()) return;
 *  }
 * 
 *  validación por contextos
 *  ************************
 *  valida que los campos agrupados en el contexto tengan un valor ingresado
 * 
 *  html
 *  <div ux-validar>
 *      <div ux-validar-contexto="parte1">
 *          <input type="text" ng-model="campoRequerido1" required/>
 *      </div>
 *      <div ux-validar-contexto="parte2">
 *          <input type="text" ng-model="campoRequerido2" required/>
 *      </div>
 *  </div>
 *  <button ng-click="guardar()">Guardar</button>
 *  <button ng-click="enviar()">Enviar</button>
 * 
 *  js:
 *  scope.guardar = function(){
 *      //valida que los campos en el contexto parte1 tengan un valor ingresado
 *      if (!scope.validar('parte1')) return;
 *  }
 *  scope.enviar = function(){
 *      //valida que los campos en el contexto parte2 tengan un valor ingresado
 *      if (!scope.validar('parte2')) return;
 * 
 *      //valida que todos los campos tengan un valor ingresado (sin importar contexto)
 *      if (!scope.validar()) return;
 *  }
 * 
 *  validación personalizada
 *  ************************
 *  agrega validación adicional a campos especiales como por ejemplo un email.
 *  En este caso tenemos un tipo indicado por el atributo `ux-validar-tipo`
 *  si el tipo está definido en el controller se utilizará ese tipo, si el tipo
 *  es un string, se utilizará la validación coincidente ya definida en la directiva,
 *  si es una función se utilizará esta función para validar el campo.
 * 
 *  html:
 *  <div ux-validar>
 *      <input type="text" ng-model="campoEmail1" required ux-validar-tipo="email"/>
 *      <input type="text" ng-model="campoEmail2" required ux-validar-tipo="tipoEmail"/>
 *  </div>
 *  <button ng-click="enviar()">Enviar</button>
 * 
 *  js:
 *  scope.enviar = function(){
 *      if (!scope.validar()) return;
 *  }
 * 
 *  js con definición para el tipo email:
 *  scope.email = function(value){
 *      //value es el valor ingresado en el campo

 *      return 'El email ingresado no es válido'; //retornado un string con el mensaje, determina que el campo es inválido.
 *      //return false; //retornado false determina que el campo es inválido, pero no agrega un mensaje informativo.
 *      //return true;  //retornando true el campo es válido.
 *  }
 *  
 *  //también puede ser un string
 *  scope.tipoEmail = 'email';  //en este caso se ejecuta la validación coincidente con 'email' definida en la directiva
 *  scope.enviar = function(){
 *      if (!scope.validar()) return;
 *  }
 * 
 *  Limpiar Campos Requeridos o con validaciones de tipo
 *  ****************************************************
 * 
 *  js:
 * 
 *  scope.noValidar = function(){
 *      scope.validar.noValidar();  //indica a la directiva que no inicie un proceso de validación
 *      scope.campoEmail1 = null;
 *      scope.campoEmail2 = null;
 *  }
 * 
 */
module.exports = [
    '$parse', '$popup', '$timeout', '$uxValidar',
    function(parse, popup, timeout, uxValidar){
        return {
            restrict: 'A',
            link: function(scope, elem){

                /**
                 * @description colección de elementos invalidos
                 */
                var invalidos = [];
                var contexto = null;
                var trigger = true;

                /**
                 * @description función inicial
                 */
                function init(){
                    verificarAtributos();
                    agregrarEvento();
                    observarNodo();
                }

                /**
                 * @description función encargada de verificar que un input tenga una entrada de texto
                 * @param {Object} elem jquery object
                 */
                function verificarInput(elem){
                    if (elem.val().length == 0) invalidos.push(elem.addClass('invalid'));
                }

                /**
                 * @description función encargada de verificar que un select tenga un elemento seleccionado
                 * @param {Object} elem jquery object
                 */
                function verificarSelect(elem){
                    if (!Number(elem.val().split(':')[1]) || elem.val() == '?') invalidos.push(elem.addClass('invalid'));
                }

                /**
                 * @description función encargada de verificar si un elemento ya existe en la colección de invalidos
                 * (para no agregarlo nuevamente)
                 * @param {Object} elem jquery object
                 */
                function existeInvalido(elem){
                    return !!find(invalidos, function(item){
                        return item.is(elem);
                    });
                }

                /**
                 * @description función encargada de verificar que un elemento tenga ingresado o seleccionado un valor
                 * @param {Object} elem jquery object
                 */
                function verificarEntrada(elem){
                    if (elem.is('input[type=text]')) verificarInput(elem);
                    else if (elem.is('input[type=password]')) verificarInput(elem);
                    else if (elem.is('select')) verificarSelect(elem);
                }

                /**
                 * @description función encargada de validar la entrada de un campo.
                 * Esta función ejecuta validaciones por defecto predefinidas en `tipoValidacion`.
                 * si la validación indicada en el atributo `ux-validar-tipo` es una función definida el controller, 
                 * esa función será ejecutada para determinar la validez de la entrada, si es un string literal
                 * se ejecuta la validación coincidente predefinida en `tipoValidacion` 
                 * @param {Object} elem jquery element a validar
                 */
                function validarEntrada(elem){
                    var tipo = elem.attr('ux-validar-tipo');

                    if (typeof tipo == 'undefined') return;

                    var val = null;
                    var invalid = existeInvalido(elem);

                    tipo = parse(tipo)(scope) || tipo;
                    if (typeof tipo == 'function') {
                        val = tipo(elem.val());
                    } else {
                        val = uxValidar.obtenerTipo(tipo)(elem.val());
                    }

                    if (typeof val == 'string' &&  !invalid) invalidos.push(elem.addClass('invalid').attr('ux-validar-msg', val));
                    else if (!val && !invalid) invalidos.push(elem.addClass('invalid'));
                    else if (!invalid) limpiarInvalidos(elem);
                }

                /**
                 * @description función encargada de revisar los atributos inciales de la directiva,
                 * estos atributos deben estar establecidos, ante de ejecutar cualquier otra acción.
                 */
                function verificarAtributos(){
                    //verificar el atributo ux-validar para determinar el scope donde registrar la función validar
                    var parent = (elem.attr('ux-validar') && parse(elem.attr('ux-validar'))(scope)) || scope;
                    validar.noValidar = noValidar;
                    parent.validar = validar;
                }

                /**
                 * @description Función encargada de observar el nodo de la directiva para agregar
                 * eventos a elementos que puedan ser agregados dinamicamente.
                 */
                function observarNodo(){
                    scope.$watch(function(){
                        return elem.get(0).childNodes.length;
                    }, agregrarEvento);
                }

                /**
                 * @description función encargada de agregar validación en evento blur a los elementos
                 * con atributo `ux-validar-tipo`
                 */
                function agregrarEvento(){
                    elem.find('[required],[ux-validar-tipo]').each(function(){
                        var field = $(this);
                        var initial = true;

                        if (field.is('.ux-validar-bound')) return;
                        else field.addClass('ux-validar-bound');

                        scope.$watch(function(){
                            return parse(field.attr('ng-model'))(scope);
                        }, function(val){
                            if (initial) {
                                initial = !initial;
                            } else {
                                if (trigger) timeout(task);
                                else timeout(function(){
                                    trigger = true;
                                    scope.$digest();
                                });
                            }
                        });
                        
                        function task(){
                            limpiarInvalidos(field);
                            verificarEntrada(field);
                            validarEntrada(field);
                        }
                    })
                }

                /**
                 * @description función encargada de obtener el contexto de validación.
                 * Esta función establece la variable `contexto` como el elemento en el cual se verificarán
                 * los campos a validar.
                 * @param {String} ctx representa el contexto a buscar. El contexto es todo elemento
                 * que tenga el atributo `ux-validar-contexto` con el valor coincidente con este parámetro `ctx`
                 */
                function obtenerContexto(ctx){
                    contexto = elem.find(`[ux-validar-contexto=${ctx}]`);
                    contexto = contexto.length > 0 ? contexto : elem;
                }

                /**
                 * @description función encargada de iterar los elementos del contexto requeridos a ser verificados
                 * con un valor ingresado.
                 */
                function verificarRequeridos(){
                    contexto.find('[required]:enabled').each(function(){
                        verificarEntrada($(this));
                    });
                }

                /**
                 * @description función encargada de iterar los elementos del contexto para validar su valor ingresado
                 */
                function validarTipos(){
                    contexto.find('[ux-validar-tipo]').each(function(){
                        validarEntrada($(this));
                    });
                }

                /**
                 * @description función encargada de limpiar colección y marcas de inválidos
                 * @param {Object} obj jquery object
                 */
                function limpiarInvalidos(obj){
                    (obj || elem.find('[required],[ux-validar-tipo]')).removeClass('invalid').removeAttr('ux-validar-msg');
                    if (obj) {
                        remove(invalidos, function(item){
                            return item.is(obj);
                        });
                    } else {
                        invalidos = [];
                    }
                }

                /**
                 * @description función encargada de gatillar la validación
                 * esta será ejecutada desde el controller de la vista que
                 * implemente esta directiva
                 * @param {String} contexto determina el contexto de la validación. El contexto
                 * es determinado con el atributo `ux-validar-contexto` en el elemento hijo
                 * del elemento que implemente la directiva `ux-validar`
                 */
                function validar(contexto){
                    limpiarInvalidos();
                    obtenerContexto(contexto);
                    verificarRequeridos();
                    validarTipos();
                    if (invalidos.length > 0){
                        invalidos[0].focus();
                        popup.warnAlert(invalidos[0].attr('ux-validar-msg') || uxValidar.obtenerMensajeDefecto());
                        return false;
                    }
                    return true;
                }

                /**
                 * @description Función que permite indicar a la directiva que se iniciará un
                 * proceso de limpieza de campos y estos no deben ser considerados como invalidos
                 * si estos tienen valor `vacio`
                 */
                function noValidar(){
                    trigger = false;
                }
                
                init();
                
            }
        }
    }
];