'use strict';

import Pikaday from 'pikaday';
import {date} from 'commons/util';

/**
 * @author Sinecio Rodrigo Bermúdez Jacque
 * @description directiva input con calendario para selección de fecha,
 *      por defecto, al escribir una fecha erronea en el input, será cambiada por la fecha actual siempre y cuando
 *      la fecha esté en el rango de minDate y maxDate, de lo contrario se establecerá la fecha minima permitida según
 *      el rango establecido.
 * 
 * @param {String|Date} minDate permite establecer una fecha minima para seleccionar
 * @param {String|Date} maxDate permite establecer una fecha máxima para seleccionar
 * 
 * @example
 *  <input type="text" ux-datepicker min-date="minFecha" max-date="maxFecha"/>
 */
module.exports = [

    "$timeout",

    function (timeout) {
        return {
            require: 'ngModel',
            scope: {
                minDate: "=",
                maxDate: "=",
                pickerBlur: "&"
            },
            link: function(scope, elem, attrs, ngModel){

                var picker = new Pikaday({
                    field: elem[0],
                    minDate: date(scope.minDate),
                    maxDate: date(scope.maxDate),
                    firstDay: 1,
                    format: 'DD/MM/YYYY',
                    i18n: {
                        previousMonth : 'Siguiente',
                        nextMonth     : 'Anterior',
                        months        : ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
                        weekdays      : ['Domingo','Lunes','Martes','Miércoles','Jueves','Viernes','Sábado'],
                        weekdaysShort : ['Do','Lu','Ma','Mi','Ju','Vi','Sa']
                    },
                    onClose: function(){
                        scope.$parent.$apply(function(){
                            setPickerDate();
                            scope.pickerBlur();
                        });
                    }
                });

                var setPickerDate = function(event){
                    if (event && event.type == "keyup" && event.which != 13) return;

                    if (typeof ngModel.$modelValue == "string") {
                        picker.setDate(date(ngModel.$modelValue));
                    } else {
                        picker.setDate(ngModel.$modelValue);
                    }
                };

                scope.$on('$destroy', function(){
                    picker.destroy();
                });

                scope.$watch('minDate', function(){
                    if (!scope.minDate) return;
                    picker.setMinDate(date(scope.minDate));
                });

                scope.$watch('maxDate', function(){
                    if (!scope.maxDate) return;
                    picker.setMaxDate(date(scope.maxDate));
                });

                timeout(setPickerDate);

                elem.on("mousedown", setPickerDate);
                elem.on("keyup", setPickerDate);
            }
        };
    }
]