'use strict';

/**
 * @author Nicolas Andres Rivera Acevedo
 * @description Directiva que permite parsear y formatear agregando
 * puntos de miles y guión a un input para representar un Rut Formateado de la siguiente forma
 * 99.999.999-K
 */
module.exports = [
    '$browser',
    function(browser){
        return {
            restrict: 'A',
            require: 'ngModel',
            link: function(scope, elem, attr, ngModelCtrl){
                
                ngModelCtrl.$parsers.push(function(viewValue) {
                    return limpiarEntrada(viewValue);
                });
                
                ngModelCtrl.$render = function() {
                    elem.val(agregarMiles(ngModelCtrl.$viewValue));
                }
                
                elem.bind('change', listener)
                elem.bind('keydown', function(event) {
                    var key = event.keyCode
                    // If the keys include the CTRL, SHIFT, ALT, or META keys, or the arrow keys, do nothing.
                    // This lets us support copy and paste too
                    if (key == 91 || (15 < key && key < 19) || (37 <= key && key <= 40)) return;
                    browser.defer(listener) // Have to do this or changes don't get picked up properly
                });
                
                elem.bind('paste cut', function() {
                    browser.defer(listener)  
                });

                function listener() {
                    var value = limpiarEntrada(elem.val())
                    elem.val(agregarMiles(value));
                }

                function limpiarEntrada(value){
                    return value.toString().replace(/[^(\d|k)]/ig,'');
                }

                function agregarMiles(value){
                    value = (value || '').toString();
                    var d = value[value.length-1];
                    if (!value.length) return value;
                    value = value.substr(0, value.length-1).replace(/\B(?=(\d{3})+(?!\d))/g, ".");
                    return value.length >= 1 ? value + '-' + d : d;
                }
            }
        }
    }
]