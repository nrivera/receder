'use strict';

/**
 * @author Nicolas Andres Rivera Acevedo
 * @description indice de todas las directivas del modulo 'app.commons'
 */

angular.module('app.commons')
.directive('circleChart', require('./circle-chart/circle-chart'))
.directive('uxDatepicker', require('./ux-datepicker/ux-datepicker'))
.directive('uxRut', require('./ux-rut/ux-rut'))
.directive('uxDownload', require('./ux-download/ux-download'))
.directive('uxNumbers', require('./ux-numbers/ux-numbers'))
.directive('uxPagination', require('./ux-pagination/ux-pagination'))
.directive('uxTooltip', require('./ux-tooltip/ux-tooltip'))
.directive('uxValidar', require('./ux-validar/ux-validar'))