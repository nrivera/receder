'use strict';

/**
 * @author Sinecio Bermúdez Jacque
 * @description directiva que permite dibujar un grafico circular de porcentaje
 */
module.exports = [

    function(){
        return {
            restrict: 'E',
            template: require('./template.tmpl'),
            scope: {
                totalAmount: '=',
                amount: '='
            },
            link: function(scope, elem, attr){
                var canvas = elem.find('canvas');
                var ctx = canvas.get(0).getContext('2d');

                var mW = canvas.width() / 2;
                var mH = canvas.height() / 2;


                scope.$watch('totalAmount', init);
                scope.$watch('amount', init);

                function init(){
                    limpiarCanvas();
                    dibujarCirculoExterior();
                    dibujarCirculoCentral();
                    dibujarTexto();
                }

                function limpiarCanvas(){
                    ctx.clearRect(0, 0, mW, mH);
                }

                function calcularPorcentaje(){
                    var total = Number(scope.totalAmount) || 0;
                    var amount = Number(scope.amount) || 0;
                    return (amount / total) || 0;
                }

                function dibujarCirculoExterior(){
                    var radio = mW * 1;    // 100 porciento del ancho;

                    ctx.save();
                    ctx.translate(mW, mH);
                    ctx.rotate(-90 * (Math.PI/180));

                    ctx.beginPath();
                    ctx.fillStyle = '#1e1d75';
                    ctx.arc(0, 0, radio, 0, 2 * Math.PI * calcularPorcentaje());
                    ctx.lineTo(0, 0);
                    ctx.fill();

                    ctx.restore();
                }

                function dibujarCirculoCentral(){
                    var radio = mW * .8;    // 80 porciento del ancho;

                    ctx.beginPath();
                    ctx.fillStyle = '#980083';
                    ctx.arc(mW, mH, radio, 0, 2 * Math.PI);
                    ctx.fill();
                }

                function dibujarTexto(){
                    var fontSize = mW * .6; // 60% del ancho;
                    var fontFace = 'arial';
                    var porcentaje = Math.ceil(calcularPorcentaje() * 100);
                    porcentaje = porcentaje > 100 ? 100 : porcentaje;

                    var texto = porcentaje + '%';
                    
                    ctx.save();
                    ctx.translate(mW, mH);
                    ctx.fillStyle = '#ffffff';
                    ctx.textAlign = 'center';
                    ctx.textBaseline = 'middle';
                    ctx.font = fontSize + "px " + fontFace;
                    ctx.fillText(texto, 0, 0);
                    ctx.restore();
                }
            }
        }
    }

];