/**
 * @author Sinecio Rodrigo Bermúdez Jacque
 * @description Servicio de utilidad, contiene 
 *              una colección de funciones para alertas y modales
 */
module.exports = [
	'$uibModal', '$auditoria',
	function(modal, auditoria){

		/**
		 * @description Función encargada de intercambiar palabras claves por
		 * elementos de html
		 * @param {String} input string de entrada con palabras claves de iconos:
		 * 		$warn  	: icono de triangulo de warning
		 * 		$info	: icono de circulo de información
		 * 		$times 	: icono de x de error
		 * 		$check	: icono de un tick en un circulo verde
		 */
		function normalizeEntities(input){
			return input.
				replace(/\$warn/g, '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>').
				replace(/\$info/g, '<i class="fa fa-info-circle" aria-hidden="true"></i>').
				replace(/\$times/g, '<i class="fa fa-times-circle" aria-hidden="true"></i>').
				replace(/\$check/g, '<i class="fa fa-check-circle" aria-hidden="true"></i>').
				replace(/\$br/g, '<br/>');
		}

		return {
			/**
			 * @description Permite mostrar un mensaje de alerta
			 * @param {object} params  objeto con los parámetros:
			 * @param {string} params.message mensaje a mostrar
			 * @param {function} params.onAccept función callback a ejectuar al presionar 'aceptar'
			 * @param {string} params.template plantilla a utilizar.
			 * @param {function} params.promise funcion callback que informa la promesa a resolver antes de aceptar (opcional)
			 */
			alert: function(params, cb, template){
				if (params == null) params = 'Mensaje no definido';
				if (typeof params == 'string') {
					params = { message: normalizeEntities(params) };
					if (typeof cb == 'function') params.onAccept = cb;
				}
				modal.open({
					template: params.template || template || require('./alert-template.tmpl'),
					windowClass: 'modal-alert',
					size: 'md',
	                controller: ['$scope', '$uibModalInstance', function($scope, $uibModalInstance){
	                	$scope.mensaje = normalizeEntities(params.message);
	                	$scope.volver = function(){
							if (typeof params.promise == 'function') {
								params.promise($scope).then(function(resp){
									$uibModalInstance.close(resp);
								});
							} else {
								$uibModalInstance.close($scope);
							}
	                	}
	                }]
				}).result.then(function(res){
					if (typeof params.onAccept == 'function') params.onAccept(res);
				});
			},

			/**
			 * @description Permite mostrar un mensaje para confirmar
			 * @param {object} params objeto con los parámetros:
			 * @param {string} params.message mensaje a mostrar,
			 * @param {function} params.onAccept función callback a ejectuar al presionar 'aceptar'
			 * @param {function} params.onCancel función callback a ejecutar al presionar 'cancelar',
			 * @param {string} params.template plantilla a utilizar.
			 * @param {function} params.promise funcion callback que informa la promesa a resolver antes de aceptar (opcional)
			 */
			confirm: function(params, cbaccept, cbcancel, template){
				if (params == null) params = 'Mensaje no definido';
				if (typeof params == 'string') {
					params = { message: normalizeEntities(params) };
					if (typeof cbaccept == 'function') params.onAccept = cbaccept;
					if (typeof cbcancel == 'function') params.onCancel = cbcancel;
				}
				modal.open({
					template: params.template || template || require('./confirm-template.tmpl'),
					windowClass: 'modal-alert',
					size: 'md',
	                controller: ['$scope', '$uibModalInstance', function($scope, $uibModalInstance){
	                	$scope.mensaje = normalizeEntities(params.message);
	                	$scope.aceptar = function(){
							if (typeof params.promise == 'function') {
								params.promise($scope).then(function(resp){
									$uibModalInstance.close(resp);
								});
							} else {
								$uibModalInstance.close($scope);
							}
	                	};
	                	$scope.cancelar = function(){
	                		$uibModalInstance.dismiss($scope);
	                	};
	                }]
				}).result.then(function(res){
					if (typeof params.onAccept == 'function') params.onAccept(res);
				}, function(res){
					if (typeof params.onCancel == 'function') params.onCancel(res);
				});
			},


			/**
			 * @description Permite levantar modal
			 * @param {Object} config    objeto literal con los parámetros:
			 * 
			 * @param {String} config.view string que reprenta la ruta de la vista (relativa a src)
			 * @param {String} config.controller array con inyección de dependecias y función del controller  
			 * @param {Function} config.onClose callback de funcion close de modalInstance	
			 * @param {Function} config.onDismiss callback de funcion dismiss de modalInstance	
			 * @param {Object} config.params mapa de parámetros para el controller. Son obtenidos en el controller de la modal inyectando '$uibModalParams'
			 * @param {String} config.path path que sirve de referencia en el log de auditoria.
			 * 			   	 	
			 * @example
			 *	{
			 *		view: 'modules/.../vista.html',
			 *		controller: 'controller',
			 *      name: 'editar contacto'	//se transforma en /modal-editar-contacto en log de auditoria
			 *		params: {
			 *			p1: 1
			 *			p2: 'string',
			 *			p3: {},
			 *			p4: function() {...}
			 *		},
			 *		onClose: function(res){
			 *			console.log(res)
			 *		},
			 *		onDismiss: function(res){
			 *			console.log(res)
			 *		}
			 *	} 
			 * 
			 * @return {object}           promise
			 */
			modal: function(config){
				if (config.name) auditoria.setPath('modal-' + config.name || '');
				var instance = modal.open({
					template: config.view,
					windowClass: config.size || 'large',
	                controller: config.controller,
	                resolve: {
	                    '$uibModalParams': function () {
	                        return config.params;
						}
	                }
				});
				instance.result.then(function(params){
					if (typeof config.onClose == 'function') config.onClose(params);
				}, function(params){
					if (typeof config.onDismiss == 'function') config.onDismiss(params);
				}).finally(function(){
					auditoria.clearPath();
				});
				return instance.result;
			},

			/**
			 * @description Extensión de función alert para mensajes de errores
			 * @param {String} msj mensaje de alerta
			 * @param {Function} cb callback para el botón aceptar
			 */
			errorAlert: function(msj, cb){
				this.alert(msj, cb, require('./error-alert-template.tmpl'));
			},

			/**
			 * @description Extensión de función alert para mensajes de warning
			 * @param {String} msj mensaje de alerta
			 * @param {Function} cb callback para el botón aceptar
			 */
			warnAlert: function(msj, cb){
				this.alert(msj, cb, require('./warn-alert-template.tmpl'));
			}
		}

	}

];