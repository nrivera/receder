/**
 * @author Sinecio Bermúdez Jacque
 * @description fachada para manipular el servicio $modal y hacerlo
 * compatible con el desarrollo con las modales antiguas basadas en bootstrap
 * y agregar log de auditoría.
 */
module.exports = [

    '$modal', '$auditoria','$parentMessage',

    function(modal, auditoria , parentMessage){

        return {
            alert           : alert,
            confirm         : confirm,
            warnAlert       : warn,
            errorAlert      : error,
            modal           : openModal,
            checkboxWarn    : checkboxWarn,
            checkBoxReq     : checkBoxReq
        };

        function openModal(config){
            if (config.name) auditoria.setPath('modal-' + config.name || '');
            return modal.openModal(config).finally(function(){
                auditoria.clearPath();
            });
        }

        function alert(mensaje, aceptar) {
            return modal.alert(mensaje).then(function(){
                if (typeof aceptar == 'function') return aceptar();
            });
        }

        function confirm(mensaje, aceptar, cancelar){
            return modal.confirm(mensaje).then(function(){
                if (typeof aceptar == 'function') return aceptar();
            }).catch(function(){
                if (typeof cancelar == 'function') return cancelar();
            });
        }

        function warn(mensaje, aceptar){
            return modal.alert(mensaje, 'warning').then(function(){
                if (typeof aceptar == 'function') return aceptar();
            }).catch(function(){});
        }

        function error(mensaje, aceptar){
            return modal.alert(mensaje, 'error').then(function(){
                if (typeof aceptar == 'function') return aceptar();
            }).catch(function(){});
        }

        function checkboxWarn(mensaje, aceptar){
            return modal.alert(mensaje, 'checkboxWarning').then(function(resp){
                if (typeof aceptar == 'function') return aceptar(resp);
            }).catch(function(){});
        }

        function checkBoxReq(mensaje, aceptar){
            return modal.alert(mensaje, 'checkboxWarning').then(function(resp){
            var uri = mensaje.uri_completar_informacion;
            if(mensaje.RequiereIniciarSesion){
                uri = mensaje.uri_login;
            }
            postMessage(uri);
            }).catch(function(){});
        }

        function postMessage(uri){
            parentMessage.redireccionaPadre(uri)
        } 


    }

];