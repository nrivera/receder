'user strict';

/**
 * @author Sinecio Rodrigo Bermúdez Jacque
 * @description agrupación de todos los servicios del modulo 'app.commons'
 */
angular.module('app.commons')

.factory('$popup', require('./popup/popup'))