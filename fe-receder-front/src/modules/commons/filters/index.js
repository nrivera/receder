'user strict';

/**
 * @author Nicolas Andres Rivera Acevedo
 * @description indice de todos los filtros del modulo 'app.commons'
 */
angular.module('app.commons')
.filter('formatMiles', require('./format-miles'))
.filter('kb', require('./kb'))
.filter('rut', require('./rut'))
.filter('porcentaje', require('./porcentaje'))
