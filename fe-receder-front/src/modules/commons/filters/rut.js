'use strict';
/**
 * @author Mauricio Ross Arevalo
 * @description filtro que recibe un input y aplica formato de rut
 * @example input=123456789  return 12.345.678-9
 */
module.exports = function(){
    return function(input){
        if(input==null) return input;
        input = input.toString().replace(/[\.-]/g,'');      
        return input.slice(0,-1).replace(/\B(?=(\d{3})+(?!\d))/g, '.')+'-'+input[input.length-1];
    }
};