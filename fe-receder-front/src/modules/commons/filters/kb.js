'use strict';
/**
 * @author Nicolas Andres Rivera Acevedo
 * @description transforma un input bytes en kb
 * @example 
 *      {{input | kb}}
 */
module.exports = function(){
    return function(input){
        if (input == null) return input;
        input = Number(input) || 0;
        return Math.ceil(input / 1024) + ' kb';
    }
};