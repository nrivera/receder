'use strict';
/**
 * @author Nicolas Andres Rivera Acevedo
 * @description transforma un input decimal a porcentaje
 * @example 
 *      {{input | porcentaje}}
 */
module.exports = function(){
    return function(input){
        if (input == null) return input;
        input = Number(input) || 0;
        return (input * 100).toFixed(4) + '%';
    }
};