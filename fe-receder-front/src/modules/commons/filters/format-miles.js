'use strict';
/**
 * @author Mauricio Ross Arevalo
 * @description filtro que recibe un input y aplica formato de miles
 * @example input=100200300  return 100.200.300
 */
module.exports = function(){
    return function(input){
        if (input == null) return input;
        return input.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
    }
};