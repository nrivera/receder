'use strict';
import {configurar} from './validaciones';

/**
 * @author Nicolas Andres Rivera Acevedo
 * @description configuración del submodulo 'app.receder'
 */
module.exports = [

	'$stateProvider','$uxValidarProvider',

	function(stateProvider , uxValidarProvider){
		
			/**
		 * configurar las validaciones de la directiva ux-validar
		 */
			 configurar(uxValidarProvider); 
		/**
		 * configuración de states
		 */
		stateProvider
		.state('receder', require('./states/receder'))
		.state('error', require('./states/errorPage'))

	}

];