/**
 * @author Nicolas Andres Rivera Acevedo
 * @description configuración de vistas y controllers para
 *              el state `ErroPage`
 */
module.exports = {
	url: '/error',
	views: {
		'body@': {
			template: require('receder/views/error.html'),
		}
	}
}