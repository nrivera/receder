/**
 * @author Nicolas Andres Rivera Acevedo
 * @description configuración de vistas y controllers para
 *              el state 'receder' , se agrega el "RESOLVE" dentro
 * 				de este state , ya que permitira iniciar el postMessage antes que los controladores .
 */
module.exports = {
	url: '/receder',
	resolve: require('receder/resolves'),
	views: {
		'body@': {
			template: require('receder/views/bandeja-entrada.html'),
			controller: 'bandeja-entrada-controller'
		}
	}
}