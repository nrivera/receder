/**
 * @author Nicolas Rivera Acevedo
 * @description configuración de vistas y controllers para
 *              el state `bancos`
 */
 module.exports = {
	resolve: require('receder/resolves'),
	abstract: true,
	views: {
	}
}