'use strict';

import {
    VALIDAR_CAMPOS_REQUERIDOS,
    EMAIL_INVALIDO,
    RUT_INVALIDO,
    TELEFONO_INVALIDO,
} from 'commons/constants/messages';

/**
 * @author Sinecio Bermúdez Jacque
 * @description Script encargado de configurar las validaciones de la directiva ux-validar.
 */
module.exports = {
    configurar  : configurar
};

/**
 * @author Sinecio Bermúdez Jacque
 * @description Función encargada de configurar la directiva de validación.
 * @param {Object} uxValidarProvider provider de uxValidar
 */
function configurar(uxValidarProvider) {

    uxValidarProvider
    .mensajeDefecto(VALIDAR_CAMPOS_REQUERIDOS)
    .validacion('email', function(value){
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(value.toLowerCase()) ? true : EMAIL_INVALIDO;
    })/* 
    .validacion('telefono' , function(value){
        var re = /^(+?56)?(\s?)(0?9)(\s?)[987654]\d{7}$/;
        return re.test(value) ? true : FONO_INVALIDO;
    }) */
    .validacion('rut', function(value){
        value = value.toString().toLowerCase().replace(/[\.-]/g,"");

        var rut = value.substr(0, value.length-1);
        var factor = 2;
        var suma = 0;
        var dv;

        for(var i=(rut.length-1); i>=0; i--){
            factor = factor > 7 ? 2 : factor;
            suma += parseInt(rut[i])*(factor++);
        }

        dv = 11 -(suma % 11);
        if(dv == 11) dv = 0;
        else if (dv == 10) dv = "k";

        return dv == value.charAt(value.length-1) ? true : RUT_INVALIDO;
    });

};