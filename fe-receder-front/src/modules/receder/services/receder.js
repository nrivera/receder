'use strict';

/**
 * @author Nicolas Andres Rivera Acevedo
 * @description factory de control de servicios a utilizar en redecer
 */
module.exports = [

    '$negocioFacturas', '$tipo', '$negocioRecede',

    function(negocioFacturas, tipo, negocioRecede){

        return {
            obtenerFacturas         : negocioFacturas.get,
            obtenerTipoRecesion     : obtenerTipoRecesion,
            recederDocumento        : negocioRecede.post
        };

        function obtenerTipoRecesion(){
            return tipo.get({
                Tipo: 'Recesion'
            });
        }

    }

];