'use strict';

import {parsePath} from 'commons/util';
import {obtenerServicioGet} from 'commons/util';
/**
 * @author Nicolas Andres Rivera Acevedo
 * @description factory para peticiones bajo el recurso /fe/negocio/facturas
 */
module.exports = [

    '$http', '$tipoWizard',
    function(http, $tipoWizard){

            
        function get(params){
            
            return http.get('/fe/negocio/facturas',{
                params: params
            }).then(function(resp){
                return resp;
            }).catch(function (resp) {
                return resp;
            });
        }

        function post(params){
            return http.post(parsePath('/{prefix}/negocio/facturas', {
                prefix: $tipoWizard.obtenerPrefijo()
            }), params).then(function(resp){
                return resp;
            }).catch(function (resp) {
                return resp;
            });
        }

        function adjuntar(params){
            return http.post(parsePath('/{prefix}/negocio/facturas/adjuntas', {
                prefix: $tipoWizard.obtenerPrefijo()
            }), params).then(function(resp){
                return resp;
            }).catch(function (resp) {
                return resp;
            });
        }

        function listarRequiereReparar(params){
            return http.get(parsePath('/{prefix}/negocio/facturas/requiere-reparar', {
                prefix: $tipoWizard.obtenerPrefijo()
            }), {
                params: params
            }).then(function(resp){
                return resp;
            }).catch(function (resp) {
                return resp;
            });
        }

        return {
            get                     : get,
            post                    : post,
            adjuntar                : adjuntar,
            listarRequiereReparar   : listarRequiereReparar
        }
    }

];