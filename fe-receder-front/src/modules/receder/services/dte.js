'use strict';

import {obtenerServicioDelete} from 'commons/util';

/**
 * @author Nicolas Andres Rivera Acevedo
 * @description factory para peticiones bajo el recurso /fe/dte
 */
module.exports = [

    '$http',
    function(http){

        function del(endpoint){
            return obtenerServicioDelete(http, endpoint);
        }

        return {
            delete: del('/fe/dte/{id}')
        }
    }

];