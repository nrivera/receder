'use strict';

import {obtenerServicioGet, date} from 'commons/util';
import {
    SELECCIONE_FECHA_HABIL
} from 'commons/constants/messages';
 

/**
 * @author Nicolas Andres Rivera Acevedo
 * @description factory para peticiones bajo el recurso /fe/fecha
 */
module.exports = [

    '$http', '$q', '$popup',
    function(http, q, popup){

        return {
            esFechaHabil   : esFechaHabil
        }

        function get(endpoint){
            return obtenerServicioGet(http, endpoint);
        }

        function esFechaHabil(fecha){
            var defer = q.defer();
            
            if (!date(fecha)) defer.reject();
            else {
                get('/fe/fecha/habil')({
                    valor: fecha
                }).then(function(resp){
                    if (resp.status != 200) {
                        popup.errorAlert(resp.data.Message);
                        defer.reject();
                        return;
                    }
                    if (resp.data.esHabil) {
                        defer.resolve();
                    } else {
                        popup.warnAlert(SELECCIONE_FECHA_HABIL);
                        defer.reject();
                    }
                });
            }

            return defer.promise;
        }
    }

];