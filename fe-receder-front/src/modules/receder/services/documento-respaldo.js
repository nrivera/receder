'use strict';

import {
    obtenerServicioGet,
    obtenerServicioDelete
} from 'commons/util';

/**
 * @author Nicolas Andres Rivera Acevedo
 * @description factory para peticiones bajo el recurso /fe/documento/respaldo
 */
module.exports = [

    '$http',
    function(http){

        function get(endpoint){
            return obtenerServicioGet(http, endpoint);
        }

        function del(endpoint){
            return obtenerServicioDelete(http, endpoint);
        }

        return {
            get     : get('/fe/documento/respaldo'),
            delete  : del('/fe/documento/respaldo/{IDDocumento}'),
        }
    }

];