'use strict';

/**
 * @author Nicolas Andres Rivera Acevedo
 * @description factory para peticiones bajo el recurso `fa/auth`
 */
module.exports = [

    '$http', '$authStorage', '$q',

    function(http, authStorage, q){

        return {
            autenticar: autenticar,
            validarOrigen: validarOrigen,
            obtenerAutorizacion: obtenerAutorizacion
        }

        function autenticar(params){
            var defer = q.defer();
            authStorage.set(params);
            defer.resolve();
            return defer.promise;
        }

        function validarOrigen(origen){
            return http.post('fe/auth/origin', {
                Origin: origen
            });
        }

       
        /**
		 * @description Función encargada de informar el header de autorización
		 */
		function obtenerAutorizacion(){
			var header = authStorage.getHeaderToken();
            header[TOKEN_HEADER] = `${TOKEN_TYPE} ${auth.getToken()}`;
			/**
			 * se agrega auditoria y token de visita en este nivel, para que todas las
			 * subidas de archivos contengan esta información.
			 * Se hace aquí puesto que en todos los controller que manejan
			 * subida de archivos implementan esta función `obtenerAutorizacion`
			 * para agregar información en el header del request. Como la información
			 * de auditoría y token de visita también es un atributo del header, la agregamos aquí
			 * y así no tenemos que replicarla por todos los controllers.
			 */
/*             angular.extend(header, auditoria.get());
            angular.extend(header, authStorage.getHeaderTokenVisita()) */
			return header;
		}

    }

];