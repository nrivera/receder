'use strict';

import {
    parsePath
} from 'commons/util';

/**
 * @author Nicolas Andres Rivera Acevedo
 * @description factory para peticiones bajo el recurso /fe/negocio/acepta-cotizacion
 */
module.exports = [

    '$http', '$tipoWizard',
    function(http, $tipoWizard){

        function post(params){
            return http.post(parsePath('/{prefix}/negocio/acepta-cotizacion', {
                prefix: $tipoWizard.obtenerPrefijo()
            }), params).then(function(resp){
                return resp;
            }).catch(function (resp) {
                return resp;
            });
        }

        return {
            post: post
        }
    }

];