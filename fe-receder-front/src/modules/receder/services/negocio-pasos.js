'use strict';

import {
    parsePath,
} from 'commons/util';

/**
 * @author Nicolas Andres Rivera Acevedo
 * @description factory para peticiones bajo el recurso /fe/negocio/pasos
 */
module.exports = [

    '$http', '$tipoWizard',
    function(http, $tipoWizard){

        function get(params){
            return http.get(parsePath('/{prefix}/negocio/pasos', {
                prefix: $tipoWizard.obtenerPrefijo()
            }), {
                params: params
            }).then(function(resp){
                return resp;
            }).catch(function (resp) {
                return resp;
            });
        }

        function post(params){
            return http.post(parsePath('/{prefix}/negocio/pasos', {
                prefix: $tipoWizard.obtenerPrefijo()
            }), params).then(function(resp){
                return resp;
            }).catch(function (resp) {
                return resp;
            });
        }

        return {
            get: get,
            post: post
        }
    }

];