'use strict';

import {parsePath} from 'commons/util';

/**
 * @author Nicolas Andres Rivera Acevedo
 * @description factory para peticiones bajo el recurso /fe/ws
 */
module.exports = [

    '$http',
    function(http){

        function get(params){
            return http.get(parsePath('/fe/ws/negocio/{idNegocio}', params), {
                params: params
            }).then(function(resp){
                return resp;
            }).catch(function (resp) {
                return resp;
            });
        }

        return {
            obtenerEventosNegocio: get
        }
    }

];