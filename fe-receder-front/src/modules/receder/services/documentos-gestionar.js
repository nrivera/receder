'use strict';

/**
 * @author Nicolas Andres Rivera Acevedo
 * @description factory de control de servicios a utilizar en 
 * 'documentos > Repositorio' y 'documentos > receder'
 */
module.exports = [

    '$documentoCertificado', '$dte',

    function(documentoCertificado, dte){

        return {
            obtenerCertificados         : documentoCertificado.get,
            eliminarCertificado         : documentoCertificado.delete,
            obtenerUltimoCertificado    : documentoCertificado.last,
            eliminarDTE                 : dte.delete
        }

    }

];