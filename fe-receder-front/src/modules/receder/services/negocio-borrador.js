'use strict';

import {obtenerServicioGet, obtenerServicioPost} from 'commons/util';

/**
 * @author Mauricio Ross Arevalo
 * @description factory para peticiones bajo el recurso /fe/borrador/borradores
 */
module.exports = [

    '$http',
    function(http){

        function get(endpoint){
            return obtenerServicioGet(http, endpoint);
        }

        function post(endpoint){
            return obtenerServicioPost(http, endpoint);
        }

        return {
            get                     : get('/fe/borrador/borradores')
        }
    }

];