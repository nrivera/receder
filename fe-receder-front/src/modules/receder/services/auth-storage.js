'use strict';

/**
 * @author Nicolas Andres Rivera Acevedo
 * @description factory para almacen de autorización
 */
module.exports = [
    '$location',
    function($location){

        var _params = {};
        var _tokenAnonimo = null;

        return {
            set                     : set,
            setTokenAnonimo         : setTokenAnonimo,
            getDomain               : getDomain,
            getRutContexto          : getRutContexto,
            getToken                : getToken,
            getHeaderToken          : getHeaderToken,
            getHeaderTokenVisita    : getHeaderTokenVisita,
            getParams               : getParams,
            authConfig              : authConfig

        };

        function set(params){
            angular.extend(_params, params);
        }

        function setTokenAnonimo(token){
            _tokenAnonimo = token;
        }

        function getParams(){
            return _params;
        }

        function getToken(){
            return _params.Token;
        }
        function getDomain(){
            return _params.Origin;
        }

        function getHeaderToken(){
            if (!_params.Token) return {};
            return {
                Authorization: `Bearer ${_params.Token}`,
                Dom: _params.Origin,
            };
        }

        function getHeaderTokenVisita(){
            if (!_tokenAnonimo) return {};
            return {
                TokenVisita: _tokenAnonimo
            }
        }

        function authConfig(config){
            angular.extend(config.headers, getHeaderToken());
            angular.extend(config.params, {
                Dom: _params.Origin
            });
            if (_tokenAnonimo != null) {
                angular.extend(config.params, getHeaderTokenVisita());
            }
            
            return config;
        }
        
        function getRutContexto() {
            return _params.RutContexto || '';

        }
    }

];