'use strict';

/**
 * @author Nicolas Andres Rivera Acevedo
 * @description servicio inicial para la aplicación, este servicio
 * resuelve promesas de peticiones que deben estar antes de que se cargue la app.
 */
module.exports = [

    '$q', '$constantes',
    function(q, constantes){

        var defer = q.defer();

        return {
            getPromise  : getPromise,
            resolve     : resolve
        };

        /**
         * @description Informa la promesa de todas las subpromesas resueltas.
         */
        function getPromise(){
            return defer.promise;
        }

        /**
         * @description Función que se encarga de resolver todas las
         * promesas de peticiones que deben ejecutarse antes de cargar
         * la app
         */
        function resolve(){
            q.all(obtenerPromesas()).then(function(){
                defer.resolve();
            });
        }

        /**
         * @description Función encargada de informar todas las
         * promesas que deben resolverse.
         * @returns {Array}
         */
        function obtenerPromesas(){
            /**
             * Agregar a este array todas las funciones
             * que informan la promesa a resolver:
             */
            return [
                constantes.get()
            ];
        }
    }

];
