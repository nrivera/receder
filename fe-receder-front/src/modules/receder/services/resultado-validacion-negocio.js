'use strict';

import {obtenerServicioGet} from 'commons/util';

/**
 * @author Nicolas Andres Rivera Acevedo
 * @description factory para peticiones bajo el recurso /fe/negocio/resultado-validacion-negocio
 */
module.exports = [

    '$http',
    function(http){

        function get(endpoint){
            return obtenerServicioGet(http, endpoint);
        }

        return {
            get: get('/fe/negocio/resultado-validacion-negocio')
        }
    }

];