import auditoria from 'commons/constants/auditoria';

/**
 * @author Nicolas Andres Rivera Acevedo
 * @description Servicio para modificar el header y agregar información
 * con respecto al log de auditoría
 */
module.exports = [
    '$location',
    function(location){

        var subPath = null

        return {
            set         : set,
            get         : get,
            setPath     : setPath,
            clearPath   : clearPath 
        };

        function set(config){
            angular.extend(config.headers, get());
        }

        function get(){
            var header = {};
            var path = subPath ? subPath : '';
            header[auditoria.HEADER_KEY] = `${auditoria.DONDE}:${location.path()}${path}`;
            return header;
        }

        function setPath(path){
            subPath	= ('/' + path.replace(/\s+/g,'-').replace(/\//g,'')).toLowerCase();
        }

        function clearPath(){
            subPath = null;
        }

    }
]