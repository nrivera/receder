'use strict';

/**
 * @author Nicolas Andres Rivera Acevedo
 * @description factory de control de autenticacion.
 * Se hace de esta manera, porque este servicio es injectado en un interceptor
 * que podria generar inyección de dependencia circular.
 */
module.exports = [
    
    'SatellizerStorage', 'SatellizerConfig',

    function(SatellizerStorage, SatellizerConfig){
    
        var renewFn = null;
        var rejectFn = null;

        return {
            onTokenRenew    : onTokenRenew,
            onReject        : onReject,
            renew           : renew,
            reject          : reject,
            hasAuth         : hasAuth,
            setAuth         : setAuth
        }

        /**
         * @description Función encargada de establecer la función de autenticación para renovar token
         * @param {Function} fn callback a ejecutar
         */
        function onTokenRenew(fn){
            renewFn = fn;
        }

        /**
         * @description Función encargada de ejecutar la función para renovar token
         */
        function renew(){
            return renewFn();
        }

        /**
         * @description Función encargada de ejecutar la función que realiza tareas
         * una vez ha caducado la autenticación
         * @param {Function} fn callback que a ejecutar
         */
        function onReject(fn){
            rejectFn = fn;
        }

        /**
         * @description Función encargada de ejecutar la función para renovar token
         */
        function reject(){
            return rejectFn();
        }

        /**
         * @description Función encargada de determinar si un config de petición
         * tiene establecido el header de autenticación
         * @param {Object} config Objecto Configuración de request
         */
        function hasAuth(config){
            return !!config.headers[SatellizerConfig.tokenHeader];
        }

        /**
         * @description Función encargada de establecer el header de autenticación
         * @param {Object} config Objecto Configuración de request
         */
        function setAuth(config) {
            var header = {};
            var tokenName = SatellizerConfig.tokenPrefix ? [SatellizerConfig.tokenPrefix, SatellizerConfig.tokenName].join('_') : SatellizerConfig.tokenName;
            var token = SatellizerStorage.get(tokenName);
            if (!token) return;
            header[SatellizerConfig.tokenHeader] = `${SatellizerConfig.tokenType} ${token}`;
            angular.extend(config.headers, header);
        }
        
    }

];