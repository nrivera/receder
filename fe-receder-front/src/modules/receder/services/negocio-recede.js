'use strict';

import {obtenerServicioPost} from 'commons/util';

/**
 * @author Nicolas Andres Rivera Acevedo
 * @description factory para peticiones bajo el recurso /fe/negocio/recede
 */
module.exports = [

    '$http',
    function(http){

        function post(endpoint){
            return obtenerServicioPost(http, endpoint);
        }

        return {
            post                    : post('/fe/negocio/recede')
        }
    }

];