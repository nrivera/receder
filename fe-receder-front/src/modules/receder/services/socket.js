/**
 * @author Nicolas Andres Rivera Acevedo
 * @description Servicio para comunicación por websocket
 */
module.exports = [
    '$rootScope', '$constantes', '$location', '$wizardCrearNegocio', '$wsApi',
    
    function(rootScope, constantes, location, wizardCrearNegocio, wsApi){
        
        var socket = null;
        var pending = false; //flag para no realizar petición de eventos cuando el back aún no ha respondido la pedida anterior.

        const PORT       = constantes.obtener().SISTEMA.PUERTO_SOCKET;
        const HOST       = location.host();
        const PROTOCOL   = location.protocol().toLowerCase().lastIndexOf('s') > 0 ? 'wss' : 'ws';
        const SERVER     = `${PROTOCOL}://${HOST}:${PORT}`;
        const { SEGUNDOS_API_SOCKET } = constantes.obtener().SISTEMA;
        const {SOCKET}   = constantes.obtener();

        return {
            conectar    : conectarAPI,
            desconectar : desconectarAPI
        }

        /**
         * @description permite realizar la conexión por websocket con el servidor dado por SERVER.
         * @deprecated en producción se negó la posibilidad de utilizar websocket por politicas de seguridad.
         */
        function conectar(){
            var self = this;
            if (socket && (socket.readyState == WebSocket.OPEN || socket.readyState == WebSocket.CONNECTING)) return;
            socket = new WebSocket(SERVER);
            socket.onmessage = function(event){
                var resp = angular.fromJson(event.data);
                if (resp.IDNegocio != wizardCrearNegocio.getIdNegocio()) return;
                rootScope.$apply(function(){
                    rootScope.$broadcast(resp.Evento, resp);
                });
            };
            socket.onclose = function(){
                if (this._manualClose) return;
                self.conectar();
            }
        }

        /**
         * @description desconecta la sesión websocket actual.
         * @deprecated en producción se negó la posibilidad de utilizar websocket por politicas de seguridad.
         */
        function desconectar(){
            if (!socket || socket.readyState == WebSocket.CLOSED) return;
            socket._manualClose = true;
            socket.close();
        }

        /**
         * @description genera conexión mediante setInterval a una API
         * que resuelve las posibles respuestas sin utilizar websocket.
         */
        function conectarAPI(){
            if (socket) return;
            pending = false;
            socket = setInterval(function(){
                if (pending) return;
                pending = true;
                wsApi.obtenerEventosNegocio({
                    IDNegocio: wizardCrearNegocio.getIdNegocio()
                },{
                    loading: false
                }).then(function(resp){
                    // pending = false;
                    // if (!resp.data) return;
                    // if (lastUpdate != null 
                    //     && resp.data.FechaNotificacion > lastUpdate
                    //     && resp.data.EventosNotificacion.length <= 0){
                    //         refrescar();
                    // } else {
                    //     lastUpdate = resp.data.FechaNotificacion;
                    //     angular.forEach(resp.data.EventosNotificacion, function(resp){
                    //         rootScope.$broadcast(resp.Evento, resp);
                    //     });
                    // }
                    angular.forEach(resp.data.EventosNotificacion, function(resp){
                        rootScope.$broadcast(resp.Evento, resp);
                    });
                    pending = false;
                });
            }, SEGUNDOS_API_SOCKET * 1000);
        }

        /**
         * @description realiza un refresco pesimista,
         * reinicia todos los controllers del state actual
         * de negocio activo.
         */
        function refrescar(){
            wizardCrearNegocio.limpiarPasos();
            wizardCrearNegocio.obtenerPasos().then(function(){
                rootScope.$broadcast(SOCKET.NEGOCIO_CAMBIO_PASO);
            });
        }

        /**
         * @description cancela las llamadas a la api
         */
        function desconectarAPI(){
            clearInterval(socket);
            socket = null;
        }
    }
];