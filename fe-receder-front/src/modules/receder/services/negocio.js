'use strict';

import {
    obtenerServicioGet,
    obtenerServicioDelete
} from 'commons/util';

/**
 * @author Nicolas Andres Rivera Acevedo
 * @description factory para peticiones bajo el recurso /fe/negocio
 */
module.exports = [

    '$http',
    function(http){

        function get(endpoint){
            return obtenerServicioGet(http, endpoint);
        }

        function del(endpoint){
            return obtenerServicioDelete(http, endpoint);
        }

        return {
            get: get('/fe/negocio'),
            delete: del('/fe/negocio/{idNegocio}')
        }
    }

];