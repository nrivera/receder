'use strict';

import {
	obtenerServicioPost
} from 'commons/util';

import { 
	NO_AUTENTICADO,URL_LOGIN_SSO
} from 'commons/constants/url-types';

import {
	TOKEN_HEADER,
	TOKEN_TYPE
} from 'commons/constants/auth';

/**
 * @author Nicolas Andres Rivera Acevedo
 * @description factory para peticiones bajo el recurso /fe/auth
 * y lógica que tiene que ver con la autenticación de usuario.
 */
module.exports = [

	'$auth', '$http', '$url', '$auditoria', '$wizardCrearNegocio', '$tipoWizard',

	function(auth, http, url, auditoria, $wizardCrearNegocio, $tipoWizard){


		/**
		 * @description indica a satellizer que utilice sessionStorage 
		 * para almacenar el token
		 */
		auth.setStorageType('sessionStorage');
		function post(endpoint){
            return obtenerServicioPost(http, endpoint);
		}

		return {
			autenticar				: autenticar,
			redireccionar			: redireccionar,
			revoke					: revoke,
			logout					: logout,
			getToken				: getToken,
			obtenerAutorizacion		: obtenerAutorizacion,
			obtenerTokenRenovar		: obtenerTokenRenovar,
			obtenerTipoAuth			: obtenerTipoAuth,
		}


		function obtenerTipoAuth(){
			return http.get('/fe/auth/tipo');
		}

		/**
		 * @description función encargada de cerrar sesión en el back, para revocar
		 * la renovación del token de autenticación
		 */
		function revoke(params){
			return post('/fe/auth/logout')(params);
		}

		/**
		 * @description Función encargada de establecer el término de autenticación
		 * borrando el token local.
		 */
		function logout(){
			auth.logout();
		}

		/**
		 * @description Función encargada de obtener el token almacenado por satellizer
		 */
		function getToken(){
			return auth.getToken();
		}

		/**
		 * @description Función encargada de realizar la autenticación
		 * @param {Object} params objeto parámetros para autenticación
		 */
		function autenticar(params){
			params.Token = params.ticket || params.Token;
			return post('/fe/auth')(params).then(function(resp){
				sessionStorage.setItem('_feoauth', resp.data.TokenRenovar);
				auth.setToken(resp.data.Token);
				$tipoWizard.setSolocesion(resp.data.IDNegocio > 0);
				$wizardCrearNegocio.setIdNegocio(resp.data.IDNegocio || 0);
			});
		}


		/**
		 * @description Función encargada de informar el header de autorización
		 */
		function obtenerAutorizacion(){
			var header = {};
			header[TOKEN_HEADER] = `${TOKEN_TYPE} ${auth.getToken()}`;
			/**
			 * se agrega auditoria en este nivel, para que todas las
			 * subidas de archivos contengan esta información de auditoría.
			 * Se hace aquí puesto que en todos los controller que manejan
			 * subida de archivos implementan esta función `obtenerAutorizacion`
			 * para agregar información en el header del request. Como la información
			 * de auditoría también es un atributo del header, la agregamos aquí
			 * y así no tenemos que replicarla por todos los controllers.
			 */
			angular.extend(header, auditoria.get());
			return header;
		}

		/**
		 * @description Función encargada de informar el token necesario
		 * para realizar autenticación luego de la caducidad de un token.
		 */
		function obtenerTokenRenovar(){
			return sessionStorage.getItem('_feoauth');
		}

		/**
		 * @description Función encargada de redireccionar a la url 
		 * dada por la constante NO_AUTENTICADO
		 */
		function redireccionar(){
			this.obtenerTipoAuth().then(function(resp){
				var usarSSO = resp.data.UtilizaSso;
				url.get({
					tipo: NO_AUTENTICADO
				}).then(function(resp){
					var url  = usarSSO  ?  URL_LOGIN_SSO : resp.data.URL ;
					window.location.replace(url);  
				});
			});
		
		}

	}

];
