'use strict';

import {obtenerServicioGet} from 'commons/util';
/**
 * @author Nicolas Andres Rivera Acevedo
 * @description Servicio para inicializar el script de google manager .
 * Se utiliza para incrustar el iframe .
 * Se utiliza la llamada para obtener las constantes , ya que ha esta altura 
 * aun no se encuentra inyectado.
 */

module.exports = [
    '$http', '$constantes',
    function(http , constantes){
     /*    const PORT       = constantes.obtener().SISTEMA.PUERTO_SOCKET; */
        return {
            iniciaGooleManager    : initGoogleManager,
        }

        /**
         * @description inicializa la funcion de Google Manager usando el ID de las constantes
         */
        function initGoogleManager(){
            obtenerServicioGet(http, '/fe/constantes')().then(function(resp){
                var GTM_ID =decodeString(resp.data.constantes.GOOGLE_ANALITYCS.TAG_CODE);
                (function (w, d, s, l, i) {
                    w[l] = w[l] || [];
                    w[l].push({
                        'gtm.start': new Date().getTime(),
                        event: 'gtm.js'
                    });
                    var f = d.getElementsByTagName(s)[0],
                        j = d.createElement(s),
                        dl = l != 'dataLayer' ? '&l=' + l : '';
                    j.async = true;
                    j.src =
                        'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
                    f.parentNode.insertBefore(j, f);
                })(window, document, 'script', 'dataLayer', GTM_ID);
                appendIframe(GTM_ID);
            });
            }
            

        
        /**
         * @description Decodifica un string 
         * @param {encodeString} string string a modifca
         */
            function decodeString(encodeString){
                return window.atob(encodeString);   
            }

    
        /**
         * @description Crea un iframe dentro del TAG noscript
         * 
         */
        function appendIframe(tag_id){
            var iframe_gtm  = document.createElement("iframe");
            iframe_gtm.setAttribute("src" ,"https://www.googletagmanager.com/ns.html?id=" + tag_id);
            iframe_gtm.setAttribute("height" ,0); 
            iframe_gtm.setAttribute("width" ,0); 
            iframe_gtm.setAttribute("style" ,"display:none;visibility:hidden"); 
            document.getElementById("googleManagerReceder").appendChild(iframe_gtm);
        }

    }
];