'use strict';

import {
    parsePath,
    obtenerServicioGet,
    obtenerServicioDelete
} from 'commons/util';

/**
 * @author Nicolas Andres Rivera Acevedo
 * @description factory para peticiones bajo el recurso /fe/documento/certificado
 */
module.exports = [

    '$http', '$tipoWizard',
    function(http, $tipoWizard){

        function get(endpoint){
            return obtenerServicioGet(http, endpoint);
        }

        function del(endpoint){
            return obtenerServicioDelete(http, endpoint);
        }

        function validarPassword(params){
            return http.post(parsePath('/{prefix}/negocio/certificado/valida-password', {
                prefix: $tipoWizard.obtenerPrefijo()
            }), params).then(function(resp){
                return resp;
            }).catch(function (resp) {
                return resp;
            });
        }

        return {
            get     : get('/fe/documento/certificado'),
            delete  : del('/fe/documento/certificado'),
            last    : get('/fe/documento/certificado/last'),
            validarPassword: validarPassword,
            porExpirar: get('/fe/documento/certificado/por-expirar')
        }
    }

];