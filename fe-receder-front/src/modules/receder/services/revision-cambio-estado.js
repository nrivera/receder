'use strict';

import {parsePath} from 'commons/util';

/**
 * @author Nicolas Andres Rivera Acevedo
 * @description factory para peticiones bajo el recurso /fe/negocio/revision-cambio-estado
 */
module.exports = [

    '$http', '$tipoWizard',
    function(http, $tipoWizard){

        function get(params){
            return http.get(parsePath('/{prefix}/negocio/revision-cambio-estado', {
                prefix: $tipoWizard.obtenerPrefijo()
            }), {
                params: params
            }).then(function(resp){
                return resp;
            }).catch(function (resp) {
                return resp;
            });
        }

        return {
            get: get
        }
    }

];