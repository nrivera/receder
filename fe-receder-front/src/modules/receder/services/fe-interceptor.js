'use strict';

/**
 * @author Nicolas Andres Rivera Acevedo
 * @description Factory para determinar la autenticación y cargar información de auditoría.
 */
module.exports = [

	'$q', '$feAuthControl', '$injector', '$auditoria',

	function (q, feAuthControl, injector, auditoria) {

	    var reject = null;

		return {
			request: request,
			responseError: responseError
		}

		/**
         * @description Función encargada de revisar el request antes de realizar
		 * la petición al back.
         * @param {Object} resp 
         */
		function request(config){

            if (!feAuthControl.hasAuth(config)) feAuthControl.setAuth(config);

            /**
             * agregamos información de auditoria en el header del request
             */
            auditoria.set(config);
            
			return config || q.when(config);
		}

		/**
         * @description Función encargada de revisar los errores
         * @param {Object} resp 
         */
        function responseError(resp) {
            if (resp.status == 403){
                return feAuthControl.renew().then(function(){
                    angular.extend(resp.config, {
                        loading: false
                    });
                    return injector.get('$http')(resp.config);
                });
            } else if (reject || resp.status == 401){
                if (!reject) reject = q.defer();
                feAuthControl.reject();
                return reject.promise;
            }

            return q.reject(resp);
        }
	}

];
