/**
 * @author Nicolas Andres Rivera Acevedo
 * @description factory para peticiones para el recurso cert/constantes
 */
module.exports = [

    '$http',

    function(http){

        var _constantes = {};

        return {
            get    : get,
            obtener: obtener
        }

        function get(){
            return http.get('fe/constantes').then(function(resp){
                _constantes = resp.data.constantes;
                return resp;
            });
        }

        function obtener(){
            return _constantes;
        }

    }

];