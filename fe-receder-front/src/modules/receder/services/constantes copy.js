'use strict';

import {obtenerServicioGet} from 'commons/util';

/**
 * @author Nicolas Andres Rivera Acevedo
 * @description factory para peticiones bajo el recurso /fe/constantes
 */
module.exports = [

    '$http', 
    function(http){

        var constantes = null;

        return {
            get       : get,
            obtener   : obtener
        }

        function get(){
            return obtenerServicioGet(http, '/fe/constantes')().then(function(resp){
                constantes = resp.data.constantes;
                return resp;
            });
        }

        function obtener(){
            return constantes;
        }
    }

];