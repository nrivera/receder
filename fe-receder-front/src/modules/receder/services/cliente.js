'use strict';

import {obtenerServicioGet} from 'commons/util';

/**
 * @author Nicolas Andres Rivera Acevedo
 * @description factory para peticiones bajo el recurso /fe/cliente
 */
module.exports = [

    '$http',
    function(http){

        var cliente = null;

        return {
            get: get,
            obtener: obtener,
            obtenerPorRut: obtenerPorRut
        };

        /**
         * @description Función encargada de retornar la promesa de los datos del cliente.
         * @param {Object} params parámetros para usar como queryString
         */
        function get(params){
            return obtenerServicioGet(http, '/fe/cliente')(params).then(function(resp){
                cliente = resp.data;
                delete cliente.MontoTotalAprobado;
                delete cliente.MontoTotalAprobado;
                delete cliente.MontoTotalUtilizado;
                return resp;
            });
        }

        /**
         * @description Función encargada de informar los datos almacenados en el servicio.
         */
        function obtener(){
            return cliente;
        }

        /**
         * @description Función encargada de obtener los datos del cliente dado un rut
         * @param {String} rut rut del cliente
         */
        function obtenerPorRut(rut){
            return obtenerServicioGet(http, '/fe/cliente/{rut}')({
                rut: rut
            });
        }
    }

];