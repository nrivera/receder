'use strict';

import {
    NEGOCIO,
    SOLO_CESION
} from 'commons/constants/wizard';

/**
 * @author Nicolas Andres Rivera Acevedo
 * @description factory determinar el tipo de negocio en curso
 */
module.exports = [

    function(){

        var _esSolocesion = false;

        return {
            setSolocesion: function(bool){
                _esSolocesion = bool;
            },
            esSolocesion: function(){
                return _esSolocesion;
            },
            obtenerPrefijo: function(){
                return _esSolocesion ? SOLO_CESION : NEGOCIO;
            }
        }

    }

];