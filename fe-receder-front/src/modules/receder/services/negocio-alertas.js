'use strict';

import {
    parsePath
} from 'commons/util';

/**
 * @author Nicolas Andres Rivera Acevedo
 * @description factory para peticiones bajo el recurso /faa/negocio
 */
module.exports = [

    '$http', '$tipoWizard',
    function(http, $tipoWizard){

        function del(params){
            var p = {
                prefix: $tipoWizard.obtenerPrefijo()
            };
            angular.extend(p, params);
            return http.delete(parsePath('/{prefix}/negocio/{idNegocio}/alertas/{idAlerta}', p), {
                params: params
            }).then(function(resp){
                return resp;
            }).catch(function (resp) {
                return resp;
            });
        }

        return {
            delete: del
        }
    }

];