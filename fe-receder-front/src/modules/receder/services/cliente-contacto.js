'use strict';

import {
    obtenerServicioGet,
    obtenerServicioPost
} from 'commons/util';

/**
 * @author Nicolas Andres Rivera Acevedo
 * @description factory para peticiones bajo el recurso /fe/cliente/contacto
 */
module.exports = [

    '$http',
    function(http){

        function get(endpoint){
            return obtenerServicioGet(http, endpoint);
        }

        function post(endpoint){
            return obtenerServicioPost(http, endpoint);
        }

        return {
            get     : get('/fe/cliente/contacto'),
            post    : post('/fe/cliente/contacto')
        }
    }

];