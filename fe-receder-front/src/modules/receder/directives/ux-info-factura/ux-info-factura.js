'use strict';

/**
 * @author Mauricio Ross
 * @description Directiva para mostrar la información adicional de la factura
 * @example 
 *  <ux-info-factura class="" info="item.InfoAdicionalFactura"></ux-info-factura>
 */
module.exports = [
    '$interpolate', '$compile',
    function(interpolate, compile){
        var popover = require('scripts/popover');
        var $ = require('jquery');
        return {
            restrict: 'E',
            scope: {
                info: '='
            },
            link: function(scope, elem, attr){
                elem.on('click', function (e) { 
                    if (!$(e.target).is(popover.selected)) { 
                        $(popover.selected).popover('hide'); 
                    } 
                    e.stopPropagation(); 
                }); 
                elem.popover({
                    container: 'body',
                    html: true,
                    content: compile(interpolate(require('./content.tmpl'))(scope))(scope),
                    template: require('./template.tmpl')
                });
            }
        }
    }
]