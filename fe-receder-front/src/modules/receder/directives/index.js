'use strict';

/**
 * @author Nicolas Andres Rivera Acevedo
 * @description indice de todas las directivas del modulo 'app.fe.negocio'
 */
 angular.module('app.receder')
.directive('uxInfoCesion', require('./ux-info-cesion/ux-info-cesion'))
.directive('uxInfoFactura', require('./ux-info-factura/ux-info-factura'))