'use strict';

/**
 * @author Nicolas Andres Rivera Acevedo
 * @description resolve del servicio inicial de la aplicación
 */
module.exports = [
    '$init', function(init){
        return init.getPromise();
    }
];