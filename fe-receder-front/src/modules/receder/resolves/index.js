'use strict';

/**
 * @author Nicolas Andres Rivera Acevedo
 * @description "resolve" de datos antes de instanciar controladores
 */
module.exports = {
    init: require('./init')
};