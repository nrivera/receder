'use strict';




/**
 * @author Nicolas Andres Rivera Acevedo
 * @description controller inicial para determinar autenticación y carga de datos
 * necesarios antes de iniciar la aplicación.
 */
module.exports = [

    '$init', '$auth', '$rootScope','$googleTagManager', '$parentMessage', '$location',

    function (init, auth, rootScope , googleTagManager ,parentMessage , $location) {


        parent.postMessage({
            Event: 'loadStatus',
            Data: {
                applicationName: "FER",
                finishLoading: true
            }
        }, '*');



        parentMessage.onMessage(function(event){
           console.log('PUNTO DE PARTIDA A')
           console.log('event' , event)

            var origen = event.data.Origin  || event.origin;
            if (event.data.Event) {
                if (event.data.Event == 'renew-token'){
                    parentMessage.renovarToken(event.data.Data.Token);
                }
                return;
            } 

                run({
                    Token: event.data.Token,
                    RutContexto: event.data.RutContexto,
                    Origin: origen
                });
        });
        
        /**
         * @author Nicolas Rivera Avevedo
         * @description Inicializa el metodo de Google Tag Manager ademas de incrustar el 
         * noIframe a la pagina principal
         */
        googleTagManager.iniciaGooleManager();

        
        /**
         * @description Función inicial
         * Una vez realizada la autenticación, hace lo necesario para iniciar la aplicación
         * @param {String} params parámetros de inicialización
         */
        function run(params) {
            autenticar(params).then(function(){
                init.resolve();
            });
        }

        /**
         * @description Función encargada de autenticar
         * @param {String} params parámetros de autenticación
         */
        function autenticar(params) {

          
            return auth.autenticar(params).catch(noAutenticado);
        }

        /**
         * @description Función que realiza acciones luego de una autenticación fallida.
         */
        function noAutenticado(){
            console.warn('No autenticado...');
        }
        
    }

];
