'user strict';

/**
 * @author Nicolas Andres Rivera Acevedo
 * @description Definición del submodulo app.receder
 */
var moduleName = 'app.receder';
module.exports = moduleName;

angular.module(moduleName, [
	/**
	 * dependencias:
	 */
])
.config(require('./config'))
.run(require('./run'))

/**
 * inclusión de componentes
 */
require('./services');
require('./controllers');
require('./directives');