'use strict';

import {
    NEGOCIO_FILTRAR,
} from 'commons/constants/event-names';

import {
    APPLICATION_XML,
    TEXT_XML
} from 'commons/constants/content-types';

import { filter, map, remove } from 'lodash';
import { validarContentType, parseString } from 'commons/util';

import {
    DOS_O_MAS_ARCHIVOS_ERRONEOS,
    UN_ARCHIVO_ERRONEO,
    ELIMINAR_DOCUMENTO
} from 'commons/constants/messages';

import {
    PRELOADER_VISIBLE,
    PRELOADER_HIDDEN
} from 'commons/constants/event-names';
 
/**
 * @author Nicolas Andres Rivera Acevedo
 * @description controller de vista documentos > repositorio
 */
module.exports = [

    '$scope', '$negocioCrear', '$popup', '$timeout', 'Analytics', '$feAuth', 
    '$constantes', '$documentosGestionar', '$feAuthControl',

    function (scope, negocioCrear, popup, timeout, Analytics, feAuth, 
        constantes, documentosGestionar, feAuthControl) {

        const {ORIGEN_UPLOAD} = constantes.obtener();

        /**
         * Boolean para no levantar alertas más de una vez
         */
        var alertaEsMostrada;

        /**
         * Boolean para no mostrar alerta de error cuando ocurren
         * respuestas con status 403 para renovación de autenticación
         * y reintentar la subida de archivos
         */
        var isUploadComplete = false;
        
        /**
         * modelo para colección de archivos de ngflow
         */
        scope.$flow = {};

        scope.flowFileAdded                     = flowFileAdded;
        scope.flowFileError                     = flowFileError;
        scope.flowFileSuccess                   = flowFileSuccess;
        scope.flowFilesSubmitted                = flowFilesSubmitted;
        scope.flowComplete                      = flowComplete;

        scope.obtenerDocumentos                 = obtenerDocumentos;
        scope.cerrarCargarDocumentos            = cerrarCargarDocumentos;
        scope.eliminarTodos                     = eliminarTodos;
        scope.reintentarTodos                   = reintentarTodos;
        scope.confirmarEliminar                 = confirmarEliminar;
        scope.reemplazarDocumento               = reemplazarDocumento;


        /**
         * @description Objeto para parámetros de filtrado
         */
        scope.filtros = {};

        /**
         * @description Objeto paginación para la grilla de datos de facturas
         */
        scope.Paginacion = {
            Pagina          : 1,
            CampoOrden      : '',
            Descendente     : false
        };

        /**
         * @description Objeto modelo para mantener
         * valores de la vista
         */
        scope.modelo = {
        };

        /**
         * @description Event listener para cuando se produce la acción del filtrar.
         */
        scope.$on(NEGOCIO_FILTRAR, function (event, params) {
            angular.extend(scope.filtros, params.Filtros);

            /**
             * quitar del modelo de parámetros de filtrado los queryString informados
             * en la colección parms.Borrar
             */
            angular.forEach(params.Borrar, function (query) {
                delete scope.filtros[query];
            });

            obtenerDocumentos(true);
            Analytics.trackEvent('filtrar', 'click', 1, true);
        });

        /**
         * @description función inicial, la ejecución de las funciones deben
         * ser posterior a la verificación del paso actual.
         */
        function init() {
            establecerValoresDefecto();
            obtenerDocumentos();
        }

        /**
         * @description Función encargada de establecer valores iniciales
         * como variables u objetos que deben estar definidos antes de
         * cualquier ejecución
         */
        function establecerValoresDefecto() {
            alertaEsMostrada                        = false;
            scope.hayDatosDocumentos                = true;
            scope.cargarDocumentos                  = false;
        }

        /**
         * @description Evento que se gatilla cada vez que se agregan archivos para subir
         * @param {FlowFile} file archivo agregado, instancia de FlowFile
         * @param {Event} event objeto event
         * @param {Flow} flow objeto flow (scope.$flow)
         */
        function flowFileAdded(file, event, flow) {
            file.isValid = validarContentType(file.file, APPLICATION_XML, TEXT_XML);
        }

        /**
         * @description Evento que se gatilla cada vez que hay error al subir un archivo
         * @param {FlowFile} file archivo agregado, instancia de FlowFile
         * @param {Object} resp objeto con información de error reportado por backend 
         * @param {Flow} flow flow objeto flow (scope.$flow)
         */
        function flowFileError(file, resp, flow) {
            resp = angular.fromJson(resp);

            isUploadComplete = false;

            var status = {
                403: renew,
                401: feAuthControl.reject
            };

            resp = angular.fromJson(resp);
            (status[resp.Status] || agregarErroresCargaDocumento)(file, resp);
        }

        /**
         * @description Permite renovar el token de autenticación
         * y luego reintentar la subida de archivo.
         */
        function renew(){
            feAuthControl.renew().then(reintentarTodos);
        }

        /**
         * @description Función encargada de agregar al archivo la información de errores producidos al realizar
         * el upload de un documento.
         * @param {FlowFile} file archivo agregado, instancia de FlowFile
         * @param {Object} resp respuesta desde backend
         */
        function agregarErroresCargaDocumento(file, resp){
            isUploadComplete = true;
            if (resp.Message) file.ErrorMessage = resp.Message;
            else {
                file.DetalleErrores = map(resp.Datos.Documentos, function(doc){
                    return doc;
                });
            }
        }

        /**
         * @description Evento que se gatilla cada vez que un archivo ha subido exitosamente.
         * El archivo se marca con ok = true (subido exitosamente) y luego de x segundos, este
         * archivo es removido de la colección de archivos del objeto flow. Se utiliza el hashKey
         * provisto por angular para identificar el objeto.
         * @param {FlowFile} file archivo agregado, instancia de FlowFile
         * @param {Object} message objeto con información reportado por backend 
         * @param {Flow} flow flow objeto flow (scope.$flow)
         */
        function flowFileSuccess(file, message, flow) {
            message = angular.fromJson(message);
            file.IDDocumento = obtenerIDDocumentoCargado(message);
            file.ok = message.ResultadoOK;
            file.error = !file.ok;
            if (file.error) {
                agregarErroresCargaDocumento(file, message);
                return;
            }
            isUploadComplete = true;
            timeout(function () {
                remove(flow.files, function (f) {
                    return f.$$hashKey == file.$$hashKey;
                });
            }, 3000);
        }

        /**
         * @description Función que permite obtener los IDDocumento cargados
         * exitosamete
         * @param {Object} resp respuesta desde backend.
         */
        function obtenerIDDocumentoCargado(resp){            
            return filter(map(resp.Datos.Documentos, function(doc){
                return doc.IDDocumento;
            }), function(id){
                return id > 0;
            }).join(',');
        }

        /**
         * @description Evento que se gatilla cada vez que un archivo ha subido exitosamente
         * @param {Array} files de archivos instancias de FlowFile
         * @param {Object} message objeto con información reportado por backend 
         * @param {Flow} flow flow objeto flow (scope.$flow)
         */
        function flowFilesSubmitted(files, message, flow) {
            angular.forEach(files, function (file) {
                if (!file.isValid) file.cancel();
            });
            subirDocumentos();
        }

        /**
         * @description Evento que se gatilla cuando la carga el server ha sido completada.
         * Primero se verifica si hay archivos con errores y luego se actualiza la grilla de 
         * facturas realizando nuevamente la petición al backend con la función obtenerDocumentos()
         * @param {Flow} flow objeto flow (scope.$flow)
         */
        function flowComplete(flow) {
            if (!isUploadComplete) return;
            negocioCrear.notificar(PRELOADER_HIDDEN);
            cerrarCargarDocumentos();
            verificarErroneos();
            obtenerDocumentos();
        }

        /**
         * @description Función que revisa los archivos que se han intentado subir al servidor.
         * Si hay archivos erroneos se muestra una alerta indicando la cantidad de archivos con errores
         */
        function verificarErroneos() {
            scope.erroneos = filter(scope.$flow.files, function (file) {
                return !file.ok;
            }).length;

            if (!alertaEsMostrada && scope.erroneos > 0) {
                alertaEsMostrada = !alertaEsMostrada;
                var msj = scope.erroneos > 1 ? parseString(DOS_O_MAS_ARCHIVOS_ERRONEOS, scope.erroneos) : UN_ARCHIVO_ERRONEO;
                popup.errorAlert(msj, function () {
                    alertaEsMostrada = !alertaEsMostrada;
                });
            }
        }

        /**
         * @description Función para gatillar la subida de archivos
         */
        function subirDocumentos() {
            negocioCrear.notificar(PRELOADER_VISIBLE);
            //agregar parámetros necesarios para subida de archivo:
            angular.extend(scope.$flow.defaults.query, obtenerParametrosUpload());

            scope.$flow.upload();
        }

        /**
         * @description Función que permite reintentar la subida de todos los archivos
         * con subida erronea
         */
        function reintentarTodos(){
            //agregar parámetros necesarios para subida de archivo:
            angular.extend(scope.$flow.defaults.query, obtenerParametrosUpload());

            var erroneos = filter(scope.$flow.files, function (file) {
                return !file.ok;
            });

            angular.forEach(erroneos, function(file){
                file.retry();
            });
        }

        /** 
         * @description Función que permite eliminar todos los archivos erroneos
         */
        function eliminarTodos(){
            var erroneos = filter(scope.$flow.files, function (file) {
                return !file.ok;
            });

            angular.forEach(erroneos, function(file){
                file.cancel();
            });
        }

        /**
         * @description Función encargada de generar los parámetros necesarios
         * para la subida de archivos DTE
         */
        function obtenerParametrosUpload() {
            var params = {
                Origen: ORIGEN_UPLOAD.DTE,
                Dom: window.location.origin,
            };
            angular.extend(params, feAuth.obtenerAutorizacion());
            return params;
        }

        /**
         * @description Función para obtener parámetros para cargar la lista de
         * facturas, agrega párametros de filtros y paginación
         * @param {Boolan} esFiltrar determina si la llamada se ha producido mediante la acción filtrar
         */
        function obtenerParametros(esFiltrar) {
            var params = {
            };

            if (esFiltrar) {
                scope.Paginacion.Pagina = 1;
            }

            angular.extend(params, scope.filtros);
            angular.extend(params, scope.Paginacion);
            return params;
        }

        /**
         * @description Función que realiza la petición al backend para 
         * obtener los documentos cargados en el repositorio
         */
        function obtenerDocumentos(esFiltrar) {
            var params = obtenerParametros(esFiltrar);
            var status = {
                200: cargarDatosDocumentos,
                203: filtroSinDocumentos,
                204: sinDatos
            };
            debugger;
            negocioCrear.obtenerFacturas(params).then(function (resp) {
                return (status[resp.status] || codigoNoControlado)(resp.data);
            });
        }

        /**
         * @description Función que se encarga de cargar en el scope
         * los datos de las facturas obtenidas desde el backend.
         * Luego de cargar los datos de las facturas, se deben cargar
         * los filtros, para ello se realiza una notificación de evento.
         * @param {Object} data data respuesta desde el backend.
         */
        function cargarDatosDocumentos(data) {
            scope.datosDocumentos = data.Datos;
            scope.hayDatosDocumentos = true;
            scope.busquedaSinCoincidencias = false;

            angular.extend(scope.Paginacion, data.Paginacion);
        }

        /**
         * @description Función encargada de cerrar la ventana para carga de documentos
         */
        function cerrarCargarDocumentos(){
            scope.cargarDocumentos = false;
        }

        /**
         * @description Función que se encarga de establecer las condiciones
         * y aspectos si el backend no encuentra datos dado una combinatoria de
         * filtros establecidos por el usuario.
         * @param {Object} data data respuesta desde el backend.
         */
        function filtroSinDocumentos(data) {
            scope.datosDocumentos = [];
            scope.busquedaSinCoincidencias = true;
            angular.extend(scope.Paginacion, data.Paginacion);
        }

        /**
         * @description Función que se encarga de establecer las condiciones
         * y aspectos cuando no existen registros sin importar el filtro utilizado.
         * @param {Object} data data respuesta desde el backend.
         */
        function sinDatos(data) {
            scope.hayDatosDocumentos = false;
            scope.cargarDocumentos = true;            
            scope.datosDocumentos = [];
            scope.Paginacion.Pagina = 1;
        }

        /**
         * @description Función que se encarga de establecer las condiciones
         * y aspectos cuando ocurre un error no controlado al solicitar datos
         * de facturas.
         * @param {Object} data data respuesta desde el backend.
         */
        function codigoNoControlado(data) {
            popup.errorAlert(data.Message);
        }

        /**
         * @description Función que permite confirmar eliminación de un documento
         * @param {Object} documento item de colección de `scope.datosDocumentos`
         */
        function confirmarEliminar(documento){
            popup.confirm(parseString(ELIMINAR_DOCUMENTO, `Folio ${documento.Folio}`), function(){
                eliminarDTE(documento);
            });
        }

        /**
         * @description Función que permite eliminar un documento dado por `id`
         * @param {Object} documento item de colección de `scope.datosDocumentos`
         */
        function eliminarDTE(documento){
            var status = {
                200: obtenerDocumentos
            };
            documentosGestionar.eliminarDTE({
                id: documento.IDDocumento
            }).then(function(resp){
                return (status[resp.status] || codigoNoControlado)(resp.data);
            });
        }

        /**
         * @description Funcion que abre modal para reemplazar documento
         * sino un agregar nota de credito.
         * @param {Object} documento item de colección de `scope.datosDocumentos`
         */
        function reemplazarDocumento(documento){
            popup.modal({
                view: require('receder/views/modal-reemplazar-documento.html'),
                controller: 'fe.negocio.nuevo.modal-reemplazar-documento-controller',
                name: 'reemplazar documento',
                params: {
                    documento: documento,
                    esAgregarNotaCredio: false
                },
                onClose: function(){
                    obtenerDocumentos();
                }
            });
        }

        init();
    }
];