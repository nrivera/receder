'use strict';

/**
 * @author Nicolas Andres Rivera Acevedo
 * @description indice de todos los controllers del modulo 'app.receder'
 */
angular.module('app.receder')
.controller('bandeja-entrada-repositorio-controller', require('./bandeja-entrada-repositorio-controller'))
.controller('bandeja-entrada-controller', require('./bandeja-entrada-controller'))
.controller('receder-documento-controller', require('./receder-documento-controller'))
