'use strict';

import {obtenerMesActual} from 'commons/util'

/**
 * @author Nicolas Andres Rivera Acevedo
 * @description Controller para modal de términos y condiciones
 */
module.exports = [

    '$scope', '$uibModalInstance',
    
    function(scope, uibModalInstance){

        /**
         * @description Atributos y funciones a utilizar en la vista
         */
        scope.aceptar   = aceptar;
        scope.cancelar  = cancelar;
        scope.cerrar    = cerrar;

        /**
         * @description Función inicial
         */
        function init(){
            calcularFecha();
        }

        /**
         * @description Función encargada de generar las variables para la fecha actual
         */
        function calcularFecha(){
            var fecha = new Date();
            scope.dia = fecha.getDate();
            scope.dia = scope.dia < 9 ? scope.dia = '0' + scope.dia : scope.dia;

            scope.mes = obtenerMesActual();
            scope.anio = fecha.getFullYear();
        }

        /**
         * @description Función encargada de cerrar la modal ejecutando la acción de 'aceptar'
         */
        function aceptar(){
            uibModalInstance.close();
        }

        /**
         * @description Función encargada de cerrar la modal ejecutando la acción de 'cancelar'
         */
        function cancelar(){
            uibModalInstance.dismiss(true);
        }

        /**
         * @description Función encargada de cerrar la modal ejecutando la acción de 'cancelar' indicando
         * una acción neutra.
         */
        function cerrar(){
            uibModalInstance.dismiss(false);
        }

        init();
    }

];