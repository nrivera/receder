'use strict';

/**
 * @author Nicolas Andres Rivera Acevedo
 * @description Controller de la vista principal de documentos para receder
 */
module.exports = [

    '$scope', '$receder', '$popup','$authStorage','$location',

    function (scope, receder, popup ,authStorage , $location) {

        /**
         * @description Funciones y variables para ser utilizadas en la vista.
         */
        scope.recederDocumento  = recederDocumento;
        scope.obtenerFacturas   = obtenerFacturas;

        /**
         * @description modelo para directiva de paginación.
         */
        scope.Paginacion = {
            Pagina			: 1,     
            CampoOrden		: '',      
            Descendente		: false
        };

        /**
         * @description Función principal.
         */
        function init(){
            debugger

            validaTokenVacio();
            establecerValoresDefecto();
            obtenerFacturas();
        }




        function validaTokenVacio(){
            const _Token =authStorage.getToken();    

                if(_Token == "" || _Token == null ||_Token == undefined  ){
                    console.error('invalid token or null/void/undefined')
                    return $location.url('/error');
                }
        }

        /**
         * @description Función encargada de establecer valores
         * por defecto de variables al inicializar.
         */
        function establecerValoresDefecto(){
            scope.hayDatosFacturas = false;
        }

        /**
         * @description Función encargada de generar los
         * parámetros necesarios como querystring para la obtención
         * de facturas para receder.
         */
        function obtenerParametros(){
            var params = {
                EsParaReceder: true
            };
            angular.extend(params, scope.Paginacion);
            return params;
        }

        /** 
         * @description Función encargada de obtener las facturas
         * disponibles para receder.
        */
        function obtenerFacturas(){
            var status = {
                200: cargarDatosFacturas,
                204: sinDatos
            };
            receder.obtenerFacturas(obtenerParametros()).then(function(resp){
                (status[resp.status] || codigoNoControlado)(resp.data);
            });
        }

        /**
         * @description Función encargada de cagar los datos de las facturas
         * obtenidas desde backend en la variable de ámbito correspondiente.
         * @param {Object} data información de respuesta desde el backend.
         */
        function cargarDatosFacturas(data){
            scope.datosFacturas = data.Datos;
            scope.hayDatosFacturas = scope.datosFacturas.length > 0;
            angular.extend(scope.Paginacion, data.Paginacion);
        }

        /**
         * @description Función encargada de establecer condiciones y aspectos
         * para cuando el backend responde que no hay datos de facturas para
         * receder.
         * @param {Object} data información de respuesta desde el backend.
         */
        function sinDatos(data){
            scope.datosFacturas = [];
            scope.hayDatosFacturas = false;
            scope.Paginacion.Pagina = 1;
        }

        /**
         * @description Función encargada de establecer condiciones y aspectos
         * cuando la respuesta del backend no está controlada.
         * @param {Object} data información de respuesta desde el backend.
         */
        function codigoNoControlado(data){
            popup.errorAlert(data.Message);
        }

        /**
         * @description Función que levanta un modal para receder el documento
         * seleccionado por el usuario.
         * @param {Object} documento elemento de la colección `scope.datosFacturas`
         */
        function recederDocumento(documento){
            popup.modal({
                view: require('receder/views/receder-documento.html'),
                controller: 'receder-documento-controller',
                name: 'receder documentos',
                params: {
                    documento: documento
                },
                onClose: obtenerFacturas
            });
        }

        init();

    }

];