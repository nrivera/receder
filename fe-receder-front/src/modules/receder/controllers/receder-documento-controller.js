'use strict';

import {agregarSeleccione} from 'scripts/transforms';
import {RECESION_EXITOSA} from 'commons/constants/messages';

/**
 * @author Nicolas Andres Rivera Acevedo
 * @description Controller del modal para receder documento
 */
module.exports = [

    '$scope', '$receder', '$uibModalInstance', '$uibModalParams', '$popup', '$cliente',

    function (scope, receder, modalInstance, params, popup, cliente) {

        /**
         * @description Variables y funciones para utilizar en la vista
         */
        scope.volver                = volver;
        scope.recederDocumento      = recederDocumento;
        scope.obtenerDatosCliente   = obtenerDatosCliente;

        // objeto modelo para envíar al backend.
        scope.recesion = {
            TipoRecesion: 1,
            Cliente: {}
        };

        /**
         * @description Función principal
         */
        function init(){
            valoresDefecto();
            obtenerParametrosModal();
            obtenerTipoRecesion();
        }
    
        /**
         * @description Permite establecer valores
         * por defecto de variables
         */
        function valoresDefecto(){
            scope.fechaActual = new Date();
        }

        /**
         * @description Función encargada de obtener los parámetros
         * enviados desde la página padre
         */
        function obtenerParametrosModal(){
            scope.documento = params.documento;
            scope.recesion.IDDocumento = params.documento.IDDocumento;
        }

        /**
         * @description Función para cerrar modal.
         */
        function volver(){
            modalInstance.dismiss();
        }

        /**
         * @description Función encargada de obtener la lista
         * de tipos de recesión.
         */
        function obtenerTipoRecesion(){
            receder.obtenerTipoRecesion().then(function(resp){
                scope.ListaTipoRecesion = resp.data;
                agregarSeleccione(scope.ListaTipoRecesion, 'ID', 'Nombre');
            });
        }

        /**
         * @description Permite obtener los datos del cliente
         * dado el rut ingresado por el usuario `scope.recesion.Cliente.Rut`
         */
        function obtenerDatosCliente(event){
            if (event.type == 'keyup' && event.keyCode != 13) return;
            if (!scope.recesion.Cliente.Rut) return;
            cliente.obtenerPorRut(scope.recesion.Cliente.Rut).then(function(resp){
                if (resp.status != 200) return;
                angular.extend(scope.recesion.Cliente, resp.data);
            });
        }

        /**
         * @description Función encargada de realizar la petición
         * al backend de receder un documento. lo que se envía al
         * backend es el modelo antes preparado `scope.recesion`
         * con atributos establecidos por el usuario.
         */
        function recederDocumento(){
            if (!scope.validar()) return;

            var status = {
                200: recesionExitosa
            };
            receder.recederDocumento(scope.recesion).then(function(resp){
                (status[resp.status] || errorNoControlado)(resp.data);
            });
        }

        /**
         * @description Función encargada de establecer los aspectos y condiciones
         * luego de una respuesta exitosa al receder un documento.
         * @param {Object} data información de respuesta desde el backend
         */
        function recesionExitosa(data){
            popup.alert(RECESION_EXITOSA);
            modalInstance.close();
        }

        /**
         * @description Función que se encarga de establecer las condiciones
         * y aspectos cuando ocurre un error no controlado al receder documento.
         * @param {Object} data información de respuesta desde el backend.
         */
        function errorNoControlado(data){
            popup.errorAlert(data.Message);
        }

        init();
    }

];