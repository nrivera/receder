'use strict';

/**
 * @author Nicolas Rivera Acevedo
 * @description Controller para configurar la incrustacion y datos en la llamada del iframe solo-wizard
 */
module.exports = [

    '$scope' ,'$feAuth','$urlConfig',

    function(scope ,feAuth  , urlConfig) {
        scope.token = feAuth.getToken();
        scope.origen = window.location.origin;
        scope.url = urlConfig.IFRAME_URL.CERTIFICADOS;
        var certificados = document.getElementById('certificados');
        certificados.setAttribute('src', scope.url);
        certificados.onload = function () {
            this.contentWindow.postMessage({
                Token: scope.token,
                Origin: scope.origen
            }, '*');
        }

        window.addEventListener('message', function(event){
            if(event.data.Event == "logout-session"){
                console.log('PARENT TOKEN 401');
                feAuth.redireccionar();
            }
            console.log(event);
        });

    }
]