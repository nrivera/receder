'use strict';

/**
 * @author Nicolas Andres Rivera Acevedo
 * @description indice de todas las directivas del modulo 'app.main'
 */
angular.module('app.main')
.directive('uxLoading', require('./ux-loading/loading'))