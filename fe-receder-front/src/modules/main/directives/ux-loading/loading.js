'use strict';

import {
    PRELOADER_HIDDEN,
    PRELOADER_VISIBLE
} from 'commons/constants/event-names';

/**
 * @author Nicolas Andres Rivera Acevedo
 * @description directiva para mostrar 'cargando en cada request'
 */
module.exports = function(){
	return	{
		restrict: 'A',
		scope: {},
		link: function(scope, elem, attr){
			elem.hide();
			scope.$on(PRELOADER_VISIBLE, function(){
				elem.show();
			});
			scope.$on(PRELOADER_HIDDEN, function(){
				elem.hide();
			});
		}
	}
};
