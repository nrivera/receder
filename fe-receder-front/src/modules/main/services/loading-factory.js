'use strict';

import {
    PRELOADER_HIDDEN,
    PRELOADER_VISIBLE
} from 'commons/constants/event-names';

/**
 * @author Nicolas Andres Rivera Acevedo
 * @description Factory para utilizarlo como interceptor y mostrar un 'loading en cada request'
 */
module.exports = [

	'$q', '$rootScope',

	function(q, rootScope){
        var currentCalls = 0;

		return {
			request: function (config) { 
                if ((config.loading || config.loading == null) && config.url.indexOf('uib/') < 0) {
                    currentCalls++; 
                    rootScope.$broadcast(PRELOADER_VISIBLE); 
                }
                return config || q.when(config); 
            }, 
            response: function (response) {
                if (currentCalls > 0) currentCalls--;
                if (currentCalls <= 0) rootScope.$broadcast(PRELOADER_HIDDEN);
                return response || q.when(response);
            }, 
            responseError: function (response) {
                if (currentCalls > 0) currentCalls--;
                if (currentCalls <= 0) rootScope.$broadcast(PRELOADER_HIDDEN);
                return q.reject(response);
            },
		}
	}

];
