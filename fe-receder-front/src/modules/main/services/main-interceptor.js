'use strict';

/**
 * @author Nicolas Andres Rivera Acevedo
 * @description Factory para ser utilizado como interceptor de las peticiones ajax del
 *              servicio $http, permite reconfigurar la url de request anteponiendo
 *              el host destino, de esta manera en todos los demás servicios se utiliza una url relativa.
 */
module.exports = [

    '$q', '$urlConfig', '$authStorage', '$parentMessage', '$injector','$rutContextoCrypto',

    function (q, urlConfig, authStorage, parentMessage, injector , rutContextoCrypto) {

        const MAX_RETRY_REQUEST = 10;
        var countRetries = {}; 

        function joinUrl(baseUrl, url) {
            if (/^(?:[a-z]+:)?\/\//i.test(url)) {
                return url;
            }
            var joined = [baseUrl, url].join('/');
            var normalize = function (str) {
                return str
                    .replace(/[\/]+/g, '/')
                    .replace(/\/\?/g, '?')
                    .replace(/\/\#/g, '#')
                    .replace(/\:\//g, '://');
            };
            return normalize(joined);
        };

        return {
            request: function (config) {

                /**
                 * saltar la configuración defecto de ui-bootstrap
                 * ya que va a buscar templates predefinidas en ruta 'uib/'
                 */
                if (config.url.indexOf('uib/') == 0) return config || q.when(config);

                if (!config.params) config.params = {};
                angular.extend(config.params, {
                    Dom         : authStorage.getDomain(),
                    RutContexto : authStorage.getRutContexto(),
                    _rcksm      : rutContextoCrypto.getCheckSum(),
                    _r: urlConfig.RELEASE_NUM
                });

                authStorage.authConfig(config);

                for (var i = 0; i < urlConfig.EXCLUDE_BEGIN.length; i += 1) {
                    if (config.url.indexOf(urlConfig.EXCLUDE_BEGIN[i]) === 0) {
                        return config || q.when(config);
                    }
                }
                for (var i = 0; i < urlConfig.EXCLUDE_END.length; i += 1) {
                    if (config.url.lastIndexOf(urlConfig.EXCLUDE_END[i]) === config.url.length - urlConfig.EXCLUDE_END[i].length) {
                        return config || q.when(config);
                    }
                }

                config.url = joinUrl(urlConfig.HOST_API, config.url);

                return config || q.when(config);
            },
            response: function(resp){
                var bearer = resp.headers('bearer');
                if (bearer) {
                    authStorage.set({
                        Token: bearer
                    });
                    parentMessage.actualizarToken(bearer);
                }
                return resp;
            },
            responseError: function(resp){
                if (resp.status == 401){
                    /**
                     * reintentar request producto de una actualización de token.
                     * Esto ocurre en peticiones en paralelo donde la primera
                     * respuesta desde el backend produce un cambio de token dejando
                     * obsoletas las peticiones siguientes.
                     */
                    if (!countRetries[resp.config.url]) countRetries[resp.config.url] = 0;
                    if (countRetries[resp.config.url] < MAX_RETRY_REQUEST) {
                        countRetries[resp.config.url]++;
                        return injector.get('$http')(resp.config);
                    } else {
                        countRetries[resp.config.url] = 0;

                        /**
                         * notificar al contenedor padre de una renovación de token,
                         * en este caso escalamos el token que probablemente es invalido,
                         * lo cual en el mejor caso, hará posible el despliegue de un cuadro de dialogo de login
                         * y luego se informará un nuevo token, recién ahí se reintentan las peticiones.
                         */
                        parentMessage.renovarToken(authStorage.getToken()).then(function(event){
                            authStorage.set({
                                Token: event.data.Token
                            });
                            angular.extend(resp.config, {
                                loading: false
                            });
                            return injector.get('$http')(resp.config);
                        });
                    }
                }                
                return q.reject(resp);
            }
        }
    }

];
