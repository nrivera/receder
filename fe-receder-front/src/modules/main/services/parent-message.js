'use strict';

module.exports = [
    '$q',
    function(q){
        
        const MAX_WAITING = 120; //segundos;

        var deferRenovacion = null;
        var _onMessage = null;

        window.addEventListener('message', function(event){
            if(deferRenovacion){
                deferRenovacion.resolve(event);
                deferRenovacion = null;
            }
            if(typeof _onMessage == "function"){
                _onMessage(event);                
            }
        });

        return {
            onMessage: onMessage,
            renovarToken: renovarToken,
            actualizarToken: actualizarToken
        }

        /**
         * @description notifica al contenedor padre con token actualizado,
         * en este caso no espera una respuesta ya que el token actualizado
         * ya lo tenemos, sólo debemos informar que hay un nuevo token a utilizar.
         * @param {String} token token
         */
        function actualizarToken(token){
            parent.postMessage({
                Event: 'renew-token',
                Data: {
                    Token: token
                }
            }, '*');
        }

        /**
         * @description notifica a contenedor padre para renovar token,
         * esto ocurre luego de un codigo 401, se notifica al padre para
         * que renueve el token invalido y espera X segundos por una respuesta con un nuevo token
         * si se superan los segundos, se cancelan las peticiones.
         * @param {String} token token
         */
        function renovarToken(token){            
            deferRenovacion = q.defer();  
            parent.postMessage({
                Event: 'logout-session',
                Data: {
                    Token: token
                }
            }, '*');
            setTimeout(function(){
                if (deferRenovacion) {
                    deferRenovacion.reject();
                    deferRenovacion = null;
                }
            }, MAX_WAITING * 1000);
            return deferRenovacion.promise;
        }

        function onMessage(fn){
            _onMessage = fn;
        }

    }

];