'use strict';

/**
 * @author Nicolas Andres Rivera Acevedo
 * @description indice de todos los servicios del modulo 'app.main'
 */
angular.module('app.main')

.factory('$loadingFactory', require('./loading-factory'))
.factory('$mainInterceptor', require('./main-interceptor'))
.factory('$parentMessage', require('./parent-message'))
