'use strict';
/**
 * @author Nicolas Andres Rivera Acevedo
 * @description configuración del submodulo 'app.main'
 */
module.exports = [

	'$httpProvider', '$urlRouterProvider', '$locationProvider',

	function(httpProvider, urlRouterProvider, locationProvider ){



		locationProvider.hashPrefix('');

		/**
		 * interceptor para request ajax
		 */
		httpProvider.interceptors.push('$mainInterceptor');

		/**
		 * configuración de alias para urls de ui-router
		 */
		urlRouterProvider.otherwise('/error');
	
	}

];