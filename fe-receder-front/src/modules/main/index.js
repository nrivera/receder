'user strict';

/**
 * @author Nicolas Andres Rivera Acevedo
 * @description Definición del submodulo app.main
 */
var moduleName = 'app.main';
module.exports = moduleName;

angular.module(moduleName, [
	/**
	 * dependencias:
	 */
])
.constant('$urlConfig', require('root/bundle/conf/url-config.cfg'))
.config(require('./config'));

/**
 * inclusión de componentes
 */
require('./services');
require('./directives');