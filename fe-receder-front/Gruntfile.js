/**
 * @author Nicolas Andres Rivera Acevedo
 * @description configuración de grunt con tareas para el flujo de trabajo.
 */
module.exports = function(grunt){

	//por defecto el ambiente es dev.
	process.env.MELVIN_ENV = process.env.MELVIN_ENV || 'dev';
	
	require('load-grunt-tasks')(grunt);

	var pkg = grunt.file.readJSON('package.json');
	var publishConfig = require('./conf/publish.config');
	var portscanner = require('portscanner');
	var browserify = require('browserify');
	var exorcist = require('exorcist');
	var serveStatic = require('serve-static');
	var pathmodify = require('pathmodify');
	var fs = require('fs');
	var path = require('path');
	var nodeSass = require('node-sass');
	var watchify = require('watchify');

	var time = (new Date()).getTime();

	/**
	 * generar array de dependecias de terceros
	 * para un bundle separado y evitar sus compilaciones
	 */
	var libs = Object.keys(pkg.dependencies || {});
	var shim = require('./conf/shim.config');
	var shimlibs = Object.keys(shim.libs || {});

	/**
	 * serve statics
	 */
	var servesConfig = require('./conf/serves.config');

	/** 
	 * directorios personalizados 
	 */ 
	var customPath = require('./conf/custom-path.config'); 
	var customDirDist = []; 
	var fixPath = function(input){ 
		return input.replace(/[\\/]+/g,'/').replace(/\/$/,''); 
	}; 

	/**
	 * configuración de alias con pathmodify
	 */
	var aliases = require('./conf/aliases.config');
	var modsAliases = [
		pathmodify.mod.dir('root', __dirname)
	];
	for (var k in aliases){
		modsAliases.push(pathmodify.mod.dir(k, path.join(__dirname, aliases[k])))
	}
	modsAliases = {
		mods: modsAliases
	};
	
	/**
	 * Busca un puerto disponible para livereload
	 */
	var liveReload_port = 35720;
	var liveReload_maxPort = 35750;
	var findPort = function(callback) {
		portscanner.findAPortNotInUse(liveReload_port, liveReload_maxPort, '127.0.0.1', callback);
	};

	/**
	 * genera un string con fecha hora y minutos
	 */
	var getDate = function(){
		var d = new Date();
		return d.getFullYear() + 
			(0 + (d.getMonth() + 1).toString()).slice(-2) + 
			(0 + d.getDate().toString()).slice(-2) + '_' + 
			(0 + d.getHours().toString()).slice(-2) + 
			(0 + d.getMinutes().toString()).slice(-2);
	}

	grunt.initConfig({
		distPath: publishConfig.path,
		buildTime: time,
		connect: {
			options: {
				hostname: 'localhost',
			},
			dev: {
				options: {
					open: true,
					useAvailablePort: true,
					middleware: function(connect) {
						var serves = [];
						servesConfig.forEach(function(serv){
							if (!serv.path) serves.push(serveStatic(serv.use));
							else {
								serves.push(connect().use(serv.use, serveStatic(serv.path)));
							}
						});
						Object.keys(customPath).forEach(function(path){ 
							var virtual = fixPath(path);
							virtual = /^\//.test(virtual) ? virtual : '/' + virtual; 
							serves.push(connect().use(virtual, serveStatic(customPath[path]))); 
						}); 
						serves.push(function(req, res, next){
							res.writeHead(200, { 'Content-Type': 'text/html' });
							res.end(grunt.file.read('./src/index.html'));
						});
						return serves;
					}
				}
			},
			dist: {
				options: {
					livereload: false,
					keepalive: true,
					open: true,
					useAvailablePort: true,
					base: {
						path: '<%= distPath %>',
						options: {
							index: 'index.html'
						}
					}
				}
			}
		},

		watch: {
			options: {			
				debounceDelay: 250,	
				event: ['changed', 'added', 'deleted'],
			},
			bundle: {
				options: {
					livereload: false,
					interrupt: true,
					spawn: false,
				},
				files: ['bundle/js/app.js', 'bundle/css/app.css', 'src/**/*.html']
			},
			sass: {
				files: ['src/**/*.scss'],
				tasks: ['sass:dev']
			},
			config: {
				files: ['conf/app.config.js'],
				tasks: ['write-env','browserify-app']
			},
		},

		clean: {
			options: {
				force: true
			},
			dist: '<%= distPath %>',
			bundle: 'bundle'
		},

		copy: {
			app: {
				expand: true,
				dot: true,
				cwd: 'src',
				dest: '<%= distPath %>',
				src: ['**','!**/*.js','!bundle/**','!styles/**', '!tests/**']
			},
			bundles: {
				expand: true,
				dot: true,
				cwd: 'bundle',
				dest: '<%= distPath %>',
				src: ['js/**/*', 'css/**/*']
			},
			styles_dist: {
				expand: true,
				dot: true,
				cwd: 'src/styles',
				dest: '<%= distPath %>/css/',
				src: ['**','!sass/**']
			},
			custom: { 
				files: customDirDist 
			} 
		},

		sass: {                              
			dev: {                           
				options: {          
					implementation: nodeSass,    
					includePaths: require('./conf/sass-path.config'),        
					outputStyle: 'expanded',
					sourceMap: true,
					sourceMapContents: true 
				},
				src: ['src/styles/sass/main.scss'],
				dest: 'bundle/css/app.css'
			},
			distMin: {
				options: {
					implementation: nodeSass,
					includePaths: require('./conf/sass-path.config'),        
					outputStyle: 'compressed',
					sourceMap: publishConfig.sourceMaps,
					sourceMapContents: true
				},
				src: ['src/styles/sass/main.scss'],
				dest: '<%= distPath %>/css/app_<%= buildTime %>.min.css'
			},
			dist: {
				options: {
					implementation: nodeSass,
					includePaths: require('./conf/sass-path.config'),        
					outputStyle: 'expanded',
					sourceMap: publishConfig.sourceMaps,
					sourceMapContents: true
				},
				src: ['src/styles/sass/main.scss'],
				dest: '<%= distPath %>/css/app_<%= buildTime %>.css'
			}
		},

		uglify: {
			options: {
				sourceMap: publishConfig.sourceMaps,
				sourceMapIncludeSources: false,
				banner: '/*! ' + pkg.name + ' - ' + grunt.template.today("yyyy-mm-dd") + ' @author '+ pkg.author +'*/\n'
			},
			app: {
				files: {
					'<%= distPath %>/js/app_<%= buildTime %>.min.js' : '<%= distPath %>/js/app_<%= buildTime %>.js'
				}
			},
			libs: {
				files: {
					'<%= distPath %>/js/libs_<%= buildTime %>.min.js' : '<%= distPath %>/js/libs_<%= buildTime %>.js'
				}
			}
		},

		cleanempty: {
			options: {
				files: false,
				force: true,
				},
				dist: '<%= distPath %>/**/*',
		},

		'string-replace': {
			dist: {
				options: {
					replacements: [
						{
							pattern: /<!--\s*?bundle:(js|css)\s*?-->[\s\S]*?<!--\s*?endbundle\s*?-->/gm,
							replacement: function(match, scripts) {
								var tag = match.indexOf('bundle:js') > 0 
								? '<script src="js/libs_' + time + '.min.js?_r='+ time +'"></script>\n\t<script src="js/app_' + time + '.min.js?_r='+ time +'"></script>'
								: '<link rel="stylesheet" type="text/css" href="css/app_' + time + '.min.css?_r='+ time +'">';
								return tag;
							}
						}
					]
				},
				files: {
					'<%= distPath %>/index.html': '<%= distPath %>/index.html',
					'<%= distPath %>/test_A9BBD47ACCDDEV.html': '<%= distPath %>/test_A9BBD47ACCDDEV.html',
					'<%= distPath %>/test_A9BBD47ACCDPROD.html': '<%= distPath %>/test_A9BBD47ACCDPROD.html',
					'<%= distPath %>/error.html': '<%= distPath %>/error.html'
				}
			}
		}

	});

	grunt.registerTask('browserify-libs', 'genera un bundle de las dependencias de terceros', function(step){
		var done = this.async();
		var dir = './bundle/js';
		var reqs = [].concat(libs);
		var excludes = shim.excludes || [];
		var rnum = step == 'dist' ? '_' + time : '';
		var filePath = path.join(__dirname, dir, 'libs'+ rnum +'.js');

		var b = browserify({
			debug: false
		});

		var handleError = function(err){
			if (!err) return;
			grunt.log.error(err);
			done(false);
		};

		if (!fs.existsSync(dir)) fs.mkdirSync(dir);
		
		if (shimlibs.length > 0 && shim.exports) {
			b.transform('browserify-shim', {
				global: true
			});
			
			shimlibs.forEach(function(lib){
				var i = reqs.indexOf(lib);
				var expose = shim.exports[lib];
				var modulePath = shim.libs[lib];

				if (i >= 0) reqs.splice(i, 1);
				else {
					if (/node_modules/i.test(modulePath)){
						i = reqs.indexOf(lib);
						if (i >= 0) reqs.splice(i, 1);
					}
				}
				b.require(require.resolve(shim.libs[lib]), {
					expose: typeof expose == 'string' ? expose : expose.exports
				}) 
			});
		}

		excludes.forEach(function(e){ 
			var r = reqs.indexOf(e); 
			if (r >= 0) reqs.splice(r, 1); 
		}); 

		b.require(reqs)
		.bundle(handleError)
		.pipe(fs.createWriteStream(filePath, 'utf8')).on('finish', done);
	});

	grunt.registerTask('browserify-app', 'genera un bundle de la aplicación', function(step){
		var done = this.async();
		var dir = './bundle/js';
		var rnum = step == 'dist' ? '_' + time : '';
		var mapFilePath = path.join(__dirname, dir, 'app'+ rnum +'.js.map');
		var filePath = path.join(__dirname, dir, 'app'+ rnum +'.js');

		var b = browserify({ 
			cache: {}, 
			packageCache: {}, 
			debug: process.env.MELVIN_ENV == 'dev', 
			extensions: ['.js', '.jsx']
		});

		var handleError = function(err){
			if (!err) return;
			grunt.log.error(err);
			done(false);
		};

		if (!fs.existsSync(dir)) fs.mkdirSync(dir);

		if (shimlibs.length > 0 && shim.exports){
			shimlibs.forEach(function(lib){
				var expose = shim.exports[lib];
				expose = typeof expose == 'string' ? expose : expose.exports
				if (libs.indexOf(expose) < 0) libs.push(expose);
			});
		}	
			
		b.require('./src/modules/app.js', { entry : true })
		.plugin(watchify)
		.plugin(pathmodify, modsAliases)
		.transform('stringify')
		.transform('babelify', require('./conf/babel.config'))
		.external(libs)
		.on('update', bundle);

		function bundle(){
			if (process.env.MELVIN_ENV == 'dev' || publishConfig.sourceMaps) {
				b.bundle(handleError).pipe(exorcist(mapFilePath)).pipe(fs.createWriteStream(filePath, 'utf8')).on('finish', done);
			} else {
				b.bundle(handleError).pipe(fs.createWriteStream(filePath, 'utf8')).on('finish', done);
			}
		}

		bundle();
	});

	grunt.registerTask('find-port', 'busca un puerto para livereload', function(){
		var done = this.async();
		findPort(function(error, port){
			if (error) {
				grunt.log.writeln('No se puede encontrar un puerto libre para livereload');
				done(false);
				return false;
			}
			grunt.config.set('connect.options.livereload', port);
			grunt.config.set('watch.bundle.options.livereload', port);
			done();
		});
	});

	grunt.registerTask('write-env', 'genera archivo de configuracion de ambiente', function(){
		var conf = require('./conf/app.config')(process.env.MELVIN_ENV);
		if (!conf) {
			grunt.log.error('No se encuentra configuración de ambiente "' + process.env.MELVIN_ENV + '", revise la configuración en el archivo conf/app.config.js');
			return false;
		}
		conf.RELEASE_NUM = time;
		grunt.file.write('./bundle/conf/url-config.cfg', 'module.exports='+JSON.stringify(conf)+';');
	});

	grunt.registerTask('env', 'levanta ambiente desarrollo', function(param){
		if (param) process.env.MELVIN_ENV = param;

		grunt.task.run([
			'find-port',
			'write-env',
			'sass:dev',
			'browserify-libs',
			'browserify-app',
			'connect:dev',
			'watch'
		]);
	});

	grunt.registerTask('posDist', 'tareas post distribución', function(){
		/**
		 * deplegar distribución en navegador
		 */
		if (publishConfig.display) {
			grunt.task.run('connect:dist');
		}
	});

	grunt.registerTask('publishPath', 'prepara path de destribución', function(){
		var dest = publishConfig.path;
		var envDir = process.env.MELVIN_ENV;

		if (publishConfig.useSufixVersion) envDir += '_v' + pkg.version + '_' + getDate();
		if (publishConfig.useEnvDir) dest = path.join(dest, envDir);

		Object.keys(customPath).forEach(function(path){ 
			var virtual = fixPath(path); 
			virtual = /^\//.test(virtual) ? virtual : '/' + virtual; 
			customDirDist.push({ 
				expand: true, 
				dot: true, 
				cwd: customPath[path], 
				dest: dest + virtual, 
				src: ['**']
			}); 
		});

		grunt.config.set('distPath', dest);
	});

	grunt.registerTask('dist', 'genera empaquetado para distribución', function(param){
		process.env.MELVIN_ENV = param || publishConfig.env;
		grunt.task.run([
			'publishPath',
			'clean',
			'write-env',
			'browserify-libs:dist',
			'browserify-app:dist',
			'copy',
			'sass:distMin',
			'sass:dist',
			'uglify',
			'cleanempty',
			'string-replace:dist',
			'posDist'
		]);
	});

	grunt.registerTask('test', 'procesa lo necesario para realizar pruebas con karma', function(param){
		if (param) process.env.MELVIN_ENV = param;
		grunt.task.run([
			'write-env',
			'browserify-libs',
			'browserify-app'
		]);
	});
};