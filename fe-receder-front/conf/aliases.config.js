'use strict';
/**
 * @author Nicolas Andres Rivera Acevedo
 * @description Definición de alias para utilizar con require
 *              'root' es reservado para el directorio base del
 *              proyecto, se utiliza __dirname considerando la 
 *              ejecución desde el mismo directorio del archivo
 *              Gruntfile.js
 */
module.exports = {
	src			: 'src',
	scripts		: 'src/scripts',
	commons		: 'src/modules/commons',
	main		: 'src/modules/main',
	receder     : 'src/modules/receder',
};