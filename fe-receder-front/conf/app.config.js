'use strict';
/**
 * @author Nicolas Andres Rivera Acevedo
 * @description Definición de configuración dependiendo del ambiente
 */
module.exports = function(env){
	/**
	 * definición de ambiente,
	 * si se necesita realizar algún cambio, se debe revisar también
	 * el archivo Gruntfile.js
	 */
	var config = {
		dev: {
			HOST_API 		: 'http://localhost:57627/', //las peticiones al backend se escriben relativas api/...
			EXCLUDE_BEGIN 	: ["http://", "https://"], 	//las peticiones que comienzan con, no son modificadas por el interceptor
			EXCLUDE_END 	: ['.html', '.json'] 		//las peticiones que terminan con, no son modificadas por el interceptor
		},
		int: {
			HOST_API 		: 'https://fe.desarrollo.factoringsecurity.cl/', 
			EXCLUDE_BEGIN 	: ["http://", "https://"], 
			EXCLUDE_END 	: ['.html', '.json'] 		
		},
		prod: {
			HOST_API 		: 'https://fe.factoringsecurity.cl/', 
			EXCLUDE_BEGIN 	: ["http://", "https://"], 
			EXCLUDE_END 	: ['.html', '.json'] 		
		}
	};
	
	return config[env];
}