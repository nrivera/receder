/**
 * @author Nicolas Andres Rivera Acevedo
 * @description Configuración de librerias de terceros que no están escritas con soporte modular (UMD)
 *              o configuración de dependencias explicitas de los modulos a utilizar.
 *              Todas las rutas son relativas al archivo Gruntfile.js
 *              
 * @example
 *      libs: {
 *          'mylib': './src/scripts/mylib.js'
 *          'nglib': './node_modules/angular-lib/dist/angular-lib.js'
 *      },
 *      exports: {
 *          'mylib' : 'myLib' // exports como 'myLib'
 *          'nglib': {
 *              exports: 'myLib2', // exports como 'myLib2'
 *              depends: {
 *                  jquery: 'jQuery' //depende de jquery como 'jQuery' (hay casos donde la dependencia es '$')
 *              }
 *          }
 *      },
 *      excludes: [
 *          'net'   //excluye la dependencia 'net' 
 *      ]
 */
module.exports = {
    libs: {
        angular: './node_modules/angular/angular.js'
    },
    exports: {
        angular: {
            exports: 'angular',
            depends: {
                jquery: 'jQuery'
            }
        }
    },
    excludes: [
        'font-awesome'
    ]
};