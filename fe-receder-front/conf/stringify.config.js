/**
 * @author Nicolas Andres Rivera Acevedo
 * @description configuración para requires de archivos de texto plano
 */
module.exports = {
    appliesTo: { 
        includeExtensions: ['.html','.tmpl'] 
    },
    minify: true
}