/** 
 * @author Nicolas Andres Rivera Acevedo
 * @description configuración de rutas personalizadas, estas rutas nos permiten
 *              generar un path virtual de url de cualquier directorio que necesitemos.
 * @example
 *      por ejemplo, si tenemos un directorio './documentos/ejemplo/test'
 *      y queremos que '/test' quede bajo el path virtual: 'http://localhost:8080/test'
 *      debemos configurar:
 * 
 *      module.exports = {
 *          'test' : './documentos/ejemplo/test'
 *      };
 * 
 *      si queremos por ejemplo que '/test' quede bajo el path virtual: 'http://localhost:8080/public/test'
 *      debemos configurar:
 * 
 *      module.exports = {
 *          'public/test' : './documentos/ejemplo/test'
 *      };
 * 
 *      Nota: Cuando realizamos la distribución de nuestra aplicación, los path virtuales
 *      configurados serán generados en path de distribución configurado en publish.config.js
 *      y todos los archivos contenidos serán copiados a esa ruta.
 */
module.exports = {
    'fonts'         :   './node_modules/font-awesome/fonts'
};