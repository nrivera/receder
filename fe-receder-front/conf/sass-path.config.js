/** 
 * @author Nicolas Andres Rivera Acevedo
 * @description configuración de rutas para busqueda de archivos sass
 *              todas las rutas son relativas al archivo Gruntfile.js
 * @example
 *      si tenemos por ejemplo, _mi-estilo.scss en la ruta ./src/modules/mi-modulo/sass
 *      configuramos:
 * 
 *      module.exports = [
 *          './src/modules/mi-modulo/sass'
 *      ];
 * 
 *      luego en archivos scss podemos utilizar:
 *      @import 'mi-estilo';
 * 
 *      Nota: como recomendación no debemos configurar rutas completas como en el ejemplo,
 *      ya que pueden existir alcances de nombres en directorios distintos
 */
module.exports = [
    './node_modules',
    './src/modules'
];