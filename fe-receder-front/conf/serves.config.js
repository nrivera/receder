/** 
 * @author Nicolas Andres Rivera Acevedo
 * @description configuración de rutas para servir archivos estaticos
 *      durante el desarrollo.
 *
 * @example
 *      considerando la raiz como el mismo directorio en donde se encuentra
 *      el archivo Gruntfile.js:
 * 
 *      Si queremos servir archivos del directorio ./foo debemos configurar:
 * 
 *      module.exports = [
 *          {
 *              use: 'foo'
 *          }
 *      ];
 * 
 *      si quieremos servir archivos del directorio ./foo/files con el path virtual /buu
 *      module.exports = [
 *          {
 *              use: '/buu',
 *              path: 'foo/files'
 *          }
 *      ];
 */
module.exports = [
    { 
        use: 'src' 
    },
    { 
        use: 'bundle' 
    },
    {
        use: '/spa',
        path: 'bundle'
    },
    {
        use: '/spa',
        path: 'src'
    },
    {
        use: '/css',
        path: 'src/styles'
    },
    {
        use: '/src',
        path: 'src'
    },
    {
        use: '/spa/fonts',
        path: 'node_modules/font-awesome/fonts'
    }
];