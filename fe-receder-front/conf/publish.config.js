/** 
 * @author Nicolas Andres Rivera Acevedo
 * @description objeto de configuración para publicaciones
 * @type {Object}
 */
module.exports={
	/**
	 * @description ruta fisica donde alojar el empaquetado de distribución
	 * @type {String}
	 */
	path: './dist',
	/**
	 * @description permite desplegar la distribución resultante en el navegador
	 * @type {Boolean}
	 */
	display: false,
	/**
	 * @description configuración de ambiente para distribución
	 * @type {String}
	 */
	env: 'prod',
	/**
	 * @description permite generar mapa del fuente javascript y del fuente sass. Util para realizar debug
	 * @type {Boolean}
	 */
	sourceMaps: false,
	/**
	 * @description permite alojar el empaquetado de distribución bajo el directorio `env` 
	 * @type {Boolean}
	 */
	useEnvDir: false,
	/**
	 * @descripcion permite agregar sufijo de versión y fecha. Sólo si `useEnvDir` es true
	 * @type {Boolean}
	 */
	useSufixVersion: true
};