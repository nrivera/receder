/** 
 * @author Nicolas Andres Rivera Acevedo
 * @description objeto de configuración de transpilador babel
 * @type {Object}
 */
module.exports = {
    presets: ['@babel/env']
};