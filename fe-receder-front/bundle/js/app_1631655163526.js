(function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({1:[function(require,module,exports){
module.exports={"HOST_API":"https://fe.factoringsecurity.cl/","EXCLUDE_BEGIN":["http://","https://"],"EXCLUDE_END":[".html",".json"],"RELEASE_NUM":1631655163526};
},{}],2:[function(require,module,exports){
'use strict';
/**
 * @author Nicolas Andres Rivera Acevedo
 * @description script de entrada, definición del modulo principal 'app' y sus
 * dependencias de submodulos y componentes de terceros
 */

require('angular');

require('bootstrap');
/**
 * 	Creación de modulo 'app' e inyección de dependencias de modulos
 */


angular.module('app', [
/**
 * dependencias
 */
require('@uirouter/angularjs'), require('angular-sanitize'), require('angular-animate'), require('angular-ui-bootstrap'), require('./main'), require('./commons'), require('./receder')]);

},{"./commons":31,"./main":59,"./receder":79,"@uirouter/angularjs":"@uirouter/angularjs","angular":"angular","angular-animate":"angular-animate","angular-sanitize":"angular-sanitize","angular-ui-bootstrap":"angular-ui-bootstrap","bootstrap":"bootstrap"}],3:[function(require,module,exports){
'use strict';

var _util = require("commons/util");

/**
 * @author Nicolas Andres Rivera Acevedo
 * @description Script encargado de configurar las alertas del servicio $modal
 */
module.exports = {
  configurar: configurar
};
/**
 * @author Nicolas Andres Rivera Acevedo
 * @description Función encargada de configurar el servicio $modal
 * @param {Object} modalConfigProvider provider de configuración del servicio $modal
 */

function configurar(modalConfigProvider) {
  /**
   * define alertas por defecto:
   * alerta normal y confirmar
   */
  modalConfigProvider.alertTitle('Mensaje').confirmTitle('Confirme').alertView(require('commons/views/alert.html')).alertController(['$scope', '$modalInstance', '$modalConstant', function (scope, modalInstance, modalConstant) {
    scope.isConfirm = scope.$modalType == modalConstant.TYPE_ALERT_CONFIRM;
    scope.$message = (0, _util.normalizeEntities)(scope.$message);

    scope.aceptar = function () {
      modalInstance.close();
    };

    scope.cancelar = function () {
      modalInstance.dismiss();
    };
  }]);
  /**
   * define el tipo de alerta `warning`
   */

  modalConfigProvider.alert('warning', {
    view: require('commons/views/alert-warning.html'),
    controller: ['$scope', '$modalInstance', function (scope, modalInstance) {
      scope.$message = (0, _util.normalizeEntities)(scope.$message);

      scope.aceptar = function () {
        modalInstance.close();
      };
    }]
  });
  /**
   * define el tipo de alerta `error`
   */

  modalConfigProvider.alert('error', {
    view: require('commons/views/alert-error.html'),
    controller: ['$scope', '$modalInstance', function (scope, modalInstance) {
      scope.$message = (0, _util.normalizeEntities)(scope.$message);

      scope.aceptar = function () {
        modalInstance.close();
      };
    }]
  });
}

;

},{"commons/util":41,"commons/views/alert-error.html":53,"commons/views/alert-warning.html":54,"commons/views/alert.html":55}],4:[function(require,module,exports){
'use strict';

var _validaciones = _interopRequireDefault(require("./validaciones"));

var _alertas = _interopRequireDefault(require("./alertas"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

/**
 * @author Nicolas Andres Rivera Acevedo
 * @description configuración del submodulo 'app.commons'
 */
module.exports = ['$validateConfigProvider', '$modalConfigProvider', '$uibModalProvider', function (validateConfigProvider, modalConfigProvider, modalProvider) {
  _validaciones["default"].configurar(validateConfigProvider);

  _alertas["default"].configurar(modalConfigProvider);
  /** 
      * configuración por defecto para modales 
      */


  modalProvider.options = {
    backdrop: 'static',
    size: 'lg'
  };
}];

},{"./alertas":3,"./validaciones":5}],5:[function(require,module,exports){
'use strict';

var _messages = require("commons/constants/messages");

/**
 * @author Nicolas Andres Rivera Acevedo
 * @description Script encargado de configurar las validaciones de la directiva ux-validar.
 */
module.exports = {
  configurar: configurar
};
/**
 * @author Nicolas Andres Rivera Acevedo
 * @description Función encargada de configurar la directiva de validación.
 * @param {Object} validateConfigProvider provider de configuración de la directiva ux-validate
 */

function configurar(validateConfigProvider) {
  validateConfigProvider.defaultMessage(_messages.VALIDAR_CAMPOS_REQUERIDOS).alert(function (mensaje, focus) {
    var popup = this.get('$popup');
    return popup.warn(mensaje).then(function () {
      focus();
    });
  }).invalidItems('select', [/null/, //expresion regular, cualquier valor seleccionado que coincida con null
  -1, //valor literal == -1
  0 //valor literal == 0 
  ]);
}

;

},{"commons/constants/messages":11}],6:[function(require,module,exports){
"use strict";

/**
 * @author Nicolas Andres Rivera Acevedo
 * @description Constantes para log de auditoría
 */
module.exports = {
  HEADER_KEY: 'Auditoria',
  DONDE: 1
};

},{}],7:[function(require,module,exports){
'use strict';
/**
 * @author Nicolas Andres Rivera Acevedo
 * @description Constantes de autenticación
 */

module.exports = {
  TOKEN_HEADER: 'Authorization',
  TOKEN_TYPE: 'Bearer',
  TOKEN_PREFIX: 'fe',
  TOKEN_NAME: 'fetoken',
  AUTH_URL: 'fe/auth'
};

},{}],8:[function(require,module,exports){
'use strict';
/**
 * @author Nicolas Andres Rivera Acevedo
 * @description Constantes de tipos de archivos
 */

module.exports = {
  APPLICATION_XML: 'application/xml',
  TEXT_XML: 'text/xml',
  APPLICATION_PFX: 'application/x-pkcs12'
};

},{}],9:[function(require,module,exports){
'use strict';
/**
 * @author Nicolas Andres Rivera Acevedo
 * @description Constantes de nombres de eventos
 */

module.exports = {
  /**
   * @description Preloader
   */
  PRELOADER_HIDDEN: 'preload:hidden',
  PRELOADER_VISIBLE: 'preload:visible'
};

},{}],10:[function(require,module,exports){
'use strict';
/**
 * @description Constantes de iconos
 */

module.exports = {
  DOWNLOADING: 'fe-downloading',
  DOWNLOAD: 'fa fa-download',
  PDF: 'hst hst-document-file-pdf',
  CLOCK: 'fa fa-clock-o',
  CHECK: 'fa fa-check',
  TIMES: 'fa fa-times',
  EYE: 'fa fa-eye'
};

},{}],11:[function(require,module,exports){
"use strict";

/**
 * @description Constantes de alertas de mensajes
 */
module.exports = {
  NO_AUTENTICADO: 'Su sesión de usuario ha caducado.',
  CERRAR_SESION: '¿Desea cerrar sesión?',
  ELIMINAR_DOCUMENTO: '¿Desea eliminar el documento $1?',
  ELIMINAR_NEGOCIO: '¿Desea eliminar el negocio $1?',
  DOS_O_MAS_ARCHIVOS_ERRONEOS: 'hay $1 documentos con errores.',
  UN_ARCHIVO_ERRONEO: 'hay 1 documento con errores.',
  SIN_FACTURAS_SELECCIONADAS_PARA_MODIFICAR: 'No tiene facturas seleccionadas a modificar.',
  DOS_O_MAS_ARCHIVOS_NO_CUMPLEN_VALIDACION: "Se encontraron $1 documentos que no cumplen con la validaci\xF3n. \n    $br Corrija para proceder. $br$br $info Para visualizar detalle de las facturas con errores, \n    haga click en los iconos de estado de validaci\xF3n para desplegar informaci\xF3n.",
  UN_ARCHIVO_NO_CUMPLE_VALIDACION: "Se encontr\xF3 1 documento que no cumple con la validaci\xF3n. \n    $br Corrija para proceder. $br$br $info Para visualizar detalle de las facturas con errores, \n    haga click en los iconos de estado de validaci\xF3n para desplegar informaci\xF3n.",
  TODOS_LOS_ARCHIVOS_CUMPLEN_VALIDACION: '$check Todos los documentos cumplen con la validación.',
  NEGOCIO_REVISION_POR_EJECUTIVO: 'Su negocio está siendo revisado por su ejecutivo.',
  SELECCIONE_CERTIFICADO_PARA_NEGOCIO: 'Favor seleccione un certificado de la lista para asociar al Negocio en Curso.',
  CORREO_ACTUALIZADO: '$check Su correo ha sido actualizado exitosamente a $1.',
  DOCUMENTO_REEMPLAZO_EXITOSO: '$check Documento reemplazado exitosamente.',
  NOTA_CREDITO_DEBITO_CARGADA: '$check Nota de Crédito/Débido cargada exitosamente.',
  RECESION_EXITOSA: 'Operación realizada con éxito. $br Se enviará un email cuando la recesión sea completada.',
  SELECCIONE_FECHA_HABIL: 'Favor seleccione una fecha hábil.',
  VALIDAR_CAMPOS_REQUERIDOS: 'Ingrese los campos requeridos para continuar.',
  DESEA_ENVIAR_NEGOCIO: '¿Está seguro de Enviar el Negocio?',
  DESEA_ACEPTAR_COTIZACION: '¿Está seguro de Aceptar la Cotización?',
  FECHA_VENCIMENTO_FACTURAS_GUARDADAS: 'Fecha de vencimiento ha sido actualizada.',
  CERTIFICADO_POR_EXPIRAR: 'Certificado emitido para <strong>$1</strong> está próximo a expirar. $br Fecha de vencimiento: <strong>$2</strong>.',
  EMAIL_INVALIDO: 'E-Mail ingresado no es válido.',
  RUT_INVALIDO: 'RUT ingresado no es válido.',
  IFRAME_ERROR_CONFIG: 'Ha ocurrido un problema realizar configuración de iframe Receder'
};

},{}],12:[function(require,module,exports){
'use strict';
/**
 * @author Nicolas Andres Rivera Acevedo
 * @description Constantes de tipos de url
 */

module.exports = {
  NO_AUTENTICADO: 'no-autenticado',
  URL_LOGIN_SSO: 'https://login.factoringsecurity.cl/'
};

},{}],13:[function(require,module,exports){
"use strict";

/**
 * @description Constantes para los wizard
 */
module.exports = {
  NEGOCIO: 'fe',
  SOLO_CESION: 'sc'
};

},{}],14:[function(require,module,exports){
'use strict';
/**
 * @author Sinecio Bermúdez Jacque
 * @description directiva que permite dibujar un grafico circular de porcentaje
 */

module.exports = [function () {
  return {
    restrict: 'E',
    template: require('./template.tmpl'),
    scope: {
      totalAmount: '=',
      amount: '='
    },
    link: function link(scope, elem, attr) {
      var canvas = elem.find('canvas');
      var ctx = canvas.get(0).getContext('2d');
      var mW = canvas.width() / 2;
      var mH = canvas.height() / 2;
      var pAmount = 0;
      scope.$watch('totalAmount', init);
      scope.$watch('amount', init);

      function init() {
        limpiarCanvas();
        dibujarCirculoExterior();
        dibujarCirculoCentral();
        dibujarTexto();
      }

      function limpiarCanvas() {
        ctx.clearRect(0, 0, mW, mH);
      }

      function calcularPorcentaje() {
        var total = Number(scope.totalAmount) || 0;
        var amount = Number(scope.amount) || 0;
        return amount / total || 0;
      }

      function dibujarCirculoExterior() {
        var radio = mW * 1; // 100 porciento del ancho;

        ctx.save();
        ctx.translate(mW, mH);
        ctx.rotate(-90 * (Math.PI / 180));
        ctx.beginPath();
        ctx.fillStyle = '#1e1d75';
        ctx.arc(0, 0, radio, 0, 2 * Math.PI * calcularPorcentaje());
        ctx.lineTo(0, 0);
        ctx.fill();
        ctx.restore();
      }

      function dibujarCirculoCentral() {
        var radio = mW * .8; // 80 porciento del ancho;

        ctx.beginPath();
        ctx.fillStyle = '#980083';
        ctx.arc(mW, mH, radio, 0, 2 * Math.PI);
        ctx.fill();
      }

      function dibujarTexto() {
        var fontSize = mW * .6; // 60% del ancho;

        var fontFace = 'arial';
        var porcentaje = Math.ceil(calcularPorcentaje() * 100);
        porcentaje = porcentaje > 100 ? 100 : porcentaje;
        var texto = porcentaje + '%';
        ctx.save();
        ctx.translate(mW, mH);
        ctx.fillStyle = '#ffffff';
        ctx.textAlign = 'center';
        ctx.textBaseline = 'middle';
        ctx.font = fontSize + "px " + fontFace;
        ctx.fillText(texto, 0, 0);
        ctx.restore();
      }
    }
  };
}];

},{"./template.tmpl":15}],15:[function(require,module,exports){
module.exports = "<canvas width=139 height=139></canvas>";

},{}],16:[function(require,module,exports){
'use strict';
/**
 * @author Nicolas Andres Rivera Acevedo
 * @description indice de todas las directivas del modulo 'app.commons'
 */

angular.module('app.commons').directive('circleChart', require('./circle-chart/circle-chart')).directive('uxDatepicker', require('./ux-datepicker/ux-datepicker')).directive('uxRut', require('./ux-rut/ux-rut')).directive('uxDownload', require('./ux-download/ux-download')).directive('uxNumbers', require('./ux-numbers/ux-numbers')).directive('uxPagination', require('./ux-pagination/ux-pagination')).directive('uxTooltip', require('./ux-tooltip/ux-tooltip')).directive('uxValidar', require('./ux-validar/ux-validar'));

},{"./circle-chart/circle-chart":14,"./ux-datepicker/ux-datepicker":17,"./ux-download/ux-download":19,"./ux-numbers/ux-numbers":20,"./ux-pagination/ux-pagination":22,"./ux-rut/ux-rut":23,"./ux-tooltip/ux-tooltip":24,"./ux-validar/ux-validar":25}],17:[function(require,module,exports){
'use strict';

var _pikaday = _interopRequireDefault(require("pikaday"));

var _util = require("commons/util");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

/**
 * @author Sinecio Rodrigo Bermúdez Jacque
 * @description directiva input con calendario para selección de fecha,
 *      por defecto, al escribir una fecha erronea en el input, será cambiada por la fecha actual siempre y cuando
 *      la fecha esté en el rango de minDate y maxDate, de lo contrario se establecerá la fecha minima permitida según
 *      el rango establecido.
 * 
 * @param {String|Date} minDate permite establecer una fecha minima para seleccionar
 * @param {String|Date} maxDate permite establecer una fecha máxima para seleccionar
 * 
 * @example
 *  <input type="text" ux-datepicker min-date="minFecha" max-date="maxFecha"/>
 */
module.exports = ["$timeout", function (timeout) {
  return {
    require: 'ngModel',
    scope: {
      minDate: "=",
      maxDate: "=",
      pickerBlur: "&"
    },
    link: function link(scope, elem, attrs, ngModel) {
      var picker = new _pikaday["default"]({
        field: elem[0],
        minDate: (0, _util.date)(scope.minDate),
        maxDate: (0, _util.date)(scope.maxDate),
        firstDay: 1,
        format: 'DD/MM/YYYY',
        i18n: {
          previousMonth: 'Siguiente',
          nextMonth: 'Anterior',
          months: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
          weekdays: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
          weekdaysShort: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa']
        },
        onClose: function onClose() {
          scope.$parent.$apply(function () {
            setPickerDate();
            scope.pickerBlur();
          });
        }
      });

      var setPickerDate = function setPickerDate(event) {
        if (event && event.type == "keyup" && event.which != 13) return;

        if (typeof ngModel.$modelValue == "string") {
          picker.setDate((0, _util.date)(ngModel.$modelValue));
        } else {
          picker.setDate(ngModel.$modelValue);
        }
      };

      scope.$on('$destroy', function () {
        picker.destroy();
      });
      scope.$watch('minDate', function () {
        if (!scope.minDate) return;
        picker.setMinDate((0, _util.date)(scope.minDate));
      });
      scope.$watch('maxDate', function () {
        if (!scope.maxDate) return;
        picker.setMaxDate((0, _util.date)(scope.maxDate));
      });
      timeout(setPickerDate);
      elem.on("mousedown", setPickerDate);
      elem.on("keyup", setPickerDate);
    }
  };
}];

},{"commons/util":41,"pikaday":"pikaday"}],18:[function(require,module,exports){
module.exports = "<button class=btn-descarga ng-class=\"label ? 'btn-label ' + class : 'btn-icon'\" ng-click=descargar()><span>{{label}}</span> <i ng-class=claseIcono aria-hidden=true></i></button>";

},{}],19:[function(require,module,exports){
'use strict';

var _util = require("commons/util");

var _icons = _interopRequireDefault(require("commons/constants/icons"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

/**
 * @author Nicolas Andres Rivera Acevedo
 * @description Directiva para descarga de documentos
 */
module.exports = ['$documentoDescarga', '$popup', function (documentoDescarga, popup) {
  return {
    template: require('./template.tmpl'),
    replace: true,
    restrict: 'E',
    scope: {
      id: '=',
      icon: '@',
      tipo: '=',
      label: '@',
      "class": '@'
    },
    link: function link(scope, elem, attr) {
      scope.descargar = descargar;
      /**
       * @description Función inicial.
       */

      function init() {
        obtenerClaseIcono();
        verificarId();
      }
      /**
       * @description Función encargada de obtener el tipo de icono de documento.
       */


      function obtenerClaseIcono() {
        scope.claseIcono = _icons["default"][(scope.icon || '').toUpperCase()] || _icons["default"].DOWNLOAD;
      }
      /**
       * @description Función encargada de revisar las condiciones en función del
       * id para descarga.
       */


      function verificarId() {
        if (!scope.id) elem.remove();
      }
      /**
       * @description Función encargada de establecer el icono 'descargando'
       */


      function mostrarIconoDescargando() {
        scope.claseIcono = _icons["default"].DOWNLOADING;
      }
      /**
       * @description Función encargada de generar la petición de descarga.
       */


      function descargar() {
        mostrarIconoDescargando();
        documentoDescarga.get({
          ID: scope.id,
          TipoDocumento: scope.tipo
        }).then(function (resp) {
          if (resp.status != 200) {
            obtenerClaseIcono();
            popup.errorAlert(resp.data.Message);
            return;
          }

          generarDescarga(resp);
        });
      }
      /**
       * @description Función encargada de generar el archivos y descargarlo
       * luego de que se ha recibido respuesta desde el servidor.
       * @param {Object} resp respuesta desde el servidor.
       */


      function generarDescarga(resp) {
        if ((0, _util.esIe)()) {
          window.navigator.msSaveOrOpenBlob((0, _util.base64toBlob)(resp.data.Base64, resp.data.DataType), "".concat(resp.data.FileName, ".").concat(resp.data.Extension));
        } else {
          var elem = document.createElement('a');
          elem.setAttribute('href', "data:".concat(resp.data.DataType, ";base64,").concat(resp.data.Base64));
          elem.setAttribute('download', "".concat(resp.data.FileName, ".").concat(resp.data.Extension));
          elem.style.display = 'none';
          document.body.appendChild(elem);
          elem.click();
          document.body.removeChild(elem);
        }

        obtenerClaseIcono();
      }

      init();
    }
  };
}];

},{"./template.tmpl":18,"commons/constants/icons":10,"commons/util":41}],20:[function(require,module,exports){
'use strict';
/**
 * @author Nicolas Andres Rivera Acevedo
 * @description Directiva que permite parsear y formatear números agregando
 * puntos de miles a un input.
 */

module.exports = ['$browser', function (browser) {
  return {
    restrict: 'A',
    require: 'ngModel',
    link: function link(scope, elem, attr, ngModelCtrl) {
      ngModelCtrl.$parsers.push(function (viewValue) {
        return quitarLetras(viewValue);
      });

      ngModelCtrl.$render = function () {
        elem.val(quitarLetras(ngModelCtrl.$viewValue));
      };

      elem.bind('change', listener);
      elem.bind('keydown', function (event) {
        var key = event.keyCode; // If the keys include the CTRL, SHIFT, ALT, or META keys, or the arrow keys, do nothing.
        // This lets us support copy and paste too

        if (key == 91 || 15 < key && key < 19 || 37 <= key && key <= 40) return;
        browser.defer(listener); // Have to do this or changes don't get picked up properly
      });
      elem.bind('paste cut', function () {
        browser.defer(listener);
      });

      function listener() {
        elem.val(quitarLetras(elem.val()));
      }

      function quitarLetras(value) {
        return value.toString().replace(/[^\d]/g, "");
      }
    }
  };
}];

},{}],21:[function(require,module,exports){
module.exports = "<div class=\"row pagination-box\"><div class=\"d-inline pl-3\"><select class=\"custom-select br-3\" ng-model=registrosPorPagina ng-options=\"op as op for op in opcionesCantidadRegistros\" ng-change=cambiarRegistrosPorPagina()></select></div><nav class=\"d-inline pl-3\" aria-label=Paginación><ul class=pagination><li class=page-item><button class=\"page-link previous\" aria-label=Previous ng-click=primera() ng-disabled=\"ngModel.TotalPaginas <= 1\"><span><i class=fe-ico-angle-to-left></i></span></button></li><li class=page-item><button class=page-link aria-label=Previous ng-click=anterior() ng-disabled=\"indice == 1\"><span><i class=\"fa fa-angle-double-left\"></i></span></button></li><li class=page-item ng-class=\"{active: item.Active}\" ng-repeat=\"item in numerosPagina\"><button class=page-link ng-click=cargarPagina(item.Numero)>{{item.Numero}}</button></li><li class=page-item><button class=page-link aria-label=Next ng-click=siguiente() ng-disabled=\"indice == totalIndices\"><span><i class=\"fa fa-angle-double-right\"></i></span></button></li><li class=page-item><button class=\"page-link next\" aria-label=Next ng-click=ultima() ng-disabled=\"ngModel.Pagina >= ngModel.TotalPaginas\"><span><i class=fe-ico-angle-to-right></i></span></button></li></ul></nav></div>";

},{}],22:[function(require,module,exports){
'use strict';

var _lodash = require("lodash");

var _jquery = _interopRequireDefault(require("jquery"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

/**
 * @author Nicolas Andres Rivera Acevedo
 * @description Directiva de paginación y ordenamiento.
 * se debe agregar el modelo de Paginación en el controller de la vista (ver ejemplo js)
 * y en la vista se agrega la directiva indicando la función que realiza la llamada de los
 * datos paginados.
 * 
 * @param {Function} load función que realiza llamada a los datos paginados.
 * @param {Object} ngModel objeto modelo que mantiene los parámetros de paginación.
 * 
 * @example
 *      js:
 *      scope.Paginacion = {
 *          Pagina			: 1,     
 *          CampoOrden		: '',     
 *          Descendente		: false
 *      };
 * 
 *      scope.obtenerDatos = function(){
 *          var params = {
 *              Rut: '16146432k'
 *          };
 *          angular.extend(params, scop.Paginacion);    //agrega información de Paginación a los parámetros
 *          service.obtenerDatos(params).then(function(resp){
 *              scope.Datos = resp.Datos;
 *              //actualiza los datos de paginación con la información del backend:
 *              angular.extend(scope.Paginacion, resp.Paginacion);
 *          });
 *      }
 *      
 *      view:
 *      <ux-pagination ng-model="Paginacion" load="obtenerDatos()"></ux-pagination>
 */
module.exports = ['$constantes', function (constantes) {
  var PAGINACION = constantes.obtener().PAGINACION;
  var CANTIDAD_NUMEROS_PAGINA = 5; //cantidad de números de páginas visibles en la Paginación.

  var OPCIONES_REGISTROS_POR_PAGINA = "5,10,20,50,100"; //cantidad de registros por página.

  var REGISTROS_POR_PAGINA = 10; //registros por página por defecto, debe ser uno de las opciones de la lista anterior.

  return {
    restrict: 'E',
    replace: true,
    template: require('./template.tmpl'),
    scope: {
      load: '&',
      ngModel: '='
    },
    link: function link(scope, elem, attr) {
      /**
       * @description variables para el comportamiento de la colección de botones
       * que representan las Páginas
       */
      var paginas = []; //colección de páginas generadas como objeto (revisar función generarPaginas).

      /**
       * @description cada vez que ocurre un cambio en el modelo de paginación
       * se debe calcular la paginación, es decir, calcular las páginas visibles
       * en función de lo que se informe, para esto es muy importante el atributo TotalPaginas.
       */

      scope.$watch('ngModel', function (modelValue) {
        if (modelValue == null) return;
        verificarAtributos();
        calcularPaginacion();
      }, true);
      /**
       * @description variables para utilizar en el template de la directiva
       */

      scope.opcionesCantidadRegistros = obtenerOpcionesCantidadRegistros();
      scope.registrosPorPagina = Number(REGISTROS_POR_PAGINA) || 0;
      scope.primera = primera;
      scope.anterior = anterior;
      scope.siguiente = siguiente;
      scope.ultima = ultima;
      scope.cargarPagina = cargarPagina;
      scope.cambiarRegistrosPorPagina = cambiarRegistrosPorPagina;
      /**
       * @description ordenamiento
       */

      elem.parent().find('[orden]').on('click', function (e) {
        var header = (0, _jquery["default"])(this);
        var sort = header.attr('sort') || 'asc';
        if (header.is(scope.currentOrder)) sort = sort == 'asc' ? 'desc' : 'asc';
        scope.currentOrder = header;
        header.attr('sort', sort);
        scope.ngModel.CampoOrden = header.attr('orden');
        scope.ngModel.Descendente = sort == 'desc';
        marcarOrdenamiento.bind(this)();
        scope.$apply(scope.load);
      });
      /**
       * @description marca el ordenamiento ascendente/descendente actual
       */

      function marcarOrdenamiento() {
        if ((0, _jquery["default"])(this).attr('orden') == scope.ngModel.CampoOrden) {
          scope.currentOrder = (0, _jquery["default"])(this);
          scope.currentOrder.siblings().removeClass('asc').removeClass('desc');

          if (scope.ngModel.Descendente) {
            scope.currentOrder.removeClass('asc').addClass('desc');
            scope.currentOrder.attr('sort', 'desc');
          } else {
            scope.currentOrder.removeClass('desc').addClass('asc');
            scope.currentOrder.attr('sort', 'asc');
          }
        }
      }
      /**
       * @description Función que genera la colección de opciones
       * de registros por página.
       */


      function obtenerOpcionesCantidadRegistros() {
        return (0, _lodash.map)(OPCIONES_REGISTROS_POR_PAGINA.split(','), function (num) {
          return Number(num);
        });
      }
      /**
       * @author Mauricio Ross
       * @description Function que permite navegar hasta la primera pagina.
       */


      function primera() {
        cargarPagina(1);
      }
      /**
       * @description Función que permite navegar a la página anterior a la actual,
       * si la página anterior está fuera de las páginas visibles, se rebaja el indice
       * y se generan nuevamente los números de página.
       */


      function anterior() {
        if (scope.numerosPagina[0].Numero <= 1) return;
        scope.indice--;
        obtenerNumerosPagina();
      }
      /**
       * @description Función que permite navegar a la página siguiente a la actual,
       * si la página siguiente está fuera de las páginas visibles, se aumenta el indice
       * y se generan nuevamente los números de página.
       */


      function siguiente() {
        if (scope.numerosPagina[scope.numerosPagina.length - 1].Numero >= scope.ngModel.TotalPaginas) return;
        scope.indice++;
        obtenerNumerosPagina();
      }
      /**
       * @author Mauricio Ross
       * @description Función que permite navegar hasta la ultima pagina.
       */


      function ultima() {
        cargarPagina(scope.ngModel.TotalPaginas);
      }
      /**
       * @description Función que permite navegar a una página específica,
       * se actualiza el modelo de paginación con el núevo número de página
       * y se realiza la llamada a la función que va a buscar los datos páginados.
       * @param {Number} nro Número de página a cargar.
       */


      function cargarPagina(nro) {
        scope.ngModel.Pagina = nro;
        scope.load();
      }
      /**
       * @description Función que se encaga de cambiar los registros por página,
       * esta función se gatilla cada vez que hay un cambio en la opción seleccionada
       * del select 'Ver de a' en el template y realiza la llamada a la función 
       * que va a buscar los datos páginados.
       */


      function cambiarRegistrosPorPagina() {
        scope.ngModel.Registros = scope.registrosPorPagina;
        scope.ngModel.Pagina = 1;
        scope.load();
      }
      /**
       * @description Función encargada de revisar los atributos del modelo de paginación
       * si uno de ellos no existe o no es compatible, se debe generar correctamente.
       */


      function verificarAtributos() {
        if (!scope.ngModel.TotalPaginas) scope.ngModel.TotalPaginas = 1;
      }
      /**
       * @description Función que se encarga de orquestar las funciones principales
       * para generar el comportamiento de navegación entre las páginas disponibles.
       */


      function calcularPaginacion() {
        generarPaginas();
        obtenerIndice();
        obtenerNumerosPagina();
      }
      /**
       * @description Función que genera un subconjunto de páginas para ser listadas según
       * la constante de cantidad de números de páginas (CANTIDAD_NUMEROS_PAGINA)
       */


      function obtenerNumerosPagina() {
        scope.numerosPagina = paginas.slice(scope.indice * CANTIDAD_NUMEROS_PAGINA - CANTIDAD_NUMEROS_PAGINA, scope.indice * CANTIDAD_NUMEROS_PAGINA);
      }
      /**
       * @description Función que calcula los indices necesarios para el comportamiento
       * de subconjunto de páginas.
       * @example 
       *      supongamos el siguiente caso:
       *      paginas: [1,2,3,4,5,6];
       *      paginas visibles: 3;
       *      subconjuntos disponibles: 2 ([1,2,3] [4,5,6])
       *      página actual: 4;
       * 
       *      el indice sería 2, ya que la página 4 se encuentra en el segundo subconjunto [4,5,6]
       */


      function obtenerIndice() {
        scope.indice = Math.ceil(scope.ngModel.Pagina / CANTIDAD_NUMEROS_PAGINA);
        scope.totalIndices = Math.ceil(scope.ngModel.TotalPaginas / CANTIDAD_NUMEROS_PAGINA);
      }
      /**
       * @description Función que se encarga de generar una colección de objetos que
       * representan los botones de cada página.
       * Los objetos tienen la estructura: 
       * {
       *      Numero: Number
       *      Active: Boolean
       * }
       * el atributo Active será true cuando el número de página (i+1) coincida con la página
       * actual del modelo.
       */


      function generarPaginas() {
        paginas = [];

        for (var i = 0; i < scope.ngModel.TotalPaginas; i++) {
          paginas.push({
            Numero: i + 1,
            //Número de Página
            Active: i + 1 == scope.ngModel.Pagina //Activa cuando es la página actual del modelo

          });
        }
      }
    }
  };
}];

},{"./template.tmpl":21,"jquery":"jquery","lodash":"lodash"}],23:[function(require,module,exports){
'use strict';
/**
 * @author Nicolas Andres Rivera Acevedo
 * @description Directiva que permite parsear y formatear agregando
 * puntos de miles y guión a un input para representar un Rut Formateado de la siguiente forma
 * 99.999.999-K
 */

module.exports = ['$browser', function (browser) {
  return {
    restrict: 'A',
    require: 'ngModel',
    link: function link(scope, elem, attr, ngModelCtrl) {
      ngModelCtrl.$parsers.push(function (viewValue) {
        return limpiarEntrada(viewValue);
      });

      ngModelCtrl.$render = function () {
        elem.val(agregarMiles(ngModelCtrl.$viewValue));
      };

      elem.bind('change', listener);
      elem.bind('keydown', function (event) {
        var key = event.keyCode; // If the keys include the CTRL, SHIFT, ALT, or META keys, or the arrow keys, do nothing.
        // This lets us support copy and paste too

        if (key == 91 || 15 < key && key < 19 || 37 <= key && key <= 40) return;
        browser.defer(listener); // Have to do this or changes don't get picked up properly
      });
      elem.bind('paste cut', function () {
        browser.defer(listener);
      });

      function listener() {
        var value = limpiarEntrada(elem.val());
        elem.val(agregarMiles(value));
      }

      function limpiarEntrada(value) {
        return value.toString().replace(/[^(\d|k)]/ig, '');
      }

      function agregarMiles(value) {
        value = (value || '').toString();
        var d = value[value.length - 1];
        if (!value.length) return value;
        value = value.substr(0, value.length - 1).replace(/\B(?=(\d{3})+(?!\d))/g, ".");
        return value.length >= 1 ? value + '-' + d : d;
      }
    }
  };
}];

},{}],24:[function(require,module,exports){
'use strict';
/**
 * @author Mauricio Ross
 * @description Directive para tooltip que consume tooltip de bootstrap, por lo tanto puede consumir todos los recursos de este componente en bootstrap.
 * @example 
 * <button type="button" ux-tooltip class="btn btn-secondary"  title="Tooltip on top">
                Tooltip on top
              </button>
 */

module.exports = [function () {
  return {
    restrict: 'A',
    scope: {},
    link: function link(scope, elem, attr) {
      elem.tooltip({
        container: 'body'
      });
      elem.on('mousedown', function (e) {
        elem.tooltip('hide');
      });
    }
  };
}];

},{}],25:[function(require,module,exports){
'use strict';

var _jquery = _interopRequireDefault(require("jquery"));

var _lodash = require("lodash");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

/**
 * @author Sinecio Bermúdez Jacque
 * @description directiva que permite validar campos en la vista.
 * Provee la función `validar()` al controller de la vista donde está inserta la directiva.
 * Esta función permite lanzar el proceso de validación retornando true cuando los campos son válidos y false cuando no lo son.
 * además levanta una alerta indicando el mensaje de validación. 
 * 
 * El mensaje génerico `uxValidar.obtenerMensajeDefecto()` es mostrado cuando hay campos requeridos a los que no se les ha ingresado
 * un valor, cualquier otro mensaje será mostrado si un campo tiene una validación personalizada.
 * 
 * validar tiene la función `noValidar()` que permite indicarle a la directiva que se iniciará un proceso de limpieza
 * de campos.
 * 
 * @example
 * 
 *  validación simple
 *  *****************
 *  valida que los campos requeridos tengan un valor ingresado:
 * 
 *  html:
 *  <div ux-validar>
 *      <input type="text" ng-model="campoRequerido" required/>
 *  </div>
 *  <button ng-click="enviar()">Enviar</button>
 * 
 *  js:
 *  scope.enviar = function(){
 *      if (!scope.validar()) return;
 *  }
 * 
 *  validación por contextos
 *  ************************
 *  valida que los campos agrupados en el contexto tengan un valor ingresado
 * 
 *  html
 *  <div ux-validar>
 *      <div ux-validar-contexto="parte1">
 *          <input type="text" ng-model="campoRequerido1" required/>
 *      </div>
 *      <div ux-validar-contexto="parte2">
 *          <input type="text" ng-model="campoRequerido2" required/>
 *      </div>
 *  </div>
 *  <button ng-click="guardar()">Guardar</button>
 *  <button ng-click="enviar()">Enviar</button>
 * 
 *  js:
 *  scope.guardar = function(){
 *      //valida que los campos en el contexto parte1 tengan un valor ingresado
 *      if (!scope.validar('parte1')) return;
 *  }
 *  scope.enviar = function(){
 *      //valida que los campos en el contexto parte2 tengan un valor ingresado
 *      if (!scope.validar('parte2')) return;
 * 
 *      //valida que todos los campos tengan un valor ingresado (sin importar contexto)
 *      if (!scope.validar()) return;
 *  }
 * 
 *  validación personalizada
 *  ************************
 *  agrega validación adicional a campos especiales como por ejemplo un email.
 *  En este caso tenemos un tipo indicado por el atributo `ux-validar-tipo`
 *  si el tipo está definido en el controller se utilizará ese tipo, si el tipo
 *  es un string, se utilizará la validación coincidente ya definida en la directiva,
 *  si es una función se utilizará esta función para validar el campo.
 * 
 *  html:
 *  <div ux-validar>
 *      <input type="text" ng-model="campoEmail1" required ux-validar-tipo="email"/>
 *      <input type="text" ng-model="campoEmail2" required ux-validar-tipo="tipoEmail"/>
 *  </div>
 *  <button ng-click="enviar()">Enviar</button>
 * 
 *  js:
 *  scope.enviar = function(){
 *      if (!scope.validar()) return;
 *  }
 * 
 *  js con definición para el tipo email:
 *  scope.email = function(value){
 *      //value es el valor ingresado en el campo

 *      return 'El email ingresado no es válido'; //retornado un string con el mensaje, determina que el campo es inválido.
 *      //return false; //retornado false determina que el campo es inválido, pero no agrega un mensaje informativo.
 *      //return true;  //retornando true el campo es válido.
 *  }
 *  
 *  //también puede ser un string
 *  scope.tipoEmail = 'email';  //en este caso se ejecuta la validación coincidente con 'email' definida en la directiva
 *  scope.enviar = function(){
 *      if (!scope.validar()) return;
 *  }
 * 
 *  Limpiar Campos Requeridos o con validaciones de tipo
 *  ****************************************************
 * 
 *  js:
 * 
 *  scope.noValidar = function(){
 *      scope.validar.noValidar();  //indica a la directiva que no inicie un proceso de validación
 *      scope.campoEmail1 = null;
 *      scope.campoEmail2 = null;
 *  }
 * 
 */
module.exports = ['$parse', '$popup', '$timeout', '$uxValidar', function (parse, popup, timeout, uxValidar) {
  return {
    restrict: 'A',
    link: function link(scope, elem) {
      /**
       * @description colección de elementos invalidos
       */
      var invalidos = [];
      var contexto = null;
      var trigger = true;
      /**
       * @description función inicial
       */

      function init() {
        verificarAtributos();
        agregrarEvento();
        observarNodo();
      }
      /**
       * @description función encargada de verificar que un input tenga una entrada de texto
       * @param {Object} elem jquery object
       */


      function verificarInput(elem) {
        if (elem.val().length == 0) invalidos.push(elem.addClass('invalid'));
      }
      /**
       * @description función encargada de verificar que un select tenga un elemento seleccionado
       * @param {Object} elem jquery object
       */


      function verificarSelect(elem) {
        if (!Number(elem.val().split(':')[1]) || elem.val() == '?') invalidos.push(elem.addClass('invalid'));
      }
      /**
       * @description función encargada de verificar si un elemento ya existe en la colección de invalidos
       * (para no agregarlo nuevamente)
       * @param {Object} elem jquery object
       */


      function existeInvalido(elem) {
        return !!(0, _lodash.find)(invalidos, function (item) {
          return item.is(elem);
        });
      }
      /**
       * @description función encargada de verificar que un elemento tenga ingresado o seleccionado un valor
       * @param {Object} elem jquery object
       */


      function verificarEntrada(elem) {
        if (elem.is('input[type=text]')) verificarInput(elem);else if (elem.is('input[type=password]')) verificarInput(elem);else if (elem.is('select')) verificarSelect(elem);
      }
      /**
       * @description función encargada de validar la entrada de un campo.
       * Esta función ejecuta validaciones por defecto predefinidas en `tipoValidacion`.
       * si la validación indicada en el atributo `ux-validar-tipo` es una función definida el controller, 
       * esa función será ejecutada para determinar la validez de la entrada, si es un string literal
       * se ejecuta la validación coincidente predefinida en `tipoValidacion` 
       * @param {Object} elem jquery element a validar
       */


      function validarEntrada(elem) {
        var tipo = elem.attr('ux-validar-tipo');
        if (typeof tipo == 'undefined') return;
        var val = null;
        var invalid = existeInvalido(elem);
        tipo = parse(tipo)(scope) || tipo;

        if (typeof tipo == 'function') {
          val = tipo(elem.val());
        } else {
          val = uxValidar.obtenerTipo(tipo)(elem.val());
        }

        if (typeof val == 'string' && !invalid) invalidos.push(elem.addClass('invalid').attr('ux-validar-msg', val));else if (!val && !invalid) invalidos.push(elem.addClass('invalid'));else if (!invalid) limpiarInvalidos(elem);
      }
      /**
       * @description función encargada de revisar los atributos inciales de la directiva,
       * estos atributos deben estar establecidos, ante de ejecutar cualquier otra acción.
       */


      function verificarAtributos() {
        //verificar el atributo ux-validar para determinar el scope donde registrar la función validar
        var parent = elem.attr('ux-validar') && parse(elem.attr('ux-validar'))(scope) || scope;
        validar.noValidar = noValidar;
        parent.validar = validar;
      }
      /**
       * @description Función encargada de observar el nodo de la directiva para agregar
       * eventos a elementos que puedan ser agregados dinamicamente.
       */


      function observarNodo() {
        scope.$watch(function () {
          return elem.get(0).childNodes.length;
        }, agregrarEvento);
      }
      /**
       * @description función encargada de agregar validación en evento blur a los elementos
       * con atributo `ux-validar-tipo`
       */


      function agregrarEvento() {
        elem.find('[required],[ux-validar-tipo]').each(function () {
          var field = (0, _jquery["default"])(this);
          var initial = true;
          if (field.is('.ux-validar-bound')) return;else field.addClass('ux-validar-bound');
          scope.$watch(function () {
            return parse(field.attr('ng-model'))(scope);
          }, function (val) {
            if (initial) {
              initial = !initial;
            } else {
              if (trigger) timeout(task);else timeout(function () {
                trigger = true;
                scope.$digest();
              });
            }
          });

          function task() {
            limpiarInvalidos(field);
            verificarEntrada(field);
            validarEntrada(field);
          }
        });
      }
      /**
       * @description función encargada de obtener el contexto de validación.
       * Esta función establece la variable `contexto` como el elemento en el cual se verificarán
       * los campos a validar.
       * @param {String} ctx representa el contexto a buscar. El contexto es todo elemento
       * que tenga el atributo `ux-validar-contexto` con el valor coincidente con este parámetro `ctx`
       */


      function obtenerContexto(ctx) {
        contexto = elem.find("[ux-validar-contexto=".concat(ctx, "]"));
        contexto = contexto.length > 0 ? contexto : elem;
      }
      /**
       * @description función encargada de iterar los elementos del contexto requeridos a ser verificados
       * con un valor ingresado.
       */


      function verificarRequeridos() {
        contexto.find('[required]:enabled').each(function () {
          verificarEntrada((0, _jquery["default"])(this));
        });
      }
      /**
       * @description función encargada de iterar los elementos del contexto para validar su valor ingresado
       */


      function validarTipos() {
        contexto.find('[ux-validar-tipo]').each(function () {
          validarEntrada((0, _jquery["default"])(this));
        });
      }
      /**
       * @description función encargada de limpiar colección y marcas de inválidos
       * @param {Object} obj jquery object
       */


      function limpiarInvalidos(obj) {
        (obj || elem.find('[required],[ux-validar-tipo]')).removeClass('invalid').removeAttr('ux-validar-msg');

        if (obj) {
          (0, _lodash.remove)(invalidos, function (item) {
            return item.is(obj);
          });
        } else {
          invalidos = [];
        }
      }
      /**
       * @description función encargada de gatillar la validación
       * esta será ejecutada desde el controller de la vista que
       * implemente esta directiva
       * @param {String} contexto determina el contexto de la validación. El contexto
       * es determinado con el atributo `ux-validar-contexto` en el elemento hijo
       * del elemento que implemente la directiva `ux-validar`
       */


      function validar(contexto) {
        limpiarInvalidos();
        obtenerContexto(contexto);
        verificarRequeridos();
        validarTipos();

        if (invalidos.length > 0) {
          invalidos[0].focus();
          popup.warnAlert(invalidos[0].attr('ux-validar-msg') || uxValidar.obtenerMensajeDefecto());
          return false;
        }

        return true;
      }
      /**
       * @description Función que permite indicar a la directiva que se iniciará un
       * proceso de limpieza de campos y estos no deben ser considerados como invalidos
       * si estos tienen valor `vacio`
       */


      function noValidar() {
        trigger = false;
      }

      init();
    }
  };
}];

},{"jquery":"jquery","lodash":"lodash"}],26:[function(require,module,exports){
'use strict';
/**
 * @author Mauricio Ross Arevalo
 * @description filtro que recibe un input y aplica formato de miles
 * @example input=100200300  return 100.200.300
 */

module.exports = function () {
  return function (input) {
    if (input == null) return input;
    return input.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
  };
};

},{}],27:[function(require,module,exports){
"use strict";
'user strict';
/**
 * @author Nicolas Andres Rivera Acevedo
 * @description indice de todos los filtros del modulo 'app.commons'
 */

angular.module('app.commons').filter('formatMiles', require('./format-miles')).filter('kb', require('./kb')).filter('rut', require('./rut')).filter('porcentaje', require('./porcentaje'));

},{"./format-miles":26,"./kb":28,"./porcentaje":29,"./rut":30}],28:[function(require,module,exports){
'use strict';
/**
 * @author Nicolas Andres Rivera Acevedo
 * @description transforma un input bytes en kb
 * @example 
 *      {{input | kb}}
 */

module.exports = function () {
  return function (input) {
    if (input == null) return input;
    input = Number(input) || 0;
    return Math.ceil(input / 1024) + ' kb';
  };
};

},{}],29:[function(require,module,exports){
'use strict';
/**
 * @author Nicolas Andres Rivera Acevedo
 * @description transforma un input decimal a porcentaje
 * @example 
 *      {{input | porcentaje}}
 */

module.exports = function () {
  return function (input) {
    if (input == null) return input;
    input = Number(input) || 0;
    return (input * 100).toFixed(4) + '%';
  };
};

},{}],30:[function(require,module,exports){
'use strict';
/**
 * @author Mauricio Ross Arevalo
 * @description filtro que recibe un input y aplica formato de rut
 * @example input=123456789  return 12.345.678-9
 */

module.exports = function () {
  return function (input) {
    if (input == null) return input;
    input = input.toString().replace(/[\.-]/g, '');
    return input.slice(0, -1).replace(/\B(?=(\d{3})+(?!\d))/g, '.') + '-' + input[input.length - 1];
  };
};

},{}],31:[function(require,module,exports){
"use strict";
'user strict';
/**
 * @author Nicolas Andres Rivera Acevedo
 * @description Definición del submodulo app.commons
 *              En este modulo debe ir todo lo transversal a los demás modulos
 */

var moduleName = 'app.commons';
module.exports = moduleName;
angular.module(moduleName, [
/**
 * dependencias:
 */
require('@ns-angularjs/validate'), require('@ns-angularjs/modal')]).config(require('./config'));

require('./directives');

require('./services');

require('./providers');

require('./filters');

},{"./config":4,"./directives":16,"./filters":27,"./providers":32,"./services":34,"@ns-angularjs/modal":"@ns-angularjs/modal","@ns-angularjs/validate":"@ns-angularjs/validate"}],32:[function(require,module,exports){
"use strict";
'user strict';
/**
 * @author Sinecio Rodrigo Bermúdez Jacque
 * @description indice de todos los providers del modulo 'app.commons'
 */

angular.module('app.commons').provider('$uxValidar', require('./ux-validar'));

},{"./ux-validar":33}],33:[function(require,module,exports){
'use strict';
/**
 * @author Sinecio Bermúdez Jacque
 * @description Provider para configuración de la directiva de validaciones
 */

module.exports = function () {
  /**
   * @description Atributos privados
   */
  var _validaciones = {}; //contiene un mapa de las validaciones que puede utilizar la directiva ux-validar.

  var _msjValidacion = null; //contiene el mensaje de validación por defecto.

  /**
   * @description Funciones disponibles en el config.
   */

  this.mensajeDefecto = mensajeDefecto;
  this.validacion = validacion;
  /**
   * @description Función de instancia del factory.
   */

  this.$get = get;
  /**
   * @description Factory disponible para utilizar como '$uxValidar'
   */

  function get() {
    return {
      obtenerTipo: obtenerTipo,
      obtenerMensajeDefecto: obtenerMensajeDefecto
    };
  }
  /**
   * @description Función encargada de informar la función que realiza la validación
   * para el tipo dado por `tipo`.
   * @param {String} tipo tipo de validación a obtener
   */


  function obtenerTipo(tipo) {
    tipo = String(tipo).toLowerCase();
    return _validaciones[tipo] || angular.noop();
  }
  /**
   * @description Función encargada de informar el mensaje por defecto de validación
   * establecido en la configuración.
   */


  function obtenerMensajeDefecto() {
    return _msjValidacion;
  }
  /**
   * @description Función encargada de establecer el mensaje por defecto
   * como resultado de una validación.
   * @param {Strig} msj string que representa el mensaje por defecto a utilizar
   * para los campos marcados como requeridos y no son validos.
   */


  function mensajeDefecto(msj) {
    _msjValidacion = msj;
    return this;
  }
  /**
   * @description Función encargada de agregar tipos de validaciones
   * a utilizar en los campos marcados con el atributo `ux-validar-tipo`
   * @param {String} tipo tipo de validación, este es el tipo que se utiliza con
   * el atributo `ux-validar-tipo` en los campos a validar
   * @param {Function} fn función que ejecuta la validación
   */


  function validacion(tipo, fn) {
    tipo = String(tipo).toLowerCase();
    var validacion = {};
    validacion[tipo] = fn;
    angular.extend(_validaciones, validacion);
    return this;
  }
};

},{}],34:[function(require,module,exports){
"use strict";
'user strict';
/**
 * @author Sinecio Rodrigo Bermúdez Jacque
 * @description agrupación de todos los servicios del modulo 'app.commons'
 */

angular.module('app.commons').factory('$popup', require('./popup/popup'));

},{"./popup/popup":38}],35:[function(require,module,exports){
module.exports = "<div><div class=\"alert alert-success mb-0 p-3\"><h6 class=modal-title><i class=\"fa fa-exclamation-circle pr-2\" aria-hidden=true></i> Mensaje</h6></div><div class=modal-body ng-bind-html=mensaje></div><div class=modal-footer><button ng-click=volver() class=\"btn btn-primary\">Aceptar</button></div></div>";

},{}],36:[function(require,module,exports){
module.exports = "<div><div class=\"alert alert-confirm mb-0 p-3\"><h6 class=modal-title><i class=\"fa fa-question-circle pr-2\"></i> Confirme</h6></div><div class=modal-body ng-bind-html=mensaje></div><div class=modal-footer><button ng-click=aceptar() class=\"btn btn-primary\">Aceptar</button> <button ng-click=cancelar() class=\"btn btn-secondary\">Cancelar</button></div></div>";

},{}],37:[function(require,module,exports){
module.exports = "<div><div class=\"alert alert-danger mb-0 p-3\"><h6 class=modal-title><i class=\"fa fa-times-circle pr-2\" aria-hidden=true></i>Ha ocurrido un error</h6></div><div class=modal-body ng-bind-html=mensaje></div><div class=modal-footer><button ng-click=volver() class=\"btn btn-primary\">Aceptar</button></div></div>";

},{}],38:[function(require,module,exports){
"use strict";

/**
 * @author Sinecio Rodrigo Bermúdez Jacque
 * @description Servicio de utilidad, contiene 
 *              una colección de funciones para alertas y modales
 */
module.exports = ['$uibModal', '$auditoria', function (_modal, auditoria) {
  /**
   * @description Función encargada de intercambiar palabras claves por
   * elementos de html
   * @param {String} input string de entrada con palabras claves de iconos:
   * 		$warn  	: icono de triangulo de warning
   * 		$info	: icono de circulo de información
   * 		$times 	: icono de x de error
   * 		$check	: icono de un tick en un circulo verde
   */
  function normalizeEntities(input) {
    return input.replace(/\$warn/g, '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>').replace(/\$info/g, '<i class="fa fa-info-circle" aria-hidden="true"></i>').replace(/\$times/g, '<i class="fa fa-times-circle" aria-hidden="true"></i>').replace(/\$check/g, '<i class="fa fa-check-circle" aria-hidden="true"></i>').replace(/\$br/g, '<br/>');
  }

  return {
    /**
     * @description Permite mostrar un mensaje de alerta
     * @param {object} params  objeto con los parámetros:
     * @param {string} params.message mensaje a mostrar
     * @param {function} params.onAccept función callback a ejectuar al presionar 'aceptar'
     * @param {string} params.template plantilla a utilizar.
     * @param {function} params.promise funcion callback que informa la promesa a resolver antes de aceptar (opcional)
     */
    alert: function alert(params, cb, template) {
      if (params == null) params = 'Mensaje no definido';

      if (typeof params == 'string') {
        params = {
          message: normalizeEntities(params)
        };
        if (typeof cb == 'function') params.onAccept = cb;
      }

      _modal.open({
        template: params.template || template || require('./alert-template.tmpl'),
        windowClass: 'modal-alert',
        size: 'md',
        controller: ['$scope', '$uibModalInstance', function ($scope, $uibModalInstance) {
          $scope.mensaje = normalizeEntities(params.message);

          $scope.volver = function () {
            if (typeof params.promise == 'function') {
              params.promise($scope).then(function (resp) {
                $uibModalInstance.close(resp);
              });
            } else {
              $uibModalInstance.close($scope);
            }
          };
        }]
      }).result.then(function (res) {
        if (typeof params.onAccept == 'function') params.onAccept(res);
      });
    },

    /**
     * @description Permite mostrar un mensaje para confirmar
     * @param {object} params objeto con los parámetros:
     * @param {string} params.message mensaje a mostrar,
     * @param {function} params.onAccept función callback a ejectuar al presionar 'aceptar'
     * @param {function} params.onCancel función callback a ejecutar al presionar 'cancelar',
     * @param {string} params.template plantilla a utilizar.
     * @param {function} params.promise funcion callback que informa la promesa a resolver antes de aceptar (opcional)
     */
    confirm: function confirm(params, cbaccept, cbcancel, template) {
      if (params == null) params = 'Mensaje no definido';

      if (typeof params == 'string') {
        params = {
          message: normalizeEntities(params)
        };
        if (typeof cbaccept == 'function') params.onAccept = cbaccept;
        if (typeof cbcancel == 'function') params.onCancel = cbcancel;
      }

      _modal.open({
        template: params.template || template || require('./confirm-template.tmpl'),
        windowClass: 'modal-alert',
        size: 'md',
        controller: ['$scope', '$uibModalInstance', function ($scope, $uibModalInstance) {
          $scope.mensaje = normalizeEntities(params.message);

          $scope.aceptar = function () {
            if (typeof params.promise == 'function') {
              params.promise($scope).then(function (resp) {
                $uibModalInstance.close(resp);
              });
            } else {
              $uibModalInstance.close($scope);
            }
          };

          $scope.cancelar = function () {
            $uibModalInstance.dismiss($scope);
          };
        }]
      }).result.then(function (res) {
        if (typeof params.onAccept == 'function') params.onAccept(res);
      }, function (res) {
        if (typeof params.onCancel == 'function') params.onCancel(res);
      });
    },

    /**
     * @description Permite levantar modal
     * @param {Object} config    objeto literal con los parámetros:
     * 
     * @param {String} config.view string que reprenta la ruta de la vista (relativa a src)
     * @param {String} config.controller array con inyección de dependecias y función del controller  
     * @param {Function} config.onClose callback de funcion close de modalInstance	
     * @param {Function} config.onDismiss callback de funcion dismiss de modalInstance	
     * @param {Object} config.params mapa de parámetros para el controller. Son obtenidos en el controller de la modal inyectando '$uibModalParams'
     * @param {String} config.path path que sirve de referencia en el log de auditoria.
     * 			   	 	
     * @example
     *	{
     *		view: 'modules/.../vista.html',
     *		controller: 'controller',
     *      name: 'editar contacto'	//se transforma en /modal-editar-contacto en log de auditoria
     *		params: {
     *			p1: 1
     *			p2: 'string',
     *			p3: {},
     *			p4: function() {...}
     *		},
     *		onClose: function(res){
     *			console.log(res)
     *		},
     *		onDismiss: function(res){
     *			console.log(res)
     *		}
     *	} 
     * 
     * @return {object}           promise
     */
    modal: function modal(config) {
      if (config.name) auditoria.setPath('modal-' + config.name || '');

      var instance = _modal.open({
        template: config.view,
        windowClass: config.size || 'large',
        controller: config.controller,
        resolve: {
          '$uibModalParams': function $uibModalParams() {
            return config.params;
          }
        }
      });

      instance.result.then(function (params) {
        if (typeof config.onClose == 'function') config.onClose(params);
      }, function (params) {
        if (typeof config.onDismiss == 'function') config.onDismiss(params);
      })["finally"](function () {
        auditoria.clearPath();
      });
      return instance.result;
    },

    /**
     * @description Extensión de función alert para mensajes de errores
     * @param {String} msj mensaje de alerta
     * @param {Function} cb callback para el botón aceptar
     */
    errorAlert: function errorAlert(msj, cb) {
      this.alert(msj, cb, require('./error-alert-template.tmpl'));
    },

    /**
     * @description Extensión de función alert para mensajes de warning
     * @param {String} msj mensaje de alerta
     * @param {Function} cb callback para el botón aceptar
     */
    warnAlert: function warnAlert(msj, cb) {
      this.alert(msj, cb, require('./warn-alert-template.tmpl'));
    }
  };
}];

},{"./alert-template.tmpl":35,"./confirm-template.tmpl":36,"./error-alert-template.tmpl":37,"./warn-alert-template.tmpl":39}],39:[function(require,module,exports){
module.exports = "<div><div class=\"alert alert-warning mb-0 p-3\"><h6 class=modal-title><i class=\"fa fa-exclamation-triangle pr-2\"></i> Mensaje</h6></div><div class=modal-body ng-bind-html=mensaje></div><div class=modal-footer><button ng-click=volver() class=\"btn btn-primary\">Aceptar</button></div></div>";

},{}],40:[function(require,module,exports){
"use strict";

/**
 * @author Nicolas Andres Rivera Acevedo
 * @description Función encargada de transformar un string de base64 a blob,
 * esta función está escrita para trabajar con archivos transportados como base64
 * @param {String} b64Data String con datos en base64
 * @param {String} contentType String que representa el tipo de archivo
 */
module.exports = function (b64Data, contentType) {
  contentType = contentType || '';
  var sliceSize = 512;
  /**
   * normalizar el string de datos, se quita la primera parte hasta la coma
   * y todos los espacios en blanco.
   */

  b64Data = b64Data.replace(/^[^,]+,/, '');
  b64Data = b64Data.replace(/\s/g, '');
  var byteCharacters = window.atob(b64Data);
  var byteArrays = [];

  for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
    var slice = byteCharacters.slice(offset, offset + sliceSize);
    var byteNumbers = new Array(slice.length);

    for (var i = 0; i < slice.length; i++) {
      byteNumbers[i] = slice.charCodeAt(i);
    }

    var byteArray = new Uint8Array(byteNumbers);
    byteArrays.push(byteArray);
  }

  return new Blob(byteArrays, {
    type: contentType
  });
};

},{}],41:[function(require,module,exports){
'use strict';
/**
 * @author Nicolas Andres Rivera Acevedo
 * @description script con métodos de utilidad, deben ser atomicos y genéricos
 */

module.exports = {
  esIe: _esIe,
  date: _date,
  dateToString: _dateToString,
  obtenerFechaLarga: _obtenerFechaLarga,
  obtenerServicioGet: require('./obtenerServicioGet'),
  obtenerServicioPost: require('./obtenerServicioPost'),
  obtenerServicioDelete: require('./obtenerServicioDelete'),
  validarContentType: require('./validarContentType'),
  base64toBlob: require('./base64toBlob'),
  parseString: require('./parseString'),
  joinUrl: require('./joinUrl'),
  normalizeUrl: require('./normalizeUrl'),
  obtenerMesActual: require('./obtenerMesActual'),
  parsePath: require('./parsePath'),
  lowerCaseObject: require('./lowerCaseObject'),
  normalizeEntities: require('./normalizeEntities'),
  agregarSeleccione: _agregarSeleccione,
  agregarItemEspecial: _agregarItemEspecial
};
/**
 * @description agrega un item seleccione a la colección
 * @param {Array} coleccion colección de objetos
 * @param {String} key prop a la cual agregar el valor 0
 * @param {String} label prop a la cual agregar el valor '-- seleccione --'
 */

function _agregarSeleccione(coleccion, key, label, value) {
  value = value || 0;
  return _agregarItemEspecial(coleccion, key, label, value, '-- Seleccione --');
}
/**
 * @description agrega un item especial a la colección
 * @param {Array} coleccion colección de objetos
 * @param {String} key prop a la cual agregar el valor dado por keyValue
 * @param {String} label prop a la cual agregar el valor dado por labelValue
 * @param {*} keyValue valor a agregar en la prop dada por key
 * @param {*} labelValue valor a agregar en la prop dada por label
 */


function _agregarItemEspecial(coleccion, key, label, keyValue, labelValue) {
  if (!coleccion || typeof key == 'undefined' || typeof label == 'undefined') return coleccion;

  for (var i = 0; i < coleccion.length; i++) {
    if (coleccion[i][key] == keyValue) return coleccion;
  }

  var item = {};
  item[key] = keyValue;
  item[label] = labelValue;
  coleccion.splice(0, 0, item);
  return coleccion;
}
/**
 * @description Función que permite verificar si el navegador es Explorer de Microsoft
 */


function _esIe() {
  var ua = window.navigator.userAgent.toLowerCase();
  return ua.indexOf("msie") > -1 || ua.indexOf("trident") > -1 || ua.indexOf("edge") > -1;
}

;
/**
 * @description Permite transformar una fecha en objeto Date
 * @param  {string} input  	string que representa una fecha ('dia/mes/año')
 * @return {object}         Date
 */

function _date(input) {
  if (input == null || input instanceof Date) return input;
  input = input.replace(/-/g, "/").split("/");
  input = new Date(input[1] + "/" + input[0] + "/" + input[2]);
  if (isNaN(input.getDay()) || input.getFullYear() <= 1900) input = null;
  return input;
}
/**
 * @description Permite obtener la fecha completa actual
 */


function _obtenerFechaLarga() {
  var fecha = new Date();
  var meses = "Enero,Febrero,Marzo,Abril,Mayo,Junio,Julio,Agosto,Septiembre,Octubre,Noviembre,Diciembre".split(",");
  var dias = "Domingo,Lunes,Martes,Miércoles,Jueves,Viernes,Sábado".split(",");
  return dias[fecha.getDay()] + " " + fecha.getDate() + " de " + meses[fecha.getMonth()] + " de " + fecha.getFullYear();
}
/**
 * @description Permite transformar una fecha a una fecha tipo dd/MM/YYYY
 */


function _dateToString(date) {
  if (date == null) return date;
  var m = date.getMonth() + 1;
  var d = date.getDate();
  d = d > 9 ? d : '0' + d;
  m = m > 9 ? m : '0' + m;
  return d + '/' + m + '/' + date.getFullYear();
}

},{"./base64toBlob":40,"./joinUrl":42,"./lowerCaseObject":43,"./normalizeEntities":44,"./normalizeUrl":45,"./obtenerMesActual":46,"./obtenerServicioDelete":47,"./obtenerServicioGet":48,"./obtenerServicioPost":49,"./parsePath":50,"./parseString":51,"./validarContentType":52}],42:[function(require,module,exports){
'use strict';

var normalize = require('./normalizeUrl');
/**
 * @author Nicolas Andres Rivera Acevedo
 * @description Función que permite concatenar dos string que representan una url
 * @param {String} baseUrl url base a concatenar con `url`
 * @param {String} url url a concatenar con `baseUrl`
 */


module.exports = function joinUrl(baseUrl, url) {
  if (/^(?:[a-z]+:)?\/\//i.test(url)) {
    return url;
  }

  return normalize([baseUrl, url].join('/'));
};

},{"./normalizeUrl":45}],43:[function(require,module,exports){
"use strict";

/**
 * @description Función encargada de generar un objeto con propiedades en lower case
 * @param {Object} obj objeto a transformar sus claves en lower case
 */
module.exports = function lowerCaseObject(obj) {
  var lower = {};
  Object.keys(obj).forEach(function (k) {
    lower[k.toLowerCase()] = obj[k];
  });
  return lower;
};

},{}],44:[function(require,module,exports){
"use strict";

/**
 * @description Función encargada de intercambiar palabras claves por
 * elementos de html
 * @param {String} input string de entrada con palabras claves de iconos:
 * 		$warn  	: icono de triangulo de warning
 * 		$info	: icono de circulo de información
 * 		$times 	: icono de x de error
 * 		$check	: icono de un tick en un circulo verde
 */
module.exports = function normalizeEntities(input) {
  return input.replace(/\$warn/g, '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>').replace(/\$info/g, '<i class="fa fa-info-circle" aria-hidden="true"></i>').replace(/\$times/g, '<i class="fa fa-times-circle" aria-hidden="true"></i>').replace(/\$check/g, '<i class="fa fa-check-circle" aria-hidden="true"></i>').replace(/\$br/g, '<br/>');
};

},{}],45:[function(require,module,exports){
'use strict';
/**
 * @author Nicolas Andres Rivera Acevedo
 * @description Función que permite normalizar una url.
 * Normaliza //, /? /#, :/
 * @param {String} url string que respresenta una url
 */

module.exports = function normalizeUrl(url) {
  return url.replace(/[\/]+/g, '/').replace(/\/\?/g, '?').replace(/\/\#/g, '#').replace(/\:\//g, '://');
};

},{}],46:[function(require,module,exports){
'use strict';
/**
 * @author Nicolas Andres Rivera Acevedo
 * @description Función que permite obtener el mes de la fecha actual
 */

module.exports = function obtenerMesActual() {
  var meses = 'Enero,Febrero,Marzo,Abril,Mayo,Junio,Julio,Agosto,Septiembre,Octubre,Noviembre,Diciembre'.split(',');
  return meses[new Date().getMonth()];
};

},{}],47:[function(require,module,exports){
"use strict";

var parsePath = require('./parsePath');
/** 
 * @author Nicolas Andres Rivera Acevedo
 * @description Devuelve la función para generar servicio delete.
 * @param {Object} http Servicio `$http` de angularjs
 * @param {String} ruta String que contiene la ruta a la API.
 * @example
 * var util = require('commons/util');
 * module.exports = [
 *    '$http', 
 *   function(http){ 
 *       return { 
 *           eliminar: util.obtenerServicioDelete(http, 'api/prueba')
 *       }
 *   }
 *]
 */


module.exports = function (http, ruta) {
  return function (params, config) {
    var defaultConfig = {
      params: params
    };
    angular.extend(defaultConfig, config || {});
    return http["delete"](parsePath(ruta, params), defaultConfig).then(function (resp) {
      if (typeof success == 'function') {
        var r = success(resp.data, resp.status);
        return r || resp;
      }

      return resp;
    })["catch"](function (resp) {
      if (typeof error == 'function') {
        var r = error(resp.data, resp.status);
        return r || resp;
      }

      return resp;
    });
  };
};

},{"./parsePath":50}],48:[function(require,module,exports){
"use strict";

var parsePath = require('./parsePath');
/** 
 * @author Pablo Wenger Amengual
 * @description Servicio para comunicación asincrona para comunicación con API
 * @param {Object} http Servicio `$http` de angularjs
 * @param {String} ruta String que contiene la ruta a la API.
 * @example
 * var util = require('commons/util');
 * module.exports = [
 *    '$http', 
 *   function(http){ 
 *       return { 
 *           obtenerEvaluacion: util.obtenerServicioGet(http, 'api/evaluacionnegocio')
 *       }
 *   }
 *]
 */


module.exports = function (http, ruta) {
  return function (params, config) {
    var defaultConfig = {
      params: params
    };
    angular.extend(defaultConfig, config || {});
    return http.get(parsePath(ruta, params), defaultConfig).then(function (resp) {
      if (typeof success == 'function') {
        var r = success(resp.data, resp.status);
        return r || resp;
      }

      return resp;
    })["catch"](function (resp) {
      if (typeof error == 'function') {
        var r = error(resp.data, resp.status);
        return r || resp;
      }

      return resp;
    });
  };
};

},{"./parsePath":50}],49:[function(require,module,exports){
"use strict";

var parsePath = require('./parsePath');
/** 
 * @author Nicolas Andres Rivera Acevedo
 * @description Devuelve la función para generar servicio post
 * @param {Object} http Servicio `$http` de angularjs
 * @param {String} ruta String que contiene la ruta a la API.
 * @example
 * var util = require('commons/util');
 * module.exports = [
 *    '$http', 
 *   function(http){ 
 *       return { 
 *           obtenerEvaluacion: util.obtenerServicioPost(http, 'api/evaluacionnegocio')
 *       }
 *   }
 *]
 */


module.exports = function (http, ruta) {
  return function (params, config) {
    return http.post(parsePath(ruta, params), params, config || {}).then(function (resp) {
      if (typeof success == 'function') {
        var r = success(resp.data, resp.status);
        return r || resp;
      }

      return resp;
    })["catch"](function (resp) {
      if (typeof error == 'function') {
        var r = error(resp.data, resp.status);
        return r || resp;
      }

      return resp;
    });
  };
};

},{"./parsePath":50}],50:[function(require,module,exports){
"use strict";

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

/**
 * @author Nicolas Andres Rivera Acevedo
 * @description Función encargada de parsear un String con parámetros.
 * @param {String} path String que contiene variables del tipo `{var}`
 * @param {Object} params Objeto literal con variables a integrar en `path`
 * @example 
 *      let path = 'path/to/movies/{idMovie}/cast/{idActor}';
 *      path = parsePath(path, {
 *          idMovie: 1,
 *          idActor: 53
 *      });
 *  
 *      console.log(path); //imprime path/to/movies/1/cast/53
 */
module.exports = function parsePath(path, params) {
  if (_typeof(params) != 'object') return path;
  Object.keys(params).forEach(function (k) {
    path = path.replace(new RegExp('{' + k + '}', 'ig'), params[k] == null ? '' : params[k]);
  });
  return path;
};

},{}],51:[function(require,module,exports){
"use strict";

/**
 * @author Nicolas Andres Rivera Acevedo
 * @description Función encargada de parsear un string con parámetros.
 * El primer argumento de la función es el String a parsear, los siguientes son
 * convertidos a claves $n donde n es el número que identifica al argumento comenzando en 1
 * @example 
 *      let str = '$1 es una vocal y $2 es un número impar';
 *      let out = parseString(str, 'A', 3);
 */
module.exports = function parseString() {
  var input = String(arguments[0]);

  for (var i = 1; i < arguments.length; i++) {
    input = input.replace(new RegExp('\\$' + i, 'g'), arguments[i]);
  }

  input = input.replace(/\$\d+/g, '');
  return input;
};

},{}],52:[function(require,module,exports){
"use strict";

/**
 * @author Nicolas Andres Rivera Acevedo
 * @param {File} file instancia de File, archivo a validar content types
 * @param {Array|String|Function} contentType String o Array de String que representan los contentTypes validos,
 *     Si es una función se utilizará como un callback para validar el archivo, en la cual pasará el archivo
 *     como parámetro a la función.
 */
module.exports = function (file) {
  var contentTypes = [],
      i;

  for (i = 1; i < arguments.length; i++) {
    if (angular.isArray(arguments[i])) contentTypes = contentTypes.concat(arguments[i]);else contentTypes.push(arguments[i]);
  }

  for (i = 0; i < contentTypes.length; i++) {
    var type = contentTypes[i];
    if (typeof type == 'function' && type(file)) return true;
    if (type == '*/*' || type.toLowerCase && file.type.toLowerCase() == type.toLowerCase()) return true;
  }

  return false;
};

},{}],53:[function(require,module,exports){
module.exports = "<div class=\"modal-title alert-danger mb-0 p-3\"><h6 class=m-0><i class=\"fa fa-times-circle pr-2\"></i> {{$title}}</h6></div><div class=modal-body ng-bind-html=$message></div><div class=modal-footer><button ng-click=aceptar() class=\"btn btn-primary\">Aceptar</button></div>";

},{}],54:[function(require,module,exports){
module.exports = "<div class=\"modal-title alert-warning mb-0 p-3\"><h6 class=m-0><i class=\"fa fa-exclamation-triangle pr-2\"></i> {{$title}}</h6></div><div class=modal-body ng-bind-html=$message></div><div class=modal-footer><button ng-click=aceptar() class=\"btn btn-primary\">Aceptar</button></div>";

},{}],55:[function(require,module,exports){
module.exports = "<div class=\"modal-title mb-0 p-3\" ng-class=\"{'alert-confirm': isConfirm}\"><h6 class=m-0><i class=\"fa fa-question-circle pr-2\" ng-if=isConfirm></i> <i class=\"fa fa-exclamation-circle pr-2\" ng-if=!isConfirm></i> {{$title}}</h6></div><div class=modal-body ng-bind-html=$message></div><div class=modal-footer><button ng-click=aceptar() class=\"btn btn-primary\">Aceptar</button> <button ng-click=cancelar() class=\"btn btn-secondary\" ng-if=isConfirm>Cancelar</button></div>";

},{}],56:[function(require,module,exports){
'use strict';
/**
 * @author Nicolas Andres Rivera Acevedo
 * @description configuración del submodulo 'app.main'
 */

module.exports = ['$httpProvider', '$urlRouterProvider', '$locationProvider', function (httpProvider, urlRouterProvider, locationProvider) {
  locationProvider.hashPrefix('');
  /**
   * interceptor para request ajax
   */

  httpProvider.interceptors.push('$mainInterceptor');
  /**
   * configuración de alias para urls de ui-router
   */

  urlRouterProvider.otherwise('/error');
}];

},{}],57:[function(require,module,exports){
'use strict';
/**
 * @author Nicolas Andres Rivera Acevedo
 * @description indice de todas las directivas del modulo 'app.main'
 */

angular.module('app.main').directive('uxLoading', require('./ux-loading/loading'));

},{"./ux-loading/loading":58}],58:[function(require,module,exports){
'use strict';

var _eventNames = require("commons/constants/event-names");

/**
 * @author Nicolas Andres Rivera Acevedo
 * @description directiva para mostrar 'cargando en cada request'
 */
module.exports = function () {
  return {
    restrict: 'A',
    scope: {},
    link: function link(scope, elem, attr) {
      elem.hide();
      scope.$on(_eventNames.PRELOADER_VISIBLE, function () {
        elem.show();
      });
      scope.$on(_eventNames.PRELOADER_HIDDEN, function () {
        elem.hide();
      });
    }
  };
};

},{"commons/constants/event-names":9}],59:[function(require,module,exports){
"use strict";
'user strict';
/**
 * @author Nicolas Andres Rivera Acevedo
 * @description Definición del submodulo app.main
 */

var moduleName = 'app.main';
module.exports = moduleName;
angular.module(moduleName, [
  /**
   * dependencias:
   */
]).constant('$urlConfig', require('root/bundle/conf/url-config.cfg')).config(require('./config'));
/**
 * inclusión de componentes
 */

require('./services');

require('./directives');

},{"./config":56,"./directives":57,"./services":60,"root/bundle/conf/url-config.cfg":1}],60:[function(require,module,exports){
'use strict';
/**
 * @author Nicolas Andres Rivera Acevedo
 * @description indice de todos los servicios del modulo 'app.main'
 */

angular.module('app.main').factory('$loadingFactory', require('./loading-factory')).factory('$mainInterceptor', require('./main-interceptor')).factory('$parentMessage', require('./parent-message'));

},{"./loading-factory":61,"./main-interceptor":62,"./parent-message":63}],61:[function(require,module,exports){
'use strict';

var _eventNames = require("commons/constants/event-names");

/**
 * @author Nicolas Andres Rivera Acevedo
 * @description Factory para utilizarlo como interceptor y mostrar un 'loading en cada request'
 */
module.exports = ['$q', '$rootScope', function (q, rootScope) {
  var currentCalls = 0;
  return {
    request: function request(config) {
      if ((config.loading || config.loading == null) && config.url.indexOf('uib/') < 0) {
        currentCalls++;
        rootScope.$broadcast(_eventNames.PRELOADER_VISIBLE);
      }

      return config || q.when(config);
    },
    response: function response(_response) {
      if (currentCalls > 0) currentCalls--;
      if (currentCalls <= 0) rootScope.$broadcast(_eventNames.PRELOADER_HIDDEN);
      return _response || q.when(_response);
    },
    responseError: function responseError(response) {
      if (currentCalls > 0) currentCalls--;
      if (currentCalls <= 0) rootScope.$broadcast(_eventNames.PRELOADER_HIDDEN);
      return q.reject(response);
    }
  };
}];

},{"commons/constants/event-names":9}],62:[function(require,module,exports){
'use strict';
/**
 * @author Nicolas Andres Rivera Acevedo
 * @description Factory para ser utilizado como interceptor de las peticiones ajax del
 *              servicio $http, permite reconfigurar la url de request anteponiendo
 *              el host destino, de esta manera en todos los demás servicios se utiliza una url relativa.
 */

module.exports = ['$q', '$urlConfig', '$authStorage', '$parentMessage', '$injector', '$rutContextoCrypto', function (q, urlConfig, authStorage, parentMessage, injector, rutContextoCrypto) {
  var MAX_RETRY_REQUEST = 10;
  var countRetries = {};

  function joinUrl(baseUrl, url) {
    if (/^(?:[a-z]+:)?\/\//i.test(url)) {
      return url;
    }

    var joined = [baseUrl, url].join('/');

    var normalize = function normalize(str) {
      return str.replace(/[\/]+/g, '/').replace(/\/\?/g, '?').replace(/\/\#/g, '#').replace(/\:\//g, '://');
    };

    return normalize(joined);
  }

  ;
  return {
    request: function request(config) {
      /**
       * saltar la configuración defecto de ui-bootstrap
       * ya que va a buscar templates predefinidas en ruta 'uib/'
       */
      if (config.url.indexOf('uib/') == 0) return config || q.when(config);
      if (!config.params) config.params = {};
      angular.extend(config.params, {
        Dom: authStorage.getDomain(),
        RutContexto: authStorage.getRutContexto(),
        _rcksm: rutContextoCrypto.getCheckSum(),
        _r: urlConfig.RELEASE_NUM
      });
      authStorage.authConfig(config);

      for (var i = 0; i < urlConfig.EXCLUDE_BEGIN.length; i += 1) {
        if (config.url.indexOf(urlConfig.EXCLUDE_BEGIN[i]) === 0) {
          return config || q.when(config);
        }
      }

      for (var i = 0; i < urlConfig.EXCLUDE_END.length; i += 1) {
        if (config.url.lastIndexOf(urlConfig.EXCLUDE_END[i]) === config.url.length - urlConfig.EXCLUDE_END[i].length) {
          return config || q.when(config);
        }
      }

      config.url = joinUrl(urlConfig.HOST_API, config.url);
      return config || q.when(config);
    },
    response: function response(resp) {
      var bearer = resp.headers('bearer');

      if (bearer) {
        authStorage.set({
          Token: bearer
        });
        parentMessage.actualizarToken(bearer);
      }

      return resp;
    },
    responseError: function responseError(resp) {
      if (resp.status == 401) {
        /**
         * reintentar request producto de una actualización de token.
         * Esto ocurre en peticiones en paralelo donde la primera
         * respuesta desde el backend produce un cambio de token dejando
         * obsoletas las peticiones siguientes.
         */
        if (!countRetries[resp.config.url]) countRetries[resp.config.url] = 0;

        if (countRetries[resp.config.url] < MAX_RETRY_REQUEST) {
          countRetries[resp.config.url]++;
          return injector.get('$http')(resp.config);
        } else {
          countRetries[resp.config.url] = 0;
          /**
           * notificar al contenedor padre de una renovación de token,
           * en este caso escalamos el token que probablemente es invalido,
           * lo cual en el mejor caso, hará posible el despliegue de un cuadro de dialogo de login
           * y luego se informará un nuevo token, recién ahí se reintentan las peticiones.
           */

          parentMessage.renovarToken(authStorage.getToken()).then(function (event) {
            authStorage.set({
              Token: event.data.Token
            });
            angular.extend(resp.config, {
              loading: false
            });
            return injector.get('$http')(resp.config);
          });
        }
      }

      return q.reject(resp);
    }
  };
}];

},{}],63:[function(require,module,exports){
'use strict';

module.exports = ['$q', function (q) {
  var MAX_WAITING = 120; //segundos;

  var deferRenovacion = null;
  var _onMessage = null;
  window.addEventListener('message', function (event) {
    if (deferRenovacion) {
      deferRenovacion.resolve(event);
      deferRenovacion = null;
    }

    if (typeof _onMessage == "function") {
      _onMessage(event);
    }
  });
  return {
    onMessage: onMessage,
    renovarToken: renovarToken,
    actualizarToken: actualizarToken
  };
  /**
   * @description notifica al contenedor padre con token actualizado,
   * en este caso no espera una respuesta ya que el token actualizado
   * ya lo tenemos, sólo debemos informar que hay un nuevo token a utilizar.
   * @param {String} token token
   */

  function actualizarToken(token) {
    parent.postMessage({
      Event: 'renew-token',
      Data: {
        Token: token
      }
    }, '*');
  }
  /**
   * @description notifica a contenedor padre para renovar token,
   * esto ocurre luego de un codigo 401, se notifica al padre para
   * que renueve el token invalido y espera X segundos por una respuesta con un nuevo token
   * si se superan los segundos, se cancelan las peticiones.
   * @param {String} token token
   */


  function renovarToken(token) {
    deferRenovacion = q.defer();
    parent.postMessage({
      Event: 'logout-session',
      Data: {
        Token: token
      }
    }, '*');
    setTimeout(function () {
      if (deferRenovacion) {
        deferRenovacion.reject();
        deferRenovacion = null;
      }
    }, MAX_WAITING * 1000);
    return deferRenovacion.promise;
  }

  function onMessage(fn) {
    _onMessage = fn;
  }
}];

},{}],64:[function(require,module,exports){
'use strict';

var _validaciones = require("./validaciones");

/**
 * @author Nicolas Andres Rivera Acevedo
 * @description configuración del submodulo 'app.receder'
 */
module.exports = ['$stateProvider', '$uxValidarProvider', function (stateProvider, uxValidarProvider) {
  /**
  * configurar las validaciones de la directiva ux-validar
  */
  (0, _validaciones.configurar)(uxValidarProvider);
  /**
   * configuración de states
   */

  stateProvider.state('receder', require('./states/receder')).state('error', require('./states/errorPage'));
}];

},{"./states/errorPage":65,"./states/receder":66,"./validaciones":67}],65:[function(require,module,exports){
"use strict";

/**
 * @author Nicolas Andres Rivera Acevedo
 * @description configuración de vistas y controllers para
 *              el state `ErroPage`
 */
module.exports = {
  url: '/error',
  views: {
    'body@': {
      template: require('receder/views/error.html')
    }
  }
};

},{"receder/views/error.html":123}],66:[function(require,module,exports){
"use strict";

/**
 * @author Nicolas Andres Rivera Acevedo
 * @description configuración de vistas y controllers para
 *              el state 'receder' , se agrega el "RESOLVE" dentro
 * 				de este state , ya que permitira iniciar el postMessage antes que los controladores .
 */
module.exports = {
  url: '/receder',
  resolve: require('receder/resolves'),
  views: {
    'body@': {
      template: require('receder/views/bandeja-entrada.html'),
      controller: 'bandeja-entrada-controller'
    }
  }
};

},{"receder/resolves":80,"receder/views/bandeja-entrada.html":122}],67:[function(require,module,exports){
'use strict';

var _messages = require("commons/constants/messages");

/**
 * @author Sinecio Bermúdez Jacque
 * @description Script encargado de configurar las validaciones de la directiva ux-validar.
 */
module.exports = {
  configurar: configurar
};
/**
 * @author Sinecio Bermúdez Jacque
 * @description Función encargada de configurar la directiva de validación.
 * @param {Object} uxValidarProvider provider de uxValidar
 */

function configurar(uxValidarProvider) {
  uxValidarProvider.mensajeDefecto(_messages.VALIDAR_CAMPOS_REQUERIDOS).validacion('email', function (value) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(value.toLowerCase()) ? true : _messages.EMAIL_INVALIDO;
  })
  /* 
  .validacion('telefono' , function(value){
    var re = /^(+?56)?(\s?)(0?9)(\s?)[987654]\d{7}$/;
    return re.test(value) ? true : FONO_INVALIDO;
  }) */
  .validacion('rut', function (value) {
    value = value.toString().toLowerCase().replace(/[\.-]/g, "");
    var rut = value.substr(0, value.length - 1);
    var factor = 2;
    var suma = 0;
    var dv;

    for (var i = rut.length - 1; i >= 0; i--) {
      factor = factor > 7 ? 2 : factor;
      suma += parseInt(rut[i]) * factor++;
    }

    dv = 11 - suma % 11;
    if (dv == 11) dv = 0;else if (dv == 10) dv = "k";
    return dv == value.charAt(value.length - 1) ? true : _messages.RUT_INVALIDO;
  });
}

;

},{"commons/constants/messages":11}],68:[function(require,module,exports){
'use strict';
/**
 * @author Nicolas Andres Rivera Acevedo
 * @description Controller de la vista principal de documentos para receder
 */

module.exports = ['$scope', '$receder', '$popup', '$authStorage', '$location', function (scope, receder, popup, authStorage, $location) {
  /**
   * @description Funciones y variables para ser utilizadas en la vista.
   */
  scope.recederDocumento = recederDocumento;
  scope.obtenerFacturas = obtenerFacturas;
  /**
   * @description modelo para directiva de paginación.
   */

  scope.Paginacion = {
    Pagina: 1,
    CampoOrden: '',
    Descendente: false
  };
  /**
   * @description Función principal.
   */

  function init() {
    debugger;
    validaTokenVacio();
    establecerValoresDefecto();
    obtenerFacturas();
  }

  function validaTokenVacio() {
    var _Token = authStorage.getToken();

    if (_Token == "" || _Token == null || _Token == undefined) {
      console.error('invalid token or null/void/undefined');
      return $location.url('/error');
    }
  }
  /**
   * @description Función encargada de establecer valores
   * por defecto de variables al inicializar.
   */


  function establecerValoresDefecto() {
    scope.hayDatosFacturas = false;
  }
  /**
   * @description Función encargada de generar los
   * parámetros necesarios como querystring para la obtención
   * de facturas para receder.
   */


  function obtenerParametros() {
    var params = {
      EsParaReceder: true
    };
    angular.extend(params, scope.Paginacion);
    return params;
  }
  /** 
   * @description Función encargada de obtener las facturas
   * disponibles para receder.
  */


  function obtenerFacturas() {
    var status = {
      200: cargarDatosFacturas,
      204: sinDatos
    };
    receder.obtenerFacturas(obtenerParametros()).then(function (resp) {
      (status[resp.status] || codigoNoControlado)(resp.data);
    });
  }
  /**
   * @description Función encargada de cagar los datos de las facturas
   * obtenidas desde backend en la variable de ámbito correspondiente.
   * @param {Object} data información de respuesta desde el backend.
   */


  function cargarDatosFacturas(data) {
    scope.datosFacturas = data.Datos;
    scope.hayDatosFacturas = scope.datosFacturas.length > 0;
    angular.extend(scope.Paginacion, data.Paginacion);
  }
  /**
   * @description Función encargada de establecer condiciones y aspectos
   * para cuando el backend responde que no hay datos de facturas para
   * receder.
   * @param {Object} data información de respuesta desde el backend.
   */


  function sinDatos(data) {
    scope.datosFacturas = [];
    scope.hayDatosFacturas = false;
    scope.Paginacion.Pagina = 1;
  }
  /**
   * @description Función encargada de establecer condiciones y aspectos
   * cuando la respuesta del backend no está controlada.
   * @param {Object} data información de respuesta desde el backend.
   */


  function codigoNoControlado(data) {
    popup.errorAlert(data.Message);
  }
  /**
   * @description Función que levanta un modal para receder el documento
   * seleccionado por el usuario.
   * @param {Object} documento elemento de la colección `scope.datosFacturas`
   */


  function recederDocumento(documento) {
    popup.modal({
      view: require('receder/views/receder-documento.html'),
      controller: 'receder-documento-controller',
      name: 'receder documentos',
      params: {
        documento: documento
      },
      onClose: obtenerFacturas
    });
  }

  init();
}];

},{"receder/views/receder-documento.html":125}],69:[function(require,module,exports){
'use strict';

var _eventNames = require("commons/constants/event-names");

var _contentTypes = require("commons/constants/content-types");

var _lodash = require("lodash");

var _util = require("commons/util");

var _messages = require("commons/constants/messages");

/**
 * @author Nicolas Andres Rivera Acevedo
 * @description controller de vista documentos > repositorio
 */
module.exports = ['$scope', '$negocioCrear', '$popup', '$timeout', 'Analytics', '$feAuth', '$constantes', '$documentosGestionar', '$feAuthControl', function (scope, negocioCrear, popup, timeout, Analytics, feAuth, constantes, documentosGestionar, feAuthControl) {
  var _constantes$obtener = constantes.obtener(),
      ORIGEN_UPLOAD = _constantes$obtener.ORIGEN_UPLOAD;
  /**
   * Boolean para no levantar alertas más de una vez
   */


  var alertaEsMostrada;
  /**
   * Boolean para no mostrar alerta de error cuando ocurren
   * respuestas con status 403 para renovación de autenticación
   * y reintentar la subida de archivos
   */

  var isUploadComplete = false;
  /**
   * modelo para colección de archivos de ngflow
   */

  scope.$flow = {};
  scope.flowFileAdded = flowFileAdded;
  scope.flowFileError = flowFileError;
  scope.flowFileSuccess = flowFileSuccess;
  scope.flowFilesSubmitted = flowFilesSubmitted;
  scope.flowComplete = flowComplete;
  scope.obtenerDocumentos = obtenerDocumentos;
  scope.cerrarCargarDocumentos = cerrarCargarDocumentos;
  scope.eliminarTodos = eliminarTodos;
  scope.reintentarTodos = reintentarTodos;
  scope.confirmarEliminar = confirmarEliminar;
  scope.reemplazarDocumento = reemplazarDocumento;
  /**
   * @description Objeto para parámetros de filtrado
   */

  scope.filtros = {};
  /**
   * @description Objeto paginación para la grilla de datos de facturas
   */

  scope.Paginacion = {
    Pagina: 1,
    CampoOrden: '',
    Descendente: false
  };
  /**
   * @description Objeto modelo para mantener
   * valores de la vista
   */

  scope.modelo = {};
  /**
   * @description Event listener para cuando se produce la acción del filtrar.
   */

  scope.$on(_eventNames.NEGOCIO_FILTRAR, function (event, params) {
    angular.extend(scope.filtros, params.Filtros);
    /**
     * quitar del modelo de parámetros de filtrado los queryString informados
     * en la colección parms.Borrar
     */

    angular.forEach(params.Borrar, function (query) {
      delete scope.filtros[query];
    });
    obtenerDocumentos(true);
    Analytics.trackEvent('filtrar', 'click', 1, true);
  });
  /**
   * @description función inicial, la ejecución de las funciones deben
   * ser posterior a la verificación del paso actual.
   */

  function init() {
    establecerValoresDefecto();
    obtenerDocumentos();
  }
  /**
   * @description Función encargada de establecer valores iniciales
   * como variables u objetos que deben estar definidos antes de
   * cualquier ejecución
   */


  function establecerValoresDefecto() {
    alertaEsMostrada = false;
    scope.hayDatosDocumentos = true;
    scope.cargarDocumentos = false;
  }
  /**
   * @description Evento que se gatilla cada vez que se agregan archivos para subir
   * @param {FlowFile} file archivo agregado, instancia de FlowFile
   * @param {Event} event objeto event
   * @param {Flow} flow objeto flow (scope.$flow)
   */


  function flowFileAdded(file, event, flow) {
    file.isValid = (0, _util.validarContentType)(file.file, _contentTypes.APPLICATION_XML, _contentTypes.TEXT_XML);
  }
  /**
   * @description Evento que se gatilla cada vez que hay error al subir un archivo
   * @param {FlowFile} file archivo agregado, instancia de FlowFile
   * @param {Object} resp objeto con información de error reportado por backend 
   * @param {Flow} flow flow objeto flow (scope.$flow)
   */


  function flowFileError(file, resp, flow) {
    resp = angular.fromJson(resp);
    isUploadComplete = false;
    var status = {
      403: renew,
      401: feAuthControl.reject
    };
    resp = angular.fromJson(resp);
    (status[resp.Status] || agregarErroresCargaDocumento)(file, resp);
  }
  /**
   * @description Permite renovar el token de autenticación
   * y luego reintentar la subida de archivo.
   */


  function renew() {
    feAuthControl.renew().then(reintentarTodos);
  }
  /**
   * @description Función encargada de agregar al archivo la información de errores producidos al realizar
   * el upload de un documento.
   * @param {FlowFile} file archivo agregado, instancia de FlowFile
   * @param {Object} resp respuesta desde backend
   */


  function agregarErroresCargaDocumento(file, resp) {
    isUploadComplete = true;
    if (resp.Message) file.ErrorMessage = resp.Message;else {
      file.DetalleErrores = (0, _lodash.map)(resp.Datos.Documentos, function (doc) {
        return doc;
      });
    }
  }
  /**
   * @description Evento que se gatilla cada vez que un archivo ha subido exitosamente.
   * El archivo se marca con ok = true (subido exitosamente) y luego de x segundos, este
   * archivo es removido de la colección de archivos del objeto flow. Se utiliza el hashKey
   * provisto por angular para identificar el objeto.
   * @param {FlowFile} file archivo agregado, instancia de FlowFile
   * @param {Object} message objeto con información reportado por backend 
   * @param {Flow} flow flow objeto flow (scope.$flow)
   */


  function flowFileSuccess(file, message, flow) {
    message = angular.fromJson(message);
    file.IDDocumento = obtenerIDDocumentoCargado(message);
    file.ok = message.ResultadoOK;
    file.error = !file.ok;

    if (file.error) {
      agregarErroresCargaDocumento(file, message);
      return;
    }

    isUploadComplete = true;
    timeout(function () {
      (0, _lodash.remove)(flow.files, function (f) {
        return f.$$hashKey == file.$$hashKey;
      });
    }, 3000);
  }
  /**
   * @description Función que permite obtener los IDDocumento cargados
   * exitosamete
   * @param {Object} resp respuesta desde backend.
   */


  function obtenerIDDocumentoCargado(resp) {
    return (0, _lodash.filter)((0, _lodash.map)(resp.Datos.Documentos, function (doc) {
      return doc.IDDocumento;
    }), function (id) {
      return id > 0;
    }).join(',');
  }
  /**
   * @description Evento que se gatilla cada vez que un archivo ha subido exitosamente
   * @param {Array} files de archivos instancias de FlowFile
   * @param {Object} message objeto con información reportado por backend 
   * @param {Flow} flow flow objeto flow (scope.$flow)
   */


  function flowFilesSubmitted(files, message, flow) {
    angular.forEach(files, function (file) {
      if (!file.isValid) file.cancel();
    });
    subirDocumentos();
  }
  /**
   * @description Evento que se gatilla cuando la carga el server ha sido completada.
   * Primero se verifica si hay archivos con errores y luego se actualiza la grilla de 
   * facturas realizando nuevamente la petición al backend con la función obtenerDocumentos()
   * @param {Flow} flow objeto flow (scope.$flow)
   */


  function flowComplete(flow) {
    if (!isUploadComplete) return;
    negocioCrear.notificar(_eventNames.PRELOADER_HIDDEN);
    cerrarCargarDocumentos();
    verificarErroneos();
    obtenerDocumentos();
  }
  /**
   * @description Función que revisa los archivos que se han intentado subir al servidor.
   * Si hay archivos erroneos se muestra una alerta indicando la cantidad de archivos con errores
   */


  function verificarErroneos() {
    scope.erroneos = (0, _lodash.filter)(scope.$flow.files, function (file) {
      return !file.ok;
    }).length;

    if (!alertaEsMostrada && scope.erroneos > 0) {
      alertaEsMostrada = !alertaEsMostrada;
      var msj = scope.erroneos > 1 ? (0, _util.parseString)(_messages.DOS_O_MAS_ARCHIVOS_ERRONEOS, scope.erroneos) : _messages.UN_ARCHIVO_ERRONEO;
      popup.errorAlert(msj, function () {
        alertaEsMostrada = !alertaEsMostrada;
      });
    }
  }
  /**
   * @description Función para gatillar la subida de archivos
   */


  function subirDocumentos() {
    negocioCrear.notificar(_eventNames.PRELOADER_VISIBLE); //agregar parámetros necesarios para subida de archivo:

    angular.extend(scope.$flow.defaults.query, obtenerParametrosUpload());
    scope.$flow.upload();
  }
  /**
   * @description Función que permite reintentar la subida de todos los archivos
   * con subida erronea
   */


  function reintentarTodos() {
    //agregar parámetros necesarios para subida de archivo:
    angular.extend(scope.$flow.defaults.query, obtenerParametrosUpload());
    var erroneos = (0, _lodash.filter)(scope.$flow.files, function (file) {
      return !file.ok;
    });
    angular.forEach(erroneos, function (file) {
      file.retry();
    });
  }
  /** 
   * @description Función que permite eliminar todos los archivos erroneos
   */


  function eliminarTodos() {
    var erroneos = (0, _lodash.filter)(scope.$flow.files, function (file) {
      return !file.ok;
    });
    angular.forEach(erroneos, function (file) {
      file.cancel();
    });
  }
  /**
   * @description Función encargada de generar los parámetros necesarios
   * para la subida de archivos DTE
   */


  function obtenerParametrosUpload() {
    var params = {
      Origen: ORIGEN_UPLOAD.DTE,
      Dom: window.location.origin
    };
    angular.extend(params, feAuth.obtenerAutorizacion());
    return params;
  }
  /**
   * @description Función para obtener parámetros para cargar la lista de
   * facturas, agrega párametros de filtros y paginación
   * @param {Boolan} esFiltrar determina si la llamada se ha producido mediante la acción filtrar
   */


  function obtenerParametros(esFiltrar) {
    var params = {};

    if (esFiltrar) {
      scope.Paginacion.Pagina = 1;
    }

    angular.extend(params, scope.filtros);
    angular.extend(params, scope.Paginacion);
    return params;
  }
  /**
   * @description Función que realiza la petición al backend para 
   * obtener los documentos cargados en el repositorio
   */


  function obtenerDocumentos(esFiltrar) {
    var params = obtenerParametros(esFiltrar);
    var status = {
      200: cargarDatosDocumentos,
      203: filtroSinDocumentos,
      204: sinDatos
    };
    debugger;
    negocioCrear.obtenerFacturas(params).then(function (resp) {
      return (status[resp.status] || codigoNoControlado)(resp.data);
    });
  }
  /**
   * @description Función que se encarga de cargar en el scope
   * los datos de las facturas obtenidas desde el backend.
   * Luego de cargar los datos de las facturas, se deben cargar
   * los filtros, para ello se realiza una notificación de evento.
   * @param {Object} data data respuesta desde el backend.
   */


  function cargarDatosDocumentos(data) {
    scope.datosDocumentos = data.Datos;
    scope.hayDatosDocumentos = true;
    scope.busquedaSinCoincidencias = false;
    angular.extend(scope.Paginacion, data.Paginacion);
  }
  /**
   * @description Función encargada de cerrar la ventana para carga de documentos
   */


  function cerrarCargarDocumentos() {
    scope.cargarDocumentos = false;
  }
  /**
   * @description Función que se encarga de establecer las condiciones
   * y aspectos si el backend no encuentra datos dado una combinatoria de
   * filtros establecidos por el usuario.
   * @param {Object} data data respuesta desde el backend.
   */


  function filtroSinDocumentos(data) {
    scope.datosDocumentos = [];
    scope.busquedaSinCoincidencias = true;
    angular.extend(scope.Paginacion, data.Paginacion);
  }
  /**
   * @description Función que se encarga de establecer las condiciones
   * y aspectos cuando no existen registros sin importar el filtro utilizado.
   * @param {Object} data data respuesta desde el backend.
   */


  function sinDatos(data) {
    scope.hayDatosDocumentos = false;
    scope.cargarDocumentos = true;
    scope.datosDocumentos = [];
    scope.Paginacion.Pagina = 1;
  }
  /**
   * @description Función que se encarga de establecer las condiciones
   * y aspectos cuando ocurre un error no controlado al solicitar datos
   * de facturas.
   * @param {Object} data data respuesta desde el backend.
   */


  function codigoNoControlado(data) {
    popup.errorAlert(data.Message);
  }
  /**
   * @description Función que permite confirmar eliminación de un documento
   * @param {Object} documento item de colección de `scope.datosDocumentos`
   */


  function confirmarEliminar(documento) {
    popup.confirm((0, _util.parseString)(_messages.ELIMINAR_DOCUMENTO, "Folio ".concat(documento.Folio)), function () {
      eliminarDTE(documento);
    });
  }
  /**
   * @description Función que permite eliminar un documento dado por `id`
   * @param {Object} documento item de colección de `scope.datosDocumentos`
   */


  function eliminarDTE(documento) {
    var status = {
      200: obtenerDocumentos
    };
    documentosGestionar.eliminarDTE({
      id: documento.IDDocumento
    }).then(function (resp) {
      return (status[resp.status] || codigoNoControlado)(resp.data);
    });
  }
  /**
   * @description Funcion que abre modal para reemplazar documento
   * sino un agregar nota de credito.
   * @param {Object} documento item de colección de `scope.datosDocumentos`
   */


  function reemplazarDocumento(documento) {
    popup.modal({
      view: require('receder/views/modal-reemplazar-documento.html'),
      controller: 'fe.negocio.nuevo.modal-reemplazar-documento-controller',
      name: 'reemplazar documento',
      params: {
        documento: documento,
        esAgregarNotaCredio: false
      },
      onClose: function onClose() {
        obtenerDocumentos();
      }
    });
  }

  init();
}];

},{"commons/constants/content-types":8,"commons/constants/event-names":9,"commons/constants/messages":11,"commons/util":41,"lodash":"lodash","receder/views/modal-reemplazar-documento.html":124}],70:[function(require,module,exports){
'use strict';
/**
 * @author Nicolas Andres Rivera Acevedo
 * @description indice de todos los controllers del modulo 'app.receder'
 */

angular.module('app.receder').controller('bandeja-entrada-repositorio-controller', require('./bandeja-entrada-repositorio-controller')).controller('bandeja-entrada-controller', require('./bandeja-entrada-controller')).controller('receder-documento-controller', require('./receder-documento-controller'));

},{"./bandeja-entrada-controller":68,"./bandeja-entrada-repositorio-controller":69,"./receder-documento-controller":71}],71:[function(require,module,exports){
'use strict';

var _transforms = require("scripts/transforms");

var _messages = require("commons/constants/messages");

/**
 * @author Nicolas Andres Rivera Acevedo
 * @description Controller del modal para receder documento
 */
module.exports = ['$scope', '$receder', '$uibModalInstance', '$uibModalParams', '$popup', '$cliente', function (scope, receder, modalInstance, params, popup, cliente) {
  /**
   * @description Variables y funciones para utilizar en la vista
   */
  scope.volver = volver;
  scope.recederDocumento = recederDocumento;
  scope.obtenerDatosCliente = obtenerDatosCliente; // objeto modelo para envíar al backend.

  scope.recesion = {
    TipoRecesion: 1,
    Cliente: {}
  };
  /**
   * @description Función principal
   */

  function init() {
    valoresDefecto();
    obtenerParametrosModal();
    obtenerTipoRecesion();
  }
  /**
   * @description Permite establecer valores
   * por defecto de variables
   */


  function valoresDefecto() {
    scope.fechaActual = new Date();
  }
  /**
   * @description Función encargada de obtener los parámetros
   * enviados desde la página padre
   */


  function obtenerParametrosModal() {
    scope.documento = params.documento;
    scope.recesion.IDDocumento = params.documento.IDDocumento;
  }
  /**
   * @description Función para cerrar modal.
   */


  function volver() {
    modalInstance.dismiss();
  }
  /**
   * @description Función encargada de obtener la lista
   * de tipos de recesión.
   */


  function obtenerTipoRecesion() {
    receder.obtenerTipoRecesion().then(function (resp) {
      scope.ListaTipoRecesion = resp.data;
      (0, _transforms.agregarSeleccione)(scope.ListaTipoRecesion, 'ID', 'Nombre');
    });
  }
  /**
   * @description Permite obtener los datos del cliente
   * dado el rut ingresado por el usuario `scope.recesion.Cliente.Rut`
   */


  function obtenerDatosCliente(event) {
    if (event.type == 'keyup' && event.keyCode != 13) return;
    if (!scope.recesion.Cliente.Rut) return;
    cliente.obtenerPorRut(scope.recesion.Cliente.Rut).then(function (resp) {
      if (resp.status != 200) return;
      angular.extend(scope.recesion.Cliente, resp.data);
    });
  }
  /**
   * @description Función encargada de realizar la petición
   * al backend de receder un documento. lo que se envía al
   * backend es el modelo antes preparado `scope.recesion`
   * con atributos establecidos por el usuario.
   */


  function recederDocumento() {
    if (!scope.validar()) return;
    var status = {
      200: recesionExitosa
    };
    receder.recederDocumento(scope.recesion).then(function (resp) {
      (status[resp.status] || errorNoControlado)(resp.data);
    });
  }
  /**
   * @description Función encargada de establecer los aspectos y condiciones
   * luego de una respuesta exitosa al receder un documento.
   * @param {Object} data información de respuesta desde el backend
   */


  function recesionExitosa(data) {
    popup.alert(_messages.RECESION_EXITOSA);
    modalInstance.close();
  }
  /**
   * @description Función que se encarga de establecer las condiciones
   * y aspectos cuando ocurre un error no controlado al receder documento.
   * @param {Object} data información de respuesta desde el backend.
   */


  function errorNoControlado(data) {
    popup.errorAlert(data.Message);
  }

  init();
}];

},{"commons/constants/messages":11,"scripts/transforms":127}],72:[function(require,module,exports){
'use strict';
/**
 * @author Nicolas Andres Rivera Acevedo
 * @description indice de todas las directivas del modulo 'app.fe.negocio'
 */

angular.module('app.receder').directive('uxInfoCesion', require('./ux-info-cesion/ux-info-cesion')).directive('uxInfoFactura', require('./ux-info-factura/ux-info-factura'));

},{"./ux-info-cesion/ux-info-cesion":75,"./ux-info-factura/ux-info-factura":78}],73:[function(require,module,exports){
module.exports = "<div class=row><div class=col-12><strong>Fecha Cesión:</strong></div><div class=col>{{info.FechaCesion}}</div></div><div class=row><div class=col-12><strong>Secuencial:</strong></div><div class=col>{{info.Secuencial}}</div></div><div class=row><div class=col-12><strong>Cendente:</strong></div><div class=col><div class=row><div class=col-12>Rut</div><div class=col>{{info.RutCedente | rut}}</div></div><div class=row><div class=col-12>Nombre/Razón Social</div><div class=col>{{info.NombreRazonCedente}}</div></div><div class=row><div class=col-12>Declaración Jurada: <i class=\"fa fa-eye cursor\" ng-click=\"verMas = !verMas\"></i></div><div class=col ng-class=\"{'ver-mas': !verMas}\">{{info.DeclaracionJurada}}</div></div></div></div><div class=row><div class=col-12><strong>Cesionario</strong></div><div class=col><div class=row><div class=col>Rut</div><div class=col-12>{{info.RutCesionario | rut}}</div></div><div class=row><div class=col-12>Nombre/Razón Social</div><div class=col>{{info.NombreRazonCesionario}}</div></div></div></div>";

},{}],74:[function(require,module,exports){
module.exports = "<div class=\"popover mw-35\" role=tooltip><div class=arrow></div><h3 class=popover-header></h3><div class=popover-body></div></div>";

},{}],75:[function(require,module,exports){
'use strict';
/**
 * @author Mauricio Ross
 * @description Directiva para mostrar la información de última cesión.
 * @example 
 *  <ux-info-cesion class="" info="item.InfoAdicionalFactura"></ux-info-cesion>
 */

module.exports = ['$interpolate', '$compile', function (interpolate, compile) {
  var popover = require('scripts/popover');

  var $ = require('jquery');

  return {
    restrict: 'E',
    scope: {
      info: '='
    },
    link: function link(scope, elem, attr) {
      elem.on('click', function (e) {
        if (!$(e.target).is(popover.selected)) {
          $(popover.selected).popover('hide');
        }

        e.stopPropagation();
      });
      elem.popover({
        container: 'body',
        html: true,
        content: compile(interpolate(require('./content.tmpl'))(scope))(scope),
        template: require('./template.tmpl')
      });
    }
  };
}];

},{"./content.tmpl":73,"./template.tmpl":74,"jquery":"jquery","scripts/popover":126}],76:[function(require,module,exports){
module.exports = "<div class=row><div class=col-12><strong>Tipo:</strong></div><div class=col>{{info.TipoDocumento}}</div></div><div class=row><div class=col-12><strong>Fecha Vencimiento Original:</strong></div><div class=col>{{info.FechaVencimientoOriginal}}</div></div><div class=row><div class=col-12><strong>Dirección Receptor:</strong></div><div class=col>{{info.DireccionReceptor}}</div></div><div class=row><div class=col-12><strong>Totales:</strong></div><div class=col><div class=row><div class=col>Monto Neto</div><div class=\"col text-right\">{{info.MontoNeto | formatMiles}}</div></div><div class=row><div class=col>Monto Exento</div><div class=\"col text-right\">{{info.MontoExcento | formatMiles}}</div></div><div class=row ng-if=info.TasaIvaDespliegue><div class=col>IVA ({{info.TasaIvaDespliegue}})</div><div class=\"col text-right\">{{info.MontoIVA | formatMiles}}</div></div><div class=row><div class=col><strong>Total:</strong></div><div class=\"col text-right\"><strong>{{info.Total | formatMiles}}</strong></div></div></div></div>";

},{}],77:[function(require,module,exports){
arguments[4][74][0].apply(exports,arguments)
},{"dup":74}],78:[function(require,module,exports){
'use strict';
/**
 * @author Mauricio Ross
 * @description Directiva para mostrar la información adicional de la factura
 * @example 
 *  <ux-info-factura class="" info="item.InfoAdicionalFactura"></ux-info-factura>
 */

module.exports = ['$interpolate', '$compile', function (interpolate, compile) {
  var popover = require('scripts/popover');

  var $ = require('jquery');

  return {
    restrict: 'E',
    scope: {
      info: '='
    },
    link: function link(scope, elem, attr) {
      elem.on('click', function (e) {
        if (!$(e.target).is(popover.selected)) {
          $(popover.selected).popover('hide');
        }

        e.stopPropagation();
      });
      elem.popover({
        container: 'body',
        html: true,
        content: compile(interpolate(require('./content.tmpl'))(scope))(scope),
        template: require('./template.tmpl')
      });
    }
  };
}];

},{"./content.tmpl":76,"./template.tmpl":77,"jquery":"jquery","scripts/popover":126}],79:[function(require,module,exports){
"use strict";
'user strict';
/**
 * @author Nicolas Andres Rivera Acevedo
 * @description Definición del submodulo app.receder
 */

var moduleName = 'app.receder';
module.exports = moduleName;
angular.module(moduleName, [
  /**
   * dependencias:
   */
]).config(require('./config')).run(require('./run'));
/**
 * inclusión de componentes
 */

require('./services');

require('./controllers');

require('./directives');

},{"./config":64,"./controllers":70,"./directives":72,"./run":82,"./services":99}],80:[function(require,module,exports){
'use strict';
/**
 * @author Nicolas Andres Rivera Acevedo
 * @description "resolve" de datos antes de instanciar controladores
 */

module.exports = {
  init: require('./init')
};

},{"./init":81}],81:[function(require,module,exports){
'use strict';
/**
 * @author Nicolas Andres Rivera Acevedo
 * @description resolve del servicio inicial de la aplicación
 */

module.exports = ['$init', function (init) {
  return init.getPromise();
}];

},{}],82:[function(require,module,exports){
'use strict';
/**
 * @author Nicolas Andres Rivera Acevedo
 * @description controller inicial para determinar autenticación y carga de datos
 * necesarios antes de iniciar la aplicación.
 */

module.exports = ['$init', '$auth', '$rootScope', '$googleTagManager', '$parentMessage', '$location', function (init, auth, rootScope, googleTagManager, parentMessage, $location) {
  parent.postMessage({
    Event: 'loadStatus',
    Data: {
      applicationName: "FER",
      finishLoading: true
    }
  }, '*');
  parentMessage.onMessage(function (event) {
    console.log('PUNTO DE PARTIDA A');
    console.log('event', event);
    var origen = event.data.Origin || event.origin;

    if (event.data.Event) {
      if (event.data.Event == 'renew-token') {
        parentMessage.renovarToken(event.data.Data.Token);
      }

      return;
    }

    run({
      Token: event.data.Token,
      RutContexto: event.data.RutContexto,
      Origin: origen
    });
  });
  /**
   * @author Nicolas Rivera Avevedo
   * @description Inicializa el metodo de Google Tag Manager ademas de incrustar el 
   * noIframe a la pagina principal
   */

  googleTagManager.iniciaGooleManager();
  /**
   * @description Función inicial
   * Una vez realizada la autenticación, hace lo necesario para iniciar la aplicación
   * @param {String} params parámetros de inicialización
   */

  function run(params) {
    autenticar(params).then(function () {
      init.resolve();
    });
  }
  /**
   * @description Función encargada de autenticar
   * @param {String} params parámetros de autenticación
   */


  function autenticar(params) {
    return auth.autenticar(params)["catch"](noAutenticado);
  }
  /**
   * @description Función que realiza acciones luego de una autenticación fallida.
   */


  function noAutenticado() {
    console.warn('No autenticado...');
  }
}];

},{}],83:[function(require,module,exports){
"use strict";

var _auditoria = _interopRequireDefault(require("commons/constants/auditoria"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

/**
 * @author Nicolas Andres Rivera Acevedo
 * @description Servicio para modificar el header y agregar información
 * con respecto al log de auditoría
 */
module.exports = ['$location', function (location) {
  var subPath = null;
  return {
    set: set,
    get: get,
    setPath: setPath,
    clearPath: clearPath
  };

  function set(config) {
    angular.extend(config.headers, get());
  }

  function get() {
    var header = {};
    var path = subPath ? subPath : '';
    header[_auditoria["default"].HEADER_KEY] = "".concat(_auditoria["default"].DONDE, ":").concat(location.path()).concat(path);
    return header;
  }

  function setPath(path) {
    subPath = ('/' + path.replace(/\s+/g, '-').replace(/\//g, '')).toLowerCase();
  }

  function clearPath() {
    subPath = null;
  }
}];

},{"commons/constants/auditoria":6}],84:[function(require,module,exports){
'use strict';
/**
 * @author Nicolas Andres Rivera Acevedo
 * @description factory para almacen de autorización
 */

module.exports = ['$location', function ($location) {
  var _params = {};
  var _tokenAnonimo = null;
  return {
    set: set,
    setTokenAnonimo: setTokenAnonimo,
    getDomain: getDomain,
    getRutContexto: getRutContexto,
    getToken: getToken,
    getHeaderToken: getHeaderToken,
    getHeaderTokenVisita: getHeaderTokenVisita,
    getParams: getParams,
    authConfig: authConfig
  };

  function set(params) {
    angular.extend(_params, params);
  }

  function setTokenAnonimo(token) {
    _tokenAnonimo = token;
  }

  function getParams() {
    return _params;
  }

  function getToken() {
    return _params.Token;
  }

  function getDomain() {
    return _params.Origin;
  }

  function getHeaderToken() {
    if (!_params.Token) return {};
    return {
      Authorization: "Bearer ".concat(_params.Token),
      Dom: _params.Origin
    };
  }

  function getHeaderTokenVisita() {
    if (!_tokenAnonimo) return {};
    return {
      TokenVisita: _tokenAnonimo
    };
  }

  function authConfig(config) {
    angular.extend(config.headers, getHeaderToken());
    angular.extend(config.params, {
      Dom: _params.Origin
    });

    if (_tokenAnonimo != null) {
      angular.extend(config.params, getHeaderTokenVisita());
    }

    return config;
  }

  function getRutContexto() {
    return _params.RutContexto || '';
  }
}];

},{}],85:[function(require,module,exports){
'use strict';
/**
 * @author Nicolas Andres Rivera Acevedo
 * @description factory para peticiones bajo el recurso `fa/auth`
 */

module.exports = ['$http', '$authStorage', '$q', function (http, authStorage, q) {
  return {
    autenticar: autenticar,
    validarOrigen: validarOrigen,
    obtenerAutorizacion: obtenerAutorizacion
  };

  function autenticar(params) {
    var defer = q.defer();
    authStorage.set(params);
    defer.resolve();
    return defer.promise;
  }

  function validarOrigen(origen) {
    return http.post('fe/auth/origin', {
      Origin: origen
    });
  }
  /**
  * @description Función encargada de informar el header de autorización
  */


  function obtenerAutorizacion() {
    var header = authStorage.getHeaderToken();
    header[TOKEN_HEADER] = "".concat(TOKEN_TYPE, " ").concat(auth.getToken());
    /**
     * se agrega auditoria y token de visita en este nivel, para que todas las
     * subidas de archivos contengan esta información.
     * Se hace aquí puesto que en todos los controller que manejan
     * subida de archivos implementan esta función `obtenerAutorizacion`
     * para agregar información en el header del request. Como la información
     * de auditoría y token de visita también es un atributo del header, la agregamos aquí
     * y así no tenemos que replicarla por todos los controllers.
     */

    /*             angular.extend(header, auditoria.get());
                angular.extend(header, authStorage.getHeaderTokenVisita()) */

    return header;
  }
}];

},{}],86:[function(require,module,exports){
'use strict';

var _util = require("commons/util");

/**
 * @author Nicolas Andres Rivera Acevedo
 * @description factory para peticiones bajo el recurso /fe/cliente/contacto
 */
module.exports = ['$http', function (http) {
  function get(endpoint) {
    return (0, _util.obtenerServicioGet)(http, endpoint);
  }

  function post(endpoint) {
    return (0, _util.obtenerServicioPost)(http, endpoint);
  }

  return {
    get: get('/fe/cliente/contacto'),
    post: post('/fe/cliente/contacto')
  };
}];

},{"commons/util":41}],87:[function(require,module,exports){
'use strict';

var _util = require("commons/util");

/**
 * @author Nicolas Andres Rivera Acevedo
 * @description factory para peticiones bajo el recurso /fe/cliente
 */
module.exports = ['$http', function (http) {
  var cliente = null;
  return {
    get: get,
    obtener: obtener,
    obtenerPorRut: obtenerPorRut
  };
  /**
   * @description Función encargada de retornar la promesa de los datos del cliente.
   * @param {Object} params parámetros para usar como queryString
   */

  function get(params) {
    return (0, _util.obtenerServicioGet)(http, '/fe/cliente')(params).then(function (resp) {
      cliente = resp.data;
      delete cliente.MontoTotalAprobado;
      delete cliente.MontoTotalAprobado;
      delete cliente.MontoTotalUtilizado;
      return resp;
    });
  }
  /**
   * @description Función encargada de informar los datos almacenados en el servicio.
   */


  function obtener() {
    return cliente;
  }
  /**
   * @description Función encargada de obtener los datos del cliente dado un rut
   * @param {String} rut rut del cliente
   */


  function obtenerPorRut(rut) {
    return (0, _util.obtenerServicioGet)(http, '/fe/cliente/{rut}')({
      rut: rut
    });
  }
}];

},{"commons/util":41}],88:[function(require,module,exports){
"use strict";

/**
 * @author Nicolas Andres Rivera Acevedo
 * @description factory para peticiones para el recurso cert/constantes
 */
module.exports = ['$http', function (http) {
  var _constantes = {};
  return {
    get: get,
    obtener: obtener
  };

  function get() {
    return http.get('fe/constantes').then(function (resp) {
      _constantes = resp.data.constantes;
      return resp;
    });
  }

  function obtener() {
    return _constantes;
  }
}];

},{}],89:[function(require,module,exports){
'use strict';

var _util = require("commons/util");

/**
 * @author Nicolas Andres Rivera Acevedo
 * @description factory para peticiones bajo el recurso /fe/documento/certificado
 */
module.exports = ['$http', '$tipoWizard', function (http, $tipoWizard) {
  function get(endpoint) {
    return (0, _util.obtenerServicioGet)(http, endpoint);
  }

  function del(endpoint) {
    return (0, _util.obtenerServicioDelete)(http, endpoint);
  }

  function validarPassword(params) {
    return http.post((0, _util.parsePath)('/{prefix}/negocio/certificado/valida-password', {
      prefix: $tipoWizard.obtenerPrefijo()
    }), params).then(function (resp) {
      return resp;
    })["catch"](function (resp) {
      return resp;
    });
  }

  return {
    get: get('/fe/documento/certificado'),
    "delete": del('/fe/documento/certificado'),
    last: get('/fe/documento/certificado/last'),
    validarPassword: validarPassword,
    porExpirar: get('/fe/documento/certificado/por-expirar')
  };
}];

},{"commons/util":41}],90:[function(require,module,exports){
'use strict';

var _util = require("commons/util");

/**
 * @author Nicolas Andres Rivera Acevedo
 * @description factory para peticiones bajo el recurso /fe/documento/descarga
 */
module.exports = ['$http', function (http) {
  function get(endpoint) {
    return (0, _util.obtenerServicioGet)(http, endpoint);
  }

  return {
    get: get('/fe/documento/descarga')
  };
}];

},{"commons/util":41}],91:[function(require,module,exports){
'use strict';

var _util = require("commons/util");

/**
 * @author Nicolas Andres Rivera Acevedo
 * @description factory para peticiones bajo el recurso /fe/documento/respaldo
 */
module.exports = ['$http', function (http) {
  function get(endpoint) {
    return (0, _util.obtenerServicioGet)(http, endpoint);
  }

  function del(endpoint) {
    return (0, _util.obtenerServicioDelete)(http, endpoint);
  }

  return {
    get: get('/fe/documento/respaldo'),
    "delete": del('/fe/documento/respaldo/{IDDocumento}')
  };
}];

},{"commons/util":41}],92:[function(require,module,exports){
'use strict';
/**
 * @author Nicolas Andres Rivera Acevedo
 * @description factory de control de servicios a utilizar en 
 * 'documentos > Repositorio' y 'documentos > receder'
 */

module.exports = ['$documentoCertificado', '$dte', function (documentoCertificado, dte) {
  return {
    obtenerCertificados: documentoCertificado.get,
    eliminarCertificado: documentoCertificado["delete"],
    obtenerUltimoCertificado: documentoCertificado.last,
    eliminarDTE: dte["delete"]
  };
}];

},{}],93:[function(require,module,exports){
'use strict';

var _util = require("commons/util");

/**
 * @author Nicolas Andres Rivera Acevedo
 * @description factory para peticiones bajo el recurso /fe/dte
 */
module.exports = ['$http', function (http) {
  function del(endpoint) {
    return (0, _util.obtenerServicioDelete)(http, endpoint);
  }

  return {
    "delete": del('/fe/dte/{id}')
  };
}];

},{"commons/util":41}],94:[function(require,module,exports){
'use strict';
/**
 * @author Nicolas Andres Rivera Acevedo
 * @description factory de control de autenticacion.
 * Se hace de esta manera, porque este servicio es injectado en un interceptor
 * que podria generar inyección de dependencia circular.
 */

module.exports = ['SatellizerStorage', 'SatellizerConfig', function (SatellizerStorage, SatellizerConfig) {
  var renewFn = null;
  var rejectFn = null;
  return {
    onTokenRenew: onTokenRenew,
    onReject: onReject,
    renew: renew,
    reject: reject,
    hasAuth: hasAuth,
    setAuth: setAuth
  };
  /**
   * @description Función encargada de establecer la función de autenticación para renovar token
   * @param {Function} fn callback a ejecutar
   */

  function onTokenRenew(fn) {
    renewFn = fn;
  }
  /**
   * @description Función encargada de ejecutar la función para renovar token
   */


  function renew() {
    return renewFn();
  }
  /**
   * @description Función encargada de ejecutar la función que realiza tareas
   * una vez ha caducado la autenticación
   * @param {Function} fn callback que a ejecutar
   */


  function onReject(fn) {
    rejectFn = fn;
  }
  /**
   * @description Función encargada de ejecutar la función para renovar token
   */


  function reject() {
    return rejectFn();
  }
  /**
   * @description Función encargada de determinar si un config de petición
   * tiene establecido el header de autenticación
   * @param {Object} config Objecto Configuración de request
   */


  function hasAuth(config) {
    return !!config.headers[SatellizerConfig.tokenHeader];
  }
  /**
   * @description Función encargada de establecer el header de autenticación
   * @param {Object} config Objecto Configuración de request
   */


  function setAuth(config) {
    var header = {};
    var tokenName = SatellizerConfig.tokenPrefix ? [SatellizerConfig.tokenPrefix, SatellizerConfig.tokenName].join('_') : SatellizerConfig.tokenName;
    var token = SatellizerStorage.get(tokenName);
    if (!token) return;
    header[SatellizerConfig.tokenHeader] = "".concat(SatellizerConfig.tokenType, " ").concat(token);
    angular.extend(config.headers, header);
  }
}];

},{}],95:[function(require,module,exports){
'use strict';

var _util = require("commons/util");

var _urlTypes = require("commons/constants/url-types");

var _auth = require("commons/constants/auth");

/**
 * @author Nicolas Andres Rivera Acevedo
 * @description factory para peticiones bajo el recurso /fe/auth
 * y lógica que tiene que ver con la autenticación de usuario.
 */
module.exports = ['$auth', '$http', '$url', '$auditoria', '$wizardCrearNegocio', '$tipoWizard', function (auth, http, url, auditoria, $wizardCrearNegocio, $tipoWizard) {
  /**
   * @description indica a satellizer que utilice sessionStorage 
   * para almacenar el token
   */
  auth.setStorageType('sessionStorage');

  function post(endpoint) {
    return (0, _util.obtenerServicioPost)(http, endpoint);
  }

  return {
    autenticar: autenticar,
    redireccionar: redireccionar,
    revoke: revoke,
    logout: logout,
    getToken: getToken,
    obtenerAutorizacion: obtenerAutorizacion,
    obtenerTokenRenovar: obtenerTokenRenovar,
    obtenerTipoAuth: obtenerTipoAuth
  };

  function obtenerTipoAuth() {
    return http.get('/fe/auth/tipo');
  }
  /**
   * @description función encargada de cerrar sesión en el back, para revocar
   * la renovación del token de autenticación
   */


  function revoke(params) {
    return post('/fe/auth/logout')(params);
  }
  /**
   * @description Función encargada de establecer el término de autenticación
   * borrando el token local.
   */


  function logout() {
    auth.logout();
  }
  /**
   * @description Función encargada de obtener el token almacenado por satellizer
   */


  function getToken() {
    return auth.getToken();
  }
  /**
   * @description Función encargada de realizar la autenticación
   * @param {Object} params objeto parámetros para autenticación
   */


  function autenticar(params) {
    params.Token = params.ticket || params.Token;
    return post('/fe/auth')(params).then(function (resp) {
      sessionStorage.setItem('_feoauth', resp.data.TokenRenovar);
      auth.setToken(resp.data.Token);
      $tipoWizard.setSolocesion(resp.data.IDNegocio > 0);
      $wizardCrearNegocio.setIdNegocio(resp.data.IDNegocio || 0);
    });
  }
  /**
   * @description Función encargada de informar el header de autorización
   */


  function obtenerAutorizacion() {
    var header = {};
    header[_auth.TOKEN_HEADER] = "".concat(_auth.TOKEN_TYPE, " ").concat(auth.getToken());
    /**
     * se agrega auditoria en este nivel, para que todas las
     * subidas de archivos contengan esta información de auditoría.
     * Se hace aquí puesto que en todos los controller que manejan
     * subida de archivos implementan esta función `obtenerAutorizacion`
     * para agregar información en el header del request. Como la información
     * de auditoría también es un atributo del header, la agregamos aquí
     * y así no tenemos que replicarla por todos los controllers.
     */

    angular.extend(header, auditoria.get());
    return header;
  }
  /**
   * @description Función encargada de informar el token necesario
   * para realizar autenticación luego de la caducidad de un token.
   */


  function obtenerTokenRenovar() {
    return sessionStorage.getItem('_feoauth');
  }
  /**
   * @description Función encargada de redireccionar a la url 
   * dada por la constante NO_AUTENTICADO
   */


  function redireccionar() {
    this.obtenerTipoAuth().then(function (resp) {
      var usarSSO = resp.data.UtilizaSso;
      url.get({
        tipo: _urlTypes.NO_AUTENTICADO
      }).then(function (resp) {
        var url = usarSSO ? _urlTypes.URL_LOGIN_SSO : resp.data.URL;
        window.location.replace(url);
      });
    });
  }
}];

},{"commons/constants/auth":7,"commons/constants/url-types":12,"commons/util":41}],96:[function(require,module,exports){
'use strict';
/**
 * @author Nicolas Andres Rivera Acevedo
 * @description Factory para determinar la autenticación y cargar información de auditoría.
 */

module.exports = ['$q', '$feAuthControl', '$injector', '$auditoria', function (q, feAuthControl, injector, auditoria) {
  var reject = null;
  return {
    request: request,
    responseError: responseError
  };
  /**
         * @description Función encargada de revisar el request antes de realizar
   * la petición al back.
         * @param {Object} resp 
         */

  function request(config) {
    if (!feAuthControl.hasAuth(config)) feAuthControl.setAuth(config);
    /**
     * agregamos información de auditoria en el header del request
     */

    auditoria.set(config);
    return config || q.when(config);
  }
  /**
         * @description Función encargada de revisar los errores
         * @param {Object} resp 
         */


  function responseError(resp) {
    if (resp.status == 403) {
      return feAuthControl.renew().then(function () {
        angular.extend(resp.config, {
          loading: false
        });
        return injector.get('$http')(resp.config);
      });
    } else if (reject || resp.status == 401) {
      if (!reject) reject = q.defer();
      feAuthControl.reject();
      return reject.promise;
    }

    return q.reject(resp);
  }
}];

},{}],97:[function(require,module,exports){
'use strict';

var _util = require("commons/util");

var _messages = require("commons/constants/messages");

/**
 * @author Nicolas Andres Rivera Acevedo
 * @description factory para peticiones bajo el recurso /fe/fecha
 */
module.exports = ['$http', '$q', '$popup', function (http, q, popup) {
  return {
    esFechaHabil: esFechaHabil
  };

  function get(endpoint) {
    return (0, _util.obtenerServicioGet)(http, endpoint);
  }

  function esFechaHabil(fecha) {
    var defer = q.defer();
    if (!(0, _util.date)(fecha)) defer.reject();else {
      get('/fe/fecha/habil')({
        valor: fecha
      }).then(function (resp) {
        if (resp.status != 200) {
          popup.errorAlert(resp.data.Message);
          defer.reject();
          return;
        }

        if (resp.data.esHabil) {
          defer.resolve();
        } else {
          popup.warnAlert(_messages.SELECCIONE_FECHA_HABIL);
          defer.reject();
        }
      });
    }
    return defer.promise;
  }
}];

},{"commons/constants/messages":11,"commons/util":41}],98:[function(require,module,exports){
'use strict';

var _util = require("commons/util");

/**
 * @author Nicolas Andres Rivera Acevedo
 * @description Servicio para inicializar el script de google manager .
 * Se utiliza para incrustar el iframe .
 * Se utiliza la llamada para obtener las constantes , ya que ha esta altura 
 * aun no se encuentra inyectado.
 */
module.exports = ['$http', '$constantes', function (http, constantes) {
  /*    const PORT       = constantes.obtener().SISTEMA.PUERTO_SOCKET; */
  return {
    iniciaGooleManager: initGoogleManager
  };
  /**
   * @description inicializa la funcion de Google Manager usando el ID de las constantes
   */

  function initGoogleManager() {
    (0, _util.obtenerServicioGet)(http, '/fe/constantes')().then(function (resp) {
      var GTM_ID = decodeString(resp.data.constantes.GOOGLE_ANALITYCS.TAG_CODE);

      (function (w, d, s, l, i) {
        w[l] = w[l] || [];
        w[l].push({
          'gtm.start': new Date().getTime(),
          event: 'gtm.js'
        });
        var f = d.getElementsByTagName(s)[0],
            j = d.createElement(s),
            dl = l != 'dataLayer' ? '&l=' + l : '';
        j.async = true;
        j.src = 'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
        f.parentNode.insertBefore(j, f);
      })(window, document, 'script', 'dataLayer', GTM_ID);

      appendIframe(GTM_ID);
    });
  }
  /**
   * @description Decodifica un string 
   * @param {encodeString} string string a modifca
   */


  function decodeString(encodeString) {
    return window.atob(encodeString);
  }
  /**
   * @description Crea un iframe dentro del TAG noscript
   * 
   */


  function appendIframe(tag_id) {
    var iframe_gtm = document.createElement("iframe");
    iframe_gtm.setAttribute("src", "https://www.googletagmanager.com/ns.html?id=" + tag_id);
    iframe_gtm.setAttribute("height", 0);
    iframe_gtm.setAttribute("width", 0);
    iframe_gtm.setAttribute("style", "display:none;visibility:hidden");
    document.getElementById("googleManagerReceder").appendChild(iframe_gtm);
  }
}];

},{"commons/util":41}],99:[function(require,module,exports){
'use strict';
/**
 * @author Nicolas Andres Rivera Acevedo
 * @description indice de todos los servicios del modulo 'app.receder'
 */

angular.module('app.receder').factory('$init', require('./init')).factory('$auth', require('./auth')).factory('$authStorage', require('./auth-storage')).factory('$rutContextoCrypto', require('./rut-contexto-crypto')).factory('$constantes', require('./constantes')).factory('$googleTagManager', require('./google-tag-manager')).service('$init', require('./init')).factory('$feAuth', require('./fe-auth')).factory('$feAuthControl', require('./fe-auth-control')).factory('$feInterceptor', require('./fe-interceptor')).factory('$socket', require('./socket')).factory('$constantes', require('./constantes')).factory('$resumenEjecutivo', require('./resumen-ejecutivo')).factory('$negocioEstados', require('./negocio-estados')).factory('$negocioPasos', require('./negocio-pasos')).factory('$negocioFacturas', require('./negocio-facturas')).factory('$negocioFiltros', require('./negocio-filtros')).factory('$negocioResumen', require('./negocio-resumen')).factory('$cliente', require('./cliente')).factory('$documentoCertificado', require('./documento-certificado')).factory('$documentoRespaldo', require('./documento-respaldo')).factory('$documentoDescarga', require('./documento-descarga')).factory('$resultadoEnvioNegocio', require('./resultado-envio-negocio')).factory('$resultadoValidacionNegocio', require('./resultado-validacion-negocio')).factory('$negocioAceptaCotizacion', require('./negocio-acepta-cotizacion')).factory('$negocioAsociaCertificado', require('./negocio-asocia-certificado')).factory('$revisionCambioEstado', require('./revision-cambio-estado')).factory('$negocio', require('./negocio')).factory('$tipo', require('./tipo')).factory('$url', require('./url')).factory('$fecha', require('./fecha')).factory('$negocioRecede', require('./negocio-recede')).factory('$clienteContacto', require('./cliente-contacto')).factory('$dte', require('./dte')).factory('$wsApi', require('./wsApi')).factory('$auditoria', require('./auditoria')).factory('$negocioAlertas', require('./negocio-alertas')).factory('$tipoWizard', require('./tipoWizard')).factory('$documentosGestionar', require('./documentos-gestionar')).factory('$receder', require('./receder'));

},{"./auditoria":83,"./auth":85,"./auth-storage":84,"./cliente":87,"./cliente-contacto":86,"./constantes":88,"./documento-certificado":89,"./documento-descarga":90,"./documento-respaldo":91,"./documentos-gestionar":92,"./dte":93,"./fe-auth":95,"./fe-auth-control":94,"./fe-interceptor":96,"./fecha":97,"./google-tag-manager":98,"./init":100,"./negocio":110,"./negocio-acepta-cotizacion":101,"./negocio-alertas":102,"./negocio-asocia-certificado":103,"./negocio-estados":104,"./negocio-facturas":105,"./negocio-filtros":106,"./negocio-pasos":107,"./negocio-recede":108,"./negocio-resumen":109,"./receder":111,"./resultado-envio-negocio":112,"./resultado-validacion-negocio":113,"./resumen-ejecutivo":114,"./revision-cambio-estado":115,"./rut-contexto-crypto":116,"./socket":117,"./tipo":118,"./tipoWizard":119,"./url":120,"./wsApi":121}],100:[function(require,module,exports){
'use strict';
/**
 * @author Nicolas Andres Rivera Acevedo
 * @description servicio inicial para la aplicación, este servicio
 * resuelve promesas de peticiones que deben estar antes de que se cargue la app.
 */

module.exports = ['$q', '$constantes', function (q, constantes) {
  var defer = q.defer();
  return {
    getPromise: getPromise,
    resolve: resolve
  };
  /**
   * @description Informa la promesa de todas las subpromesas resueltas.
   */

  function getPromise() {
    return defer.promise;
  }
  /**
   * @description Función que se encarga de resolver todas las
   * promesas de peticiones que deben ejecutarse antes de cargar
   * la app
   */


  function resolve() {
    q.all(obtenerPromesas()).then(function () {
      defer.resolve();
    });
  }
  /**
   * @description Función encargada de informar todas las
   * promesas que deben resolverse.
   * @returns {Array}
   */


  function obtenerPromesas() {
    /**
     * Agregar a este array todas las funciones
     * que informan la promesa a resolver:
     */
    return [constantes.get()];
  }
}];

},{}],101:[function(require,module,exports){
'use strict';

var _util = require("commons/util");

/**
 * @author Nicolas Andres Rivera Acevedo
 * @description factory para peticiones bajo el recurso /fe/negocio/acepta-cotizacion
 */
module.exports = ['$http', '$tipoWizard', function (http, $tipoWizard) {
  function post(params) {
    return http.post((0, _util.parsePath)('/{prefix}/negocio/acepta-cotizacion', {
      prefix: $tipoWizard.obtenerPrefijo()
    }), params).then(function (resp) {
      return resp;
    })["catch"](function (resp) {
      return resp;
    });
  }

  return {
    post: post
  };
}];

},{"commons/util":41}],102:[function(require,module,exports){
'use strict';

var _util = require("commons/util");

/**
 * @author Nicolas Andres Rivera Acevedo
 * @description factory para peticiones bajo el recurso /faa/negocio
 */
module.exports = ['$http', '$tipoWizard', function (http, $tipoWizard) {
  function del(params) {
    var p = {
      prefix: $tipoWizard.obtenerPrefijo()
    };
    angular.extend(p, params);
    return http["delete"]((0, _util.parsePath)('/{prefix}/negocio/{idNegocio}/alertas/{idAlerta}', p), {
      params: params
    }).then(function (resp) {
      return resp;
    })["catch"](function (resp) {
      return resp;
    });
  }

  return {
    "delete": del
  };
}];

},{"commons/util":41}],103:[function(require,module,exports){
'use strict';

var _util = require("commons/util");

/**
 * @author Nicolas Andres Rivera Acevedo
 * @description factory para peticiones bajo el recurso /fe/negocio/asocia-certificado
 */
module.exports = ['$http', '$tipoWizard', function (http, $tipoWizard) {
  function post(params) {
    return http.post((0, _util.parsePath)('/{prefix}/negocio/asocia-certificado', {
      prefix: $tipoWizard.obtenerPrefijo()
    }), params).then(function (resp) {
      return resp;
    })["catch"](function (resp) {
      return resp;
    });
  }

  return {
    post: post
  };
}];

},{"commons/util":41}],104:[function(require,module,exports){
'use strict';

var _util = require("commons/util");

/**
 * @author Nicolas Andres Rivera Acevedo
 * @description factory para peticiones bajo el recurso /fe/negocio/estados
 */
module.exports = ['$http', function (http) {
  function get(endpoint) {
    return (0, _util.obtenerServicioGet)(http, endpoint);
  }

  return {
    get: get('/fe/negocio/estados')
  };
}];

},{"commons/util":41}],105:[function(require,module,exports){
'use strict';

var _util = require("commons/util");

/**
 * @author Nicolas Andres Rivera Acevedo
 * @description factory para peticiones bajo el recurso /fe/negocio/facturas
 */
module.exports = ['$http', '$tipoWizard', function (http, $tipoWizard) {
  function get(params) {
    return http.get('/fe/negocio/facturas', {
      params: params
    }).then(function (resp) {
      return resp;
    })["catch"](function (resp) {
      return resp;
    });
  }

  function post(params) {
    return http.post((0, _util.parsePath)('/{prefix}/negocio/facturas', {
      prefix: $tipoWizard.obtenerPrefijo()
    }), params).then(function (resp) {
      return resp;
    })["catch"](function (resp) {
      return resp;
    });
  }

  function adjuntar(params) {
    return http.post((0, _util.parsePath)('/{prefix}/negocio/facturas/adjuntas', {
      prefix: $tipoWizard.obtenerPrefijo()
    }), params).then(function (resp) {
      return resp;
    })["catch"](function (resp) {
      return resp;
    });
  }

  function listarRequiereReparar(params) {
    return http.get((0, _util.parsePath)('/{prefix}/negocio/facturas/requiere-reparar', {
      prefix: $tipoWizard.obtenerPrefijo()
    }), {
      params: params
    }).then(function (resp) {
      return resp;
    })["catch"](function (resp) {
      return resp;
    });
  }

  return {
    get: get,
    post: post,
    adjuntar: adjuntar,
    listarRequiereReparar: listarRequiereReparar
  };
}];

},{"commons/util":41}],106:[function(require,module,exports){
'use strict';

var _util = require("commons/util");

/**
 * @author Nicolas Andres Rivera Acevedo
 * @description factory para peticiones bajo el recurso /fe/negocio/pasos
 */
module.exports = ['$http', '$tipoWizard', function (http, $tipoWizard) {
  function get(params) {
    return http.get((0, _util.parsePath)('/{prefix}/negocio/filtros', {
      prefix: $tipoWizard.obtenerPrefijo()
    }), {
      params: params
    }).then(function (resp) {
      return resp;
    })["catch"](function (resp) {
      return resp;
    });
  }

  return {
    get: get
  };
}];

},{"commons/util":41}],107:[function(require,module,exports){
'use strict';

var _util = require("commons/util");

/**
 * @author Nicolas Andres Rivera Acevedo
 * @description factory para peticiones bajo el recurso /fe/negocio/pasos
 */
module.exports = ['$http', '$tipoWizard', function (http, $tipoWizard) {
  function get(params) {
    return http.get((0, _util.parsePath)('/{prefix}/negocio/pasos', {
      prefix: $tipoWizard.obtenerPrefijo()
    }), {
      params: params
    }).then(function (resp) {
      return resp;
    })["catch"](function (resp) {
      return resp;
    });
  }

  function post(params) {
    return http.post((0, _util.parsePath)('/{prefix}/negocio/pasos', {
      prefix: $tipoWizard.obtenerPrefijo()
    }), params).then(function (resp) {
      return resp;
    })["catch"](function (resp) {
      return resp;
    });
  }

  return {
    get: get,
    post: post
  };
}];

},{"commons/util":41}],108:[function(require,module,exports){
'use strict';

var _util = require("commons/util");

/**
 * @author Nicolas Andres Rivera Acevedo
 * @description factory para peticiones bajo el recurso /fe/negocio/recede
 */
module.exports = ['$http', function (http) {
  function post(endpoint) {
    return (0, _util.obtenerServicioPost)(http, endpoint);
  }

  return {
    post: post('/fe/negocio/recede')
  };
}];

},{"commons/util":41}],109:[function(require,module,exports){
'use strict';

var _util = require("commons/util");

/**
 * @author Nicolas Andres Rivera Acevedo
 * @description factory para peticiones bajo el recurso /fe/negocio/resumen
 */
module.exports = ['$http', '$tipoWizard', function (http, $tipoWizard) {
  function get(params) {
    return http.get((0, _util.parsePath)('/{prefix}/negocio/resumen', {
      prefix: $tipoWizard.obtenerPrefijo()
    }), {
      params: params
    }).then(function (resp) {
      return resp;
    })["catch"](function (resp) {
      return resp;
    });
  }

  return {
    get: get
  };
}];

},{"commons/util":41}],110:[function(require,module,exports){
'use strict';

var _util = require("commons/util");

/**
 * @author Nicolas Andres Rivera Acevedo
 * @description factory para peticiones bajo el recurso /fe/negocio
 */
module.exports = ['$http', function (http) {
  function get(endpoint) {
    return (0, _util.obtenerServicioGet)(http, endpoint);
  }

  function del(endpoint) {
    return (0, _util.obtenerServicioDelete)(http, endpoint);
  }

  return {
    get: get('/fe/negocio'),
    "delete": del('/fe/negocio/{idNegocio}')
  };
}];

},{"commons/util":41}],111:[function(require,module,exports){
'use strict';
/**
 * @author Nicolas Andres Rivera Acevedo
 * @description factory de control de servicios a utilizar en redecer
 */

module.exports = ['$negocioFacturas', '$tipo', '$negocioRecede', function (negocioFacturas, tipo, negocioRecede) {
  return {
    obtenerFacturas: negocioFacturas.get,
    obtenerTipoRecesion: obtenerTipoRecesion,
    recederDocumento: negocioRecede.post
  };

  function obtenerTipoRecesion() {
    return tipo.get({
      Tipo: 'Recesion'
    });
  }
}];

},{}],112:[function(require,module,exports){
'use strict';

var _util = require("commons/util");

/**
 * @author Nicolas Andres Rivera Acevedo
 * @description factory para peticiones bajo el recurso /fe/negocio/resultado-envio-negocio'
 */
module.exports = ['$http', function (http) {
  function get(endpoint) {
    return (0, _util.obtenerServicioGet)(http, endpoint);
  }

  return {
    get: get('/fe/negocio/resultado-envio-negocio')
  };
}];

},{"commons/util":41}],113:[function(require,module,exports){
'use strict';

var _util = require("commons/util");

/**
 * @author Nicolas Andres Rivera Acevedo
 * @description factory para peticiones bajo el recurso /fe/negocio/resultado-validacion-negocio
 */
module.exports = ['$http', function (http) {
  function get(endpoint) {
    return (0, _util.obtenerServicioGet)(http, endpoint);
  }

  return {
    get: get('/fe/negocio/resultado-validacion-negocio')
  };
}];

},{"commons/util":41}],114:[function(require,module,exports){
'use strict';

var _util = require("commons/util");

/**
 * @author Nicolas Andres Rivera Acevedo
 * @description factory para peticiones bajo el recurso /fe/resumen-ejecutivo
 */
module.exports = ['$http', function (http) {
  function get(endpoint) {
    return (0, _util.obtenerServicioGet)(http, endpoint);
  }

  return {
    get: get('/fe/resumen-ejecutivo')
  };
}];

},{"commons/util":41}],115:[function(require,module,exports){
'use strict';

var _util = require("commons/util");

/**
 * @author Nicolas Andres Rivera Acevedo
 * @description factory para peticiones bajo el recurso /fe/negocio/revision-cambio-estado
 */
module.exports = ['$http', '$tipoWizard', function (http, $tipoWizard) {
  function get(params) {
    return http.get((0, _util.parsePath)('/{prefix}/negocio/revision-cambio-estado', {
      prefix: $tipoWizard.obtenerPrefijo()
    }), {
      params: params
    }).then(function (resp) {
      return resp;
    })["catch"](function (resp) {
      return resp;
    });
  }

  return {
    get: get
  };
}];

},{"commons/util":41}],116:[function(require,module,exports){
'use strict';
/**
 * @author Nicolas Rivera Acevedo
 * @description factory para generar un _checksum basado en el _token y RutContexto.
 */

module.exports = ['$authStorage', function (authStorage) {
  var _params = authStorage.getParams();

  return {
    getCheckSum: sha256
  };

  function sha256(ascii) {
    if (_params.Token == null || _params.Token == undefined || _params.Token == '') {
      return '';
    }

    if (!ascii) {
      ascii = _params.Token + _params.RutContexto;
    }

    function rightRotate(value, amount) {
      return value >>> amount | value << 32 - amount;
    }

    ;
    var mathPow = Math.pow;
    var maxWord = mathPow(2, 32);
    var lengthProperty = 'length';
    var i, j; // Used as a counter across the whole file

    var result = '';
    var words = [];
    var asciiBitLength = ascii[lengthProperty] * 8; //* caching results is optional - remove/add slash from front of this line to toggle
    // Initial hash value: first 32 bits of the fractional parts of the square roots of the first 8 primes
    // (we actually calculate the first 64, but extra values are just ignored)

    var hash = sha256.h = sha256.h || []; // Round constants: first 32 bits of the fractional parts of the cube roots of the first 64 primes

    var k = sha256.k = sha256.k || [];
    var primeCounter = k[lengthProperty];
    /*/
    var hash = [], k = [];
    var primeCounter = 0;
    //*/

    var isComposite = {};

    for (var candidate = 2; primeCounter < 64; candidate++) {
      if (!isComposite[candidate]) {
        for (i = 0; i < 313; i += candidate) {
          isComposite[i] = candidate;
        }

        hash[primeCounter] = mathPow(candidate, .5) * maxWord | 0;
        k[primeCounter++] = mathPow(candidate, 1 / 3) * maxWord | 0;
      }
    }

    ascii += '\x80'; // Append Ƈ' bit (plus zero padding)

    while (ascii[lengthProperty] % 64 - 56) {
      ascii += '\x00';
    } // More zero padding


    for (i = 0; i < ascii[lengthProperty]; i++) {
      j = ascii.charCodeAt(i);
      if (j >> 8) return; // ASCII check: only accept characters in range 0-255

      words[i >> 2] |= j << (3 - i) % 4 * 8;
    }

    words[words[lengthProperty]] = asciiBitLength / maxWord | 0;
    words[words[lengthProperty]] = asciiBitLength; // process each chunk

    for (j = 0; j < words[lengthProperty];) {
      var w = words.slice(j, j += 16); // The message is expanded into 64 words as part of the iteration

      var oldHash = hash; // This is now the undefinedworking hash", often labelled as variables a...g
      // (we have to truncate as well, otherwise extra entries at the end accumulate

      hash = hash.slice(0, 8);

      for (i = 0; i < 64; i++) {
        // var i2 = i + j; never used
        // Expand the message into 64 words
        // Used below if 
        var w15 = w[i - 15],
            w2 = w[i - 2]; // Iterate

        var a = hash[0],
            e = hash[4];
        var temp1 = hash[7] + (rightRotate(e, 6) ^ rightRotate(e, 11) ^ rightRotate(e, 25)) // S1
        + (e & hash[5] ^ ~e & hash[6]) // ch
        + k[i] // Expand the message schedule if needed
        + (w[i] = i < 16 ? w[i] : w[i - 16] + (rightRotate(w15, 7) ^ rightRotate(w15, 18) ^ w15 >>> 3) // s0
        + w[i - 7] + (rightRotate(w2, 17) ^ rightRotate(w2, 19) ^ w2 >>> 10) // s1
        | 0); // This is only used once, so *could* be moved below, but it only saves 4 bytes and makes things unreadble

        var temp2 = (rightRotate(a, 2) ^ rightRotate(a, 13) ^ rightRotate(a, 22)) + ( // S0
        a & hash[1] ^ a & hash[2] ^ hash[1] & hash[2]); // maj

        hash = [temp1 + temp2 | 0].concat(hash); // We don't bother trimming off the extra ones, they're harmless as long as we're truncating when we do the slice()

        hash[4] = hash[4] + temp1 | 0;
      }

      for (i = 0; i < 8; i++) {
        hash[i] = hash[i] + oldHash[i] | 0;
      }
    }

    for (i = 0; i < 8; i++) {
      for (j = 3; j + 1; j--) {
        var b = hash[i] >> j * 8 & 255;
        result += (b < 16 ? 0 : '') + b.toString(16);
      }
    }

    return result;
  }

  ;
}];

},{}],117:[function(require,module,exports){
"use strict";

/**
 * @author Nicolas Andres Rivera Acevedo
 * @description Servicio para comunicación por websocket
 */
module.exports = ['$rootScope', '$constantes', '$location', '$wizardCrearNegocio', '$wsApi', function (rootScope, constantes, location, wizardCrearNegocio, wsApi) {
  var socket = null;
  var pending = false; //flag para no realizar petición de eventos cuando el back aún no ha respondido la pedida anterior.

  var lastUpdate = null; //última aplicación de eventos

  var PORT = constantes.obtener().SISTEMA.PUERTO_SOCKET;
  var HOST = location.host();
  var PROTOCOL = location.protocol().toLowerCase().lastIndexOf('s') > 0 ? 'wss' : 'ws';
  var SERVER = "".concat(PROTOCOL, "://").concat(HOST, ":").concat(PORT);
  var SEGUNDOS_API_SOCKET = constantes.obtener().SISTEMA.SEGUNDOS_API_SOCKET;

  var _constantes$obtener = constantes.obtener(),
      SOCKET = _constantes$obtener.SOCKET;

  return {
    conectar: conectarAPI,
    desconectar: desconectarAPI
  };
  /**
   * @description permite realizar la conexión por websocket con el servidor dado por SERVER.
   * @deprecated en producción se negó la posibilidad de utilizar websocket por politicas de seguridad.
   */

  function conectar() {
    var self = this;
    if (socket && (socket.readyState == WebSocket.OPEN || socket.readyState == WebSocket.CONNECTING)) return;
    socket = new WebSocket(SERVER);

    socket.onmessage = function (event) {
      var resp = angular.fromJson(event.data);
      if (resp.IDNegocio != wizardCrearNegocio.getIdNegocio()) return;
      rootScope.$apply(function () {
        rootScope.$broadcast(resp.Evento, resp);
      });
    };

    socket.onclose = function () {
      if (this._manualClose) return;
      self.conectar();
    };
  }
  /**
   * @description desconecta la sesión websocket actual.
   * @deprecated en producción se negó la posibilidad de utilizar websocket por politicas de seguridad.
   */


  function desconectar() {
    if (!socket || socket.readyState == WebSocket.CLOSED) return;
    socket._manualClose = true;
    socket.close();
  }
  /**
   * @description genera conexión mediante setInterval a una API
   * que resuelve las posibles respuestas sin utilizar websocket.
   */


  function conectarAPI() {
    if (socket) return;
    pending = false;
    socket = setInterval(function () {
      if (pending) return;
      pending = true;
      wsApi.obtenerEventosNegocio({
        IDNegocio: wizardCrearNegocio.getIdNegocio()
      }, {
        loading: false
      }).then(function (resp) {
        // pending = false;
        // if (!resp.data) return;
        // if (lastUpdate != null 
        //     && resp.data.FechaNotificacion > lastUpdate
        //     && resp.data.EventosNotificacion.length <= 0){
        //         refrescar();
        // } else {
        //     lastUpdate = resp.data.FechaNotificacion;
        //     angular.forEach(resp.data.EventosNotificacion, function(resp){
        //         rootScope.$broadcast(resp.Evento, resp);
        //     });
        // }
        angular.forEach(resp.data.EventosNotificacion, function (resp) {
          rootScope.$broadcast(resp.Evento, resp);
        });
        pending = false;
      });
    }, SEGUNDOS_API_SOCKET * 1000);
  }
  /**
   * @description realiza un refresco pesimista,
   * reinicia todos los controllers del state actual
   * de negocio activo.
   */


  function refrescar() {
    wizardCrearNegocio.limpiarPasos();
    wizardCrearNegocio.obtenerPasos().then(function () {
      rootScope.$broadcast(SOCKET.NEGOCIO_CAMBIO_PASO);
    });
  }
  /**
   * @description cancela las llamadas a la api
   */


  function desconectarAPI() {
    clearInterval(socket);
    socket = null;
  }
}];

},{}],118:[function(require,module,exports){
'use strict';

var _util = require("commons/util");

/**
 * @author Nicolas Andres Rivera Acevedo
 * @description factory para peticiones bajo el recurso /fe/tipo
 */
module.exports = ['$http', function (http) {
  function get(endpoint) {
    return (0, _util.obtenerServicioGet)(http, endpoint);
  }

  return {
    get: get('/fe/tipo')
  };
}];

},{"commons/util":41}],119:[function(require,module,exports){
'use strict';

var _wizard = require("commons/constants/wizard");

/**
 * @author Nicolas Andres Rivera Acevedo
 * @description factory determinar el tipo de negocio en curso
 */
module.exports = [function () {
  var _esSolocesion = false;
  return {
    setSolocesion: function setSolocesion(bool) {
      _esSolocesion = bool;
    },
    esSolocesion: function esSolocesion() {
      return _esSolocesion;
    },
    obtenerPrefijo: function obtenerPrefijo() {
      return _esSolocesion ? _wizard.SOLO_CESION : _wizard.NEGOCIO;
    }
  };
}];

},{"commons/constants/wizard":13}],120:[function(require,module,exports){
'use strict';

var _util = require("commons/util");

/**
 * @author Nicolas Andres Rivera Acevedo
 * @description factory para peticiones bajo el recurso /fe/url
 */
module.exports = ['$http', function (http) {
  function get(endpoint) {
    return (0, _util.obtenerServicioGet)(http, endpoint);
  }

  return {
    get: get('/fe/url')
  };
}];

},{"commons/util":41}],121:[function(require,module,exports){
'use strict';

var _util = require("commons/util");

/**
 * @author Nicolas Andres Rivera Acevedo
 * @description factory para peticiones bajo el recurso /fe/ws
 */
module.exports = ['$http', function (http) {
  function get(params) {
    return http.get((0, _util.parsePath)('/fe/ws/negocio/{idNegocio}', params), {
      params: params
    }).then(function (resp) {
      return resp;
    })["catch"](function (resp) {
      return resp;
    });
  }

  return {
    obtenerEventosNegocio: get
  };
}];

},{"commons/util":41}],122:[function(require,module,exports){
module.exports = "<div class=\"col-md-4 col-sm-12 p-0 mb-2\"><nav aria-label=breadcrumb><ol class=\"breadcrumb pl-0\"><li class=breadcrumb-item><i class=\"fa fa-file\"></i> Receder</li></ol></nav></div><article class=\"col-12 p-0\"><div class=table-responsive><table class=\"table table-striped table-sortable\"><thead class=\"bg-violeta text-white thead-filter\"><tr><th orden=RutCliente><span>Rut Cliente (receptor)</span></th><th orden=NombreCompleto><span>Nombre/Razón social</span></th><th orden=Folio><span>Folio</span></th><th orden=FechaEmision><span>Fecha Emisión</span></th><th orden=FechaVencimientoNegocio><span>Fecha Vencimiento Factoring</span></th><th orden=MontoTotalPesos><span>Monto $</span></th><th orden=Tipo><span>Tipo</span></th><th orden=Tenedor><span>Tenedor</span></th><th><span>Info. Adicional de la factura</span></th><th><span>Información Ultima Cesión</span></th><th><span>Factura</span></th><th><span>Nota Crédito/Débito</span></th><th></th></tr></thead><tbody><tr ng-repeat=\"item in datosFacturas\"><td class=\"text-left text-nowrap\">{{item.RutCliente | rut}}</td><td class=text-left>{{item.NombreCompleto}}</td><td class=text-center>{{item.Folio}}</td><td class=text-center>{{item.FechaEmision}}</td><td class=text-center>{{item.FechaVencimientoNegocio}}</td><td class=text-right>{{item.MontoTotalPesos | formatMiles}}</td><td class=text-center><label class=\"br-5 pl-2 pr-2\" ng-class=item.Tipo.toLowerCase()>{{item.Tipo}}</label></td><td class=text-center>{{item.Tenedor}}</td><td class=text-center><ux-info-factura class=\"hst hst-information cursor\" info=item.InfoAdicionalFactura ng-if=item.InfoAdicionalFactura></ux-info-factura></td><td class=text-center><ux-info-cesion class=\"hst hst-information cursor\" info=item.InfoUltimaCesion ng-if=item.InfoUltimaCesion></ux-info-cesion></td><td class=text-center><ux-download icon=pdf tipo=item.TipoDocumentoFactura id=item.IDDocumento></ux-download></td><td class=text-center><ux-download icon=pdf tipo=item.TipoDocumentoNotaCredito id=item.IDNotaCredito></ux-download></td><td class=text-center><button class=\"btn btn-sm btn-secondary br-3\" ng-click=recederDocumento(item)>Receder</button></td></tr><tr ng-if=!hayDatosFacturas><td colspan=13 class=\"text-center alert-warning p-2\"><span>No existen Facturas para re-ceder</span></td></tr></tbody></table></div><ux-pagination ng-model=Paginacion load=obtenerFacturas()></ux-pagination></article>";

},{}],123:[function(require,module,exports){
module.exports = "<div class=\"container p-0 error-page\"><div class=\"alert alert-danger mb-0 p-3\"><i class=\"fa fa-times-circle pr-2\"></i> Algo ha salido mal.</div><div class=\"error-page-content p-3 text-center\">Es posible que se haya generado una Excepción, favor volver a intentar.<br>En caso de que el problema persista contactarse con el administrador.</div></div>";

},{}],124:[function(require,module,exports){
module.exports = "<div class=modal-header><h5 class=modal-title ng-if=!esAgregarNotaCredio>Reemplazar Documento - Folio {{documento.Folio}}</h5><h5 class=modal-title ng-if=esAgregarNotaCredio>Agregar Nota de Crédito/Débito - Folio {{documento.Folio}}</h5><button type=button class=close data-dismiss=modal aria-label=Close ng-click=volver()><span aria-hidden=true>&times;</span></button></div><div class=modal-body><div flow-init=$flowInit flow-name=$parent.$flow flow-files-submitted=\"flowFilesSubmitted($files, $event, $flow)\" flow-file-added=\"flowFileAdded($file, $event, $flow)\" flow-file-error=\"flowFileError($file, $message, $flow)\" flow-file-success=\"flowFileSuccess($file, $message, $flow)\" flow-complete=flowComplete($flow)><div class=\"reemplazar-documento col-12\" ux-validar=$parent><div class=\"row mb-2\"><div class=col-10><div><strong>Documento</strong></div><div><div class=input-file><input class=align-middle type=text ng-model=modelo.documento.name readonly=readonly required> <button class=\"btn btn-sm btn-primary mr-1\" flow-btn><i class=\"fa fa-search\"></i> Seleccionar</button></div></div></div><div class=\"col-2 text-right\"><button class=\"btn btn-sm btn-primary mt-3 btn-carga\" ng-click=subirDocumento()><i class=\"fa fa-upload\"></i> Cargar</button></div></div></div></div></div><div class=modal-footer></div>";

},{}],125:[function(require,module,exports){
module.exports = "<div class=modal-header><h5 class=modal-title>Receder Documento - Folio {{documento.Folio}}</h5><button type=button class=close data-dismiss=modal aria-label=Close ng-click=volver()><span aria-hidden=true>&times;</span></button></div><div class=modal-body ux-validar><div class=\"row mb-2\"><div class=col-4><div><strong>Tipo Recesión</strong></div><div><select class=\"p-1 w-100\" ng-model=recesion.TipoRecesion ng-options=\"op.ID as op.Nombre for op in ListaTipoRecesion\" required></select></div></div></div><div ng-if=\"recesion.TipoRecesion == 2\"><div class=\"row mb-2\"><div class=col-4><div><strong>Rut</strong></div><div><input class=\"w-100 form-control\" type=text ng-model=recesion.Cliente.Rut ux-rut ng-keyup=obtenerDatosCliente($event) ng-blur=obtenerDatosCliente($event) ux-validar-tipo=rut required></div></div><div class=col-8><div><strong>Dirección</strong></div><div><input class=\"w-100 form-control\" type=text ng-model=recesion.Cliente.DireccionContacto required></div></div></div><div class=\"row mb-2\"><div class=col-4><div><strong>Razón Social</strong></div><div><input class=\"w-100 form-control\" type=text ng-model=recesion.Cliente.NombreCompleto required></div></div><div class=col-4><div><strong>E-Mail</strong></div><div><input class=\"w-100 form-control\" type=text ng-model=recesion.Cliente.EmailContacto ux-validar-tipo=email required></div></div><div class=col-4><div><strong>Último Vencimiento</strong></div><div><input class=form-control type=text ng-model=recesion.UltimoVencimiento ux-datepicker required min-date=fechaActual></div></div></div></div></div><div class=modal-footer><button ng-click=recederDocumento() class=\"btn btn-primary\">Aceptar</button></div>";

},{}],126:[function(require,module,exports){
'use strict';
/**
 * @description Scripts para manejar los eventos en el document para tratar acciones
 * relacionadas con popovers
 * @author Mauricio Ross
 */

var $ = require('jquery');

var popover = {
  selected: null
};
/** 
 * Al hacer click mientras la directiva esta en contexto esta validara si se ha hecho click dentro o fuera de la directiva o dentro de un hijo de la directiva. 
 */

$(document).on('click', function (e) {
  if (!$(e.target).is(popover.selected) && !$(e.target).closest('.popover').is(popover.selected)) {
    $(popover.selected).popover('hide');
  }
});
/** 
 * el evento shown.bs.popover ejecuta la accion mientras se trabaja en el css de la directiva, con el objetivo de asignar a la variable global selected el popover seleccionado. 
 */

$(document).on('shown.bs.popover', function () {
  popover.selected = $('.popover');
});
module.exports = popover;

},{"jquery":"jquery"}],127:[function(require,module,exports){
"use strict";

/**
 * @author Nicolas Andres Rivera Acevedo
 * @description script con métodos genéricos que permiten la transformación de objetos
 * no utilizan inyección de dependencia de angular
 */
var lodash = require('lodash');

module.exports = {
  normaliza: _normaliza,
  agregarSeleccione: _agregarSeleccione,
  agregarItemEspecial: _agregarItemEspecial,
  obtenerMapa: _obtenerMapa,
  agruparMapa: _agruparMapa,
  remapear: _remapear,
  lowerCaseObject: _lowerCaseObject
};
/**
 * @description normaliza la colección dada por parámetro para agregar un identificador dado por key
 * @param {Array} coleccion array de objetos
 * @param {String} key identificador para agregar
 */

function _normaliza(coleccion, key) {
  if (!coleccion || typeof key == 'undefined') return coleccion;

  for (var i = 0; i < coleccion.length; i++) {
    if (typeof coleccion[i][key] != 'undefined') continue;
    coleccion[i][key] = i + 1;
  }

  return coleccion;
}
/**
 * @description agrega un item seleccione a la colección
 * @param {Array} coleccion colección de objetos
 * @param {String} key prop a la cual agregar el valor 0
 * @param {String} label prop a la cual agregar el valor '-- seleccione --'
 */


function _agregarSeleccione(coleccion, key, label) {
  return _agregarItemEspecial(coleccion, key, label, 0, '-- Seleccione --');
}
/**
 * @description agrega un item especial a la colección
 * @param {Array} coleccion colección de objetos
 * @param {String} key prop a la cual agregar el valor dado por keyValue
 * @param {String} label prop a la cual agregar el valor dado por labelValue
 * @param {*} keyValue valor a agregar en la prop dada por key
 * @param {*} labelValue valor a agregar en la prop dada por label
 */


function _agregarItemEspecial(coleccion, key, label, keyValue, labelValue) {
  if (!coleccion || typeof key == 'undefined' || typeof label == 'undefined') return coleccion;

  for (var i = 0; i < coleccion.length; i++) {
    if (coleccion[i][key] == 0) return coleccion;
  }

  var item = {};
  item[key] = keyValue;
  item[label] = labelValue;
  coleccion.splice(0, 0, item);
  return coleccion;
}
/**
 * @description devuelve un objeto literal con claves obtenidas por el parámetro key
 * @param {Array} coleccion colección de objetos
 * @param {String} key nombre del campo del cual obtener la clave
 * @returns objeto donde cada propiedad es clave obtenida por el parámetro key
 */


function _obtenerMapa(coleccion, key) {
  var mapa = {};
  if (!coleccion || typeof key != 'string') return mapa;

  for (var i = 0; i < coleccion.length; i++) {
    var obj = coleccion[i];
    mapa[lodash.get(obj, key)] = obj;
  }

  return mapa;
}
/**
 * @description Agrupa un mapa en Arrays de acuerdo a la cantidad dada por el parámetro cantidad
 * @param {Object} mapa Objeto literal
 * @param {Number} cantidad Número de keys a agrupar
 * @returns {Array} colección de keys agrupadas
 */


function _agruparMapa(mapa, cantidad) {
  var result = [];
  var i = 0;
  angular.forEach(mapa, function (item) {
    var nodo = result[i] || result[result.push([]) - 1];
    nodo.push(item);
    if (nodo.length % cantidad == 0) i++;
  });
  return result;
}
/**
 * @description Reestructura un objeto literal de acuerdo al valor dado por key
 * @param {Object} mapa Objeto literal
 * @param {String} key Nombre del campo o path del campo por cual mapear
 * @param {String} value Nombre del campo o path del campo a obtener el valor a enlazar con key (opcional)
 *  si no es dado el este parámetro, se enlazará el primer nodo/objeto/valor encontrado.
 */


function _remapear(mapa, key, value) {
  var keys = Object.keys(mapa);
  var result = {};

  for (var i = 0; i < keys.length; i++) {
    result[lodash.get(mapa[keys[i]], key)] = value ? lodash.get(mapa[keys[i]], value) : mapa[keys[i]];
  }

  return result;
}
/**
 * @description Función encargada de generar un objeto con propiedades en lower case
 * @param {Object} obj objeto a transformar sus claves en lower case
 */


function _lowerCaseObject(obj) {
  var lower = {};
  Object.keys(obj).forEach(function (k) {
    lower[k.toLowerCase()] = obj[k];
  });
  return lower;
}

},{"lodash":"lodash"}]},{},[2]);
