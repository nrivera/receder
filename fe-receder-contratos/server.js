/**
 * @author Nicolas Andres Rivera Acevedo
 * @description script principal para crear servidor para API's mock 
 */
var config = require('./config');
var grunt = require('grunt');

var express = require('express'),
	session = require('express-session'),
	bodyParser = require('body-parser'),
	socket = require('./socket'),
	WebSocketServer = require('ws').Server;
	api = express();
	wss = new WebSocketServer({
		port: config.wsPort
	});

var refWs = null;
var sendwsmsg = function(msg){
	if (!refWs || refWs.readyState != 1) return 'websocket no conectado';
	msg = typeof msg == 'string' ? msg : JSON.stringify(msg);
	refWs.send(msg);
	return msg;
};
wss.on('connection', function (ws) {
	refWs = ws;
	ws.on('message', function (message) {
		
	});
	socket(function(msg, ms){
		msg = typeof msg == 'string' ? msg : JSON.stringify(msg);
		setTimeout(send, getms());
		function send(){
			if (ws.readyState != 1) return;
			ws.send(msg);
			setTimeout(send, getms())
		}
		function getms(){
			return ms ? ms : Math.floor(Math.random() * 15) * 1000;
		}
	});
});

/*
	configuración del servidor api
*/
api.use(session({
	secret: config.apiSessionSecret,
	resave: config.apiSessionResave,
	key: config.apiSessionKey,
	saveUninitialized: true
}));

api.use(bodyParser.json());
api.use(bodyParser.urlencoded({extended: true}));
api.use(function(request, response, next){
	response.header('Access-Control-Allow-Origin', '*');
	response.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
	
	//headers por defecto
	var headers = ['authorization','Content-Type'];
	var requestHeaders = request.headers['access-control-request-headers'];
	
	//agregar los headers del request;
	if (requestHeaders) {
		headers = headers.concat(requestHeaders.split(',')).join(',');
	}

    response.header('Access-Control-Allow-Headers', headers);
    next();
});

require('./fe')(api, sendwsmsg);

api.listen(config.apiPort, function(){
	grunt.log.ok('servidor mock: http://localhost:' +  config.apiPort);
	grunt.log.ok('servidor ws mock: ws://localhost:' +  config.wsPort)
});

