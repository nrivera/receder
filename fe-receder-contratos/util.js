var lodash = require('lodash');
var constants = require('./fe/constantes/get.constantes')();

/**
 * transforma un input a su representación en Date
 */
module.exports.parseDate = function(input){
    if (input == null || input instanceof Date) return input || null;
    input = input.replace(/-/g,"/").split("/");
    input = new Date(input[1]+"/"+input[0]+"/"+input[2]);
    if (isNaN(input.getDay()) || input.getFullYear() <= 1900) input = null;
    return input;
}

/**
 * transforma un input a su representación en Boolean
 */
module.exports.parseBoolean = function(input){
    if (typeof input === "boolean") return input;
    if (input == null) return false;
    return parseInt(input, input) > 0 || input.toLowerCase() == "true";
}

/**
 * transforma un string a su representación en Number
 */
module.exports.parseNumber = function(stringValue){
    var number = Number(stringValue);
    if (isNaN(number)) {
        number = 0;
    }
    return number;
}

/**
 * transforma un string a su representación en Number de punto flotante
 */
module.exports.parseFloat = function(stringValue, dec) {
    var fvalue = parseFloat(stringValue).toFixed(dec);
    if (isNaN(fvalue)) {
        fvalue = 0;
    }
    return parseFloat(fvalue);
}

/**
 * transforma un string a su representación en entero 
 */
module.exports.parseInt = function(stringValue) {
    var ivalue = parseInt(stringValue, stringValue);
    if (isNaN(ivalue)) {
        ivalue = 0;
    }
    return ivalue;
}

/**
 * @author Nicolas Andres Rivera Acevedo
 * @description Permite paginar una colección simulando la respuesta desde el servidor
 * @param  {Array} coleccion colección de datos a ordenar
 * @param  {String} params objeto literal con parámetros de paginación y Filtro (opcional):
 *      @param  {Number} Pagina número de página a obtener
 *      @param  {String} CampoOrden string para determinar el campo por el cual realizar ordenamiento
 *      @param  {Number} Registros número que indica la cantidad de registros por página
 *      @param  {Boolean} descendente flag para ordenar de forma descendente (opcional)
 * @param  {Function} success callback de retorno de la colección paginada
 * @param  {Function} error callback si hay error
 */
module.exports.paginarColeccion = function(coleccion, params, success, error) {
    try {
        var temp = { Paginacion: {} };
        Object.assign(temp.Paginacion, params);
        delete temp.Paginacion.Filtro;
        if (typeof params.Filtro == 'function') coleccion = lodash.filter(coleccion, params.Filtro);
        temp.Paginacion.TotalRegistros = coleccion.length;
        if (temp.Paginacion.CampoOrden) coleccion = this.ordenarColeccion(coleccion, temp.Paginacion.CampoOrden, temp.Paginacion.Descendente);
        temp.Paginacion.Registros = params.Registros > 0 ? params.Registros : constants.PAGINACION.REGISTROS_POR_PAGINA;
        temp.Paginacion.TotalPaginas = Math.ceil(coleccion.length / (temp.Paginacion.Registros > 0 ? temp.Paginacion.Registros : 1));
        temp.Datos = coleccion.slice(temp.Paginacion.Pagina * temp.Paginacion.Registros - temp.Paginacion.Registros, temp.Paginacion.Pagina * temp.Paginacion.Registros);
        if (typeof success == 'function') return success(temp);
    } catch (e) {
        if (typeof error == 'function') return error(e);
    }
}

/**
 * @author Nicolas Andres Rivera Acevedo
 * @description Permite ordenar una colección
 * @param  {Array} coleccion colección de datos a ordenar
 * @param  {String} campo String que contiene la ruta a la propiedad a obtener (prop.child ...)
 * @param  {Boolean} descendente flag para ordenar de forma descendente (opcional)
 */
module.exports.ordenarColeccion = function(coleccion, campo, descendente){
    var temp = [], fecRegex = /\d{2}\/\d{2}\/\d{4}/, self = this;
    descendente = this.parseBoolean(descendente);
    var insertSort = function(item){
        if (temp.length <= 0) {
            temp.push(item);
            return;
        }
        var valItem = campo ? self.obtenerValorCampo(item, campo) : item;
        if (typeof valItem == "string" && valItem.match(fecRegex)) valItem = self.parseDate(valItem);
        else if (!isNaN(valItem)) valItem = self.parseNumber(valItem);

        for(var i=0; i<temp.length; i++){
            var valTemp = campo ? self.obtenerValorCampo(temp[i], campo) : temp[i];
            if (typeof valTemp == "string" && valTemp.match(fecRegex)) valTemp = self.parseDate(valTemp);
            else if (!isNaN(valTemp)) valTemp = self.parseNumber(valTemp);

            if ((descendente && valItem < valTemp)||(!descendente && valItem > valTemp)){
                if (i<temp.length-1) continue
                temp.push(item);
                break;
            }
            temp.splice(i, 0, item);
            break;
        }
    };
    for (var i=0; i<coleccion.length; i++){
        insertSort(coleccion[i]);
    }
    return temp;
}

/**
 * @author Nicolas Andres Rivera Acevedo
 * @description Permite obtener el valor de una propiedad contenida en un Objeto
 * @param  {object} item  - Objeto padre
 * @param  {string} campo - String que contiene la ruta a la propiedad a obtener
 * @return {object}       - valor de la propiedad obtenida, puede ser un objeto.
 *
 * @example
 *     let obj = {
 *         objHijo: {
 *             prop: 6
 *         }
 *     };
 *     let padre = {
 *         hijos: [
 *             {
 *                 nombre: 'hijo 1',
 *                 edad: '12'
 *             },
 *             {
 *                 nombre: 'hijo 2',
 *                 edad: '5'
 *             },
 *         ]
 *     }
 *
 *     obtenerValorCampo(obj, 'objHijo.prop') //retorna 6
 *     obtenerValorCampo(padre, 'hijos[1].edad') //retorna 5
 *     obtenerValorCampo(padre, 'hijos[0]') //retorna: { nombre: 'hijo 1', edad: '12'}
 *     obtenerValorCampo(padre, 'hijos[edad:5]') //retorna: { nombre: 'hijo 2', edad: '5'}
 */
module.exports.obtenerValorCampo = function(item, campo){
    if (!campo) return undefined;
    campo = campo.split('.');
    var val = item;
    var re = /(\w+)\[(?:(\w+):)?(\d+)\]/;
    for (var i = 0; i < campo.length; i++) {
        if (!val) break;
        var r = re.exec(campo[i]);
        if (r) {
            if (r[2] && r[3]) {
                val = lodash.find(val[r[1]], r[2], r[3]);
            } else {
                val = val[r[1]][r[3]]
            }
        } else {
            val = val[campo[i]];
        }
    }
    return val;
}