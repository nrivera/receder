/**
 * @author Nicolas Andres Rivera Acevedo
 * @description Definición de tareas a procesar con grunt
 * @param  {Object} grunt -instancia grunt
 */
module.exports = function(grunt){

	require('load-grunt-tasks')(grunt);

	grunt.initConfig({
		nodemon: {
            options: {
                env: {
                    PORT: 5858
                }
            },
            dev: {
                script: 'server.js',
            },
            debug: {
                script: 'server.js',
                options: {
                    //--debug, --debug-brk (deprecados)
                    nodeArgs: ['--inspect'],  //--inspect, --inspect-brk
                }
            }
        }
	});

	grunt.registerTask('dev', 'establece un watcher de archivos', function(){
        grunt.task.run([
	      	'nodemon:dev'
	    ]);
	});

    grunt.registerTask('debug', 'permite realizar debug de los contratos', function(){
        grunt.task.run([
	      	'nodemon:debug'
	    ]);
	});
};
