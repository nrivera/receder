/**
 * @author Nicolas Andres Rivera Acevedo
 * @description Configuración para el servidor de API's mock (contratos)
 */
module.exports = {
	apiPort				: 57627,
	wsPort				: 8291,
	apiSessionSecret	: 'AHX%#122GX02017',
	apiSessionResave	: false,
	apiSessionKey		: 'sid'
}