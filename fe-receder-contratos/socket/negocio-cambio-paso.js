/**
 * @author Nicolas Andres Rivera Acevedo
 * @description Contrato para evento NEGOCIO_CAMBIO_PASO
 */
module.exports = {
    Evento: 'NEGOCIO_CAMBIO_PASO',
    IDNegocio: 1,
    /**
     * Data corresponde al Objeto que representa un Paso en particular.
     */
    Data: {
        Nro: 1,
        Nombre: 'Negocio Aceptado',
        EsActivo: true
    }
}