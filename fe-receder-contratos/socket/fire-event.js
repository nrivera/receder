/**
 * @author Nicolas Andres Rivera Acevedo
 * @description mock api para disparar eventos websocket
 * @param {Number} id identificador del evento a disparar
 * @example
 * 		/ws/fire-event?id=1
 */
module.exports = function(send) {

    return function(request, response){

        /**
         * @description identificadores de eventos a disparar.
         * En este objeto deben estar listados los eventos
         * para disparar manualmente mediante un id
         */
        var events = {
            1: require('./negocio-cambio-paso'),
            2: require('./negocio-cambio-estado'),
            3: require('./resultado-envio-negocio'),
            4: require('./resultado-validacion-negocio')
        };

        var dto = events[request.query.id];

        if (dto) dto = send(dto);
        else dto = 'Evento no registrado';

        response.status(200).json(dto);

    }
}