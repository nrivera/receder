/**
 * @author Nicolas Andres Rivera Acevedo
 * @description permite enviar en intervalos de tiempo los eventos registrados
 * @param {Function} send función que permite enviar por websocket un msj
 */
module.exports = function(send){

    /**
     * send tiene los siguientes parámetros
     * @param {*} msg mensaje a enviar hacia el front mediante websocket
     * @param {Number} ms intervalo en ms para enviar el mensaje, si es 0 o no se indica
     * se considera aleatoreamente un valor entre 0 y 15 * 1000.
     */

    /**
     * enviar por websocket los siguientes contratos de eventos websocket:
     */
    //send(require('./negocio-cambio-estado'), 20000);
    //send(require('./resultado-envio-negocio'), 12000);
    //send(require('./resultado-validacion-negocio'), 13000);

};