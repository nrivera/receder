/**
 * @author Nicolas Andres Rivera Acevedo
 * @description Contrato para evento NEGOCIO_ALERTA
 */
module.exports = {
    Evento: 'NEGOCIO_ALERTA',
    IDNegocio: 1,
    Data: [
        {
            IDAlerta: 1,
            Mensaje: 'ha ocurrido un error...',
            TipoAlerta: 1
        }
    ]
}