/**
 * @author Nicolas Andres Rivera Acevedo
 * @description Contrato para evento NEGOCIO_CAMBIO_ESTADO
 */
module.exports = {
    Evento: 'NEGOCIO_CAMBIO_ESTADO',
    IDNegocio: 1,
    Data: {
        IDNegocio: 1,
        Estado: 'Rechazado',
        IDEstado: 3, //identificador del atributo Estado, debe ser el mismo tipo en las constantes
        PuedeAsociarDte: true,
        MontoDocto: 80682,
        MontoCedido: 10000,
        MontoNoCedido: 2000,
        CantDoctos: 2,
        CantDoctosCedidos: 2,
        CantDoctosNoCedidos : 1,        
        NroOperacion: 0,
        PuedeCeder: true,
        PuedeEliminarNegocio: true
    }
}