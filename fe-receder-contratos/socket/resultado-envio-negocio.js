/**
 * @author Nicolas Andres Rivera Acevedo
 * @description Contrato para evento RESULTADO_ENVIO_NEGOCIO
 */
module.exports = {
    "Evento": "RESULTADO_ENVIO_NEGOCIO",
    "IDNegocio": 1,
    "Data": {
        "Progreso": 100,
        "Detalle": [{
                "IDDocumento": 1,
                "EstaAceptado": false,
                "ObservacionDoctoNegocio": "La factura no se puede comprar",
                "ValidacionInterna": {
                    "Estado": 3,
                    "Detalle": "La factura no se puede comprar",
                    "Accion": null
                },
                "RegistroSII": {
                    "Estado": 3,
                    "Detalle": null,
                    "Accion": null
                },
                "Anotaciones": {
                    "Estado": 2,
                    "Detalle": null,
                    "Accion": null
                },
                "AplicaValidacionSII": false
            },
            {
                "IDDocumento": 2,
                "EstaAceptado": true,
                "ObservacionDoctoNegocio": null,
                "ValidacionInterna": {
                    "Estado": 2,
                    "Detalle": "Validación Exitosa",
                    "Accion": null
                },
                "RegistroSII": {
                    "Estado": 3,
                    "Detalle": null,
                    "Accion": null
                },
                "Anotaciones": {
                    "Estado": 2,
                    "Detalle": null,
                    "Accion": null
                },
                "AplicaValidacionSII": false
            },
            {
                "IDDocumento": 3,
                "EstaAceptado": false,
                "ObservacionDoctoNegocio": "La factura no se puede comprar",
                "ValidacionInterna": {
                    "Estado": 3,
                    "Detalle": "La factura no se puede comprar",
                    "Accion": null
                },
                "RegistroSII": {
                    "Estado": 3,
                    "Detalle": null,
                    "Accion": null
                },
                "Anotaciones": {
                    "Estado": 2,
                    "Detalle": null,
                    "Accion": null
                },
                "AplicaValidacionSII": false
            }
        ]
    }
}