/**
 * @author Nicolas Andres Rivera Acevedo
 * @description Contrato para evento NEGOCIO_CAMBIO_ESTADO
 */
module.exports = {
    Evento: 'NEGOCIO_CAMBIO_ESTADO',
    IDNegocio: 1,
    Data: {
        IDNegocio: 1,
        Estado: 'Rechazado',
        IDEstado: 3, //identificador del atributo Estado, debe ser el mismo tipo en las constantes
        PuedeEnviarNegocio: true,
        MontoDocto: 80682,
        CantDoctos: 2,
        Plazo: 6,
        MontoFinanciado: 888,
        Gastos: 999,
        Comision: 777,
        MontoIVA: 1111,
        MontoGirar: 777,
        ValorTasa: 0.63,
        ValorTasaDespliegue: '0,63',
        NroOperacion: 0,
        EstaCotizado: false,
        EstaOtorgado: false,
        EsCotizacionAceptada: true,
        PuedeCeder: true,
        PuedeEliminarNegocio: true,
        PuedeVolverPaso: true,
        DiferenciaPrecio: 111111111111,
        MontoAplicacion: 222222
    }
}