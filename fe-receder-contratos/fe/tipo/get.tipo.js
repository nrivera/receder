/**
 * @author Nicolas Andres Rivera Acevedo
 * @description Responde con una lista de tipos dado por el parámetro tipo
 * @param {String} Tipo identificador para la lista de tipos a obtener.
 * @example
 * 		/fe/tipo?tipo=Recesion
 */
module.exports = function(request, response){  
    
    /**
     * @description dto de ejempo para el tipo=Recesion
     */
    var dto = [
        {
            ID: 1,
            Nombre: 'Receder a Mi'
        },
        {
            ID: 2,
            Nombre: 'Receder a Terceros'
        }
    ];

	response.status(200).json(dto);
}