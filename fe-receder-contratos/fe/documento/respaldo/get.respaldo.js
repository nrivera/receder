/**
 * @author Rodrigo Tapia Leiva
 * @description responde con una lista de documentos de respaldo dada el IDDocumento de una factura.
 * @param {Number} IDDocumento identificador de la factura.
 * @param {Number} IDNegocio identificador del negocio.
 * @example
 * 		/fe/documentos/respaldo?IDDocumento=1
 */
module.exports = function(request, response){
        	
    var dto = {
        Datos: [
            {
                IDDocumentoRespaldo: 1,
                Nombre: 'nombre documento 1',
                FechaAdjunto: '12/05/2018',
                FolioAsociado: 123
            },
            {
                IDDocumentoRespaldo: 2,
                Nombre: 'nombre documento 2',
                FechaAdjunto: '13/05/2018',
                FolioAsociado: 125
            }
        ],
        Paginacion: {
            Pagina			: 1,        //pagina actual
            CampoOrden		: '',       //campo por el cual se realiza ordenamiento
            Registros		: 10,       //cantidad de registros por página
            Descendente		: false,    //descendente / ascendente
            TotalRegistros	: 1,        //cantidad total de registros
            TotalPaginas	: 1         //cantidad total de páginas
        }
    }

    /**
     * @description agregar documentos subidos recientemente
     */
    
    var doctos = require('../upload/uploads').get();
    doctos = doctos.map(function(d){
        return {
            IDDocumentoRespaldo:  dto.Datos.length + 1,
            RutEmisor: d.RutA,
            Nombre: d.Nombre,
            FechaAdjunto: d.FechaActual,
            FolioAsociado: d.Folio
        }
    });

    /**
     * @description Paginar datos
     */
    require('../../../util').paginarColeccion(dto.Datos.concat(doctos), request.query, function(resp){
        dto.Datos = resp.Datos;
        dto.Paginacion = resp.Paginacion;
    });

	response.status(200).json(dto);
}