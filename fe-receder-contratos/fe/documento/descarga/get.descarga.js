﻿
/**
 * @author Juan Rosales Orellana
 * @description retorna archivo base64 que representa el documento en algun formato de archivo.
 * @param {Number} ID identificador del documento a descargar.
 * @param {Number} TipoDocumento identificador del tipo de documento a descargar
 * @example
 * 		/fe/documento/descarga?ID=1&TipoDocumento=1
 */
module.exports = function(request, response){

    /**
     * TipoDocumento:
     * 1=Factura
     * 2=Liquidación
     * 3=Nota de credito/debito
     * 4=Términos y condiciones
     */
     
     /**
      * @description Dto de respuesta exitosa - status 200
      */
    var dto = {
        "Identificador": 0,
        "DataType": "application/pdf",
        "Charset": "",
        "Base64":
            "JVBERi0xLjQNCiW0tba3DQolDQoxIDAgb2JqDQo8PA0KL1BhZ2VzIDIgMCBSDQovUGFnZUxheW91dCAvT25lQ29sdW1uDQovUGFnZU1vZGUgL1VzZU5vbmUNCi9WaWV3ZXJQcmVmZXJlbmNlcyAzIDAgUg0KL1R5cGUgL0NhdGFsb2cNCj4+DQoNCmVuZG9iag0KMiAwIG9iag0KPDwNCi9Db3VudCAxDQovS2lkcyBbNCAwIFJdDQovVHlwZSAvUGFnZXMNCj4+DQoNCmVuZG9iag0KNCAwIG9iag0KPDwNCi9QYXJlbnQgMiAwIFINCi9SZXNvdXJjZXMgPDwNCi9Qcm9jU2V0IFsvUERGIC9UZXh0IC9JbWFnZUNdDQovWE9iamVjdCA8PA0KL2RlY25tZWxka2hsb2tnamVwa2ppaWhmbWdwbGdqbW9tIDUgMCBSDQo+Pg0KDQo+Pg0KDQovTWVkaWFCb3ggWzAuMDAwMDAgMC4wMDAwMCA2MTIuMDAwMDAgNzkyLjAwMDAwXQ0KL0NvbnRlbnRzIFs2IDAgUl0NCi9UeXBlIC9QYWdlDQo+Pg0KDQplbmRvYmoNCjYgMCBvYmoNCjw8DQovRmlsdGVyIC9GbGF0ZURlY29kZQ0KL0xlbmd0aCAxMjcNCj4+DQpzdHJlYW0NCnheXYzLDYMwEETvK7mHrcBZm9jr3NNATikAHIM/GOj/EKLgCGVOT5p5Q5I+wa6BVfog7hptHp84C8BVQNuxYamVc2fjKtkpbfa9gK9A0tzYGouHRmf4r36XhNiX3cbL4Pu5+DykMdcUol9SnKbxVcKSQyy14L0KeAh4A3WRLLANCmVuZHN0cmVhbQ0KDQplbmRvYmoNCjMgMCBvYmoNCjw8DQovRGlzcGxheURvY1RpdGxlIGZhbHNlDQovTm9uRnVsbFNjcmVlbkJlaGF2aW9yIC9Vc2VOb25lDQovRml0V2luZG93IGZhbHNlDQovQ2VudGVyV2luZG93IGZhbHNlDQovSGlkZVRvb2xiYXIgZmFsc2UNCi9IaWRlV2luZG93VUkgZmFsc2UNCi9IaWRlTWVudWJhciBmYWxzZQ0KPj4NCg0KZW5kb2JqDQo3IDAgb2JqDQo8PA0KL1Byb2R1Y2VyIChTZWxlY3QuUGRmIEh0bWwgVG8gUGRmIGZvciAuTkVUIENvbW11bml0eSBFZGl0aW9uIHYxOC4zLjApDQo+Pg0KDQplbmRvYmoNCjUgMCBvYmoNCjw8DQovRmlsdGVyIC9GbGF0ZURlY29kZQ0KL1Jlc291cmNlcyA8PA0KL1Byb2NTZXQgWy9QREYgL1RleHQgL0ltYWdlQ10NCj4+DQoNCi9CQm94IFswLjAwMDAwIDAuMDAwMDAgMTAyNC4wMDAwMCA4LjAwMDAwXQ0KL1N1YnR5cGUgL0Zvcm0NCi9MZW5ndGggODQNCi9UeXBlIC9YT2JqZWN0DQovTmFtZSAvZGVjbm1lbGRraGxva2dqZXBramlpaGZtZ3BsZ2ptb20NCj4+DQpzdHJlYW0NCnheM9AzAAEFGG1oYGQCZVpA6aJUhXCFPF4uhUIgNkCTzOXlwqIlB0XUACFqgCmUwcsVzssFNd8Q5gw0uigdoZd6Vhtg6k3TAjpEIRBIADEAErAzHg0KZW5kc3RyZWFtDQoNCmVuZG9iag0KeHJlZg0KMCA4DQowMDAwMDAwMDAwIDY1NTM1IGYNCjAwMDAwMDAwMjAgMDAwMDAgbg0KMDAwMDAwMDE0NyAwMDAwMCBuDQowMDAwMDAwNjU4IDAwMDAwIG4NCjAwMDAwMDAyMTMgMDAwMDAgbg0KMDAwMDAwMDk0MCAwMDAwMCBuDQowMDAwMDAwNDQ4IDAwMDAwIG4NCjAwMDAwMDA4NDIgMDAwMDAgbg0KdHJhaWxlcg0KPDwNCi9JbmZvIDcgMCBSDQovUm9vdCAxIDAgUg0KL1NpemUgOA0KPj4NCg0Kc3RhcnR4cmVmDQoxMjc1DQolJUVPRg0K",
        "FileName": "Factura_203",
        "Extension": "pdf"
    }

    /**
     * @description Dto de error
     */
    var dtoError = {
        Message: 'ha ocurrido un error...'
    };

    response.status(200).json(dto);
}