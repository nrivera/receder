/**
 * @author Nicolas Andres Rivera Acevedo
 * @description responde con el certificado proximo a expirar
 * @example
 * 		/fe/documento/certificado/por-expirar
 */
module.exports = function(request, response) {
  var dto = {
    IDCertificado: 8,
    EmitidoPara: "Emitido Para",
    FechaVencimiento: "12/12/2018"
  };

  response.status(204).json(dto);
};
