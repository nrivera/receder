/**
 * @author Rodrigo Tapia Leiva
 * @description responde con una lista de certificados
 * @param {boole} EsVigente :Filtra solo los certificados vigentes
 * @example
 * 		/fe/documentos/certificados
 */
module.exports = function(request, response){
        	
    var dto = {
        Datos: [
            {
                IDCertificado: 1,
                Rut: 171476617,
                EmitidoA: 'Asesorías e Inversiones Nectia Limitada',      
                FechaExpiracion: '03/12/2017',
                EntidadEmisora: 'Asesorías e Inversiones Nectia Limitada2',
                EmailContacto: 'asistente@factoring.cl',
                TerminosCondiciones: '.....',
            },          
            {
                IDCertificado: 2,
                Rut: 171476617,
                EmitidoA: 'Asesorías e Inversiones Nectia Limitada',
                FechaExpiracion: '03/12/2017',
                EntidadEmisora: 'Asesorías e Inversiones Nectia Limitada2',
                EmailContacto: 'asistente@factoring.cl',
                TerminosCondiciones: '.....',
            }
        ],
        Paginacion: {
            Pagina			: 2,        //pagina actual
            CampoOrden		: '',       //campo por el cual se realiza ordenamiento
            Registros		: 10,       //cantidad de registros por página
            Descendente		: false,    //descendente / ascendente
            TotalRegistros	: 1,        //cantidad total de registros
            TotalPaginas	: 2        //cantidad total de páginas
        }
    }

	response.status(200).json(dto);
}