/**
 * @author Rodrigo Tapia Leiva
 * @description responde con el último registro de certificado.
 * @example
 * 		/fe/documentos/certificados/last
 */
module.exports = function(request, response){
        	
    var dto = {
        IDCertificado: 1,
        Identificador: 171476617,
        RutCliente: 171476617,
        EmitidoA: 'Asesorías e Inversiones Nectia Limitada',      
        FechaExpiracion: '03/12/2017',
        EntidadEmisora: 'Asesorías e Inversiones Nectia Limitada2',
        Email: 'asistente@factoring.cl',
        TerminosCondiciones: '.....',
        Estado:'Vigente',
    }

	response.status(200).json(dto);
}