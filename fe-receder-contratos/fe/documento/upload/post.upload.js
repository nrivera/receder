/**
 * @author Rodrigo Tapia Leiva
 * @description realiza subida de un documento con flowjs
 * @param {Number} flowChunkNumber
 * @param {Number} flowChunkSize
 * @param {Number} flowCurrentChunkSize
 * @param {Number} flowTotalSize
 * @param {Number} flowIdentifier
 * @param {Number} flowFilename
 * @param {Number} flowRelativePath
 * @param {Number} flowTotalChunks
 * @example
 * 		/fe/documentos/post
 */
module.exports = function(request, response){
    console.log(request.query);
    /**
     * @description Dto con atributos del documento cargado
     */
    var dto = {  
        "ResultadoOK": true,
        "IDNegocio": 1,
        "Datos": {    
            "Documentos": [      
                {        
                    "RutEmisor": "995526211",        
                    "IDTipoDocumento": 2,        
                    "Folio": 162,        
                    "IDDocumento": 1,        
                    "EsExitoso": true,        
                    "Mensaje": null,
                    "EsCedible": true
                },
                {        
                    "RutEmisor": "995526212",        
                    "IDTipoDocumento": 1,        
                    "Folio": 163,        
                    "IDDocumento": 2,        
                    "EsExitoso": false,        
                    "Mensaje": 'ha ocurrido un error al procesar el contenido del archivo',
                    "EsCedible": true
                }
            ]  
        }
    };

    var dtoUploadCertificado = {
        "Datos": {
          "IDCertificado": 0,
          "RutCliente": '19K',
          "RutResponsable": "27",
          "EmitidoA": " PABLO JUAN CAROCA BACHUR",
          "FechaExpiracion": "2019-08-25T10:24:00-03:00",
          "EntidadEmisora": " E-SIGN S.A.",
          "Email": null,
          "Estado": "Vigente",
          "Validacion": {
              "Cumple": false,
              "EsBloqueante": false,
              "Mensaje": "mensaje"
          }
        },
        "ResultadoOK": true
    }

    /**
     * mantener la subida para generar respuestas
     * dummy lógicas.
     */
    require('./uploads').push(request.files);

    /**
     * @description Respuesta de error
     */
    var dto_error = {
        Status: 200,
        Message: 'No es posible leer el archivo.'
    }

    response.status(409).json(dto_error);
}