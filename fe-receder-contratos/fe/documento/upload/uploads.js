/**
 * @author Nicolas Andres Rivera Acevedo
 * @description contenedor para mantener las subidas de documentos
 * y generar respuesta dummy lógica para el front.
 */
var documentos = [];

/**
 * @description genera un rut aleatorio
 */
function obtenerRut(){
    var rut = (Math.floor(Math.random() * 6000000) + 1000000)
    var dv = (Math.floor(Math.random() * 10) + 0);
    return rut.toString() + (dv == 10 ? 'K' : dv);
}

/**
 * @description obtener fecha actual
 */
function obtenerFecha(){
    var fecha = new Date();
    return fecha.getDate() + '/' + (fecha.getMonth() + 1) + '/' + fecha.getFullYear();
}

/**
 * @description permite agregar un documento
 * @param {Array|Object} doctos archivos subidos
 */
module.exports.push = function(doctos){
    doctos = doctos instanceof Array ? doctos : [doctos];
    doctos.forEach(function(d){
        documentos.push({
            ID: 1 + documentos.length,
            RutA: obtenerRut(),
            RutB: obtenerRut(),
            IDTipoDocumento: 1,
            Folio: 1000 + documentos.length,
            EsExitoso: true,
            Mensaje: null,
            Nombre: d.originalname,
            FechaActual: obtenerFecha()
        });
    });
}

/**
 * @description permite recuperar la lista dummy
 * de los documentos subidos
 */
module.exports.get = function(){
    return documentos;
};