/**
 * @author Nicolas Andres Rivera Acevedo
 * @description Responde con una url dada por tipo
 * @param {String} Tipo identificador para la url a obtener
 * @example
 * 		/fe/url?tipo=no-autorizado
 */
module.exports = function(request, response){  
    
    /**
     * @description Tipos disponibles
     * no-autorizado     : url a redireccionar al tener autorizacion
     */

    /**
     * @description dto de respuesta
     */
    var dto = {
        URL: 'http://http://127.0.0.1:8001/#/home'
    };

	response.status(200).json(dto);
}