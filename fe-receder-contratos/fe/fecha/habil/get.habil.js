/**
 * @author María Teresa Calbul
 * @description Indica si la fecha ingresada es hábil.
 * @param {String} valor Fecha y su formato en dd/MM/yyyy.
 * @example
 * 		/fe/fecha/habil?valor=22%2F8%2F2018
 */
module.exports = function(request, response){  
    
    /**
     * @description dto de respuesta correcta
     */
    var dto = {
        "esHabil": false
    };

    /**
     * @description dto de error
     */
    var dto_error = {
        "Message": 'La fecha ingresada no es válida'
    };

	response.status(200).json(dto);
}