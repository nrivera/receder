/**
 * @author Nicolas Andres Rivera Acevedo
 * @description guarda información de contacto del cliente
 * @example
 * 		/fe/cliente/contacto
 */
module.exports = function(request, response){    	

    /**
     * @description dto enviado desde front. Es el mismo para el get y el post
     * cuando es get el atributo `dto.EmailCodigoConfirmacion` siempre es null
     */
    var dto = {
        Nombre: 'Nombre contacto',
        ApellidoPaterno: 'Apellido Paterno',
        ApellidoMaterno: null,
        EmailRPETC: 'email@rpetc.com',
        EmailEsperaConfirmacion: false,
        EmailCodigoConfirmacion: null    // cuando codigo es null, se debe generar un codigo y enviar email para confirmar
    };
    
    Object.assign(dto, request.body);

    /**
     * @description dto de respuesta de error
     */
    var dto_error = {
        Message: 'Ha ocurrido un error...'
    };

    /**
     * @description dto de respuesta de error al verificar código de confirmación
     */
    var dto_error_codigo = {
        Message: 'El código de verificación no es correcto. Favor revise el código enviado a ' + dto.EmailRPETC
    };

    /**
     * @description lógica dummy para el manejo de verificación de código:
     * 1.- el código se genera cuando el atributo `dto.EmailCodigoConfirmacion` es nulo
     * 2.- si se genera código debe enviarse al email indicado en el atributo `dto.EmailRPETC`
     * 3.- si el código no es nulo, se debe verificar con el que se envió al email.
     */
    if (request.sync) return dto;

    if (!dto.EmailCodigoConfirmacion) {
        //se genera código y se envía al email
        dto.EmailEsperaConfirmacion = true;
    } else {
        if (dto.EmailCodigoConfirmacion != '1234'){
            response.status(403).json(dto_error_codigo);
            return;
        }
    }
    
    response.status(200).json(dto);
}