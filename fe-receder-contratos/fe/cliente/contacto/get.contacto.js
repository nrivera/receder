/**
 * @author Nicolas Andres Rivera Acevedo
 * @description obtiene información de contacto del cliente
 * @example
 * 		/fe/cliente/contacto
 */
module.exports = function(request, response){    	

    /**
     * @description dto de respuesta definido en `./post.contacto`
     */
    request.sync = true;
    var dto = require('./post.contacto')(request, response);

	response.status(200).json(dto);
}