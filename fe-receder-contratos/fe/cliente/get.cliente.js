/**
 * @author Nicolas Andres Rivera Acevedo
 * @description 
 * @param {String} rut rut para obtener datos del cliente
 * @example
 * 		/fe/cliente/{rut}
 */
module.exports = function(request, response){    	
    var dto = {
        NombreCompleto: "ASEORÍAS E INVERSIONES NECTIA LIMITADA",
        Rut: "169012997",
        Email: "finanzas@nombredeempresa.com",

        NombreEjecutivo: "CARLOS PICAND LAGE",
        FonoEjecutivo: "(56-2) 2 2901 66 09",
        EmailEjecutivo: "ejecutivo@empresa.cl",

        NombreAsistente: "VICENTE PEREZ ROSALES",
        FonoAsistente: "(56-2) 2 4477 77 88",
        EmailAsistente: "asistente@empresa.cl",

        // MontoTotalAprobado: 1000000,
        // MontoTotalDisponible: 700000,
        // MontoTotalUtilizado: 300000,

        EmailContacto: "yoshi@nectia.com",
        DireccionContacto: "Aldea pinguino 27",
        Nombres: "ASEORÍAS",
        ApellidoPaterno: "E",
        ApellidoMaterno: null,
    };

	response.status(200).json(dto);
}