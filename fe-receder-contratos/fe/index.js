var upload = require('multer')();

/**
 * @author Nicolas Andres Rivera Acevedo
 * @description indice de todas las api disponibles
 * @param {Object} api instancia del servidor api de contratos
 * @param {Function} send función que permite enviar mensajes por websocket
 */
module.exports = function(api, send){

	/**
	 * api mock para disparar eventos websocket
	 */
	api.get('/ws/fire-event', require('../socket/fire-event')(send));

	/**
	 * apis de ejemplo
	 */
	api.get('/fe/test/prueba', require('./test/get.prueba'));
	api.get('/fe/test/prueba2', require('./test/get.prueba2'));
	api.get('/fe/test/prueba3', require('./test/get.prueba3'));

	/**
	 * autenticacion
	 */
	 api.post('/fe/auth', require('./auth/post.auth'));
	 api.post('/fe/auth/origin', require('./auth/origin/post.origin'));

	/**
	 * constantes
	 */
	api.get('/fe/constantes', require('./constantes/get.constantes'));

    /**
	 * Cliente
	 */
	api.get('/fe/cliente', require('./cliente/get.cliente'));
	api.get('/fe/cliente/:rut', require('./cliente/get.cliente'));

	/**
	 * Cliente/Contacto
	 */
	api.get('/fe/cliente/contacto', require('./cliente/contacto/get.contacto'));
	api.post('/fe/cliente/contacto', require('./cliente/contacto/post.contacto'));

	/**
	 * Resumen Ejecutivo
	 */
	api.get('/fe/resumen-ejecutivo', require('./resumen-ejecutivo/get.resumen-ejecutivo'));

	/**
	 * Negocio
	 */
	api.get('/fe/negocio', require('./negocio/get.negocio'));
	api.delete('/fe/negocio/:id', require('./negocio/delete.negocio'));
	api.get('/fe/negocio/estados', require('./negocio/estados/get.estados'));

	api.get('/fe/negocio/pasos', require('./negocio/pasos/get.pasos'));
	api.post('/fe/negocio/pasos', require('./negocio/pasos/post.pasos'));

	api.get('/fe/negocio/filtros', require('./negocio/filtros/get.filtros'));

	api.get('/fe/negocio/facturas', require('./negocio/facturas/get.facturas'));
	api.post('/fe/negocio/facturas', require('./negocio/facturas/post.facturas'));

	api.post('/fe/negocio/facturas/adjuntas', require('./negocio/facturas/adjuntas/post.adjuntas'));

	api.get('/fe/negocio/facturas/requiere-reparar', require('./negocio/facturas/requiere-reparar/get.requiere-reparar'))

	api.get('/fe/negocio/resumen', require('./negocio/resumen/get.resumen'));
	api.get('/fe/negocio/resultado-envio-negocio', require('./negocio/resultado-envio-negocio/get.resultado-envio-negocio'));
	api.get('/fe/negocio/resultado-validacion-negocio', require('./negocio/resultado-validacion-negocio/get.resultado-validacion-negocio'));
	api.get('/fe/negocio/revision-cambio-estado', require('./negocio/revision-cambio-estado/get.revision-cambio-estado'));
	api.post('/fe/negocio/acepta-cotizacion', require('./negocio/acepta-cotizacion/post.acepta-cotizacion'));
	api.post('/fe/negocio/recede', require('./negocio/recede/post.recede'));
	api.post('/fe/negocio/asocia-certificado', require('./negocio/asocia-certificado/post.asocia-certificado'));
	api.post('/fe/negocio/certificado/valida-password', require('./negocio/certificado/valida-password/post.valida-pasword'));

    /**
	 * Documentos
	 */
	api.get('/fe/documento/certificado', require('./documento/certificado/get.certificado'));
	api.get('/fe/documento/certificado/last', require('./documento/certificado/last/get.last'));
	api.get('/fe/documento/certificado/por-expirar', require('./documento/certificado/por-expirar/get.por-expirar'));
	api.get('/fe/documento/respaldo', require('./documento/respaldo/get.respaldo'));
	api.delete('/fe/documento/respaldo/:id', require('./documento/respaldo/delete.respaldo'));
	api.get('/fe/documento/descarga', require('./documento/descarga/get.descarga'));
	api.post('/fe/documento/upload', upload.any(), require('./documento/upload/post.upload'));

	/**
	 * DTE
	 */
	api.delete('/fe/dte/:id', require('./dte/delete.dte'));

    /**
     * Login Reporteria
     */
    api.post('/mobile/factoring/TokenLegacySystem', require('./reporteria/post.login'));

    /**
     * Reporteria
     */
    api.post('/mobile/factoring/getClientAccount', require('./reporteria/cartola-linea/post.linea-cliente'));
    api.post('/mobile/factoring/getLastTransactionsSummary', require('./reporteria/cartola-linea/post.ultimos-movimientos'));
    api.post('/mobile/factoring/getSurplusSummary', require('./reporteria/cartola-linea/post.excedentes-liberados'));
    api.post('/mobile/factoring/getSurplusDetail', require('./reporteria/cartola-linea/post.detalle-excedentes-liberados'));
    api.post('/mobile/factoring/getPaidInvoicesSummary', require('./reporteria/cartola-linea/post.pagos-abonos'));
    api.post('/mobile/factoring/getProtestedDocumentsSummary', require('./reporteria/cartola-linea/post.documentos-protestados'));
    api.post('/mobile/factoring/getProcessedOperations', require('./reporteria/cartola-linea/post.historial-operaciones'));
    api.post('/mobile/factoring/getProcessedOperationDetail', require('./reporteria/cartola-linea/post.detalle-historial-operaciones'));
    api.post('/mobile/factoring/getCurseOperations', require('./reporteria/cartola-linea/post.operaciones-curso'));
    api.post('/mobile/factoring/getCollectionDocuments', require('./reporteria/cartola-linea/post.documentos-cobranza'));
    api.post('/mobile/factoring/getCollectionDocumentDetail', require('./reporteria/cartola-linea/post.detalle-documentos-cobranza'));
    api.post('/mobile/factoring/getMyDueDates', require('./reporteria/cartola-linea/post.fechas-vencimiento'));
    api.post('/mobile/factoring/getDocumentsWithDueDateX', require('./reporteria/cartola-linea/post.detalle-fechas-vencimiento'));

    /**
     * Notificaciones
     */
    api.post('/mobile/factoring/getAlerts', require('./reporteria/notificaciones/post.notificaciones'));
    api.post('/mobile/factoring/markAlertsAsReaded', require('./reporteria/notificaciones/post.notificacion-leida'));

	/**
	 * Tipo
	 */
	api.get('/fe/tipo', require('./tipo/get.tipo'));

	/**
	 * Url
	 */
	api.get('/fe/url', require('./url/get.url'));

	/**
	 * Fecha
	 */
	api.get('/fe/fecha/habil', require('./fecha/habil/get.habil'));

	/**
	 * Websocket
	 */
	api.get('/fe/ws/negocio/:idNegocio', require('./ws/negocio/get.negocio'));


	/**
	 * test
	 */
	api.get('/fe/test/prueba', require('./test/get.prueba'));
	api.get('/fe/test/prueba2', require('./test/get.prueba2'));

	/**
	 * solocesion,
	 */
	api.get('/sc/negocio/pasos', require('./negocio/pasos/get.pasos'));
	api.post('/sc/negocio/pasos', require('./negocio/pasos/post.pasos'));
	api.get('/sc/negocio/filtros', require('./negocio/filtros/get.filtros'));
	api.get('/sc/negocio/facturas', require('./negocio/facturas/get.facturas'));
	api.post('/sc/negocio/facturas', require('./negocio/facturas/post.facturas'));
	api.post('/sc/negocio/facturas/adjuntas', require('./negocio/facturas/adjuntas/post.adjuntas'));
	api.get('/sc/negocio/resumen', require('./negocio/resumen/get.resumen'));
	api.get('/sc/ws/negocio/:idNegocio', require('./ws/negocio/get.negocio'));
	api.get('/sc/negocio/revision-cambio-estado', require('./negocio/revision-cambio-estado/get.sc-revision-cambio-estado'));
	api.post('/sc/negocio/certificado/valida-password', require('./negocio/certificado/valida-password/post.valida-pasword'));


		/**
	 * bancos
	 */
	api.get('/p360/bancos', require('./bancos/get.bancos'));

	/**
	 * cuentas del banco
	 */
    api.get('/p360/bancos/cuentas', require('./bancos/cuentas/get.cuentas'));
    api.post('/p360/bancos/cuentas', require('./bancos/cuentas/post.cuentas'));
	api.post('/fe/bancos/cuentas/seleccionar', require('./bancos/cuentas/post.seleccionar'));
	
};
