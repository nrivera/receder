/**
 * @author Nicolas Andres Rivera Acevedo
 * @description Permite realizar autenticación
 * @example
 * 		/faa/auth
 */
module.exports = function(request, response){
    
    /**
     * @description Dto de información enviadada desde front
     */
    var dto = {
        Token: 'ajr12hs$aja%!ah15aFAA'
    }

    /**
     * @description Dto de respuesta al front
     */
    var dto_respuesta = {
        Token: 'HAA15AFDA#981aAA',           //nuevo token a utilizar en el header
    }

    dto_respuesta.Token += Math.floor((Math.random() * 999) + 100);

	response.status(200).json(dto_respuesta);
}