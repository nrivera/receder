/**
 * @author Nicolas Andres Rivera Acevedo
 * @description Permite realizar el cierre de sesión activa.
 * @example
 * 		/cert/auth/logout
 */
module.exports = function(request, response){

    /**
     * @description No es necesario enviar parámetros desde el front
     * puesto que el token de autenticación siempre viaja en el header
     * del request y con eso ya tenemos lo necesario para indentificar
     * al usuario que está cerrando sesión.
     */    
	response.status(200).json('');
}