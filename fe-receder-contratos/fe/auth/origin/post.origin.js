/**
 * @author Nicolas Andres Rivera Acevedo
 * @description Permite verificar si un origen es válido para insertar
 * un iframe de certificados o de simulación.
 * @example
 * 		/faa/auth/origin
 */
module.exports = function(request, response){
    
    /**
     * @description Dto de información enviadada desde front
     */
    var dto = {
        Origin: 'http://localhost:8000'
    };

    /**
     * Código de estado 200 determina que el Origen es válido,
     * cualquier otro código, el Origen no es válido.
     */

	response.status(200).json('');
}