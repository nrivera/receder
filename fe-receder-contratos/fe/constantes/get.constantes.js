/**
 * @author Juan Rosales
 * @description 
 * @example
 * 		/cert/constantes
 */
module.exports = function(request, response){
    var config = require('../../config');
    var dto = {
        constantes: {
            PAGINACION: {
                CANTIDAD_NUMEROS_PAGINA: 5,
                OPCIONES_REGISTROS_POR_PAGINA: '5,10,20,50,100',
                REGISTROS_POR_PAGINA: 10
            },
            SISTEMA: {
                SEGUNDOS_API_SOCKET             : 10,
                PUERTO_SOCKET                   : config.wsPort
            },
            SOCKET: {
                RESULTADO_ENVIO_NEGOCIO         : 'RESULTADO_ENVIO_NEGOCIO',
                RESULTADO_VALIDACION_NEGOCIO    : 'RESULTADO_VALIDACION_NEGOCIO',
                NEGOCIO_CAMBIO_ESTADO           : 'NEGOCIO_CAMBIO_ESTADO',
                NEGOCIO_CAMBIO_PASO             : 'NEGOCIO_CAMBIO_PASO'
            },
            NEGOCIO: {
                TIPO_NEGOCIO_EN_CURSO    :   1,
                TIPO_NEGOCIO_OTORGADO    :   2,
            },
            DOCUMENTO_DESCARGA: {
                TIPO_FACTURA        : 1,
                TIPO_LIQUIDACION    : 2,
                TIPO_NOTA_CREDITO   : 3,
                TIPO_TERMINOS_CONDICIONES_CD    :4
            },
            ACCION_SII: {
                CAMBIA_CERTIFICADO: 'CCD',
                CAMBIA_DTE: 'CDTE',
                CAMBIA_FECHA_VTO: 'CFV',
                REQUIERE_NOTA_CREDITO: 'RNC'
            },
            ESTADO_VALIDACION_NEGOCIO: {
                ESPERA : 1,
                EXITOSO: 2,
                ERROR: 3
            },
            ORIGEN_UPLOAD: {
                DTE                       : 1,
                CERTIFICADO_DIGITAL       : 2,
                DOCUMENTO_RESPALDO        : 3
            },
            ESTADO_NEGOCIO: {
                RECHAZADO: 3
            },
            GOOGLE_ANALITYCS: {
                TAG_CODE: "R1RNLVA2V1E3Mlg="
            },
            MENSAJES:{
                SUBTITULOBANCOS: 'Cuenta de depósito debe corresponder al RUT de la Empresa'

            }
        }
    };

    if (!response) return dto.constantes;

	response.status(200).json(dto);
}