/**
 * @author Nicolas Andres Rivera Acevedo
 * @description responde con una lista de pasos y su estado
 * @param {Number} IDNegocio identificador del negocio en curso
 * @example
 * 		/fe/negocio/pasos?IDNegocio=1
 */
module.exports = function(request, response) {

    var dto = {
        Datos: [{
                Nro: 1,
                Nombre: 'Seleccionar Facturas',
                EsActivo: true //indica si el paso ya ha sido pasado o es el actual
            },
            {
                Nro: 2,
                Nombre: 'Ceder Documentos', //'Completar Datos'
                EsActivo: true //si este paso ha sido pasado (true), necesariamente el anterior 1 debe ser marcado como true
            },
            {
                Nro: 3,
                Nombre: 'Resumen', //'Enviar Negocio'
                EsActivo: true
            },
            {
                Nro: 4,
                Nombre: 'Aceptar Cotización',
                EsActivo: true
            },
            {
                Nro: 5,
                Nombre: 'Negocio Aceptado',
                EsActivo: false
            }
        ],
        Paginacion: {
            Pagina: 1, //pagina actual
            CampoOrden: '', //campo por el cual se realiza ordenamiento
            Registros: 10, //cantidad de registros por página
            Descendente: false, //descendente / ascendente
            TotalRegistros: 1, //cantidad total de registros
            TotalPaginas: 1 //cantidad total de páginas
        }
    }

    response.status(200).json(dto);
}