/**
 * @author Nicolas Andres Rivera Acevedo
 * @description envía al backend el paso actual para ser marcado como EsActivo
 * @example
 * 		/fe/negocio/pasos
 */
module.exports = function(request, response){
    
    /**
     * @description Dto de información enviadada desde front
     */
    var dto = {
        IDNegocio: 1,
        Nro: 1,
        EsActivo: true
    }

    var dtoError = {
        Message: 'No tiene un certificado vigente adjunto al Negocio en Curso. $br ¿Desea adjuntar uno?'
    }

	response.status(200).json(dtoError);
}