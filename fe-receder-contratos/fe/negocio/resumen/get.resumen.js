/**
 * @author Nicolas Andres Rivera Acevedo
 * @description Informa al front atributos de resumen negocio dado por IDNegocio
 * @param {Number} IDNegocio identificador del negocio en curso
 * @example
 * 		/fe/negocio/resumen?IDNegocio=1
 */
module.exports = function(request, response){
        	
    /**
     * @author Nicolas Andres Rivera Acevedo
     * @description DTO de respuesta con atributos del resumen negocio.
     * Es la misma respuesta que el evento websocket NEGOCIO_CAMBIO_ESTADO
     * `\socket\negocio-cambia-estado.js`
     */
    var dto = require('../../../socket/negocio-cambio-estado').Data;

	response.status(200).json(dto);
}