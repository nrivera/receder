/**
 * @author Nicolas Andres Rivera Acevedo
 * @description responde con los últimos 3 estados del negocio
 * @example
 * 		/fe/negocio/estados
 */
module.exports = function(request, response){
        	
    var dto = {
        Datos: [
            {
                Estado: 1,
                NroOperacion: '#121202',
                MontoPesos: 999999
            },
            {
                Estado: 2,
                NroOperacion: '#121202',
                MontoPesos: 999999
            },
            {
                Estado: 3,
                NroOperacion: '#121202',
                MontoPesos: 999999
            }
        ],
        Paginacion: {
            Pagina			: 1,        //pagina actual
            CampoOrden		: '',       //campo por el cual se realiza ordenamiento
            Registros		: 10,       //cantidad de registros por página
            Descendente		: false,    //descendente / ascendente
            TotalRegistros	: 1,        //cantidad total de registros
            TotalPaginas	: 1         //cantidad total de páginas
        }
    }

	response.status(200).json(dto);
}