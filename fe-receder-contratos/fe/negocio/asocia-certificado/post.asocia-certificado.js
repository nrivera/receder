/**
 * @author Nicolas Andres Rivera Acevedo
 * @description asocia un certificado a un negocio dado por IDNegocio e IDCertificado
 * @param {Number} IDNegocio identificador del Negocio
 * @param {Number} IDCertificado identificador del Certificado.
 * @example
 * 		/fe/negocio/asocia-certificado
 */
module.exports = function(request, response){
        
    /**
     * @description modelo de entrada
     */
    var dto = {
        IDNegocio: 1,
        IDCertificado: 2
    };

    /**
     * @description dto como respuesta de error
     */
    var dto_error = {
        Message: 'Ha ocurrido un error...'
    };

	response.status(200).json(dto);
}