/**
 * @author Nicolas Andres Rivera Acevedo
 * @description responde con una lista de filtros
 * @example
 * 		/fe/negocio/filtros
 */
module.exports = function (request, response) {

    var dto = {
        Datos: [
            {
                Nombre: 'Fecha Vencimiento',
                Tipo: 'date',           //cuando es date, front agrega un input con calendario
                Datos: null,
                Param: 'FechaVencimiento',
                EsPorDefecto: true
            },
            {
                Nombre: 'Rut Cliente',
                Tipo: 'select',
                Datos: [
                    {
                        ID: 1,
                        Nombre: '16.147.666-1'
                    },
                    {
                        ID: 2,
                        Nombre: '56.237.275-K'
                    }
                ],
                Param: 'RutCliente',
                EsPorDefecto: true
            },
            {
                Nombre: 'Tipo 1',       //nombre de despliegue
                Tipo: 'select',         //tipo de filtro
                Datos: [                //cuando es select, datos es una colección de posibles opciones
                    {
                        ID: 1,
                        Nombre: 'AEC'
                    },
                    {
                        ID: 2,
                        Nombre: 'DTE'
                    }
                ],
                Param: 'Tipo1',          //nombre del filtro a utilizar como queryString
                EsPorDefecto: true
            },
            {
                Nombre: 'Tipo 2',         //nombre de despliegue
                Tipo: 'select',         //tipo de filtro
                Datos: [                //cuando es select, datos es una colección de posibles opciones
                    {
                        ID: 1,
                        Nombre: 'AEC'
                    },
                    {
                        ID: 2,
                        Nombre: 'DTE'
                    }
                ],
                Param: 'Tipo2',          //nombre del filtro a utilizar como queryString
                EsPorDefecto: true
            },
            {
                Nombre: 'Tipo 3',         //nombre de despliegue
                Tipo: 'select',         //tipo de filtro
                Datos: [                //cuando es select, datos es una colección de posibles opciones
                    {
                        ID: 1,
                        Nombre: 'AEC'
                    },
                    {
                        ID: 2,
                        Nombre: 'DTE'
                    }
                ],
                Param: 'Tipo3',          //nombre del filtro a utilizar como queryString
                EsPorDefecto: false
            },
            {
                Nombre: 'Tipo 4',         //nombre de despliegue
                Tipo: 'select',         //tipo de filtro
                Datos: [                //cuando es select, datos es una colección de posibles opciones
                    {
                        ID: 1,
                        Nombre: 'AEC'
                    },
                    {
                        ID: 2,
                        Nombre: 'DTE'
                    }
                ],
                Param: 'Tipo4',          //nombre del filtro a utilizar como queryString
                EsPorDefecto: false
            },
            {
                Nombre: 'Tipo 5',         //nombre de despliegue
                Tipo: 'select',         //tipo de filtro
                Datos: [                //cuando es select, datos es una colección de posibles opciones
                    {
                        ID: 1,
                        Nombre: 'AEC'
                    },
                    {
                        ID: 2,
                        Nombre: 'DTE'
                    }
                ],
                Param: 'Tipo5',          //nombre del filtro a utilizar como queryString
                EsPorDefecto: false
            },
            {
                Nombre: 'Tipo 6',         //nombre de despliegue
                Tipo: 'select',         //tipo de filtro
                Datos: [                //cuando es select, datos es una colección de posibles opciones
                    {
                        ID: 1,
                        Nombre: 'AEC'
                    },
                    {
                        ID: 2,
                        Nombre: 'DTE'
                    }
                ],
                Param: 'Tipo6',          //nombre del filtro a utilizar como queryString
                EsPorDefecto: false
            },
            {
                Nombre: 'datos es una colección de posibles opciones',         //nombre de despliegue
                Tipo: 'select',         //tipo de filtro
                Datos: [                //cuando es select, datos es una colección de posibles opciones
                    {
                        ID: 1,
                        Nombre: 'AEC'
                    },
                    {
                        ID: 2,
                        Nombre: 'DTE'
                    }
                ],
                Param: 'Tipo7',          //nombre del filtro a utilizar como queryString
                EsPorDefecto: false
            },
            {
                Nombre: 'datos es una colección de posibles opciones',         //nombre de despliegue
                Tipo: 'select',         //tipo de filtro
                Datos: [                //cuando es select, datos es una colección de posibles opciones
                    {
                        ID: 1,
                        Nombre: 'AEC'
                    },
                    {
                        ID: 2,
                        Nombre: 'DTE'
                    }
                ],
                Param: 'Tipo8',          //nombre del filtro a utilizar como queryString
                EsPorDefecto: false
            },
            {
                Nombre: 'Tipo 9',         //nombre de despliegue
                Tipo: 'select',         //tipo de filtro
                Datos: [                //cuando es select, datos es una colección de posibles opciones
                    {
                        ID: 1,
                        Nombre: 'AEC'
                    },
                    {
                        ID: 2,
                        Nombre: 'DTE'
                    }
                ],
                Param: 'Tipo9',          //nombre del filtro a utilizar como queryString
                EsPorDefecto: false
            },
            {
                Nombre: 'Tipo 10',         //nombre de despliegue
                Tipo: 'select',         //tipo de filtro
                Datos: [                //cuando es select, datos es una colección de posibles opciones
                    {
                        ID: 1,
                        Nombre: 'AEC'
                    },
                    {
                        ID: 2,
                        Nombre: 'DTE'
                    }
                ],
                Param: 'Tipo10',          //nombre del filtro a utilizar como queryString
                EsPorDefecto: false
            },
            {
                Nombre: 'Monto',        //nombre de despliegue
                Tipo: 'range',          //cuado es range, front agrega un slidebar y dos input de entrada
                Datos: [
                    1000,               //valor Mínimo del Rango
                    2000                //valor Máximo del Rango
                ],
                Param: 'Monto',          //cuando es range el queryString será Monto=1000-2000
                EsPorDefecto: false
            },
            {
                Nombre: 'Monto2',        //nombre de despliegue
                Tipo: 'range',          //cuado es range, front agrega un slidebar y dos input de entrada
                Datos: [
                    4000,               //valor Mínimo del Rango
                    5000                //valor Máximo del Rango
                ],
                Param: 'Monto2',          //cuando es range el queryString será Monto=1000-2000
                EsPorDefecto: false
            },
            {
                Nombre: 'Fecha de Creacion',    //nombre de despliegue
                Tipo: 'daterange',              //cuado es range, front agrega un slidebar y dos input de entrada
                Datos: [
                    '01/04/2018',                       //valor Mínimo del Rango
                    '30/05/2018'                        //valor Máximo del Rango
                ],
                Param: 'FechaCreacion',         //cuando es range el queryString será Monto=1000-2000
                EsPorDefecto: false
            }
        ],
        Paginacion: {
            Pagina: 1,        //pagina actual
            CampoOrden: '',       //campo por el cual se realiza ordenamiento
            Registros: 10,       //cantidad de registros por página
            Descendente: false,    //descendente / ascendente
            TotalRegistros: 1,        //cantidad total de registros
            TotalPaginas: 1         //cantidad total de páginas
        }
    }

    response.status(200).json(dto);
}