/**
 * @author Nicolas Andres Rivera Acevedo
 * @description Permite realizar eliminación de un negocio en curso
 * @param {String} id identificador del documento a eliminar
 * @example
 * 		/fe/negocio/1
 */
module.exports = function(request, response){  

    /**
     * @description dto de error
     */
    var dto_error = {
        Message: 'Ha ocurrido un error'
    };

	response.status(200).json('');
}