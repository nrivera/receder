/**
 * @author Nicolas Andres Rivera Acevedo
 * @description Envía al backend datos de documentos Factura para actualizar
 * @param {Number} IDNegocio identficador del negocio en curso
 * @example
 * 		/fe/negocio/facturas?IDNegocio=1
 */
module.exports = function(request, response){

    /**
     * @description DTO con atributos de varios documentos a ser actualizados
     */
    var dto2 = {
        IDNegocio: 1,
        Documentos: [
            {IDDocumento: '1',
            FechaVencimientoNegocio: '02-05-2018'},
            {IDDocumento: '27',
            FechaVencimientoNegocio: '02-05-2018'},
            {IDDocumento: '32',
            FechaVencimientoNegocio: '02-05-2018'},
        ]
    };
    
    /**
     * @author Nicolas Andres Rivera Acevedo
     * @description DTO de respuesta con atributos del resumen negocio.
     * Es la misma respuesta que el evento websocket NEGOCIO_CAMBIO_ESTADO
     * `\socket\negocio-cambia-estado.js`
     */
    var dto = {
        ResumenNegocio: require('../../../socket/negocio-cambio-estado').Data
    };

	response.status(200).json(dto);
}