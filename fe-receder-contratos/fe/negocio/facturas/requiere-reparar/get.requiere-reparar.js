/**
 * @author Juan Rosales
 * @description responde con una lista facturas
 * @param {Number} IDNegocio tipo de negocio a listar
 * @param {String} Accion tipo acción que identifica el problema de las facturas.
 * @example
 * 		/fe/negocio/facturas/requiere-reparar?IDNegocio=1&Acccion=CFV
 */
module.exports = function(request, response){
        	
    var dto = {
        Datos: [
            {
                IDDocumento: 1,
                RutCliente: 171476617,
                NombreCompleto: 'Asesorías e Inversiones Nectia Limitada',
                Folio: '#12311',
                FechaVencimientoOriginal: '03/12/2017',
                FechaVencimientoNegocio: '03/12/2017'
            },
            {
                IDDocumento: 2,
                RutCliente: 182003008,
                NombreCompleto: 'Asesorías e Inversiones Nectia Limitada2',
                Folio: '#12312',
                FechaVencimientoOriginal: '03/12/2017',
                FechaVencimientoNegocio: '03/12/2017'
            },
        ],
        Paginacion: {
            Pagina			: 8,        //pagina actual
            CampoOrden		: '',       //campo por el cual se realiza ordenamiento
            Registros		: 10,       //cantidad de registros por página
            Descendente		: false,    //descendente / ascendente
            TotalRegistros	: 1,        //cantidad total de registros
            TotalPaginas	: 9        //cantidad total de páginas
        }
    }
    
	response.status(200).json(dto);
}