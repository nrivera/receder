/**
 * @author Nicolas Andres Rivera Acevedo
 * @description Envía al backend datos de un documento Factura para adjuntar al negocio
 * @example
 * 		/fe/negocio/facturas/adjuntas
 */
module.exports = function(request, response){
            
    /**
     * @description DTO con atributos del documento a ser adjuntado
     */
    var body = {
        IDDocumento: '1,2,3',
        PerteneceNegocio: true
    };

    /**
     * @author Nicolas Andres Rivera Acevedo
     * @description DTO de respuesta con atributos del resumen negocio.
     * Es la misma respuesta que el evento websocket NEGOCIO_CAMBIO_ESTADO
     * `\socket\negocio-cambia-estado.js`
     */
    var dto = {
        ResumenNegocio: require('../../../../socket/negocio-cambio-estado').Data
    };

    /**
     * @description DTO de respuesta cuando ocurre un error
     */
    var dtoError = {
        Message: 'Ha ocurrido un error',
        Detalle: [
            {
                IDDocumento: 1,
                Mensaje: "Documento Folio 000 ya existe en otro negocio"
            }
        ]
    };

    //409
    
    var dtoErrorMsg = {
        
        Message:"No se informa documento para asociar a negocio"
    }

    
	response.status(409).json(dtoErrorMsg);
}