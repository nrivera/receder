/**
 * @author Nicolas Andres Rivera Acevedo
 * @description Solicita recesión de un documento al cliente o a un tercero.
 * @example
 * 		/fe/negocio/recede
 */
module.exports = function(request, response){
    
    /**
     * @description Dto de información enviadada desde front
     */
    var dto = {
        IDDocumento : 1,
        TipoRecesion: 1,    //1: a mi | 2: a terceros
        Rut : 1471262222,
        RazonSocial : 'razón social',
        Direccion : 'la dirección',
        Email : 'e@mail.com',
        UltimoVencimiento: '12/07/2018'
    }

    /**
     * @description Dto de error
     */
    var dtoError = {
        Message: "ha ocurrido un error..."
    }

	response.status(200).json(dtoError);
}