/**
 * @author Nicolas Andres Rivera Acevedo
 * @description Contrato para actualizar el resumen y estado del negocio.
 * esta api es utilizada cuando eventualmente el usuario sale y vuelve al paso 4 del wizard
 * de crear negocio mientras el backend está procesando el envío de negocio.
 * Este contrato es el mismo que se utiliza mediante websocket `\socket\negocio-cambio-estado.js`.
 * @example
 * 		/fe/negocio/revision-cambio-estado
 */
module.exports = function(request, response){
    var dto = require('../../../socket/sc-negocio-cambio-estado').Data;
    response.status(200).json(dto);
};