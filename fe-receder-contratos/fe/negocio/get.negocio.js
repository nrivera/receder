/**
 * @author Mauricio Ross Arevalo
 * @description responde con una lista de negocios de acuerdo al parámetro `TipoNegocio`
 * @param {Number} TipoNegocio tipo de negocio a listar
 * @example
 * 		/fe/negocio?TipoNegocio=1
 */
module.exports = function(request, response){
            
    var dto = {
        Datos: [
            {
                NroNegocio: 1,
                IDNegocio: 1,
                Estado : 'Cotizado',
                FechaCreacion:'01/01/2018',
                FechaOtorgamiento: '12/09/2018',
                UltimaModificacion:'13/06/2018',
                CantidadDocumentos:2,
                MontoDocumentos: 10000,
                MontoGirar: 1000,
                Plazo: '30 días',
                Tasa: 0.0326,
                TasaDespliegue: '0,0326',
                PuedeEliminar: true,
            },
            {
                NroNegocio: 2,
                IDNegocio: 2,
                Estado : 'Cotizado',
                FechaCreacion:'01/01/2018',
                FechaOtorgamiento: '12/09/2018',
                UltimaModificacion:'12/06/2018',
                CantidadDocumentos:3,
                MontoDocumentos: 20000,
                MontoGirar: 2000,
                Plazo: '60 días',
                Tasa: 0.0362,
                TasaDespliegue: '0,0362',
                PuedeEliminar: false
            }
        ],
        Paginacion: {
            Pagina			: 1,        //pagina actual
            CampoOrden		: '',       //campo por el cual se realiza ordenamiento
            Registros		: 10,       //cantidad de registros por página
            Descendente		: false,    //descendente / ascendente
            TotalRegistros	: 1,        //cantidad total de registros
            TotalPaginas	: 15        //cantidad total de páginas
        }
    }
    
	response.status(200).json(dto);
}