/**
 * @author Nicolas Andres Rivera Acevedo
 * @description Contrato para actualizar el resultado de envío de negocio.
 * esta api es utilizada cuando eventualmente el usuario sale y vuelve al paso 3 del wizard
 * de crear negocio mientras el backend está procesando el envío de negocio.
 * Este contrato es el mismo que se utiliza mediante websocket `\socket\resultado-envio-negocio.js`.
 * @example
 * 		/fe/negocio/resultado-envio-negocio
 */
module.exports = function(request, response){
    var dto = require('../../../socket/resultado-envio-negocio')
    response.status(200).json(dto);
};