/**
 * @author Nicolas Rivera Acevedo
 * @description Permite validar la password de un certificado asociado al negocio
 * @example
 * 		/fe/negocio/certificado/valida-password
 */
module.exports = function(request, response){
        
    /**
     * @description Dto de información enviadada desde front
     */
    var dto = {
        Passoword: 'Axad11',
        IDNegocio: 1
    };

    /**
     * @description Dto de error como resultado de validación de password (status 409)
     */
    var dtoErrorPass = {
        
        Message: 'Password de certificado es incorrecta'
    };


       /**
     * @description Dto de error como resultado de validación de la vigencia del certificado (status 417)
     */
    var dtoErrorCert = {
        Message: 'No tiene un certificado vigente adjunto al Negocio en Curso. $br ¿Desea adjuntar uno?'    
    }

    /* response.status(409).json(dtoErrorPass); */
    
	response.status(417).json(dtoErrorCert);


	/* response.status(200).json(dto); */

}