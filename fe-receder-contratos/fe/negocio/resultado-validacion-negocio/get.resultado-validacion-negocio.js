/**
 * @author Nicolas Andres Rivera Acevedo
 * @description Contrato para actualizar el resultado de validación de negocio.
 * esta api es utilizada cuando eventualmente el usuario sale y vuelve al paso 4 del wizard
 * de crear negocio mientras el backend está procesando.
 * Este contrato es el mismo que se utiliza mediante websocket `\socket\resultado-validacion-negocio.js`.
 * @example
 * 		/fe/negocio/resultado-envio-negocio
 */
module.exports = function(request, response){
    var dto = require('../../../socket/resultado-validacion-negocio');
    response.status(200).json(dto);
};