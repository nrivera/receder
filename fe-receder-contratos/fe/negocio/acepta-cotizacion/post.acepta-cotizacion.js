/**
 * @author Nicolas Andres Rivera Acevedo
 * @description indica al backend que el usuario ha aceptado la cotización dado el IDNegocio
 * @param {Number} IDNegocio identificador del negocio en curso.
 * @example
 * 		/fe/negocio/acepta-cotizacion
 */
module.exports = function(request, response){
            
    /**
     * @description Dto de parámetros desde el front:
     */
    var dto = {
        IDNegocio: 1
    }

	response.status(200).json(dto);
}