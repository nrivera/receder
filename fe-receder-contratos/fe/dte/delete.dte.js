/**
 * @author Nicolas Andres Rivera Acevedo
 * @description Permite realizar eliminación de un documento dte dado por id
 * @param {String} id identificador del documento a eliminar
 * @example
 * 		/fe/dte/1
 */
module.exports = function(request, response){  

    /**
     * @description dto de error
     */
    var dto_error = {
        Message: 'Ha ocurrido un error'
    };

	response.status(200).json('');
}