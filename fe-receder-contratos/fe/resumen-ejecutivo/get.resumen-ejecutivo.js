/**
 * @author Nicolas Andres Rivera Acevedo
 * @description 
 * @examples
 * 		/fe/resumen-ejecutivo/
 */
module.exports = function(request, response){
        	
    var dto = {
        TotalFacturasCargadas: 99999,
        TotalCertificadosCargados: 9999,
        NegociosEnCurso: 999,
        NegociosEnBorrador: 99,
        OperacionesOtorgadas: 9
    };

	response.status(200).json(dto);
}