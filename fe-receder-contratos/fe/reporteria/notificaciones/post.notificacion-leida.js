/**
 * @author Eduardo Antonio de la Hoz Henriquez
 * @description marca una notificación como leída
 * @example
 *        /mobile/notificaciones-leida
 */
module.exports = function (request, response) {

    let dto = {
        status_code: 1,
        error_code: 0,
        message: "sin error",
        alert_id: 32131313
    };

    response.status(200).json(dto);
};
