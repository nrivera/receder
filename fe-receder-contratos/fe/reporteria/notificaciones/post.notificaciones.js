/**
 * @author Eduardo Antonio de la Hoz Henriquez
 * @description response con la data de las notificaciones
 * @example
 *        /mobile/notificaciones
 */
module.exports = function (request, response) {

    let dto = {
        status_code: 1,
        error_code: 0,
        message: "sin error",
        alerts_list: [
            {
                alert_type: 1,
                alert_id: 122111344,
                alert_text: "La Operación 259133 fue girada en el Banco BCI",
                alert_object_id: 8764759,
                readed: 1
            },
            {
                alert_type: 2,
                alert_id: 122111344,
                alert_text: "La Operación 259133 fue girada en el Banco BCI",
                alert_object_id: 8764759,
                readed: 1
            },
            {
                alert_type: 3,
                alert_id: 122111344,
                alert_text: "La Operación 259133 fue girada en el Banco BCI",
                alert_object_id: 8764759,
                readed: 1
            },
            {
                alert_type: 4,
                alert_id: 122111344,
                alert_text: "La Operación 259133 fue girada en el Banco BCI",
                alert_object_id: 8764759,
                readed: 1
            },
            {
                alert_type: 5,
                alert_id: 122111344,
                alert_text: "La Operación 259133 fue girada en el Banco BCI",
                alert_object_id: 8764759,
                readed: 1
            },
            {
                alert_type: 6,
                alert_id: 122111344,
                alert_text: "La Operación 259133 fue girada en el Banco BCI",
                alert_object_id: 8764759,
                readed: 1
            },
            {
                alert_type: 7,
                alert_id: 122111344,
                alert_text: "La Operación 259133 fue girada en el Banco BCI",
                alert_object_id: 8764759,
                readed: 1
            },
            {
                alert_type: 1,
                alert_id: 122111344,
                alert_text: "La Operación 259133 fue girada en el Banco BCI",
                alert_object_id: 8764759,
                readed: 1
            },
            {
                alert_type: 2,
                alert_id: 122111344,
                alert_text: "La Operación 259133 fue girada en el Banco BCI",
                alert_object_id: 8764759,
                readed: 1
            },
            {
                alert_type: 3,
                alert_id: 122111344,
                alert_text: "La Operación 259133 fue girada en el Banco BCI",
                alert_object_id: 8764759,
                readed: 1
            },
            {
                alert_type: 4,
                alert_id: 122111344,
                alert_text: "La Operación 259133 fue girada en el Banco BCI",
                alert_object_id: 8764759,
                readed: 1
            }
        ]
    };

    let Datos = [];
    let total = parseInt(request.body.page_size);
    let type = 1;
    let readed = [0, 1];
    let indexReaded = 0;

    for (let i = 0; i < total; i++) {
        Datos.push({
            alert_type: type,
            alert_id: i * 2,
            alert_text: "La Operación 259133 fue girada en el Banco BCI",
            alert_object_id: i * 3,
            readed: readed[indexReaded]
        });

        indexReaded++;
        if (indexReaded > 1) indexReaded = 0;

        type++;
        if (type > 7) type = 1;
    }

    let dtoGenerado = {};
    dtoGenerado.alerts_list = Datos;
    dtoGenerado.status_code = 1;
    dtoGenerado.error_code = 0;
    dtoGenerado.message = "sin error";

    dto.alerts_list = [];

    response.status(200).json(dtoGenerado);
};
