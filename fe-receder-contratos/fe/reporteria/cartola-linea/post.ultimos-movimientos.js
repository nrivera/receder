/**
 * @author Eduardo Antonio de la Hoz Henriquez
 * @description response con la data de los últimos movimientos
 * @example
 *        /mobile/cartola-linea/ultimos-movimientos
 */
module.exports = function (request, response) {

    let dto = {
        status_code: 1,
        error_code: 0,
        message: "sin error",
        movement_charges_total: 5675422,
        movement_payments_total: 196285765,
        movements_list: [
            {
                movement_date: "01/01/2011",
                movement_type: 1,
                movement_amount: 567542222,
                document_number: 234233232,
                operation_type: "Anticipo de Documento",
                identification: "Cobro operación"
            },
            {
                movement_date: "03/04/2012",
                movement_type: 1,
                movement_amount: 196285765,
                document_number: 632346,
                operation_type: "Anticipo de Documento",
                identification: "Pago Factura"
            },
            {
                movement_date: "02/04/2013",
                movement_type: 2,
                movement_amount: 5675422,
                document_number: 234232,
                operation_type: "Recaudación",
                identification: "Cobro operación"
            },
            {
                movement_date: "05/04/2015",
                movement_type: 1,
                movement_amount: 567542222,
                document_number: 234233232,
                operation_type: "Diferencia por Cobrar",
                identification: "Cobro operación"
            },
            {
                movement_date: "11/09/2015",
                movement_type: 1,
                movement_amount: 567542222,
                document_number: 234233232,
                operation_type: "Diferencia por Cobrar",
                identification: "Cobro operación"
            },
            {
                movement_date: "11/09/2017",
                movement_type: 1,
                movement_amount: 567542222,
                document_number: 234233232,
                operation_type: "Anticipo de Documento",
                identification: "Cobro operación"
            },
            {
                movement_date: "11/09/2018",
                movement_type: 1,
                movement_amount: 567542222,
                document_number: 234233232,
                operation_type: "Anticipo de Documento",
                identification: "Cobro operación"
            },
            {
                movement_date: "11/09/2018",
                movement_type: 1,
                movement_amount: 567542222,
                document_number: 234233232,
                operation_type: "Anticipo de Documento",
                identification: "Cobro operación"
            },
            {
                movement_date: "11/09/2019",
                movement_type: 1,
                movement_amount: 567542222,
                document_number: 234233232,
                operation_type: "Diferencia por Cobrar",
                identification: "Cobro operación"
            },
            {
                movement_date: "11/09/2015",
                movement_type: 1,
                movement_amount: 567542222,
                document_number: 234233232,
                operation_type: "Diferencia por Cobrar",
                identification: "Cobro operación"
            },
            {
                movement_date: "11/09/2016",
                movement_type: 1,
                movement_amount: 567542222,
                document_number: 234233232,
                operation_type: "Recaudación",
                identification: "Cobro operación"
            },
            {
                movement_date: "11/09/2015",
                movement_type: 1,
                movement_amount: 567542222,
                document_number: 234233232,
                operation_type: "Diferencia por Cobrar",
                identification: "Cobro operación"
            },
            {
                movement_date: "11/09/2016",
                movement_type: 1,
                movement_amount: 567542222,
                document_number: 234233232,
                operation_type: "Recaudación",
                identification: "Cobro operación"
            },
            {
                movement_date: "11/09/2015",
                movement_type: 1,
                movement_amount: 567542222,
                document_number: 234233232,
                operation_type: "Anticipo de Documento",
                identification: "Cobro operación"
            },
            {
                movement_date: "11/09/2015",
                movement_type: 1,
                movement_amount: 567542222,
                document_number: 234233232,
                operation_type: "Anticipo de Documento",
                identification: "Cobro operación"
            },
            {
                movement_date: "11/09/2015",
                movement_type: 1,
                movement_amount: 567542222,
                document_number: 234233232,
                operation_type: "Recaudación",
                identification: "Cobro operación"
            },
            {
                movement_date: "11/09/2015",
                movement_type: 1,
                movement_amount: 567542222,
                document_number: 234233232,
                operation_type: "Recaudación",
                identification: "Cobro operación"
            },
            {
                movement_date: "11/09/2015",
                movement_type: 1,
                movement_amount: 567542222,
                document_number: 234233232,
                operation_type: "Recaudación",
                identification: "Cobro operación"
            },
            {
                movement_date: "11/09/2015",
                movement_type: 1,
                movement_amount: 567542222,
                document_number: 234233232,
                operation_type: "Diferencia por Cobrar",
                identification: "Cobro operación"
            },
            {
                movement_date: "11/09/2015",
                movement_type: 1,
                movement_amount: 567542222,
                document_number: 234233232,
                operation_type: "Diferencia por Cobrar",
                identification: "Cobro operación"
            }
        ]
    };

    response.status(200).json(dto);
};
