/**
 * @author Eduardo Antonio de la Hoz Henriquez
 * @description response con la data de los excedentes liberados
 * @example
 *        /mobile/cartola-linea/detalle-excedentes-liberados
 */
module.exports = function (request, response) {

    let dto = {
        status_code: 1,
        error_code: 0,
        message: "sin error",
        surplus_list: [
            {
                surplus_date: "03/04/2015",
                document_type: "Factura",
                surplus_amount: 111111,
                surplus_number: 123,
                debtor_rut: '16901299'
            },
            {
                surplus_date: "01/04/2015",
                document_type: "Factura",
                surplus_amount: 222222,
                surplus_number: 4545,
                debtor_rut: '21949985'
            },
            {
                surplus_date: "03/06/2015",
                document_type: "Tipo2",
                surplus_amount: 33333333,
                surplus_number: 9999999,
                debtor_rut: '1111-4'
            },
            {
                surplus_date: "03/01/2018",
                document_type: "Factura",
                surplus_amount: 63335,
                surplus_number: 888888,
                debtor_rut: '1111-3'
            },
            {
                surplus_date: "11/01/2011",
                document_type: "Factura",
                surplus_amount: 24433,
                surplus_number: 8889999,
                debtor_rut: '1111-2'
            },
            {
                surplus_date: "03/06/2015",
                document_type: "Factura",
                surplus_amount: 3454524,
                surplus_number: 345,
                debtor_rut: '1111-6'
            },
            {
                surplus_date: "03/02/2009",
                document_type: "Tipo2",
                surplus_amount: 4545456,
                surplus_number: 456666,
                debtor_rut: '1111-5'
            },
            {
                surplus_date: "11/01/2011",
                document_type: "Factura",
                surplus_amount: 561236,
                surplus_number: 632346,
                debtor_rut: '1111-5'
            },
            {
                surplus_date: "03/06/2015",
                document_type: "Tipo3",
                surplus_amount: 23234234,
                surplus_number: 632346,
                debtor_rut: '1111-4'
            },
            {
                surplus_date: "03/04/2018",
                document_type: "Factura",
                surplus_amount: 1243533,
                surplus_number: 632346,
                debtor_rut: '1111-3'
            },
            {
                surplus_date: "11/01/2011",
                document_type: "Tipo2",
                surplus_amount: 2334534,
                surplus_number: 632346,
                debtor_rut: '1111-2'
            }
        ]
    };

    response.status(200).json(dto);
};
