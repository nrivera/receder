/**
 * @author Eduardo Antonio de la Hoz Henriquez
 * @description response con la data de la línea del cliente
 * @example
 *        /mobile/cartola-linea/linea-cliente
 */
module.exports = function (request, response) {

    let dto = {
        status_code: 1,
        error_code: 0,
        message: "sin error",
        approved_amount: 1500000,
        used_amount: 400000
    };

    response.status(200).json(dto);
};
