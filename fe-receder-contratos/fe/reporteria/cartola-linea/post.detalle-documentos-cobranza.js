/**
 * @author Eduardo Antonio de la Hoz Henriquez
 * @description response con la data del detalle de los documentos en cobranza
 * @example
 *        /mobile/cartola-linea/detalle-documentos-cobranza
 */
module.exports = function (request, response) {
    let dto = [
        {
            status_code: 1,
            error_code: 0,
            message: "sin error",
            document_number: 24537,
            due_date: "24/05/2015",
            debtor_name: "Cencosud S.A.",
            debtor_rut: "16901299",
            status_last_date: '21/04/2015',
            status: {
                status_name: "Vencida",
                status_info: "30 días"
            }
        },
        {
            status_code: 1,
            error_code: 0,
            message: "sin error",
            document_number: 24537,
            due_date: "24/05/2015",
            debtor_name: "Cencosud S.A.",
            debtor_rut: "2222",
            status_last_date: '21/08/2015',
            status: {
                status_name: "Pendiente de Pago",
                status_info: "se contacta deudor,envío de correo,deudor contactado, deudor indica que pagará en 10 días más"
            }
        },
        {
            status_code: 1,
            error_code: 0,
            message: "sin error",
            document_number: 24537,
            due_date: "24/05/2015",
            debtor_name: "Cencosud S.A.",
            debtor_rut: "2444444",
            status_last_date: '24/04/2015',
            status: {
                status_name: "Vencida",
                status_info: "30 días"
            }
        },
        {
            status_code: 1,
            error_code: 0,
            message: "sin error",
            document_number: 24537,
            due_date: "24/05/2015",
            debtor_name: "Cencosud S.A.",
            debtor_rut: "44444",
            status_last_date: '12/04/2015',
            status: {
                status_name: "Con Problemas",
                status_info: "Factura en canje"
            }
        },
        {
            status_code: 1,
            error_code: 0,
            message: "sin error",
            document_number: 24537,
            due_date: "24/05/2015",
            debtor_name: "Cencosud S.A.",
            debtor_rut: "55555",
            status_last_date: '21/04/2017',
            status: {
                status_name: "Con Problemas",
                status_info: "Factura cancelada al cliente por motivos de fuerza mayor y bla bla bla bla bla da sdasd asd as da sd ad sas d asdadasd"
            }
        },
        {
            status_code: 1,
            error_code: 0,
            message: "sin error",
            document_number: 24537,
            due_date: "24/05/2015",
            debtor_name: "Cencosud S.A.",
            debtor_rut: "233333",
            status_last_date: '21/04/2016',
            status: {
                status_name: "Pago Confirmado",
                status_info: ""
            }
        },
        {
            status_code: 1,
            error_code: 0,
            message: "sin error",
            document_number: 24537,
            due_date: "24/05/2015",
            debtor_name: "Cencosud S.A.",
            debtor_rut: "77777",
            status_last_date: '21/02/2015',
            status: {
                status_name: "Pago Confirmado",
                status_info: ""
            }
        }
    ];

    let result = dto.filter(element => element.debtor_rut.toString().replace(/(\.)|(\-([\d]+)|\-k+)/ig, '') == request.body.debtor_rut.toString().replace(/(\.)|(\-([\d]+)|\-k+)/ig, ''));

    response.status(200).json(result[0]);
};
