/**
 * @author Eduardo Antonio de la Hoz Henriquez
 * @description response con la data de las operaciones-curso
 * @example
 *        /mobile/cartola-linea/operaciones-curso
 */
module.exports = function (request, response) {

    let dto = {
        status_code: 1,
        error_code: 0,
        message: "sin error",
        operation_list: [
            {
                operation_number: 245761,
                operation_date: "24/05/2009",
                operation_amount: 1340651,
                n_of_documents: 45,
                status: 1,
                status_info: "La operación ha sido ingresada"
            },
            {
                operation_number: 111111,
                operation_date: "24/05/2011",
                operation_amount: 342344234,
                n_of_documents: 6,
                status: 1,
                status_info: "La operación ha sido rechazada"
            },
            {
                operation_number: 232323,
                operation_date: "24/05/2012",
                operation_amount: 45545456,
                n_of_documents: 4,
                status: 2,
                status_info: "La operación se encuentra pendiente"
            },
            {
                operation_number: 45454555,
                operation_date: "24/05/2013",
                operation_amount: 67876877,
                n_of_documents: 36,
                status: 3,
                status_info: "La operación ha sido ingresada"
            },
            {
                operation_number: 565656,
                operation_date: "24/05/2014",
                operation_amount: 8888888,
                n_of_documents: 26,
                status: 4,
                status_info: "La operación se encuentra pendiente"
            },
            {
                operation_number: 6767687868,
                operation_date: "24/05/2015",
                operation_amount: 99989898,
                n_of_documents: 6,
                status: 1,
                status_info: "La operación se encuentra pendiente"
            },
            {
                operation_number: 245761,
                operation_date: "24/05/2015",
                operation_amount: 90909990,
                n_of_documents: 1,
                status: 4,
                status_info: "La operación ha sido ingresada"
            },
            {
                operation_number: 2457613333,
                operation_date: "24/05/2019",
                operation_amount: 22222222,
                n_of_documents: 2,
                status: 2,
                status_info: "La operación ha sido rechazada"
            },
            {
                operation_number: 3333333666,
                operation_date: "24/05/2019",
                operation_amount: 1345171,
                n_of_documents: 16,
                status: 1,
                status_info: "La operación se encuentra pendiente"
            }
        ]
    };

    let Datos = [];
    let total = 100;
    let info = ["La operación se encuentra pendiente", "La operación se encuentra pendiente", "La operación ha sido ingresada", "La operación ha sido rechazada"];
    let indexInfo = 0;
    let status = 1;

    for (let i = 0; i < total; i++) {
        Datos.push({
            operation_number: 33333 + i,
            operation_date: "24/05/20" + i,
            operation_amount: 13451 + i,
            n_of_documents: i,
            status: status,
            status_info: info[indexInfo]
        });

        status++;
        if (status > 4) status = 1;

        indexInfo++;
        if (indexInfo > 3) indexInfo = 0;
    }

    let dtoGenerado = {};
    dtoGenerado.operation_list = Datos;
    dtoGenerado.status_code = 1;
    dtoGenerado.error_code = 0;
    dtoGenerado.message = "sin error";

    response.status(200).json(dtoGenerado);
};
