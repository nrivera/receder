/**
 * @author Eduardo Antonio de la Hoz Henriquez
 * @description response con la data del historial de operaciones
 * @example
 *        /mobile/cartola-linea/historial-operaciones
 */
module.exports = function (request, response) {

    let dto1 = {
        status_code: 1,
        error_code: 0,
        message: "sin error",
        operations_list: [
            {
                operation_date: "24/08/2015",
                operation_number: 1
            },
            {
                operation_date: "11/08/2015",
                operation_number: 2
            },
            {
                operation_date: "24/04/2015",
                operation_number: 3
            },
            {
                operation_date: "24/08/2010",
                operation_number: 4
            },
            {
                operation_date: "24/08/2011",
                operation_number: 5
            },
            {
                operation_date: "24/08/2014",
                operation_number: 6
            },
            {
                operation_date: "24/08/2014",
                operation_number: 7
            },
            {
                operation_date: "24/08/2013",
                operation_number: 8
            },
            {
                operation_date: "24/08/2012",
                operation_number: 9
            },
            {
                operation_date: "24/08/2009",
                operation_number: 10
            }
        ]
    };

    let dto1b = {
        status_code: 1,
        error_code: 0,
        message: "sin error",
        operations_list: [
            {
                operation_date: "24/08/2015",
                operation_number: 11
            },
            {
                operation_date: "11/08/2015",
                operation_number: 12
            },
            {
                operation_date: "24/04/2015",
                operation_number: 13
            },
            {
                operation_date: "24/08/2010",
                operation_number: 14
            },
            {
                operation_date: "24/08/2011",
                operation_number: 15
            },
            {
                operation_date: "24/08/2014",
                operation_number: 16
            },
            {
                operation_date: "24/08/2014",
                operation_number: 17
            },
            {
                operation_date: "24/08/2013",
                operation_number: 18
            },
            {
                operation_date: "24/08/2012",
                operation_number: 19
            },
            {
                operation_date: "24/08/2009",
                operation_number: 20
            }
        ]
    };

    let dto1c = {
        status_code: 1,
        error_code: 0,
        message: "sin error",
        operations_list: [
            {
                operation_date: "24/08/2015",
                operation_number: 21
            },
            {
                operation_date: "11/08/2015",
                operation_number: 22
            },
            {
                operation_date: "24/04/2015",
                operation_number: 23
            },
            {
                operation_date: "24/08/2010",
                operation_number: 24
            },
            {
                operation_date: "24/08/2011",
                operation_number: 25
            },
            {
                operation_date: "24/08/2014",
                operation_number: 26
            },
            {
                operation_date: "24/08/2014",
                operation_number: 27
            },
            {
                operation_date: "24/08/2013",
                operation_number: 28
            },
            {
                operation_date: "24/08/2012",
                operation_number: 29
            },
            {
                operation_date: "24/08/2009",
                operation_number: 30
            }
        ]
    };

    let dto2 = {
        status_code: 1,
        error_code: 0,
        message: "sin error",
        operations_list: [
            {
                operation_date: "24/08/2015",
                operation_number: 343434434
            },
            {
                operation_date: "11/08/2015",
                operation_number: 123213
            },
            {
                operation_date: "24/04/2015",
                operation_number: 555555556666
            },
            {
                operation_date: "24/08/2010",
                operation_number: 7767676
            },
            {
                operation_date: "24/08/2011",
                operation_number: 66666777
            }
        ]
    };

    let dto3 = {
        status_code: 1,
        error_code: 0,
        message: "sin error",
        operations_list: [
            {
                operation_date: "24/08/2014",
                operation_number: 5656565
            },
            {
                operation_date: "24/08/2014",
                operation_number: 45454566
            },
            {
                operation_date: "24/08/2013",
                operation_number: 434444
            },
            {
                operation_date: "24/08/2012",
                operation_number: 232323
            },
            {
                operation_date: "24/08/2009",
                operation_number: 11112
            }
        ]
    };

    let Datos = [];
    let total = parseInt(request.body.page_size);

    for (let i = 0; i < total; i++) {
        Datos.push({
            operation_date: `24/08/20${i}`,
            operation_number: i
        });
    }

    let dtoGenerado = {};
    dtoGenerado.operations_list = Datos;
    dtoGenerado.status_code = 1;
    dtoGenerado.error_code = 0;
    dtoGenerado.message = "sin error";

    response.status(200).json(dtoGenerado);
};
