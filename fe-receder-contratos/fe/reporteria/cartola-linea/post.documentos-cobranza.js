/**
 * @author Eduardo Antonio de la Hoz Henriquez
 * @description response con la data de los documentos en cobranza
 * @example
 *        /mobile/cartola-linea/documentos-cobranza
 */
module.exports = function (request, response) {

    let dto1 = {
        status_code: 1,
        error_code: 0,
        message: "sin error",
        debtor_list: [
            {
                debtor_rut: "96787436",
                debtor_name: "Cencosud S.A.",
            },
            {
                debtor_id: "96453782",
                debtor_name: "Nestlé S.A."
            }
        ],
        document_list: [
            {
                document_number: 111111,
                document_status: 1,
                debtor_rut: "21949985"
            },
            {
                document_number: 2222111,
                document_status: 1,
                debtor_rut: "2222"
            },
            {
                document_number: 11111,
                document_status: 2,
                debtor_rut: "33333"
            },
            {
                document_number: 545353534,
                document_status: 3,
                debtor_rut: "44444"
            },
            {
                document_number: 4343435,
                document_status: 3,
                debtor_rut: "55555"
            },
            {
                document_number: 6544545,
                document_status: 4,
                debtor_rut: "66666"
            },
            {
                document_number: 456456456456,
                document_status: 4,
                debtor_rut: "77777"
            },
            {
                document_number: 13213453,
                document_status: 4,
                debtor_rut: "888888"
            },
            {
                document_number: 645646646456,
                document_status: 4,
                debtor_rut: "999999"
            },
            {
                document_number: 7124535341231,
                document_status: 4,
                debtor_rut: "1222222"
            },
            {
                document_number: 34242,
                document_status: 4,
                debtor_rut: "133333"
            },
            {
                document_number: 132,
                document_status: 4,
                debtor_rut: "1444444"
            },
            {
                document_number: 345435345,
                document_status: 4,
                debtor_rut: "1555555"
            },
            {
                document_number: 56756756,
                document_status: 4,
                debtor_rut: "1666666"
            },
            {
                document_number: 6786786,
                document_status: 4,
                debtor_rut: "1777777"
            },
            {
                document_number: 567567567,
                document_status: 4,
                debtor_rut: "188888"
            },
            {
                document_number: 789789789,
                document_status: 4,
                debtor_rut: "1999999"
            },
            {
                document_number: 98089098,
                document_status: 4,
                debtor_rut: "2111111"
            },
            {
                document_number: 7121231,
                document_status: 4,
                debtor_rut: "233333"
            },
            {
                document_number: 4443343,
                document_status: 2,
                debtor_rut: "2444444"
            },
            {
                document_number: 33333,
                document_status: 2,
                debtor_rut: "25555555"
            },
            {
                document_number: 44444,
                document_status: 2,
                debtor_rut: "266666666"
            },
            {
                document_number: 1114444,
                document_status: 2,
                debtor_rut: "266666666"
            },
            {
                document_number: 5675675,
                document_status: 2,
                debtor_rut: "266666666"
            },
            {
                document_number: 55556666,
                document_status: 2,
                debtor_rut: "266666666"
            },
            {
                document_number: 77771111,
                document_status: 2,
                debtor_rut: "266666666"
            },
            {
                document_number: 1213213123,
                document_status: 2,
                debtor_rut: "266666666"
            },
            {
                document_number: 44441111,
                document_status: 2,
                debtor_rut: "2444444"
            },
            {
                document_number: 2223333,
                document_status: 2,
                debtor_rut: "2444444"
            },
            {
                document_number: 4445555,
                document_status: 2,
                debtor_rut: "2444444"
            },
            {
                document_number: 8888999,
                document_status: 2,
                debtor_rut: "2444444"
            },
            {
                document_number: 66666666,
                document_status: 2,
                debtor_rut: "2444444"
            },
            {
                document_number: 6667777,
                document_status: 2,
                debtor_rut: "2444444"
            },
            {
                document_number: 44333444,
                document_status: 2,
                debtor_rut: "299999"
            },
            {
                document_number: 666777666,
                document_status: 2,
                debtor_rut: "2444444"
            },
            {
                document_number: 3333333534353543,
                document_status: 2,
                debtor_rut: "16901299"
            }
        ]
    };


    let Datos = [];
    let status = 1;
    let total = parseInt(request.body.page_size);

    for (let i = 0; i < total; i++) {
        Datos.push({
            document_number: '0456' + i,
            document_status: status,
            debtor_rut: 9387374 + "-" + i
        });

        if (status === 4) status = 1;
        else status++;
    }

    let dtoGenerado = {
        document_list: Datos
    };
    dtoGenerado.status_code = 1;
    dtoGenerado.error_code = 0;
    dtoGenerado.message = "sin error";

    //dto1.document_list = [];
    response.status(200).json(dto1);

    /*if (request.body.page_number === 1) {
        dto1.document_list = dto1.document_list.slice(0, request.body.page_size);
        response.status(200).json(dto1);
    } else {
        dto1.document_list = [];
        response.status(200).json(dto1);
    }*/
};
