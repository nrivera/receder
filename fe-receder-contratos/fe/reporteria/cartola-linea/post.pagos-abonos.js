/**
 * @author Eduardo Antonio de la Hoz Henriquez
 * @description response con la data de los pagos y abonos
 * @example
 *        /mobile/cartola-linea/pagos-abonos
 */
module.exports = function (request, response) {

    let dto = {
        status_code: 1,
        error_code: 0,
        message: "sin error",
        invoices_list: [
            {
                debtor_name: "Eduardo de la hoz",
                invoice_amount: 123,
                invoice_number: 5093526,
                debtor_rut: "16901299",
                payment_amount: 26203530,
                oustanding_balance_amount: 500210,
                founds_source: "Deudor",
                payment_type: "Cheque",
                payment_date: "30/03/2011",
                due_date: "28/03/2015",
                release_date: "31/03/2009"
            },
            {
                debtor_name: "cristian",
                invoice_amount: 4222234,
                invoice_number: 5029526,
                debtor_rut: "21949985",
                payment_amount: 1111,
                oustanding_balance_amount: 222222,
                founds_source: "Deudor 2",
                payment_type: "Cheque",
                payment_date: "12/03/2016",
                due_date: "20/03/2015",
                release_date: "31/06/2011"
            },
            {
                debtor_name: "test",
                invoice_amount: 34444535,
                invoice_number: 509526,
                debtor_rut: "76101812",
                payment_amount: 3343434,
                oustanding_balance_amount: 343344,
                founds_source: "Deudor 3",
                payment_type: "Cheque 3",
                payment_date: "30/03/2013",
                due_date: "28/03/2008",
                release_date: "31/03/2014"
            },
            {
                debtor_name: "Nestlé Chile S.A.",
                invoice_amount: 55555555,
                invoice_number: 509526,
                debtor_rut: "76101812-4",
                payment_amount: 333333,
                oustanding_balance_amount: 55555555555,
                founds_source: "Deudor 3",
                payment_type: "Cheque 2",
                payment_date: "01/03/2011",
                due_date: "28/03/2009",
                release_date: "31/03/2014"
            },
            {
                debtor_name: "Nestlé Chile S.A.",
                invoice_amount: 65665566,
                invoice_number: 509526,
                debtor_rut: "76101812-4",
                payment_amount: 4333434,
                oustanding_balance_amount: 65464564564,
                founds_source: "Deudor 2",
                payment_type: "Cheque 2",
                payment_date: "05/03/2012",
                due_date: "28/03/2013",
                release_date: "31/03/2015"
            },
            {
                debtor_name: "Nestlé Chile S.A.",
                invoice_amount: 777777,
                invoice_number: 509526,
                debtor_rut: "76101812-8",
                payment_amount: 45455454,
                oustanding_balance_amount: 12313,
                founds_source: "Deudor 2",
                payment_type: "Cheque 3",
                payment_date: "01/09/2009",
                due_date: "28/03/2012",
                release_date: "31/03/2015"
            },
            {
                debtor_name: "Nestlé Chile S.A.",
                invoice_amount: 88888,
                invoice_number: 509526,
                debtor_rut: "76101812-5",
                payment_amount: 656656,
                oustanding_balance_amount: 234234242,
                founds_source: "Deudor 2",
                payment_type: "Cheque 3",
                payment_date: "01/09/2010",
                due_date: "21/03/2011",
                release_date: "31/03/2016"
            },
            {
                debtor_name: "Nestlé Chile S.A.",
                invoice_amount: 777777,
                invoice_number: 509526,
                debtor_rut: "76101812-8",
                payment_amount: 45455454,
                oustanding_balance_amount: 12313,
                founds_source: "Deudor 2",
                payment_type: "Cheque 3",
                payment_date: "01/09/2009",
                due_date: "28/03/2012",
                release_date: "31/03/2015"
            },
            {
                debtor_name: "Nestlé Chile S.A.",
                invoice_amount: 88888,
                invoice_number: 509526,
                debtor_rut: "76101812-5",
                payment_amount: 656656,
                oustanding_balance_amount: 234234242,
                founds_source: "Deudor 2",
                payment_type: "Cheque 3",
                payment_date: "01/09/2010",
                due_date: "21/03/2011",
                release_date: "31/03/2016"
            },
            {
                debtor_name: "Nestlé Chile S.A.",
                invoice_amount: 777777,
                invoice_number: 509526,
                debtor_rut: "76101812-8",
                payment_amount: 45455454,
                oustanding_balance_amount: 12313,
                founds_source: "Deudor 2",
                payment_type: "Cheque 3",
                payment_date: "01/09/2009",
                due_date: "28/03/2012",
                release_date: "31/03/2015"
            },
            {
                debtor_name: "Nestlé Chile S.A.",
                invoice_amount: 88888,
                invoice_number: 509526,
                debtor_rut: "76101812-5",
                payment_amount: 656656,
                oustanding_balance_amount: 234234242,
                founds_source: "Deudor 2",
                payment_type: "Cheque 3",
                payment_date: "01/09/2010",
                due_date: "21/03/2011",
                release_date: "31/03/2016"
            }
        ]
    };

    response.status(200).json(dto);
};
