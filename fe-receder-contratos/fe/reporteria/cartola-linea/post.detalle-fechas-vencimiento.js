/**
 * @author Eduardo Antonio de la Hoz Henriquez
 * @description response con la data del detalle de las fechas de vencimiento
 * @example
 *        /mobile/cartola-linea/fechas-vencimiento
 */
module.exports = function (request, response) {

    let dto = {
        status_code: 1,
        error_code: 0,
        message: "sin error",
        document_list: [
            {
                document_number: 23423,
                due_date: "24/08/2015",
                amount: 250000,
                debtor_name: "Cencosud S.A.",
                document_status: 1,
                document_type: "Factura"
            },
            {
                document_number: 23423,
                due_date: "24/08/2015",
                amount: 250000,
                debtor_name: "Cencosud S.A.",
                document_status: 1,
                document_type: "Factura"
            },
            {
                document_number: 23423,
                due_date: "24/08/2015",
                amount: 250000,
                debtor_name: "Cencosud S.A.",
                document_status: 1,
                document_type: "Factura"
            },
            {
                document_number: 23423,
                due_date: "24/08/2015",
                amount: 250000,
                debtor_name: "Cencosud S.A.",
                document_status: 1,
                document_type: "Factura"
            },
            {
                document_number: 23423,
                due_date: "24/08/2015",
                amount: 250000,
                debtor_name: "Cencosud S.A.",
                document_status: 1,
                document_type: "Factura"
            },
            {
                document_number: 23423,
                due_date: "24/08/2015",
                amount: 250000,
                debtor_name: "Cencosud S.A.",
                document_status: 1,
                document_type: "Factura"
            },
            {
                document_number: 23423,
                due_date: "24/08/2015",
                amount: 250000,
                debtor_name: "Cencosud S.A.",
                document_status: 1,
                document_type: "Factura"
            },
            {
                document_number: 23423,
                due_date: "24/08/2015",
                amount: 250000,
                debtor_name: "Cencosud S.A.",
                document_status: 1,
                document_type: "Factura"
            },
            {
                document_number: 23423,
                due_date: "24/08/2015",
                amount: 250000,
                debtor_name: "Cencosud S.A.",
                document_status: 1,
                document_type: "Factura"
            },
            {
                document_number: 23423,
                due_date: "24/08/2015",
                amount: 250000,
                debtor_name: "Cencosud S.A.",
                document_status: 1,
                document_type: "Factura"
            },
            {
                document_number: 23423,
                due_date: "24/08/2015",
                amount: 250000,
                debtor_name: "Cencosud S.A.",
                document_status: 1,
                document_type: "Factura"
            },
            {
                document_number: 23423,
                due_date: "24/08/2015",
                amount: 250000,
                debtor_name: "Cencosud S.A.",
                document_status: 1,
                document_type: "Factura"
            },
            {
                document_number: 23423,
                due_date: "24/08/2015",
                amount: 250000,
                debtor_name: "Cencosud S.A.",
                document_status: 1,
                document_type: "Factura"
            },
            {
                document_number: 23423,
                due_date: "24/08/2015",
                amount: 250000,
                debtor_name: "Cencosud S.A.",
                document_status: 1,
                document_type: "Factura"
            },
            {
                document_number: 23423,
                due_date: "24/08/2015",
                amount: 250000,
                debtor_name: "Cencosud S.A.",
                document_status: 1,
                document_type: "Factura"
            },
            {
                document_number: 23423,
                due_date: "24/08/2015",
                amount: 250000,
                debtor_name: "Cencosud S.A.",
                document_status: 1,
                document_type: "Factura"
            }
        ]
    };

    let Datos = [];
    let total = parseInt(request.body.page_size);
    let status = 1;
    let tipo = ['Factura', 'Cheque', 'Letras'];
    let indexTipo = 0;

    for (let i = 0; i < total; i++) {
        if (indexTipo === 1) status = 1;
        if (indexTipo === 2) status = 2;

        Datos.push({
            due_date: `24/08/20${i}`,
            document_number: i,
            amount: (250000 + i),
            debtor_name: "Cencosud S.A. " + i,
            document_status: status,
            document_type: tipo[indexTipo]
        });

        status++;
        if (status > 4) status = 1;

        indexTipo++;
        if (indexTipo > 2) indexTipo = 0;
    }

    let dtoGenerado = {};
    dtoGenerado.document_list = Datos;
    dtoGenerado.status_code = 1;
    dtoGenerado.error_code = 0;
    dtoGenerado.message = "sin error";

    response.status(200).json(dtoGenerado);
};
