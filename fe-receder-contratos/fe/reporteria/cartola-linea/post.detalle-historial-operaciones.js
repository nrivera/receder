/**
 * @author Eduardo Antonio de la Hoz Henriquez
 * @description response con la data del detalle de las operaciones
 * @example
 *        /mobile/cartola-linea/detalle-operaciones
 */
module.exports = function (request, response) {
    let dto = {
        status_code: 1,
        error_code: 0,
        message: "sin error",
        operation_date: "05/11/2015",
        operation_number: 259133,
        payment_type: "3,Banco Crédito e Inversiones",
        document_type: "Factura",
        documents_count: 4,
        documents_amount: 38632630,
        advance_amount: 36700999,
        operation_rate: 4.8,
        total_cost_operation: 214157,
        operation_discounts: 214157,
        cashed_amount: 36486842,
        documents_details: [
            {
                document_number: 1,
                document_amount: 456464,
                due_date: "06/01/11"
            },
            {
                document_number: 2,
                document_amount: 234234,
                due_date: "06/04/12"
            },
            {
                document_number: 3,
                document_amount: 34535,
                due_date: "01/04/15"
            },
            {
                document_number: 4,
                document_amount: 31322,
                due_date: "06/06/15"
            },
            {
                document_number: 2,
                document_amount: 234234,
                due_date: "06/04/12"
            },
            {
                document_number: 3,
                document_amount: 34535,
                due_date: "01/04/15"
            },
            {
                document_number: 4,
                document_amount: 31322,
                due_date: "06/06/15"
            },
            {
                document_number: 2,
                document_amount: 234234,
                due_date: "06/04/12"
            },
            {
                document_number: 3,
                document_amount: 34535,
                due_date: "01/04/15"
            },
            {
                document_number: 4,
                document_amount: 31322,
                due_date: "06/06/15"
            },
            {
                document_number: 2,
                document_amount: 234234,
                due_date: "06/04/12"
            },
            {
                document_number: 3,
                document_amount: 34535,
                due_date: "01/04/15"
            },
            {
                document_number: 4,
                document_amount: 31322,
                due_date: "06/06/15"
            }
        ]
    };

    response.status(200).json(dto);
};