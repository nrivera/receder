/**
 * @author Eduardo Antonio de la Hoz Henriquez
 * @description response con la data de los últimos movimientos
 * @example
 *        /mobile/cartola-linea/fechas-vencimiento
 */
module.exports = function (request, response) {

    let dto = {
        status_code: 1,
        error_code: 0,
        message: "sin error",
        due_days: [30, 26, 28, 27],
        upcoming_due_dates: [
            {
                due_date: "30/08/2018",
                document_type: "Factura"
            },
            {
                due_date: "26/08/2018",
                document_type: "Cheque"
            },
            {
                due_date: "28/08/2018",
                document_type: "Factura"
            }
            ,
            {
                due_date: "27/08/2018",
                document_type: "Cheque"
            }
            ,
            {
                due_date: "27/08/2018",
                document_type: "Cheque"
            },
            {
                due_date: "27/08/2018",
                document_type: "Cheque"
            },
            {
                due_date: "27/08/2018",
                document_type: "Cheque"
            },
            {
                due_date: "27/08/2018",
                document_type: "Cheque"
            },
            {
                due_date: "27/08/2018",
                document_type: "Cheque"
            },
            {
                due_date: "27/08/2018",
                document_type: "Cheque"
            },
            {
                due_date: "27/08/2018",
                document_type: "Cheque"
            },
            {
                due_date: "27/08/2018",
                document_type: "Cheque"
            },
            {
                due_date: "27/08/2018",
                document_type: "Cheque"
            },
            {
                due_date: "27/08/2018",
                document_type: "Cheque"
            },
            {
                due_date: "27/08/2018",
                document_type: "Cheque"
            },
            {
                due_date: "27/08/2018",
                document_type: "Cheque"
            }
        ]
    };

    response.status(200).json(dto);
};
