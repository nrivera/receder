/**
 * @author Eduardo Antonio de la Hoz Henriquez
 * @description response con la data del login
 */
module.exports = function (request, response) {
    let res = {
        status_code: 1,
        error_code: 0,
        message: "sin error",
        session_token: "SDAD763HD93JSD29009SDSADN2",
        executive_name: "Carolina Saavedra",
        executive_phone_number: 564576576,
        executive_email: "factoring@test.com"
    };

    setTimeout(function () {
        response.status(200).json(res);
    }, 1000);
};