module.exports = function(request, response){

    var dto = [
        {
            "IDBanco": 1,
            "Nombre": "BANCO SECURITY "
        },
        {
            "IDBanco": 2,
            "Nombre": "AMERICAN EXPRESS BANK"
        },
        {
            "IDBanco": 3,
            "Nombre": "BANCO BICE "
        },
        {
            "IDBanco": 4,
            "Nombre": "BANCO BILBAO VIZCAYA ARGENTARIA (BBVA)  "
        },
        {
            "IDBanco": 5,
            "Nombre": "BANCO CONSORCIO "
        },
        {
            "IDBanco": 6,
            "Nombre": "BANCO DE CHILE "
        },
        {
            "IDBanco": 7,
            "Nombre": "BANCO DE CREDITO E INVERSIONES "
        },
        {
            "IDBanco": 8,
            "Nombre": "BANCO DE LA NACION ARGENTINA "
        },
        {
            "IDBanco": 9,
            "Nombre": "BANCO DO BRASIL S.A. "
        },
        {
            "IDBanco": 10,
            "Nombre": "BANCO ESTADO "
        },
        {
            "IDBanco": 11,
            "Nombre": "BANCO FALABELLA "
        },
        {
            "IDBanco": 12,
            "Nombre": "BANCO INTERNACIONAL "
        },
        {
            "IDBanco": 13,
            "Nombre": "BANCO ITAÚ"
        },
        {
            "IDBanco": 14,
            "Nombre": "BANCO PARIS "
        },
        {
            "IDBanco": 15,
            "Nombre": "BANCO PENTA "
        },
        {
            "IDBanco": 16,
            "Nombre": "BANCO RIPLEY "
        },
        {
            "IDBanco": 17,
            "Nombre": "BANCO SANTANDER"
        },
        {
            "IDBanco": 18,
            "Nombre": "CORPBANCA "
        },
        {
            "IDBanco": 19,
            "Nombre": "DEUTSCHE BANK "
        },
        {
            "IDBanco": 20,
            "Nombre": "HSBC BANK "
        },
        {
            "IDBanco": 21,
            "Nombre": "JP MORGAN CHASE BANK, N. A. "
        },
        {
            "IDBanco": 22,
            "Nombre": "RABOBANK"
        },
        {
            "IDBanco": 23,
            "Nombre": "SCOTIABANK"
        },
        {
            "IDBanco": 24,
            "Nombre": "THE BANK OF TOKYO-MITSUBISHI UFJ, LTD "
        }
    ]
    ;

	response.status(200).json(dto);
}