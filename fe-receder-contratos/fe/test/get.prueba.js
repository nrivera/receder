/**
 * @author Nicolas Andres Rivera Acevedo
 * @description toma un queryString y lo almacena en sesión con estructura json
 * @example
 * 		/api/test/prueba?param1=hola&param2=mundo
 */
module.exports = function(request, response){

	var session = request.session;
	var dto = {
		info: "parametros guardados en sesión",
		params: request.query
	};

	session.queryString = request.query;

	response.status(200).json(dto);
}