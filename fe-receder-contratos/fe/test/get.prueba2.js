/**
 * @author Nicolas Andres Rivera Acevedo
 * @description recupera un json almacenado en sesión anteriormente definido
 *              por queryString de /api/test/prueba
 */
module.exports = function(request, response){

	var session = request.session;
	var dto = {
		info: "desde sesión a",
		params: session.queryString
	}

	response.status(200).json(dto);

}