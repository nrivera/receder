/**
 * @author Nicolas Andres Rivera Acevedo
 * @description prueba de logica de acuerdo a parametros de entrada
 * @param {string} tipo -parametro requerido para este ejemplo, si no es informado
 *                      -la api responde con status 400
 * @example
 * 		http://.../api/test/prueba3 			-responde con status 400
 * 		http://.../api/test/prueba3?tipo = 2	-responde un json informando el tipo que ha sido informado
 */
module.exports = function(request, response){

	var params = request.query;
	
	if (!params.tipo) {
		response.status(400).json({
			Mensaje: "tipo es requerido"
		});
		return;
	}

	response.status(200).json({
		tipo: params.tipo
	});

}