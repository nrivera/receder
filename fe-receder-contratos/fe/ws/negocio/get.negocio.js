/**
 * @author Nicolas Andres Rivera Acevedo
 * @description Api que enmascara la funcionalidad dada por websocket
 * @example
 * 		/cert/ws/negocio/{idNegocio}
 */
module.exports = function(request, response){
            
    /**
     * @description DTO de respuesta es una colección de objetos
     * de la forma descrita en los contratos de websocket en `../../../socket`.
     * Dado el proceso ocurrido en el backend, esta colección puede tener 1
     * o más objetos, si al momento de realizar la petición el backend determina
     * que no hay eventos a procesar, entonces envía una respuesta null con código 204 
     */
    var dto = [
        require('../../../socket/negocio-cambio-estado'),
        require('../../../socket/negocio-cambio-paso'),
        require('../../../socket/resultado-envio-negocio'),
        require('../../../socket/resultado-validacion-negocio')
    ];

    /**
     * @description DTO de error
     */
    var dto_error = {
        Message: 'Ha ocurrido un error...'
    };

    response.status(200).json(dto);
}